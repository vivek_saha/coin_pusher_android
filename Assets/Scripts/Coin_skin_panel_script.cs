﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using CoinPusher;

public enum Coin_skin { normal, kiwi, papaya, corn , mangosteen }
public class Coin_skin_panel_script : MonoBehaviour
{
    public Coin_manager c_manager;
    public Setting_api_script s_api;
    [HideInInspector]
    public GameObject[] Use_but;
    [HideInInspector]
    public GameObject[] selected;
    [HideInInspector]
    public GameObject[] buy_but;
    [HideInInspector]
    public TextMeshProUGUI[] Buy_value;
    [HideInInspector]
    public Image[] buy_coin_type;
    [HideInInspector]
    public Material Coin_material;
    [HideInInspector]
    public Texture[] coin_textures;
    public Coin_skin active_coin_skinType
    {
        get
        {
            switch (PlayerPrefs.GetString("active_coin_skin", "normal"))
            {
                case "normal":
                    return Coin_skin.normal;
                    break;
                case "kiwi":
                    return Coin_skin.kiwi;
                    break;
                case "papaya":
                    return Coin_skin.papaya;
                    break;
                case "corn":
                    return Coin_skin.corn;
                    break;
                case "mangosteen":
                     return Coin_skin.mangosteen;
                    break;
                default:
                    return Coin_skin.normal;
                    break;

            }
        }
        set
        {
            switch (value)
            {
                case Coin_skin.normal:
                    PlayerPrefs.SetString("active_coin_skin", "normal");
                    break;
                case Coin_skin.kiwi:
                    PlayerPrefs.SetString("active_coin_skin", "kiwi");
                    break;
                case Coin_skin.papaya:
                    PlayerPrefs.SetString("active_coin_skin", "papaya");
                    break;
                case Coin_skin.corn:
                    PlayerPrefs.SetString("active_coin_skin", "corn");
                    break;
                case Coin_skin.mangosteen:
                    PlayerPrefs.SetString("active_coin_skin", "mangosteen");
                    break;
                default:
                    PlayerPrefs.SetString("active_coin_skin", "normal");
                    break;
            }

        }
    }

    public bool skin1_purchased
    {
        get
        {
            if (PlayerPrefs.GetInt("skin1_purchased", 0) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        set
        {
            if (value == true)
            {
                PlayerPrefs.SetInt("skin1_purchased", 1);
            }
        }
    }
    public bool skin2_purchased
    {
        get
        {
            if (PlayerPrefs.GetInt("skin2_purchased", 0) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        set
        {
            if (value == true)
            {
                PlayerPrefs.SetInt("skin2_purchased", 1);
            }
        }
    }
    public bool skin3_purchased
    {
        get
        {
            if (PlayerPrefs.GetInt("skin3_purchased", 0) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        set
        {
            if (value == true)
            {
                PlayerPrefs.SetInt("skin3_purchased", 1);
            }
        }
    }
    public bool skin4_purchased
    {
        get
        {
            if (PlayerPrefs.GetInt("skin4_purchased", 0) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        set
        {
            if (value == true)
            {
                PlayerPrefs.SetInt("skin4_purchased", 1);
            }
        }
    }
  
    // Start is called before the first frame update
    void Start()
    {
        Coin_skin_but_changer();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Use_coin_skin(int num)
    {
        switch (num)
        {
            case 0:
                active_coin_skinType = Coin_skin.normal;
                break;
            case 1:
               
                active_coin_skinType = Coin_skin.kiwi;
                break;
            case 2:
              
                active_coin_skinType = Coin_skin.papaya;
                break;
            case 3:
               
                active_coin_skinType = Coin_skin.corn;
                break;
            case 4:
              
                active_coin_skinType = Coin_skin.mangosteen;
                break;
        }
        Coin_skin_but_changer();
    }

    public void OnClick_purchase(int num)
    {
        switch (num)
        {
            case 1:
                if (coin_checker(s_api.coin_skin_coin_type[0], s_api.coin_skin_coin_value[0]))
                {
                    purchased_coin_skin(1);
                }
                else
                {
                    nem_popup();
                }
                break;
            case 2:
                if (coin_checker(s_api.coin_skin_coin_type[1], s_api.coin_skin_coin_value[1]))
                {
                    purchased_coin_skin(2);
                }
                else
                {
                    nem_popup();
                }
                break;
            case 3:
                if (coin_checker(s_api.coin_skin_coin_type[2], s_api.coin_skin_coin_value[2]))
                {
                    purchased_coin_skin(3);
                }
                else
                {
                    nem_popup();
                }
                break;
            case 4:
                if (coin_checker(s_api.coin_skin_coin_type[3], s_api.coin_skin_coin_value[3]))
                {
                    purchased_coin_skin(4);
                }
                else
                {
                    nem_popup();
                }
                break;
        }
    }

    public void purchased_coin_skin(int num)
    {
        c_manager.update_ui();
        switch (num)
        {
            case 1:
                skin1_purchased = true;
                active_coin_skinType = Coin_skin.kiwi;
                break;
            case 2:
                skin2_purchased = true;
                active_coin_skinType = Coin_skin.papaya;
                break;
            case 3:
                skin3_purchased = true;
                active_coin_skinType = Coin_skin.corn;
                break;
            case 4:
                skin4_purchased = true;
                active_coin_skinType = Coin_skin.mangosteen;
                break;
        }
        Coin_skin_but_changer();
    }

    public void Coin_skin_but_changer()
    {
        switch (active_coin_skinType)
        {
            case Coin_skin.normal:
                looper_but_setter(0);
                break;
            case Coin_skin.kiwi:
                looper_but_setter(1);
                break;
            case Coin_skin.papaya:
                looper_but_setter(2);
                break;
            case Coin_skin.corn:
                looper_but_setter(3);
                break;
            case Coin_skin.mangosteen:
                looper_but_setter(4);
                break;
            default:
                break;
        }
        Material_changer();
    }

    public void Material_changer()
    {
        switch (active_coin_skinType)
        {
            case Coin_skin.normal:
                Coin_material.mainTexture = coin_textures[0];
                break;
            case Coin_skin.kiwi:
                Coin_material.mainTexture = coin_textures[1];
                break;
            case Coin_skin.papaya:
                Coin_material.mainTexture = coin_textures[2];
                break;
            case Coin_skin.corn:
                Coin_material.mainTexture = coin_textures[3];
                break;
            case Coin_skin.mangosteen:
                Coin_material.mainTexture = coin_textures[4];
                break;
        }
    }

    public void looper_but_setter(int s)
    {
        for (int i = 0; i < 5; i++)
        {
            if (i == s)
            {
                buy_but[i].SetActive(false);
                Use_but[i].SetActive(false);
                selected[i].SetActive(true);
            }
            else
            {
                if (i == 0)
                {
                    buy_but[i].SetActive(false);
                    Use_but[i].SetActive(true);
                    selected[i].SetActive(false);
                }
                else if (i == 1)
                {
                    buy_but[i].SetActive(!skin1_purchased);
                    Use_but[i].SetActive(skin1_purchased);
                    selected[i].SetActive(false);
                }
                else if (i == 2)
                {
                    buy_but[i].SetActive(!skin2_purchased);
                    Use_but[i].SetActive(skin2_purchased);
                    selected[i].SetActive(false);
                }
                else if (i == 3)
                {
                    buy_but[i].SetActive(!skin3_purchased);
                    Use_but[i].SetActive(skin3_purchased);
                    selected[i].SetActive(false);
                }
                else if (i == 4)
                {
                    buy_but[i].SetActive(!skin4_purchased);
                    Use_but[i].SetActive(skin4_purchased);
                    selected[i].SetActive(false);
                }
            }
        }
    }

    public void nem_popup()
    {
        Gamemanager.Instance.nem_popup_fun("Not Enough Coins");
    }

    public bool coin_checker(Coin_type c_type, int c_coins)
    {
        switch (c_type)
        {
            case Coin_type.gold:
                if (c_manager._Gold_coin_current_total >= c_coins)
                {
                    c_manager._Gold_coin_current_total -= c_coins;
                    return true;
                }
                else
                {
                    return false;
                }
                break;
            case Coin_type.green:
                if (c_manager._Green_coin_current_total >= c_coins)
                {
                    c_manager._Green_coin_current_total -= c_coins;
                    return true;
                }
                else
                {
                    return false;
                }
                break;
            case Coin_type.black:
                if (c_manager._Black_coin_current_total >= c_coins)
                {
                    c_manager._Black_coin_current_total -= c_coins;
                    return true;
                }
                else
                {
                    return false;
                }
                break;
            default:
                return false;
                break;
        }

    }
}
