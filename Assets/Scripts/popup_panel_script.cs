﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using CoinPusher;
using DG.Tweening;


public enum popup_enum
{
    coin, froze, triple_coin, double_dmg, slot, wall, shake
}
public enum popuppanel_type
{
    free, video_free, normal
}
public class popup_panel_script : MonoBehaviour
{
    public static popup_panel_script Instance;
    public PowerUp_manager p_manager;
    public Coin_manager cm;

    public TextMeshProUGUI Title, Details;
    public Image Icon;
    public popup_enum current_reward_enum;
    [HideInInspector]
    public Sprite[] Icons;
    [HideInInspector]
    public string[] Title_array;
    [HideInInspector]
    public string[] Details_array;
    public popuppanel_type active_panel_type;
    public GameObject normal, video_free, free;

    //public 

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {

    }


    private void OnEnable()
    {
        //transform.position = 
        //transform.DOMove(Vector3.zero,0.5f);

    }
    // Update is called once per frame
    void Update()
    {

    }

    public void set_panel()
    {
        switch (current_reward_enum)
        {
            case popup_enum.coin:
                Icon.sprite = Icons[0];
                Title.text = Title_array[0];
                Details.text = Details_array[0];
                break;
            case popup_enum.froze:
                Icon.sprite = Icons[1];
                Title.text = Title_array[1];
                Details.text = Details_array[1];
                break;
            case popup_enum.triple_coin:
                Icon.sprite = Icons[2];
                Title.text = Title_array[2];
                Details.text = Details_array[2];
                break;
            case popup_enum.double_dmg:
                Icon.sprite = Icons[3];
                Title.text = Title_array[3];
                Details.text = Details_array[3];
                break;
            case popup_enum.slot:
                Icon.sprite = Icons[4];
                Title.text = Title_array[4];
                Details.text = Details_array[4];
                break;
            case popup_enum.wall:
                Icon.sprite = Icons[5];
                Title.text = Title_array[5];
                Details.text = Details_array[5];
                break;
            case popup_enum.shake:
                Icon.sprite = Icons[6];
                Title.text = Title_array[6];
                Details.text = Details_array[6];
                break;
            default:
                break;
        }
    }
    public void set_panel_type()
    {
        switch (active_panel_type)
        {
            case popuppanel_type.free:
                free.SetActive(true);
                free.GetComponent<Button>().onClick.AddListener(() => Ondone());
                normal.SetActive(false);
                video_free.SetActive(false);
                break;
            case popuppanel_type.video_free:
                video_free.SetActive(true);
                //video_free.GetComponent<Button>().onClick.AddListener(() =>);
                normal.SetActive(false);
                free.SetActive(false);
                break;
            case popuppanel_type.normal:
                normal.SetActive(true);
                //normal.GetComponent<Button>().onClick.AddListener(() =>);
                video_free.SetActive(false);
                free.SetActive(false);
                break;
            default:
                break;
        }

    }

    public void Ondone()
    {
        switch (current_reward_enum)
        {
            case popup_enum.coin:
                Shoot_coin.Instance.Add_coin_40();
                break;
            case popup_enum.froze:
                p_manager.powerup_activate();
                p_manager.start_counter(2);
                break;
            case popup_enum.triple_coin:
                p_manager.powerup_activate();
                p_manager.start_counter(2);
                break;
            case popup_enum.double_dmg:
                p_manager.powerup_activate();
                p_manager.start_counter(2);
                break;
            case popup_enum.slot:
                Slot_coin_grabber.Instance.spin_counter += 1;
                break;
            case popup_enum.wall:
                Platform_Manager.Instance.Front_wall_on();
                p_manager.start_counter(1);
                break;
            case popup_enum.shake:
                ReelsScript.Instance.coin_spawner_Shake_coin();
                p_manager.start_counter(0);
                break;
            default:
                break;
        }
        Gamemanager.Instance.start_time();
        gameObject.SetActive(false);
    }

    public void purchasewith_10greencoin()
    {
        if (cm._Green_coin_current_total>=10)
        {
            cm._Green_coin_current_total -= 10;
            cm.update_ui();
            Ondone();
        }
        else
        {
            Gamemanager.Instance.nem_popup_fun("Not Enough Coins");
        }
    }
    public void onclose()
    {
        transform.DOLocalMove(new Vector3(1237, 0, 0), 0.5f).SetUpdate(true).OnComplete(()=> { gameObject.SetActive(false); Gamemanager.Instance.start_time(); });
    }
}
