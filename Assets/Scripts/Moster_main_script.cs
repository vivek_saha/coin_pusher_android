﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Moster_main_script : MonoBehaviour
{


     int Monster_health=10;
    public int total_monster_health=10;

        
    public GameObject health_slider;

    public float slider_camper
    {
        get 
        {
            //health_slider.transform.localPosition.x 
            float each_part = 2.654f / (float)total_monster_health;
            return (health_slider.transform.localPosition.x / each_part);

        }
        set
        {
            float each_part = 2.654f/ (float)total_monster_health;
           
            health_slider.transform.DOLocalMoveX((each_part*(float)value)+0.014f, 0.1f);
        }
    }
    //slider_full == 1 == 0.014
    //slider_empty == 0 == 2.668

    //  gap = 2.654

    // Start is called before the first frame update
    void Start()
    {

        //disable loader_under_monster
        //transform.GetChild(0).gameObject.SetActive(false);
        Monster_health = total_monster_health;
        //set it full
        health_slider.transform.localPosition = new Vector3(0.014f, 0.005f, 0);
    }
   public void Manual_Start()
    {

        //disable loader_under_monster
        //transform.GetChild(0).gameObject.SetActive(false);
        Monster_health = total_monster_health;
        //set it full
        health_slider.transform.localPosition = new Vector3(0.014f, 0.005f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name== "Coin(Clone)")
        {

            //collision.gameObject.GetComponent<Rigidbody>().velocity = collision.gameObject.GetComponent<Rigidbody>().velocity/10;
            //Debug.Log("hit");

            if (Monster_health>0)
            {
                GameObject abcd= Instantiate(Gamemanager.Instance.monster_hit_spark) as GameObject;
                abcd.transform.position = collision.transform.position;
                //GetComponent<Animator>().Play("monster_hit_anim");
               
                AudioManager.Instance.Monster_hit_sound.Play();
                Monster_health-=Platform_Manager.Instance.coin_dmg;
                slider_camper =total_monster_health -  Monster_health;
                if(Monster_health<=0)
                {
                    gameObject.transform.parent.GetComponent<Monster_manager_script>().Disable_monster();
                }
                //Debug.Log(slider_camper);
            }
            else
            {

                gameObject.transform.parent.GetComponent<Monster_manager_script>().Disable_monster();
                //Destroy(this.gameObject);
            }
        }

    }
}
