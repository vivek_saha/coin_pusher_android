﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Setting_panel_script : MonoBehaviour
{
    public static Setting_panel_script Instance;
    public Setting_api_script set_api;
    public Toggle sound, vibrate;




    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void set_toogles()
    {
        sound.isOn = AudioManager.Instance.Sound_bool;
        //Debug.Log(sound.isOn);
        vibrate.isOn = AudioManager.Instance.Vibrate_bool;
    }
    // Update is called once per frame
    void Update()
    {
        
    }


    public void Onstart()
    {
        transform.DOLocalMove(Vector3.zero, 0.5f).SetUpdate(true);
    }
    public void OnClose()
    {
        transform.DOLocalMove(new Vector3(-1237,0,0),0.5f).SetUpdate(true).OnComplete(()=> { gameObject.SetActive(false); Gamemanager.Instance.start_time(); });
    }
    public void Onterms()
    {
        Application.OpenURL(set_api.terms_and_conditions);
    }
    public void OnPrivacy()
    {
        Application.OpenURL(set_api.privacy_policy);
    }
    public void SendEmail()
    {
        string email = "sahajanandinfotech1@gmail.com";
        string subject = MyEscapeURL("");
        string body = MyEscapeURL("");
        Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
    }
    string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }

    public void toogle_sound()
    {
        //Debug.Log(sound.isOn);
        AudioManager.Instance.Sound_bool = sound.isOn;
    }
    public void toogle_vibrate()
    {
        //Debug.Log(vibrate.isOn);
        AudioManager.Instance.Vibrate_bool = vibrate.isOn;
    }

}
