﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(SphereCollider))]
public class BlackHole_effect : MonoBehaviour
{
    [SerializeField] public float GRAVITY_PULL = .78f;
    public static float m_GravityRadius = 1f;
    //int counter = 0;
    bool zero_collider = false;
    Collider _collision;
    void Awake()
    {
        m_GravityRadius = GetComponent<SphereCollider>().radius;
    }
    /// <summary>
    /// Attract objects towards an area when they come within the bounds of a collider.
    /// This function is on the physics timer so it won't necessarily run every frame.
    /// </summary>
    /// <param name="other">Any object within reach of gravity's collider</param>
    void OnTriggerStay(Collider other)
    {
        if (other.attachedRigidbody)
        {
            _collision = other;
            float gravityIntensity = Vector3.Distance(transform.position, other.transform.position) / m_GravityRadius;
            if (Mathf.Abs(transform.position.z - other.transform.position.z) < 0.01f)
            {
                //other.attachedRigidbody.velocity = Vector3.zero;
            }
            else
            {
                other.attachedRigidbody.AddForce((transform.position - other.transform.position) * gravityIntensity * other.attachedRigidbody.mass * GRAVITY_PULL * Time.smoothDeltaTime);
                Debug.DrawRay(other.transform.position, transform.position - other.transform.position);
            }
        }





        //Collider[] hitColliders = Physics.OverlapSphere(transform.position, 0.5f);
        //Debug.Log("hitColliders.Length" + hitColliders.Length);
        //if (hitColliders.Length <= 3)
        //{
        //    Invoke("generate_tower", 0.5f);
        //    //gameObject.SetActive(false);
        //    zero_collider = true;
        //}
        //else if (hitColliders.Length > 3)
        //{
        //    if (zero_collider)
        //    {
        //        CancelInvoke("generate_tower");
        //    }
        //}
    }

    private void FixedUpdate()
    {
        if (_collision == null)
        {
            if (zero_collider == false)
            {
                Invoke("generate_tower", 0.5f);
                zero_collider = true;
                //Debug.Log("No trigger.");
            }
        }
        else
        {
            if (zero_collider)
            {
                //Debug.Log("Trigger");
                CancelInvoke("generate_tower");
                zero_collider = false;
            }
            _collision = null;
        }
    }

    void generate_tower()
    {
        //Debug.Log("Here");
        //Debug.Log()
        if (!Platform_Manager.Instance.generate_gold_tower)
        {
            Platform_Manager.Instance.generate_gold_tower = true;
            Platform_Manager.Instance.spawn_cointower_goldtower();
            zero_collider = false;
        }
    }
}