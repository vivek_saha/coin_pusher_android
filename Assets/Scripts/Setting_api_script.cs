﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using DG.Tweening;
using CoinPusher;
using System;
using UnityEngine.UI;
using TMPro;

public class Setting_api_script : MonoBehaviour
{

    public Coin_manager cm_script;
    public Model_skin_panel_script model_script;
    public Coin_skin_panel_script coin_skin_script;
    string rawJson;
    private JSONNode jsonResult;

    public int app_version;
    public int is_undermaintenance;
    public string app_update_message;
    public string android_app_link;
    public string ios_app_link;
    public string privacy_policy;
    public string app_message;
    public string fb_share_link_title;
    public string share_private_link_message;
    public string terms_and_conditions;

    public int gold_coin;
    public float green_coin;
    public int big_gold_coin;
    public float big_green_coin;
    public int black_bar_coin;
    public int shake_coin;

    public Coin_type model_skin_coin_type;
    public int model_skin_coin_value;

    public Coin_type[] coin_skin_coin_type;
    public int[] coin_skin_coin_value;

    public int is_blocked = 0;

    public string token;
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(GetData());
        //if(PlayerPrefs.GetString("Token");)
        //Invoke("manual_start", 1f);
    }

    public void manual_start()
    {
        StartCoroutine(GetData());
    }


    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get("http://134.209.103.120/CoinPusher/mapi/1/app-settings/eyJ0eXAiOiJKV1QiLCdlcMdGciOiJSUzI1");
        //req_call = false;
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        token = PlayerPrefs.GetString("Token");
        yield return www.Send();
        //Debug.Log(www);
        if (www.isNetworkError)
        {
            Debug.Log(www.error);


            Check_Internet_connection();
            //req_call = true;
        }
        else
        {
            if (Notice_panel_script.Instance != null)
            {
                close_Retry_panel();
            }// Show results as text
            //Debug.Log(www.downloadHandler.text);
            rawJson = www.downloadHandler.text;
            verify();
            //Manual_Start();
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }


    void verify()
    {

        //jsonResult = JSON.Parse(rawJson);
        Set_Json_data();

        Invoke("temp_waiter", 0.5f);


    }
    public void temp_waiter()
    {
        if (is_undermaintenance == 0)
        {
            if (Version_check())
            {
                //temp_before_login_start
                if (is_blocked == 0)
                {
                    Normal_start();
                    Invoke("start_game", 0.5f);
                }
                else
                {
                    Blocked_user();
                }
            }
            else
            {
                UPdate_app_notice();
            }
        }
        else
        {
            //app_message = jsonResult["data"]["app_config"]["app_message"].Value;
            Undermaintenance_Notice();
        }
    }
    public void Set_Json_data()
    {
        jsonResult = JSON.Parse(rawJson);
        //app_config
#if UNITY_ANDROID
        is_undermaintenance = jsonResult["data"]["app_config"]["is_undermaintenance"].AsInt;
#elif UNITY_IOS
        is_undermaintenance = jsonResult["data"]["app_config"]["ios_is_undermaintenance"].AsInt;
#else
        is_undermaintenance = jsonResult["data"]["app_config"]["is_undermaintenance"].AsInt;
#endif
        app_message = jsonResult["data"]["app_config"]["app_message"].Value;
        android_app_link = jsonResult["data"]["app_config"]["android_app_link"].Value;
        ios_app_link = jsonResult["data"]["app_config"]["ios_app_link"].Value;
        privacy_policy = jsonResult["data"]["app_config"]["privacy_policy"].Value;
        terms_and_conditions = jsonResult["data"]["app_config"]["privacy_policy"].Value;
        fb_share_link_title = jsonResult["data"]["app_config"]["fb_share_link_title"].Value;
        share_private_link_message = jsonResult["data"]["app_config"]["share_private_link_message"].Value;

        //coin_master 

        gold_coin = jsonResult["data"]["coin_master"]["gold_coin"].AsInt;
        green_coin = jsonResult["data"]["coin_master"]["green_coin"].AsFloat;
        big_gold_coin = jsonResult["data"]["coin_master"]["big_gold_coin"].AsInt;
        big_green_coin = jsonResult["data"]["coin_master"]["big_green_coin"].AsFloat;
        shake_coin = jsonResult["data"]["coin_master"]["shake_coin"].AsInt;
        black_bar_coin = jsonResult["data"]["coin_master"]["black_bar_coin"].AsInt;

        //userDATA
        is_blocked = jsonResult["data"]["user"]["is_blocked"].AsInt;

        coin_skin_coin_type = new Coin_type[4];
        coin_skin_coin_value = new int[4];
        int counter = 0;
        foreach (JSONNode node in jsonResult["data"]["skin"])
        {
            if (node["skin_type"] == "Model")
            {
                model_skin_coin_type = Coin_manager.REturn_coin_type(node["coin_type"].Value);
                model_skin_coin_value = node["coin"].AsInt;
                model_script.coin_type_image.sprite = Coin_manager.Return_coin_type_image(model_skin_coin_type);
                model_script.Coin_value.text = model_skin_coin_value.ToString();
            }
            else if (node["skin_type"] == "Coin")
            {
                coin_skin_coin_type[counter] = Coin_manager.REturn_coin_type(node["coin_type"].Value);
                coin_skin_coin_value[counter] = node["coin"].AsInt;
                //Debug.Log("coin_skin_coin_value[counter]" + coin_skin_coin_value[counter]);
                coin_skin_script.buy_coin_type[counter + 1].sprite = Coin_manager.Return_coin_type_image(coin_skin_coin_type[counter]);
                coin_skin_script.Buy_value[counter + 1].text = coin_skin_coin_value[counter].ToString();
                counter++;
            }
        }
        model_script.Change_theme_tex();

        //is_daily_bonus = jsonResult["data"]["user"]["is_daily_bonus"].AsBool;
        //if (is_daily_bonus)
        //{
        //    //disable_daily_bonus
        //    Gamemanager.Instance.transform.GetComponent<DailyReward>().disable_dialy_bonus_fromgetapi_data();
        //}
        //else
        //{
        //    Gamemanager.Instance.transform.GetComponent<DailyReward>().enable_dialy_bonus_fromgetapi_data();
        //}
        ////Debug.Log("is_unlocked"+is_unlocked);

        //if (is_blocked == 0)
        //{
        //    Set_User_data.Instance.setdata_after_get();
        //}
        //else if (is_blocked == 1)
        //{
        //    Splash_Panel_scripts.Instance.BLoked_fun();
        //}

    }
    public void start_game()
    {
        Splash_panel_script.Instance.Splash_slider.DOValue(1, 2f).OnComplete(() =>{ start_methods();});
    }

    private void start_methods()
    {
        Platform_Manager.Instance.onStart_coin_tower();
        Splash_panel_script.Instance.gameObject.SetActive(false);
        //if (Ads_initialize_API.Instance.init_done)
        //{
            call_ads();
            //Ads_priority_script.Instance.Show_interrestial();
        //}
        //else
        //{
        //    //InvokeRepeating("checkInit", 0f, 1f);
        //}
    }


    public void checkInit()
    {
        if(Ads_initialize_API.Instance.init_done)
        {
            call_ads();
            CancelInvoke("checkInit");
        }
        
    }
    public void call_ads()
    {
        Debug.Log("Ads_priority_script.Instance.Show_interrestial()");
        Ads_priority_script.Instance.Show_interrestial();
    }

    public void Normal_start()
    {
        cm_script.manual_start();
    }
    public bool Version_check()
    {
#if UNITY_ANDROID
        app_version = jsonResult["data"]["app_config"]["app_version"].AsInt;
#elif UNITY_IOS
        app_version = jsonResult["data"]["app_config"]["ios_app_version"].AsInt;
#else
         app_version = jsonResult["data"]["app_config"]["app_version"].AsInt;
#endif
        //app_version = jsonResult["data"]["app_config"]["ios_app_version"].AsInt;
        //Debug.Log(Application.version);
        if (Gamemanager.Instance.App_ver_code >= app_version)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void Undermaintenance_Notice()
    {
        Notice_panel_script.Instance.notice_panel.SetActive(true);
        Notice_panel_script.Instance.Under_maintenance_panel.GetComponentInChildren<TextMeshProUGUI>().text = app_message;
        Notice_panel_script.Instance.Under_maintenance_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Close_app());
        Notice_panel_script.Instance.Under_maintenance_panel.SetActive(true);

    }



    public void UPdate_app_notice()
    {
        Notice_panel_script.Instance.notice_panel.SetActive(true);
        Notice_panel_script.Instance.Update_panel.GetComponentInChildren<TextMeshProUGUI>().text = app_message;
        android_app_link = jsonResult["data"]["app_config"]["android_app_link"].Value;
        Notice_panel_script.Instance.Update_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Update_app());
        Notice_panel_script.Instance.Update_panel.SetActive(true);

    }

    public void Blocked_user()
    {
        Notice_panel_script.Instance.notice_panel.SetActive(true);
        //Notice_panel_script.Instance.Under_maintenance_panel.GetComponentInChildren<TextMeshProUGUI>().text = app_message;
        Notice_panel_script.Instance.Bloacked_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Close_app());
        Notice_panel_script.Instance.Bloacked_panel.SetActive(true);

    }
    public void Close_app()
    {
        Application.Quit();
    }

    public void Update_app()
    {

        //print("called");
        Application.OpenURL(android_app_link);
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_panel.SetActive(true);
                    Notice_panel_script.Instance.Something_went_wrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_panel.SetActive(true);
                    Notice_panel_script.Instance.no_internet_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.no_internet_panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }

    public void Retry_connection()
    {
        manual_start();

        Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(false);
        Notice_panel_script.Instance.no_internet_panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_panel.SetActive(false);
        manual_start();
    }
    public void close_Retry_panel()
    {
        Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(false);
        Notice_panel_script.Instance.no_internet_panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_panel.SetActive(false);
    }


}

