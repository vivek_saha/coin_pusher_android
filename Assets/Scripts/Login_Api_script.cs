﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using System;
using UnityEngine.UI;

public class Login_Api_script : MonoBehaviour
{
    //public string login_type;
    private string device_token;
    private string Token_autorization;
    private JSONNode jsonResult;

    string rawJson;
    // Start is called before the first frame update
    void Start()
    {
        device_token = SystemInfo.deviceUniqueIdentifier;

        //Start_upload();
        if (PlayerPrefs.GetString("Token", string.Empty) == string.Empty)
        {
            Start_upload();

        }
        else
        {
            Invoke("NewMethod",2f);
            //NewMethod();//GetComponent<Setting_api_script>().manual_start();
        }
    }

    private void NewMethod()
    {
        GetComponent<Setting_api_script>().manual_start();
    }

    public void Start_upload()
    {
        StartCoroutine("Upload");
    }

    IEnumerator Upload()
    {
        //Debug.Log("device_token" + device_token);
        WWWForm form = new WWWForm();
        form.AddField("login_type", "guest");

        form.AddField("device_token", device_token);

        UnityWebRequest www = UnityWebRequest.Post("http://134.209.103.120/CoinPusher/mapi/1/login", form);
        yield return www.SendWebRequest();
        Debug.Log("www: " + www.responseCode);
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            Check_Internet_connection();
        }
        else
        {
            if (Notice_panel_script.Instance != null)
            {
                close_Retry_panel();
            }
            rawJson = www.downloadHandler.text;
            Get_response_data();
            Debug.Log("Form upload complete!");
        }

    }

    public void Get_response_data()
    {
        jsonResult = JSON.Parse(rawJson);


        Token_autorization = "Bearer " + jsonResult["data"]["token"].Value;
        PlayerPrefs.SetString("Token", Token_autorization);
        //Debug.Log("Token_autorization : " + Token_autorization);
        GetComponent<Setting_api_script>().manual_start();

    }



    // Update is called once per frame
    void Update()
    {

    }
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_panel.SetActive(true);
                    Notice_panel_script.Instance.Something_went_wrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_panel.SetActive(true);
                    Notice_panel_script.Instance.no_internet_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.no_internet_panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }

    public void Retry_connection()
    {
        Start_upload();

        Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(false);
        Notice_panel_script.Instance.no_internet_panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_panel.SetActive(false);
        Start_upload();
    }
    public void close_Retry_panel()
    {
        Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(false);
        Notice_panel_script.Instance.no_internet_panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_panel.SetActive(false);
    }
}
