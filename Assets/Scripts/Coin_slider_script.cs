﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin_slider_script : MonoBehaviour
{
	private Vector3 initPosition;
	private Vector3 newPosition;
	//public Vector3 position;

	public GameObject Largepush_Collider;
	public float speed = 2f;


	public bool UpDown = true;
	public bool IsLargePush = false;
	public bool IsLargeGo = true;
	public bool IsNormal = true;
	void Start()
	{
		initPosition = new Vector3(0f, 1f, 5.3f);
		newPosition = new Vector3(0f, 1f, 2f);
	}

	void Update()
	{
		//position = transform.position;

		if (!IsLargePush)
		{
			if (IsNormal)
			{
				if (UpDown)
				{
					transform.Translate(Vector3.forward * speed * Time.deltaTime);
				}
				else
				{
					transform.Translate(-Vector3.forward * speed * Time.deltaTime);
				}

				if (transform.position.z >= 56f)
				{
					UpDown = true;
					IsLargeGo = true;

				}
				else if (transform.position.z <= 42f)
				{
					UpDown = false;
					IsLargeGo = true;
				}
			}
			else
			{
				if (UpDown)
				{
					transform.Translate(Vector3.forward * speed * Time.deltaTime);
				}
				else
				{
					transform.Translate(-Vector3.forward * speed * Time.deltaTime);
				}

				if (transform.position.z >= 56f)
				{
					UpDown = true;
					IsLargeGo = true;

				}
				else if (transform.position.z <= 42f)
				{
					UpDown = false;
					IsLargeGo = true;
				}
			}

		}
		else
		{
			if (IsLargeGo)
			{
				Largepush_Collider.SetActive(true);
				transform.Translate(Vector3.forward * 6 * Time.deltaTime);
			}
			else
			{
				transform.Translate(-Vector3.forward * 6 * Time.deltaTime);
			}

			if (transform.position.z >= 56f)
			{
				IsLargeGo = true;
				IsLargePush = false;
				Platform_Manager.Instance.large_push_done();
				//Largepush_Collider.SetActive(false);

			}
			else if (transform.position.z <= 33f)
			{
				IsLargeGo = false;
                Largepush_Collider.SetActive(false);

            }


		}


	}

	public void OnFullSpeed(int Speed, int time)
	{
		speed = Speed;
		Invoke("OnDounSpeed", time);


	}

	void OnDounSpeed()
	{
		speed = 1.5f;

	}


    //private void OnCollisionEnter(Collision collision)
    //{
    //    if(collision.gameObject.name== "Coin(Clone)")
    //    {
    //        //collision.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
    //    }
    //}
}
