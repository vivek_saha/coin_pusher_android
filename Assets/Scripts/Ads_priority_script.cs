﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ads_priority_script : MonoBehaviour
{
    public static Ads_priority_script Instance;

    //public GameObject popup_watchads;
    public bool time_control = false;
    public GameObject AdsLoading_panel;
    bool startttt = true;

    int counter = 0;
    private void Awake()
    {
        Instance = this;
        //DontDestroyOnLoad(this);
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void Initialize_ads()
    {
        GetComponent<Admob>().Manual_Start();
        GetComponent<Unity_Ads_script>().Manual_Start();
        GetComponent<FB_ADS_script>().Manual_Start();

    }
    public void Show_interrestial()
    {
        if (Ads_initialize_API.Instance.interstitial_ads_priority == "admob")
        {
            if (transform.GetComponent<Admob>().check_loaded_InterstitialAd())
            {
                startttt = false;
                AdsLoading_panel.SetActive(false);
                transform.GetComponent<Admob>().ShowInterstitialAd();
            }
            else if (transform.GetComponent<FB_ADS_script>().Check_interstatial_fb_ads())
            {
                startttt = false;
                AdsLoading_panel.SetActive(false);
                transform.GetComponent<FB_ADS_script>().ShowInterstitial();
            }
            else if (transform.GetComponent<Unity_Ads_script>().check_unity_interstatial())
            {
                startttt = false;
                AdsLoading_panel.SetActive(false);
                transform.GetComponent<Unity_Ads_script>().ShowInterstitialAd();
            }
            else
            {
                Turn_on_AdsLoading_panel();
            }
        }
        else if (Ads_initialize_API.Instance.interstitial_ads_priority == "fb")
        {
            if (transform.GetComponent<FB_ADS_script>().Check_interstatial_fb_ads())
            {
                startttt = false;
                AdsLoading_panel.SetActive(false);
                transform.GetComponent<FB_ADS_script>().ShowInterstitial();
            }
            else if (transform.GetComponent<Unity_Ads_script>().check_unity_interstatial())
            {
                startttt = false;
                AdsLoading_panel.SetActive(false);
                transform.GetComponent<Unity_Ads_script>().ShowInterstitialAd();
            }
            else if (transform.GetComponent<Admob>().check_loaded_InterstitialAd())
            {
                startttt = false;
                AdsLoading_panel.SetActive(false);
                transform.GetComponent<Admob>().ShowInterstitialAd();
            }
            else
            {
                Turn_on_AdsLoading_panel();
            }

        }
        else if (Ads_initialize_API.Instance.interstitial_ads_priority == "unity")
        {
            if (transform.GetComponent<Unity_Ads_script>().check_unity_interstatial())
            {
                startttt = false;
                AdsLoading_panel.SetActive(false);
                transform.GetComponent<Unity_Ads_script>().ShowInterstitialAd();
            }
            else if (transform.GetComponent<FB_ADS_script>().Check_interstatial_fb_ads())
            {
                startttt = false;
                AdsLoading_panel.SetActive(false);
                transform.GetComponent<FB_ADS_script>().ShowInterstitial();
            }
            else if (transform.GetComponent<Admob>().check_loaded_InterstitialAd())
            {
                startttt = false;
                AdsLoading_panel.SetActive(false);
                transform.GetComponent<Admob>().ShowInterstitialAd();
            }
            else
            {
                Turn_on_AdsLoading_panel();
            }

        }

    }

    public void Show_RewardedAds()
    {
        if (Ads_initialize_API.Instance.ads_priority == "admob")
        {
            if (transform.GetComponent<Admob>().check_loaded_rewarded())
            {
                transform.GetComponent<Admob>().ShowRewardedAd();
            }
            else if (transform.GetComponent<FB_ADS_script>().Check_rewarded_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<Unity_Ads_script>().check_unity_rewarded())
            {
                transform.GetComponent<Unity_Ads_script>().ShowRewardedVideo();
            }
            else
            {
                show_popup();
            }
        }
        else if (Ads_initialize_API.Instance.ads_priority == "fb")
        {
            if (transform.GetComponent<FB_ADS_script>().Check_rewarded_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<Unity_Ads_script>().check_unity_rewarded())
            {
                transform.GetComponent<Unity_Ads_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<Admob>().check_loaded_rewarded())
            {
                transform.GetComponent<Admob>().ShowRewardedAd();
            }
            else
            {
                show_popup();
            }

        }
        else if (Ads_initialize_API.Instance.ads_priority == "unity")
        {
            if (transform.GetComponent<Unity_Ads_script>().check_unity_rewarded())
            {
                transform.GetComponent<Unity_Ads_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<FB_ADS_script>().Check_rewarded_fb_ads())
            {
                transform.GetComponent<FB_ADS_script>().ShowRewardedVideo();
            }
            else if (transform.GetComponent<Admob>().check_loaded_rewarded())
            {
                transform.GetComponent<Admob>().ShowRewardedAd();
            }
            else
            {
                show_popup();
            }
        }
    }


    public void Turn_on_AdsLoading_panel()
    {
        //Debug.Log("here");
        Debug.Log("starttt" + startttt);
        if (counter < 3)
        {
            if (startttt)
            {
                AdsLoading_panel.SetActive(true);
                Invoke("loadadagaininstart", 2f);
            }
        }
        else
        {
            AdsLoading_panel.SetActive(false);
        }
    }

    public void loadadagaininstart()
    {
        counter++;
        Show_interrestial();
    }

    public void Add_reward()
    {
        //Invoke("rewardsss",0.5f);
        StartCoroutine(notstoppedtime());
    }


    IEnumerator notstoppedtime()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        rewardsss();
    }

    public void rewardsss()
    {
        GetComponent<Admob>().Disable_admob_bg();
        popup_panel_script.Instance.Ondone();
        GetComponent<Admob>().waitted_req();
    }
    public void show_popup()
    {
        Gamemanager.Instance.nem_popup_fun("Ads Are Not Ready");
        GetComponent<Admob>().REquest_ads();       
    }

    public void hide_popup()
    {
        //popup_watchads.SetActive(false);
    }
}
