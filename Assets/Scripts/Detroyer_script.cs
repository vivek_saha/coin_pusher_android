﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detroyer_script : MonoBehaviour
{
    public Coin_Counter_script cc_script;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        //Destroy(collision.gameObject);
        if (collision != null)
        {
            if (cc_script != null)
            {
                cc_script.OnCollisionEnter(collision);
            }
        }
    }
}
