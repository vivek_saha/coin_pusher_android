﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using System;
using UnityEngine.UI;

public class History_api_script : MonoBehaviour
{
    public GameObject cashout_history_card, coupon_history_card;
    public Transform history_content;
    public GameObject History_loader;

    [HideInInspector]
    public string[] card_type;
    [HideInInspector]
    public string[] item_type;
    [HideInInspector]
    public string[] discount;
    [HideInInspector]
    public string[] id;
    [HideInInspector]
    public string[] store_link;
    [HideInInspector]
    public string[] use_link;
    [HideInInspector]
    public string[] code;
    [HideInInspector]
    public string[] time;


    public string rawJson;
    private JSONNode jsonResult;
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine("GetData");
    }


    public void get_data()
    {
        StartCoroutine("GetData");
    }
    IEnumerator GetData()
    {
        WWWForm form = new WWWForm();
        UnityWebRequest www = UnityWebRequest.Post("http://134.209.103.120/CoinPusher/mapi/1/get-user-withdraw",form);
        //req_call = false;
        Debug.Log(PlayerPrefs.GetString("Token"));
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        yield return www.Send();
        //Debug.Log(www);
        if (www.isNetworkError)
        {
            Debug.Log(www.error);


            Check_Internet_connection();
            //req_call = true;
        }
        else
        {
            if (Notice_panel_script.Instance != null)
            {
                close_Retry_panel();
            }
            // Show results as text
            //Debug.Log(www.downloadHandler.text);
            rawJson = www.downloadHandler.text;
            if (rawJson != string.Empty)
            {
                Set_Json_data();
            }//Manual_Start();
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }
    public void Set_Json_data()
    {
        jsonResult = JSON.Parse(rawJson);

        //app_config
        if (jsonResult["success"].AsBool == true)
        {
            //card_type = jsonResult["data"]["user"]["name"].Value;
            int m = 0;
            foreach (JSONNode message in jsonResult["data"]["withdraw_history"])
            {
                m++;
            }
            card_type = new string[m];
            item_type = new string[m];
            store_link = new string[m];
            discount = new string[m];
            id = new string[m];
            use_link = new string[m];
            code = new string[m];
            time = new string[m];

            int counter = 0;
            foreach (JSONNode message in jsonResult["data"]["withdraw_history"])
            {
                card_type[counter] = message["card_type"].Value;
                item_type[counter] = message["item_type"].Value;
                store_link[counter] = message["store_link"].Value;
                discount[counter] = message["discount"].Value;
                id[counter] = message["id"].Value;
                use_link[counter] = message["use_link"].Value;
                code[counter] = message["code"].Value;
                time[counter] = message["dates"]["date"].Value;
                counter++;
            }
            manual_start();
        }
    }

    public void manual_start()
    {
        if(history_content.childCount!=0)
        {
            foreach(Transform a in history_content)
            {
                Destroy(a.gameObject);
            }
        }
        for (int i = 0; i < id.Length; i++)
        {
            if (card_type[i] == "discount")
            {
                //Debug.Log("here");
                GameObject temp = Instantiate(coupon_history_card, Vector3.zero, Quaternion.identity);
                temp.transform.SetParent(history_content);
                temp.GetComponent<History_card_script>().br = set_cardtype(item_type[i]);
                temp.GetComponent<History_card_script>().set_brand(temp.GetComponent<History_card_script>().br);
                temp.GetComponent<History_card_script>().code.text = "Code :"+code[i];
                temp.GetComponent<History_card_script>().time.text =set_time( time[i]);
                temp.GetComponent<History_card_script>().discount.text ="Up to "+ discount[i]+" off";
                temp.GetComponent<History_card_script>().addlistner(store_link[i],use_link[i]);
            }
            else
            {
                GameObject temp = Instantiate(cashout_history_card, Vector3.zero, Quaternion.identity);
                temp.transform.SetParent(history_content);
                temp.GetComponent<History_card_script>().br = set_cardtype(item_type[i]);
                temp.GetComponent<History_card_script>().set_brand(temp.GetComponent<History_card_script>().br);
                temp.GetComponent<History_card_script>().time.text =set_time( time[i]);

                temp.GetComponent<History_card_script>().discount.text =  discount[i] ;
                //temp.GetComponent<History_card_script>().addlistner();
            }


        }
        History_loader.SetActive(false);

    }

    public brand_type set_cardtype(string a)
    {
        switch (a)
        {
            case "amazon":
                return brand_type.Amazon;
                break;
            case "paypal":
                return brand_type.Paypal;
                break;
            case "walmart":
                return brand_type.Walmart;
                break;
            case "asos":
                return brand_type.Asos;
                break;
            case "lego":
                return brand_type.Lego;
                break;
            case "mlbshop":
                return brand_type.Mlbshop;
                break;
            case "nike":
                return brand_type.Nike;
                break;
            case "primenow":
                return brand_type.Primenow;
                break;
            default:
                return brand_type.Amazon;
                break;

        }
    }



    public string set_time(string st)
    {
        string[] main = st.Split(' ');
        string[] a_date = main[0].Split('-');
        string[] a_tiime = main[1].Split(':');
        string date = a_date[0] + "/" + a_date[1] + "/" + a_date[2];
        string tiime = a_tiime[0] + ":" + a_tiime[1];
        return date + " " + tiime;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_panel.SetActive(true);
                    Notice_panel_script.Instance.Something_went_wrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_panel.SetActive(true);
                    Notice_panel_script.Instance.no_internet_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.no_internet_panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }

    public void Retry_connection()
    {
        get_data();

        Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(false);
        Notice_panel_script.Instance.no_internet_panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_panel.SetActive(false);
        get_data();
    }
    public void close_Retry_panel()
    {
        Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(false);
        Notice_panel_script.Instance.no_internet_panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_panel.SetActive(false);
    }
}
