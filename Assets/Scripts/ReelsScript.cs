﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReelsScript : MonoBehaviour
{

    public static ReelsScript Instance;

    public popup_panel_script popup_script;
	public GameObject coin,big_coin,greencoin,big_green_coin, shake_coin, black_bar_coin;
	public float PreSpinDuration = 2f;  // Number of seconds it will spin for before they start stopping.
	public int NumberOfItemsPerReel = 8; // Number of items on each column.
	public float rotation_fix_minor;
	private float ReelIncrements;

	public GameObject[] Reel;
    public GameObject Circus_reel;
    public GameObject Normal_reel;

	public bool EnableFixSpin = false;
	public int[] FixSpinReel;
	public int[] EndReelValues;
	private float[] Reel_Rotation;

	public float SpeedModifier = 1000f;

	public delegate void OnReelsFinishedEvent(GameObject e);
	public event OnReelsFinishedEvent OnReelsFinished;

	public delegate void OnFullMatchEvent(GameObject e);
	public event OnFullMatchEvent OnFullMatch;

	private bool bSpinIt = false;

	public bool EnableSound = true;
	public float SlotVolume = 1f;
	public AudioClip Spin;
	public AudioClip Stop;
	public AudioClip Win;

    [HideInInspector]
    public GameObject[] Toys;

	public GameObject Spawner;

	public static ReelsScript _reelScript;
    public bool endturn = true;

	public GameObject Pusher;



    private void Awake()
    {
        Instance = this;
    }


    // Use this for initialization
    void Start()
	{
		if (_reelScript == null)
			_reelScript = this;


		if (Reel.Length == 0 || FixSpinReel.Length == 0)
			throw new System.ArgumentException("Reels and FixSpin size cannot be zero");

		if (Reel.Length != FixSpinReel.Length)
			throw new System.ArgumentException("Reels and FixSpin size must be the same");

		Reel_Rotation = new float[Reel.Length];
		EndReelValues = new int[Reel.Length];

		ReelIncrements = 360f / NumberOfItemsPerReel;
        //ReelIncrements += rotation_fix_minor;
        //On777Spawn();
        //StartStopReels(true, 2, 2, 2);



    }


    #region Start & Stop Reel
    internal void StartReels()
    {
        endturn = false;
        bSpinIt = true;
        StartCoroutine(StartStopINum(true));
    }

    internal void StopReels()
    {
        bSpinIt = false;


    } 
	internal void StartStopReels(bool FixSpin, int p1,int p2,int p3 )
	{
		//GameManager.IsReelSpinMode = true;
		EnableFixSpin = FixSpin;
		FixSpinReel.SetValue(p1, 0);
		//FixSpinReel.SetValue(p2, 1);
		//FixSpinReel.SetValue(p3, 2);

		StartCoroutine(StartStopINum(false));
	}
    #endregion

    #region Reel_logic
    internal void NudgeReels(bool bNudgeUp, int iReel)
    {
        if (Reel[iReel] == null)
            throw new System.ArgumentException("Number of Reels = " + Reel.Length.ToString() + ", but you have tried to nudge a invalid reel.");

        if (bNudgeUp)
            Reel_Rotation[iReel] = (((Reel_Rotation[iReel] + ReelIncrements) / 360f) % 1f) * 360f;
        else
            Reel_Rotation[iReel] = (((Reel_Rotation[iReel] - ReelIncrements) / 360f) % 1f) * 360f;

        if (Reel_Rotation[iReel] < 0)
            Reel_Rotation[iReel] += 360f;

        Reel[iReel].transform.eulerAngles = new Vector3(0f, Reel_Rotation[iReel], 0f);

        PlaySlotSound(2, 1f);

        ReelsFinishedRolling();
    }

    IEnumerator StartStopINum(bool bInfinitePreSpin)
    {
        //if()
        bool[] ReelDone = new bool[Reel.Length];
        float[] ReelSpinDuration = new float[Reel.Length];

        for (int i = 0; i < (Reel.Length); i++)
        {
            ReelDone[i] = false;
            EndReelValues[i] = -1;
        }
        AudioManager.Instance.slot_sound.Play();
        PlaySlotSound(1, 1f);

        if (bInfinitePreSpin)
        {
            while (bSpinIt)
            {
                for (int i = 0; i < (Reel.Length); i++)
                {
                    if (Reel[i] == null)
                        throw new System.ArgumentException("Number of Reels = " + Reel.Length.ToString() + ", but you have not assigned all the reel objects.");

                    Reel_Rotation[i] = NewRotation(Reel_Rotation[i]);
                    Reel[i].transform.eulerAngles = new Vector3(0f, Reel_Rotation[i], 0f);
                }

                //Reel[0].transform.eulerAngles = new Vector3(0f, Reel_Rotation[0]-10f, 0f);
                yield return null;
            }
        }

        float StartTime = Time.time;
        float TimePassed = 0;

        if (bInfinitePreSpin)
            ReelSpinDuration[0] = Random.Range(0.5f, 2f);
        else
            ReelSpinDuration[0] = PreSpinDuration + Random.Range(0.5f, 2f);

        for (int i = 1; i < (Reel.Length); i++) // i = 1 because we have already set the 0 part of the array
            ReelSpinDuration[i] = ReelSpinDuration[i - 1] + Random.Range(0.5f, 2f);

        while (TimePassed < ReelSpinDuration[Reel.Length - 1])
        {
            TimePassed = Time.time - StartTime;

            for (int i = 0; i < (Reel.Length); i++)
            {
                if (Reel[i] == null)
                    throw new System.ArgumentException("Number of Reels = " + Reel.Length.ToString() + ", but you have not assigned all the reel objects.");

                if (!ReelDone[i] && TimePassed < ReelSpinDuration[i])
                {
                    Reel_Rotation[i] = NewRotation(Reel_Rotation[i]);
                    Reel[i].transform.eulerAngles = new Vector3(0f, Reel_Rotation[i], 0f);
                }
                else if (!ReelDone[i])
                {
                    Reel[i].transform.rotation = Quaternion.Euler(0, JumpToNearest(ref Reel_Rotation[i], FixSpinReel[i]), 0);
                    //Reel[i].transform.rotation = Quaternion.Euler(0, Reel[i].transform.rotation.y - 10f, 0);
                    PlaySlotSound(2, 1f);

                    ReelDone[i] = true;
                }
            }

            yield return null;
        }

        ReelsFinishedRolling();
    }

    float NewRotation(float fOldRotation)
    {
        fOldRotation += Time.deltaTime * SpeedModifier;
        float ab = ((fOldRotation / 360f) % 1f) * 360f;
        //Debug.Log("E :- "+ ab );
        return ((fOldRotation / 360f) % 1f) * 360f;
    }

    float JumpToNearest(ref float ReelRotation, int iFixSpin)
    {
        if (!EnableFixSpin)
        {
            //Debug.Log("ReelIncrements" + ReelIncrements);
            float fTempPosition = ReelRotation / ReelIncrements;
            float fRoundedValue = Mathf.Round(fTempPosition);
            ReelRotation = ReelIncrements * (fRoundedValue * ReelIncrements == 360f ? 0f : fRoundedValue);
            ReelRotation -= 10;
            return ReelRotation;
        }
        else
        {

            ReelRotation = ReelIncrements * iFixSpin;
            return ReelIncrements * iFixSpin;
        }
    }

    int GetValue(float fReelRotaion)
    {
        return (int)(fReelRotaion / ReelIncrements);
    }

    void ReelsFinishedRolling()
    {
        bool bMatching = true;

        for (int i = 0; i < (Reel.Length); i++)
        {
            EndReelValues[i] = GetValue(Reel_Rotation[i]);

            if (bMatching)
            {
                if (i == 0) // Always true for one reel
                    bMatching = true;
                //else if (EndReelValues[i] == EndReelValues[i - 1])
                //	bMatching = true;
                //else
                //	bMatching = false;
            }
        }

        if (bMatching)
        {
            AudioManager.Instance.slot_sound.Stop();
            PlaySlotSound(3, 1f);
            OnRewards();
            if (OnFullMatch != null)
                OnFullMatch(this.gameObject);
        }
        else
        {
            Invoke("WaitForReward", 3f);
        }

        if (OnReelsFinished != null)
            OnReelsFinished(this.gameObject);


    } 
    #endregion

    #region OnRewards
    void OnRewards()
    {


        //coin_spawner();
        int A1;
        if (EndReelValues[0] < 0)
        {
            A1 = EndReelValues[0] * -1;
        }
        else
        {
            A1 = EndReelValues[0];
        }//int A2 = EndReelValues[0];
         //int A3 = EndReelValues[0];

        if (A1 == 15)
        {
            // endturn = true;
            //Coin777
            //Pusher.GetComponent<PusherScript>().IsLargePush = true;
            //Invoke("On777Spawn", 5f);
            popup_script.current_reward_enum = popup_enum.slot;
            popup_script.active_panel_type = popuppanel_type.normal;
            popup_script.set_panel();
            popup_script.set_panel_type();
            popup_script.gameObject.SetActive(true);
            popup_script.gameObject.transform.DOLocalMove(Vector3.zero, 0.5f).SetUpdate(true);
            Reserved_turn_cheker();
            Debug.Log("Miss");

        }
        else if (A1 == 14)
        {
            Debug.Log("25 small gold coin");
            InvokeRepeating("coin_spawner", 0, 0.1f);
            Invoke("Stop_coinspawner", 2.5f);
            Invoke("Reserved_turn_cheker", 2.5f);
        }
        else if (A1 == 13)
        {
            Debug.Log("50 small gold coin");
            InvokeRepeating("coin_spawner", 0, 0.1f);
            Invoke("Stop_coinspawner", 5f);
            Invoke("Reserved_turn_cheker", 5f);
            //PlayerPrefs.SetInt("CoinCounter",25);
            //speed = 0.3f;
            //OnCoinSpawner();
            //Coin25
        }
        else if (A1 == 12)
        {
            Debug.Log("toys");
            Toys_spawner();
            Invoke("Reserved_turn_cheker", 1f);
            //PlayerPrefs.SetInt("CoinCounter", 300);
            //speed = 0.05f;
            //OnCoinSpawner();
            ////Coin300
        }
        else if (A1 == 11)
        {
            Debug.Log("777 green");
            //PlayerPrefs.SetInt("MoneyCounter",10);
            //OnMoneySpawner();
            //$10
            Platform_Manager.Instance.large_push_777_green();
        }
        else if (A1 == 10)
        {
            Debug.Log("777 red");

            Platform_Manager.Instance.large_push_777_red();

            //$777
        }
        else if (A1 == 9)
        {
            Debug.Log("coin tower");
            gold_tower_reward();
            //PlayerPrefs.SetInt("CoinCounter", 75);
            //speed = 0.2f;
            //OnCoinSpawner();
            //Coin75
        }
        else if (A1 == 8)
        {
            Debug.Log("200 big gold coin");
            InvokeRepeating("big_coin_spawner", 0, 0.1f);
            Invoke("big_Stop_coinspawner", 2f);
            Invoke("Reserved_turn_cheker", 2f);
            //PlayerPrefs.SetInt("MoneyCounter", 20);
            //OnMoneySpawner();
            //$20
        }
        else if (A1 == 7)
        {
            Debug.Log("300 big gold coin");
            InvokeRepeating("big_coin_spawner", 0, 0.1f);
            Invoke("big_Stop_coinspawner", 3f);
            Invoke("Reserved_turn_cheker", 3f);
            //PlayerPrefs.SetInt("MoneyCounter", 20);
            //OnMoneySpawner();
            //$20
        }
        else if (A1 == 6)
        {
            Debug.Log("black bar");
            coin_spawner_Black_Bar_coin();
            Invoke("Reserved_turn_cheker", 1f);
            //PlayerPrefs.SetInt("MoneyCounter", 20);
            //OnMoneySpawner();
            //$20
        }
        else if (A1 == 5)
        {
            Debug.Log("Auto shot (30 coins)");
            //PlayerPrefs.SetInt("MoneyCounter", 20);
            //OnMoneySpawner();
            //$20
            auto_coin();
        }
        else if (A1 == 4)
        {
            Debug.Log("50 small green coins");
            InvokeRepeating("green_coin_spawner", 0, 0.1f);
            Invoke("green_Stop_coinspawner", 5f);
            Invoke("Reserved_turn_cheker", 5f);
            //PlayerPrefs.SetInt("MoneyCounter", 20);
            //OnMoneySpawner();
            //$20
        }
        else if (A1 == 3)
        {
            Debug.Log("20 small green coins");
            InvokeRepeating("green_coin_spawner", 0, 0.1f);
            Invoke("green_Stop_coinspawner", 2f);
            Invoke("Reserved_turn_cheker", 2f);
            //PlayerPrefs.SetInt("MoneyCounter", 20);
            //OnMoneySpawner();
            //$20
        }
        else if (A1 == 2)
        {
            Debug.Log("10 small green coins");
            InvokeRepeating("green_coin_spawner", 0, 0.1f);
            Invoke("green_Stop_coinspawner", 1f);
            Invoke("Reserved_turn_cheker", 1f);
            //PlayerPrefs.SetInt("MoneyCounter", 20);
            //OnMoneySpawner();
            //$20
        }
        else if (A1 == 1)
        {
            Debug.Log("Shake Coins");
            InvokeRepeating("coin_spawner_Shake_coin", 0, 1f);
            Invoke("Stop_coinspawner_Shake_coin", 3f);
            Invoke("Reserved_turn_cheker", 3f);
        }
        else if (A1 == 16 || A1 == 0) 
        {
            Debug.Log("200 big green coin");
            InvokeRepeating("big_green_coin_spawner", 0, 0.1f);
            Invoke("big_green_Stop_coinspawner", 2f);
            Invoke("Reserved_turn_cheker", 2f);
            //PlayerPrefs.SetInt("MoneyCounter", 20);
            //OnMoneySpawner();
            //$20
        }



    }

	#endregion

	#region coin_spawner
	public void coin_spawner()
    {
        float xa = Random.Range(-3.3f, 3.3f);
        float ya = Random.Range(10f, 11f);
        float za = Random.Range(28f, 32f);
        Instantiate(coin, new Vector3(xa, ya, za), Quaternion.identity);
    }
    public void Stop_coinspawner()
    {
        CancelInvoke("coin_spawner");
    }

    #endregion
    #region big_coin_spawner
    public void big_coin_spawner()
    {
        float xa = Random.Range(-3.3f, 3.3f);
        float ya = Random.Range(10f, 11f);
        float za = Random.Range(28f, 32f);
        Instantiate(big_coin, new Vector3(xa, ya, za), Quaternion.identity);
    }
    public void big_Stop_coinspawner()
    {
        CancelInvoke("big_coin_spawner");
    } 
    #endregion
    #region Green_coin_spawner
    public void green_coin_spawner()
    {
        float xa = Random.Range(-3.3f, 3.3f);
        float ya = Random.Range(10f, 11f);
        float za = Random.Range(28f, 32f);
        Instantiate(greencoin, new Vector3(xa, ya, za), Quaternion.identity);
    }
    public void green_Stop_coinspawner()
    {
        CancelInvoke("green_coin_spawner");
    }
    #endregion
    #region big_Green_coin_spawner
    public void big_green_coin_spawner()
    {
        float xa = Random.Range(-3.3f, 3.3f);
        float ya = Random.Range(10f, 11f);
        float za = Random.Range(28f, 32f);
        Instantiate(big_green_coin, new Vector3(xa, ya, za), Quaternion.identity);
    }
    public void big_green_Stop_coinspawner()
    {
        CancelInvoke("big_green_coin_spawner");
    }
    #endregion
    #region Shake_coin
    public void coin_spawner_Shake_coin()
    {
        float xa = Random.Range(-3.3f, 3.3f);
        float ya = Random.Range(10f, 11f);
        float za = Random.Range(28f, 32f);
        Instantiate(shake_coin, new Vector3(xa, ya, za), Quaternion.identity);
    }
    public void Stop_coinspawner_Shake_coin()
    {
        CancelInvoke("coin_spawner_Shake_coin");
    }
	public void Shake_coin()
    {
		Platform_Manager.Instance.Big_coin_Shake();
    }

    #endregion
    void On777Spawn()
    {
		GameObject A7 = Instantiate(Resources.Load("prefabschipcoinTower1")) as GameObject;
		
		A7.transform.position = Vector3.zero;

		for (int i = 0; i < A7.transform.childCount; i++)
		{
			A7.transform.GetChild(i).SetParent(null);
		}
		Invoke("WaitForReward", 1f);

	}

	float speed;

	void OnCoinSpawner()
    {
		float Xpos = Random.Range(-25f, -24);
		float YPos = 0;
		float Zpos = Random.Range(-80f, -79f);
		Vector3 NewPos =  new Vector3(Xpos, YPos, Zpos);
		//Instantiate(Spawner.GetComponent<SpawnerScript>().C_1, NewPos, Spawner.transform.rotation);
		//GameObject aN = Instantiate(Resources.Load("prefabschipchip01")) as GameObject;
		//aN.transform.localPosition = new Vector3(Xpos, YPos, Zpos);

		

		PlayerPrefs.SetInt("CoinCounter", PlayerPrefs.GetInt("CoinCounter") - 1);

		if(PlayerPrefs.GetInt("CoinCounter")>0)
        {
			Invoke("OnCoinSpawner", speed);

		}else
        {
			PlayerPrefs.SetInt("CoinCounter", 0);
			Invoke("WaitForReward", 1f);
		}
	}

	void OnMoneySpawner()
	{
		float Xpos = Random.Range(-25f, -24);
		float YPos = 0;
		float Zpos = Random.Range(-80f, -79f);
		Vector3 NewPos = new Vector3(Xpos, YPos, Zpos);
		//Instantiate(Spawner.GetComponent<SpawnerScript>().D_1, NewPos, Spawner.transform.rotation);

		PlayerPrefs.SetInt("MoneyCounter", PlayerPrefs.GetInt("MoneyCounter") - 1);

		if (PlayerPrefs.GetInt("MoneyCounter") > 0)
		{
			Invoke("OnMoneySpawner", 0.4f);

		}
		else
		{
			PlayerPrefs.SetInt("MoneyCounter", 0);
			Invoke("WaitForReward", 1f);
		}
	}


	void WaitForReward()
    {
		//GameManager.IsReelSpinMode = false;
	}

	void PlaySlotSound(int iSoundType, float fSoundVolume)
	{
		//GetComponent<AudioSource>().volume = PlayerPrefs.GetInt("Sound");
			//return;

		//if (iSoundType == 1 && Spin != null)
		//	GetComponent<AudioSource>().PlayOneShot(Spin, fSoundVolume);
		//else if (iSoundType == 2 && Stop != null)
		//	GetComponent<AudioSource>().PlayOneShot(Stop, fSoundVolume);
		//else if (iSoundType == 3 && Win != null)
		//	GetComponent<AudioSource>().PlayOneShot(Win, fSoundVolume);
	}




    public void gold_tower_reward()
    {
        Platform_Manager.Instance.Coin_platfrom_down();
    }

	#region auto_coin
	public void auto_coin()
    {
        Shoot_coin.Instance.autocoin = true;
        InvokeRepeating("Start_auto_coin", 0f, 0.2f);
        Invoke("stop_auto_coin", 6f);
        Invoke("Reserved_turn_cheker", 6f);
    }

    public void Start_auto_coin()
    {
        Shoot_coin.Instance.Auto_coin_thrower();

    }

    public void stop_auto_coin()
    {
        CancelInvoke("Start_auto_coin");
        Shoot_coin.Instance.autocoin = false;
    }
    #endregion


    public void Toys_spawner()
    {

        int x = Random.Range(0, 8);
        float xa = Random.Range(-3.3f, 3.3f);
        float ya = Random.Range(10f, 11f);
        float za = Random.Range(28f, 32f);
        Instantiate(Toys[x], new Vector3(xa, ya, za), Quaternion.identity);
    }

    public void coin_spawner_Black_Bar_coin()
    {
        float xa = Random.Range(-3.3f, 3.3f);
        float ya = Random.Range(10f, 11f);
        float za = Random.Range(28f, 32f);
        Instantiate(black_bar_coin, new Vector3(xa, ya, za), Quaternion.identity);
    }


    public void Reserved_turn_cheker()
    {
        if(Slot_coin_grabber.Instance.spin_counter>0)
        {
            Slot_coin_grabber.Instance.spinning = true;
            Slot_coin_grabber.Instance.spin_counter--;
            Slot_coin_grabber.Instance.update_spin_counter();
            StartReels();
            Invoke("StopReels", 3f);
        }
        else
        {
            Slot_coin_grabber.Instance.spinning = false;
        }
    }
}
