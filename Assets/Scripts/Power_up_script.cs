﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Power_up_script : MonoBehaviour
{
    public GameObject coin_thrower_ob;
    public GameObject bottom_sphere;
    public GameObject poll;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Triple_coin_powerup()
    {
        Invoke("Triple_coin_powerup_off", 30f);
        coin_thrower_ob.GetComponent<Shoot_coin>().three_coin_bool = true;
    }

    public void Double_dmg_coin_powerup()
    {
        Invoke("Double_dmg_coin_powerup_off", 30f);
        coin_thrower_ob.GetComponent<Shoot_coin>().double_dmg_bool = true;
    }

    public void Froze_coin_powerup()
    {
        Invoke("Froze_coin_powerup_off", 30f);
        coin_thrower_ob.GetComponent<Shoot_coin>().froze_bool = true;
        poll.GetComponent<Rotation_object_script>().speed = -15;
        bottom_sphere.GetComponent<Rotation_object_script>().speed = -15;
    }

    public void Triple_coin_powerup_off()
    {
        coin_thrower_ob.GetComponent<Shoot_coin>().three_coin_bool = false;
    }

    public void Double_dmg_coin_powerup_off()
    {
        coin_thrower_ob.GetComponent<Shoot_coin>().double_dmg_bool = false;
    }

    public void Froze_coin_powerup_off()
    {
        coin_thrower_ob.GetComponent<Shoot_coin>().froze_bool = false;
        poll.GetComponent<Rotation_object_script>().speed = -30;
        bottom_sphere.GetComponent<Rotation_object_script>().speed = -30;
    }


}
