﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using UnityEngine.UI;

public class Ads_initialize_API : MonoBehaviour
{
    public static Ads_initialize_API Instance;
    private string rawJson;
    public JSONNode jsonResult;
    string url = "http://134.209.103.120/CoinPusher/mapi/1/setting/eyJ0eXAiOiJKV1QiLCdlcMdGciOiJSUzI1";
    public string android_admob_interstitial_id;
    public string android_admob_reward_id;
    public string ios_admob_interstitial_id;
    public string ios_admob_reward_id;
    public string android_admob_native_id;
    public string ios_admob_native_id;

    public string android_admob_banner_id;
    public string ios_admob_banner_id;


    public string android_facebook_reward_id;
    public string android_facebook_interstitial_id;

    public string ios_facebook_reward_id;
    public string ios_facebook_interstitial_id;

    public string android_unity_game_id;
    public string ios_unity_game_id;


    public string ads_priority;
    public string interstitial_ads_priority;
    public int ads_click;
    //public int ios_ads_click;

    public bool Test_mode = false;

    public bool init_done = false;
    private void Awake()
    {
        Instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {
        get_data();
    }

    public void get_data()
    {
        StartCoroutine("GetData");
    }
    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        //req_call = false;
        //www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        yield return www.Send();
        //Debug.Log(www);
        if (www.isNetworkError)
        {
            Debug.Log(www.error);


            //Check_Internet_connection();
            //req_call = true;
        }
        else
        {
            //close_Retry_panel();
            // Show results as text
            //Debug.Log(www.downloadHandler.text);
            rawJson = www.downloadHandler.text;
            Manual_Start();
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }

    //    #region Internet check
    //    public void Check_Internet_connection()
    //    {
    //        StartCoroutine(Ck_net(isConnected =>
    //        {
    //            if (isConnected)
    //            {
    //                Debug.Log("Internet Available!");
    //                //return true;
    //                if (!Splash_Panel_scripts.Instance.Loading_panel.activeSelf)
    //                {
    //                    Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
    //                    Splash_Panel_scripts.Instance.Something_went_wrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
    //                    Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(true);
    //                }
    //            }
    //            else
    //            {
    //                Debug.Log("Internet Not Available");
    //                //return  false;
    //                if (!Splash_Panel_scripts.Instance.Loading_panel.activeSelf)
    //                {
    //                    Splash_Panel_scripts.Instance.Notice_panel.SetActive(true);
    //                    Splash_Panel_scripts.Instance.no_internet_connection_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
    //                    Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(true);
    //                }
    //            }
    //        }));
    //    }

    //    public IEnumerator Ck_net(Action<bool> syncResult)
    //    {
    //        const string echoServer = "http://google.com";

    //        bool result;
    //        using (var request = UnityWebRequest.Head(echoServer))
    //        {
    //            request.timeout = 0;
    //            yield return request.SendWebRequest();
    //            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
    //        }
    //        syncResult(result);
    //    }


    //    //}
    //    public void Retry_connection()
    //    {
    //        get_data();
    //        Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(false);
    //        Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(false);
    //        Splash_Panel_scripts.Instance.Loading_panel.SetActive(true);
    //        StartCoroutine("Retry_connection_coroutine");
    //    }
    //    IEnumerator Retry_connection_coroutine()
    //    {
    //        yield return new WaitForSeconds(3f);
    //        //StartCoroutine("check_connection");
    //        Splash_Panel_scripts.Instance.Loading_panel.SetActive(false);
    //        Splash_Panel_scripts.Instance.Notice_panel.SetActive(false);
    //        get_data();
    //    }
    //    public void close_Retry_panel()
    //    {
    //        Splash_Panel_scripts.Instance.Something_went_wrong_panel.SetActive(false);
    //        Splash_Panel_scripts.Instance.no_internet_connection_panel.SetActive(false);
    //        Splash_Panel_scripts.Instance.Loading_panel.SetActive(false);
    //        Splash_Panel_scripts.Instance.Notice_panel.SetActive(false);
    //    }
    //#endregion


    public void Manual_Start()
    {
        jsonResult = JSON.Parse(rawJson);
        Set_Json_data();
    }

    public void Set_Json_data()
    {


        //ADS
        if (!Test_mode)
        {
            //admob
            android_admob_interstitial_id = jsonResult["data"]["ads"]["android_admob_interstitial_id"].Value;
            android_admob_reward_id = jsonResult["data"]["ads"]["android_admob_reward_id"].Value;
            ios_admob_interstitial_id = jsonResult["data"]["ads"]["ios_admob_interstitial_id"].Value;
            ios_admob_reward_id = jsonResult["data"]["ads"]["ios_admob_reward_id"].Value;

            android_admob_native_id = jsonResult["data"]["ads"]["android_admob_native"].Value;
            ios_admob_native_id = jsonResult["data"]["ads"]["ios_admob_native"].Value;

            android_admob_banner_id = jsonResult["data"]["ads"]["android_admob_banner_id"].Value;
            ios_admob_banner_id = jsonResult["data"]["ads"]["ios_admob_banner_id"].Value;




            ////fb
            android_facebook_interstitial_id = jsonResult["data"]["ads"]["android_facebook_interstitial_id"].Value;
            android_facebook_reward_id = jsonResult["data"]["ads"]["android_facebook_reward_id"].Value;

            ios_facebook_interstitial_id = jsonResult["data"]["ads"]["ios_facebook_interstitial_id"].Value;
            ios_facebook_reward_id = jsonResult["data"]["ads"]["ios_facebook_reward_id"].Value;

            ////unity
            android_unity_game_id = jsonResult["data"]["ads"]["android_unity_game_id"].Value;
            ios_unity_game_id = jsonResult["data"]["ads"]["ios_unity_id"].Value;

        }
        else
        {
            Ads_priority_script.Instance.transform.GetComponent<Unity_Ads_script>().testMode = Test_mode;
        }
#if UNITY_ANDROID
        ads_click = jsonResult["data"]["ads"]["ads_click"].AsInt;
        ads_priority = jsonResult["data"]["ads"]["ads_priority"].Value;
        interstitial_ads_priority = jsonResult["data"]["ads"]["interstitial_ads_priority"].Value;
#elif UNITY_IOS

        ads_click = jsonResult["data"]["ads"]["ios_ads_click"].AsInt;
        ads_priority = jsonResult["data"]["ads"]["ios_ads_priority"].Value;
        interstitial_ads_priority = jsonResult["data"]["ads"]["ios_interstitial_ads_priority"].Value;
#else
        ads_click = jsonResult["data"]["ads"]["ads_click"].AsInt;
#endif
        //SEt ALL IDS TO scripts
        Manual_start_ALL_ADS();
    }

    public void Manual_start_ALL_ADS()
    {
        Debug.Log("Got_response");
        Ads_priority_script.Instance.Initialize_ads();
        //init_done = true;
        //Gamemanager.Instance.ads_controller.GetComponent<AdsController>().Manual_Start();
        //Gamemanager.Instance.ads_controller.GetComponent<Unity_Ads_script>().Manual_Start();
        //Gamemanager.Instance.ads_controller.GetComponent<FB_ADS_script>().Manual_Start();

    }
}
