﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;
using System;
using UnityEngine.UI;

public class Withdraw_api_script : MonoBehaviour
{
    public string token;
    public string redeem_id;
    public string coin_type;
    public string coins;
    public string email;
    public string username;
    public string mo_num;
    private JSONNode jsonResult;
    //public bool cashout = false;

    public TMP_InputField _email, _name, _num;
    public GameObject withdraw_panel;

    public bool success;

    public cardtype ct;
    string rawJson;


    //public GameObject selected_card;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void start_upload()
    {
        
        if(ct== cardtype.cashout)
        {
            email = _email.text;
            username = _name.text;
            mo_num = _num.text;
        }
        StartCoroutine("Upload");
    }

    IEnumerator Upload()
    {
        //Debug.Log("device_token" + device_token);
        WWWForm form = new WWWForm();
        form.AddField("login_type", "guest");

        form.AddField("redeem_id", redeem_id);
        form.AddField("coin_type", coin_type);
        form.AddField("coin", coins);

        if (ct == cardtype.cashout)
        {
            form.AddField("email", email);
            form.AddField("name", username);
            form.AddField("mo_num", mo_num);
        }

       
        UnityWebRequest www = UnityWebRequest.Post("http://134.209.103.120/CoinPusher/mapi/1/user-withdraw-coin", form);
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        yield return www.SendWebRequest();
        Debug.Log("www: " + www.responseCode);
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            Check_Internet_connection();
        }
        else
        {
            if (Notice_panel_script.Instance != null)
            {
                close_Retry_panel();
            }
            rawJson = www.downloadHandler.text;
            Get_response_data();
            Debug.Log("Form upload complete!");
        }

    }

    public void Get_response_data()
    {
        jsonResult = JSON.Parse(rawJson);

        success = jsonResult["success"].AsBool;
        GetComponent<Redeem_api_script>().redeem_loading_panel.SetActive(false);
        if(success)
        {
            minus_coins();
        }
        else
        {
            //somthing went wrong
            Gamemanager.Instance.nem_popup_fun("Purchase Failed");
        }


    }

    public void minus_coins()
    {
        switch (coin_type)
        {

            case "gold":
                GetComponent<Redeem_api_script>().cm._Gold_coin_current_total -= int.Parse(coins);
                break;
            case "green":
                GetComponent<Redeem_api_script>().cm._Green_coin_current_total -= float.Parse(coins);
                break;
            case "black":
                GetComponent<Redeem_api_script>().cm._Black_coin_current_total -= int.Parse(coins);
                break;
            default:
               
                break;
        }
        GetComponent<Redeem_api_script>().cm.update_ui();
        GetComponent<Redeem_api_script>().cards_update_ui();
        GetComponent<Update_wallet_api_script>().upload_data();
        Gamemanager.Instance.nem_popup_fun("Successfully Purchased");
    }

    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_panel.SetActive(true);
                    Notice_panel_script.Instance.Something_went_wrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_panel.SetActive(true);
                    Notice_panel_script.Instance.no_internet_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.no_internet_panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }

    public void Retry_connection()
    {
        start_upload();

        Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(false);
        Notice_panel_script.Instance.no_internet_panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_panel.SetActive(false);
        start_upload();
    }
    public void close_Retry_panel()
    {
        Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(false);
        Notice_panel_script.Instance.no_internet_panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_panel.SetActive(false);
    }
}
