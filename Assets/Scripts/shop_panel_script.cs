﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class shop_panel_script : MonoBehaviour
{
    public GameObject model_panel, fruit_panel,coin_panel;




    public void Onstart()
    {
        transform.DOLocalMove(Vector3.zero,0.5f).SetUpdate(true);
    }

    public void Onclose()
    {
        transform.DOLocalMove(new Vector3(-1237,0,0),0.5f).SetUpdate(true).OnComplete(()=> { gameObject.SetActive(false); Gamemanager.Instance.start_time(); model_panel.SetActive(true); fruit_panel.SetActive(false); coin_panel.SetActive(false); });
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
