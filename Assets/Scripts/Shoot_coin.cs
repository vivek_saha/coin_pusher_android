﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class Shoot_coin : MonoBehaviour
{
    public static Shoot_coin Instance;
    public popup_panel_script popup_panel_scrript;
    public int initial_coin;
    public GameObject coin, particle_double_dmg, particle_froze;
    public int Speed_coin;
    public Vector3 Dir_shoot;
    public TextMeshProUGUI coin_text, timer_text;
    Rigidbody rb;
    public bool autocoin = false;
    public bool three_coin_bool = false;
    public bool double_dmg_bool = false;
    public bool froze_bool = false;
    public bool boost_mode = false;
    public int speed = 3;
    public float auto_coin_adjust = 1;
    private bool UpDown = true;
    private float pos_x;
    Vector3 Leftmost;
    Vector3 rightmost;
    GameObject[] list_coins = new GameObject[4];
    long value__1;
    bool enable_timer = false;
    public float _timer;
    public float timer
    {
        get
        {
            return PlayerPrefs.GetFloat("_timer", 0);
        }
        set
        {
            timer_text.text = "Next:" + ((int)value).ToString() + "s";
            _timer = value;
            PlayerPrefs.SetFloat("_timer", _timer);
        }
    }

    public int coin_counter
    {
        get
        {
            return PlayerPrefs.GetInt("coin_counter", 40);
        }
        set
        {
            coin_text.text = value.ToString();
            PlayerPrefs.SetInt("coin_counter", value);
            if (value < 40)
            {
                //start_timer();
                if (!enable_timer)
                {
                    //timer = 400;
                    enable_timer = true;
                }
            }
            else
            {
                //disable_timer();
                enable_timer = false;
            }
        }
    }

    //fov 60 == dir_y 57
    //fov 70 == dir_y 62


    private void Awake()
    {
        Instance = this;
    }


    // Start is called before the first frame update
    void Start()
    {

        //Debug.Log(System.DateTime.Now);
        //string a = System.DateTime.Now.ToString();
        //string[] data = a.Split(' ');
        //string date = data[0];
        //string time = data[1];
        //string[] time_data = time.Split(':'); 

        list_coins[0] = ReelsScript.Instance.coin;
        list_coins[1] = ReelsScript.Instance.big_coin;
        list_coins[2] = ReelsScript.Instance.greencoin;
        list_coins[3] = ReelsScript.Instance.big_green_coin;

        Leftmost = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 10));
        rightmost = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 10));
        //coin_counter = initial_coin;
        coin_counter = PlayerPrefs.GetInt("coin_counter", 40);
        onstart();
    }

    public void onstart()
    {
        long nowDate = DateTime.UtcNow.Ticks / 10000 / 1000;
        long lastDate = long.Parse(PlayerPrefs.GetString("lastDate", nowDate.ToString()));
        //Debug.Log("lastDate =>" + lastDate);
        long secondsSinceLastDate = nowDate - lastDate;
        //Debug.Log("secondsSinceLastDate =>" + secondsSinceLastDate);
        //value__1 = PlayerPrefs.GetInt("value__1", (int)secondsSinceLastDate);
        //PlayerPrefs.SetInt()
        minus_time((int)secondsSinceLastDate);

    }

    void minus_time(int x)
    {
        if (x > 400)
        {
            int part = x / 400;
            for (int i = 0; i < part; i++)
            {
                coin_counter++;
            }
            if (x - (400 * part) > timer)
            {
                coin_counter++;
                timer = 400 - (x - (400 * part) - timer);
            }
            else
            {
                timer -= x - (400 * part);
            }
        }
        else
        {
            if (x > timer)
            {
                coin_counter++;
                timer = 400 - (x - timer);
            }
            else
            {
                timer -= x;
            }
        }
    }

    // Update is called once per frame
    #region Update

    bool IsPointerOverGameObject(int fingerId)
    {
        EventSystem eventSystem = EventSystem.current;
        return (eventSystem.IsPointerOverGameObject(fingerId)
            && eventSystem.currentSelectedGameObject != null);
    }
    void Update()
    {

        if (enable_timer)
        {
            if (timer <= 0)
            {
                if (coin_counter < 40)
                {
                    coin_counter++;
                    timer = 400;
                }
                else
                {
                    enable_timer = false;
                }
            }
            else
            {
                timer -= Time.deltaTime;
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                //Debug.Log("Clicked on the UI");
                AudioManager.Instance.button_click_sound();
            }
            else
            {
                if (Time.timeScale == 1)
                {
                    if (coin_counter > 0)
                    {
                        //Debug.Log(Input.mousePosition);
                        //Vector3 temp = Input.mousePosition;
                        //temp.y = 0;

                        Vector3 temp_final = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 0, 10));
                        //Debug.Log("temp_final"+temp_final);
                        GameObject coin_temp = Instantiate(coin, temp_final, Quaternion.identity);
                        coin_temp.transform.localScale = new Vector3(90, 15, 90);
                        if (double_dmg_bool)
                        {
                            Platform_Manager.Instance.coin_dmg = 2;
                            GameObject _particle = Instantiate(particle_double_dmg, temp_final, Quaternion.identity);
                            _particle.transform.SetParent(coin_temp.transform);
                            _particle.transform.localScale = new Vector3(1, 1, 1);
                        }
                        else if (froze_bool)
                        {
                            Platform_Manager.Instance.coin_dmg = 1;
                            GameObject _particle = Instantiate(particle_froze, temp_final, Quaternion.identity);
                            _particle.transform.SetParent(coin_temp.transform);
                            _particle.transform.localScale = new Vector3(1, 1, 1);
                        }
                        else
                        {
                            Platform_Manager.Instance.coin_dmg = 1;
                        }
                        rb = coin_temp.GetComponent<Rigidbody>();
                        rb.AddForce(Dir_shoot * Speed_coin);
                        coin_counter--;
                        if (boost_mode)
                        {
                            boost_coin_generate();
                        }
                        if (three_coin_bool)
                        {
                            Three_coin_shoot();
                        }
                        AudioManager.Instance.Coin_throw_sound.Play();
                    }
                    else
                    {
                        //Debug.Log("no coin");
                        Gamemanager.Instance.stop_time_and_show_interstatial();
                        popup_panel_scrript.current_reward_enum = popup_enum.coin;
                        popup_panel_scrript.active_panel_type = popuppanel_type.normal;
                        popup_panel_scrript.set_panel();
                        popup_panel_scrript.set_panel_type();
                        popup_panel_scrript.gameObject.SetActive(true);
                        popup_panel_scrript.gameObject.transform.DOLocalMove(Vector3.zero, 0.5f).SetUpdate(true);
                    }
                }
            }
        }
#elif UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if (IsPointerOverGameObject(Input.GetTouch(0).fingerId))
            {
                Debug.Log("Hit UI, Ignore Touch");
                 AudioManager.Instance.button_click_sound();
            }
            else
            {
            if (Time.timeScale == 1)
                {
                Debug.Log("Handle Touch");
                if (coin_counter > 0)
                {
                    //Debug.Log(Input.mousePosition);
                    //Vector3 temp = Input.mousePosition;
                    //temp.y = 0;

                    Vector3 temp_final = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 0, 10));
                    //Debug.Log("temp_final"+temp_final);
                    GameObject coin_temp = Instantiate(coin, temp_final, Quaternion.identity);
                    coin_temp.transform.localScale = new Vector3(90, 15, 90);
                    if (double_dmg_bool)
                    {
                        Platform_Manager.Instance.coin_dmg = 2;
                        GameObject _particle = Instantiate(particle_double_dmg, temp_final, Quaternion.identity);
                        _particle.transform.SetParent(coin_temp.transform);
                        _particle.transform.localScale = new Vector3(1, 1, 1);
                    }
                    else if (froze_bool)
                    {
                        Platform_Manager.Instance.coin_dmg = 1;
                        GameObject _particle = Instantiate(particle_froze, temp_final, Quaternion.identity);
                        _particle.transform.SetParent(coin_temp.transform);
                        _particle.transform.localScale = new Vector3(1, 1, 1);
                    }
                    else
                    {
                        Platform_Manager.Instance.coin_dmg = 1;
                    }
                    rb = coin_temp.GetComponent<Rigidbody>();
                    rb.AddForce(Dir_shoot * Speed_coin);
                    coin_counter--;
                    if (boost_mode)
                    {
                        boost_coin_generate();
                    }
                    if (three_coin_bool)
                    {
                        Three_coin_shoot();
                    }
                    AudioManager.Instance.Coin_throw_sound.Play();
                }
                else
                {
                    //Debug.Log("no coin");
                    Gamemanager.Instance.stop_time();
                    popup_panel_scrript.current_reward_enum = popup_enum.coin;
                    popup_panel_scrript.active_panel_type = popuppanel_type.normal;
                    popup_panel_scrript.set_panel();
                    popup_panel_scrript.set_panel_type();
                    popup_panel_scrript.gameObject.SetActive(true);
                    popup_panel_scrript.gameObject.transform.DOLocalMove(Vector3.zero, 0.5f).SetUpdate(true);
                }
            }
            }
        }

#endif

        if (autocoin)
        {
            //Debug.Log(pos_x);
            if (UpDown)
            {

                //transform.Translate(Vector3.left * speed * Time.deltaTime);
                pos_x = Mathf.Lerp(pos_x, Leftmost.x, Time.deltaTime * speed);
            }
            else
            {
                pos_x = Mathf.Lerp(pos_x, rightmost.x, Time.deltaTime * speed);
                //transform.Translate(Vector3.right * speed * Time.deltaTime);
            }


            if (Mathf.Abs(pos_x - rightmost.x) <= 0.5f)
            {
                UpDown = true;
            }
            else if (Mathf.Abs(pos_x - Leftmost.x) <= 0.5f)
            {
                UpDown = false;
            }
        }

    }
    #endregion


    #region Three_coin_shoot
    public void Three_coin_shoot()
    {
        Vector3 temp_final = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 0, 10));
        //Debug.Log("temp_final"+temp_final);
        temp_final.x += 3;
        GameObject coin_temp = Instantiate(coin, temp_final, Quaternion.identity);
        coin_temp.transform.localScale = new Vector3(100, 20, 100);
        rb = coin_temp.GetComponent<Rigidbody>();
        rb.AddForce(Dir_shoot * Speed_coin);
        //coin_counter--; 
        temp_final = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 0, 10));
        temp_final.x -= 3;
        //Debug.Log("temp_final"+temp_final);
        coin_temp = Instantiate(coin, temp_final, Quaternion.identity);
        coin_temp.transform.localScale = new Vector3(100, 20, 100);
        rb = coin_temp.GetComponent<Rigidbody>();
        rb.AddForce(Dir_shoot * Speed_coin);
        //coin_counter--;
    }
    #endregion

    #region Auto_coin_thrower
    public void Auto_coin_thrower()
    {
        GameObject coin_temp = Instantiate(coin, new Vector3(pos_x, Leftmost.y, 10), Quaternion.identity);
        coin_temp.transform.localScale = new Vector3(100, 20, 100);
        rb = coin_temp.GetComponent<Rigidbody>();
        //Debug.Log(Dir_shoot);
        rb.AddForce(Dir_shoot * Speed_coin * auto_coin_adjust);
    }
    #endregion

    #region Add_coin
    public void Add_coin()
    {
        coin_counter += 10;
    }
    public void Add_coin_40()
    {
        coin_counter += 40;
    }
    #endregion

    public void boost_coin_generate()
    {
        int x = UnityEngine.Random.Range(0, 5);
        for (int i = 0; i < x; i++)
        {
            int s = UnityEngine.Random.Range(0, 2);
            if (s == 0)
            {

            }
            else
            {
                spawn_coin(list_coins[i]);
            }
            //spawn_coin(list_coins[i]);
        }
    }

    public void spawn_coin(GameObject a)
    {
        Vector3 temp_final = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 0, 10));
        float ya = UnityEngine.Random.Range(10f, 11f);
        float za = UnityEngine.Random.Range(28f, 32f);
        int i = UnityEngine.Random.Range(0, 2);
        if (i == 0)
        {
            Instantiate(a, new Vector3(temp_final.x, ya, za), Quaternion.identity);
        }
        else
        {
            Instantiate(a, new Vector3(temp_final.x, ya, za), Quaternion.identity);
            Instantiate(a, new Vector3(temp_final.x, ya, za), Quaternion.identity);
        }
    }


    private void OnApplicationQuit()
    {
        long date = DateTime.UtcNow.Ticks / 10000 / 1000;
        PlayerPrefs.SetString("lastDate", date.ToString());
        //PlayerPrefs.SetInt("lastDate", (int)date);
    }

}
