﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sprite_scaler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Resize();
    }

    void Resize()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        if (sr == null) return;

        float spriteHeight = sr.sprite.bounds.size.y;
        float spriteWidth = sr.sprite.bounds.size.x;
        float distance = transform.position.z - Camera.main.transform.position.z;
        float screenHeight = 2 * Mathf.Tan(Camera.main.fieldOfView * Mathf.Deg2Rad / 2) * distance;
        float screenWidth = screenHeight * Camera.main.aspect;
        transform.localScale = new Vector3(screenWidth / spriteWidth, screenHeight / spriteWidth, 1f);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
