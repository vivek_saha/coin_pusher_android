﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Slot_coin_grabber : MonoBehaviour
{
    public static Slot_coin_grabber Instance;
	//public Vector3 initPosition;
	//public Vector3 newPosition;

	public float speed = 0.5f;

    public bool spinning = false;
    public int spin_counter=0;
    public TextMeshProUGUI Spin_counter_text;
	public bool UpDown = true;
    public Animator slot_counter_animator;
    public ReelsScript abc;
    //public ParticleSystem BlastParty;
    bool slot_counter_bool = true;

    private void Awake()
    {
        Instance = this;
    }
    void Start()
	{
        //abc = 
        //ReelsScrip._reelScript.StartStopReels();
        ////BlastParty.Play();
        //initPosition = new Vector3(0f, -0.32f, 0.613f);
        //newPosition = new Vector3(0f, -0.32f, 0.613f);
    }

    #region Update
    void Update()
    {

        if (UpDown)
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }


        if (transform.localPosition.x >= -0.3464f)
        {
            UpDown = true;
        }
        else if (transform.localPosition.x <= -0.4455f)
        {
            UpDown = false;
        }
    }
    #endregion

    #region Collision

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Coin") || collision.gameObject.CompareTag("Green_coin"))
        {

            Destroy(collision.gameObject);
            Debug.Log("Roll the Slot Please!!!!!!!");
            if (!spinning)
            {
                spinning = true;
                abc.StartReels();
                Invoke("stop_reels_", 1f);
            }
            else
            {
                spin_counter++;
                update_spin_counter();
            }
        }

    }

    public void update_spin_counter()
    {
        if(slot_counter_bool)
        {
            if(spin_counter>0)
            {
                slot_counter_animator.enabled = true;
                slot_counter_animator.Play("Slot_counter_popup_begin");
                slot_counter_bool = false;
            } 
        }
        else if(spin_counter<1)
        {
            slot_counter_animator.Play("Slot_counter_popup_end");
        }
        Spin_counter_text.text = spin_counter.ToString();
    }

    //private void OnCollisionStay(Collision collision)
    //{
    //    if (collision.gameObject.CompareTag("Coin") || collision.gameObject.CompareTag("Green_coin"))
    //    {
    //        Destroy(collision.gameObject);
    //        Debug.Log("Roll the Slot Please!!!!!!!");
    //        //abc.StartReels();
    //        //Invoke("stop_reels_", 1f);
    //    }
    //} 




    void stop_reels_()
    {
        Invoke("wait_stop_reells",3f);
    }

    void wait_stop_reells()
    {
        abc.StopReels();

    }
    #endregion
    //private void OnTriggerEnter(Collider other)
    //{
    //	this.GetComponent<AudioSource>().volume = PlayerPrefs.GetInt("Sound");

    //	GetComponent<AudioSource>().volume = PlayerPrefs.GetInt("Sound");
    //	if (other.gameObject.tag == "1_$")
    //	{
    //		BlastParty.Play();
    //		this.GetComponent<AudioSource>().Play();
    //		//GameManager.Gamemanager.txtCoin.GetComponent<ScoreScript>().AddMoney(0.01f);
    //		Invoke("OnPlusSlot", 1f);
    //		Destroy(other.gameObject);
    //	}
    //	if (other.gameObject.tag == "2_$")
    //	{
    //		this.GetComponent<AudioSource>().Play();
    //		//GameManager.Gamemanager.txtCoin.GetComponent<ScoreScript>().AddMoney(0.02f);
    //		BlastParty.Play();
    //		Invoke("OnPlusSlot", 1f);
    //		Destroy(other.gameObject);
    //	}
    //	if (other.gameObject.tag == "1_Coin")
    //	{
    //		this.GetComponent<AudioSource>().Play();
    //		//GameManager.Gamemanager.txtCoin.GetComponent<ScoreScript>().AddCoin(1);
    //		BlastParty.Play();
    //		Invoke("OnPlusSlot", 1f);
    //		Destroy(other.gameObject);
    //	}
    //	if (other.gameObject.tag == "5_Coin")
    //	{
    //		this.GetComponent<AudioSource>().Play();
    //		//GameManager.Gamemanager.txtCoin.GetComponent<ScoreScript>().AddCoin(5);
    //		BlastParty.Play();
    //		Invoke("OnPlusSlot", 1f);
    //		Destroy(other.gameObject);
    //	}
    //	if (other.gameObject.tag == "25_Coin")
    //	{
    //		this.GetComponent<AudioSource>().Play();
    //		//GameManager.Gamemanager.txtCoin.GetComponent<ScoreScript>().AddCoin(25);
    //		BlastParty.Play();
    //		Invoke("OnPlusSlot", 1f);
    //		Destroy(other.gameObject);
    //	}
    //	if (other.gameObject.tag == "100_Coin")
    //	{
    //		this.GetComponent<AudioSource>().Play();
    //		//GameManager.Gamemanager.txtCoin.GetComponent<ScoreScript>().AddCoin(100);
    //		BlastParty.Play();
    //		Invoke("OnPlusSlot", 1f);
    //		Destroy(other.gameObject);
    //	}
    //	if (other.gameObject.tag == "500_Coin")
    //	{
    //		this.GetComponent<AudioSource>().Play();
    //		//GameManager.Gamemanager.txtCoin.GetComponent<ScoreScript>().AddCoin(500);
    //		BlastParty.Play();
    //		Invoke("OnPlusSlot", 1f);
    //		Destroy(other.gameObject);
    //	}
    //}

    //void OnPlusSlot()
    //{
    //	PlayerPrefs.SetInt("Slot", PlayerPrefs.GetInt("Slot") + 1);
    //}
}
