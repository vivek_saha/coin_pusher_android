﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using CoinPusher;
using DG.Tweening;

public class Daily_bonus_panel_script : MonoBehaviour
{
    public static Daily_bonus_panel_script Instance;
    public Coin_manager c_manager;
    public bool bonus_10k
    {
        get
        {
            if (PlayerPrefs.GetInt("bonus_10k", 0) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            if (value == false)
            {
                PlayerPrefs.SetInt("bonus_10k", 1);
            }
        }
    }
    public bool bonus_20k
    {
        get
        {
            if (PlayerPrefs.GetInt("bonus_20k", 0) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            if (value == false)
            {
                PlayerPrefs.SetInt("bonus_20k", 1);
            }
        }
    }
    public bool bonus_30k
    {
        get
        {
            if (PlayerPrefs.GetInt("bonus_30k", 0) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            if (value == false)
            {
                PlayerPrefs.SetInt("bonus_30k", 1);
            }
        }
    }
    public bool bonus_40k
    {
        get
        {
            if (PlayerPrefs.GetInt("bonus_40k", 0) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            if (value == false)
            {
                PlayerPrefs.SetInt("bonus_40k", 1);
            }
        }
    }
    public bool bonus_50k
    {
        get
        {
            if (PlayerPrefs.GetInt("bonus_50k", 0) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            if (value == false)
            {
                PlayerPrefs.SetInt("bonus_50k", 1);
            }
        }
    }
    public bool bonus_60k
    {
        get
        {
            if (PlayerPrefs.GetInt("bonus_60k", 0) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            if (value == false)
            {
                PlayerPrefs.SetInt("bonus_60k", 1);
            }
        }
    }
    public bool bonus_70k
    {
        get
        {
            if (PlayerPrefs.GetInt("bonus_70k", 0) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            if (value == false)
            {
                PlayerPrefs.SetInt("bonus_70k", 1);
            }
        }
    }

    public DateTime old_date_time
    {
        get
        {
            if (PlayerPrefs.GetString("old_date_time", string.Empty) == string.Empty)
            {

                return DateTime.MinValue;
            }
            else
            {
                return DateTime.Parse(PlayerPrefs.GetString("old_date_time"));
            }
        }
        set
        {
            PlayerPrefs.SetString("old_date_time", value.ToString());
        }
    }
    [HideInInspector]
    public TextMeshProUGUI[] title;
    [HideInInspector]
    public TextMeshProUGUI[] coins;
    [HideInInspector]
    public Button[] bonus_btns;
    [HideInInspector]
    public GameObject[] ticks;
    int adding_coins;
    private void Awake()
    {
        Instance = this;

    }





    public void Onstart()
    {
        transform.DOLocalMove(Vector3.zero, 0.5f).SetUpdate(true);
    }

    public void Onclose()
    {
        transform.DOLocalMove(new Vector3(-1237, 0, 0), 0.5f).SetUpdate(true).OnComplete(()=> { gameObject.SetActive(false); Gamemanager.Instance.start_time(); });
    }
    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(System.DateTime.UtcNow);
        //Debug.Log(System.DateTime.Now);
        but_changer();
    }

    public void Turnon_panel()
    {
        but_changer();
        gameObject.SetActive(true);
        Onstart();
    }
    public void but_changer()
    {
        bonus_btns[0].interactable = bonus_10k;
        bonus_btns[1].interactable = bonus_20k;
        bonus_btns[2].interactable = bonus_30k;
        bonus_btns[3].interactable = bonus_40k;
        bonus_btns[4].interactable = bonus_50k;
        bonus_btns[5].interactable = bonus_60k;
        bonus_btns[6].interactable = bonus_70k;
        text_chnager();
    }

    public void text_chnager()
    {
        for(int i=0;i<7;i++)
        {
            if(!bonus_btns[i].interactable)
            {
                ticks[i].SetActive(true);
                title[i].color = Color.white;
                coins[i].color = Color.white;
            }
            else
            {

            }
        }
        close_object();
    }
    public void on_click_but(int num)
    {
        if (num != 0)
        {
            if (bonus_btns[num - 1].interactable)
            {

            }
            else
            {

                if (time_checker())
                {
                    //claim bonus
                    Claim_bonus(num);
                }
                else
                {
                    //today's reward already claimed
                    Gamemanager.Instance.nem_popup_fun("Today's Reward Already Claimed");
                }
            }
        }
        else
        {
            //claim bonus
            Claim_bonus(num);
        }
    }



    public void Claim_bonus(int num)
    {
       
        switch (num)
        {
            case 0:
                bonus_10k = false;
                adding_coins = 10000;
                break;
            case 1:
                bonus_20k = false;
                adding_coins = 20000;
                break;
            case 2:
                bonus_30k = false;
                adding_coins = 30000;
                break;
            case 3:
                bonus_40k = false;
                adding_coins = 40000;
                break;
            case 4:
                bonus_50k = false;
                adding_coins = 50000;
                break;
            case 5:
                bonus_60k = false;
                adding_coins = 60000;
                break;
            case 6:
                bonus_70k = false;
                adding_coins = 70000;
                break;
        }
        but_changer();
        add_coins();
        old_date_time = DateTime.Now;
    }

    public void add_coins()
    {
        c_manager._Gold_coin_current_total += adding_coins;
        c_manager.update_ui();
        //Invoke("close_object", 0.5f);
    }

    public void close_object()
    {
        Gamemanager.Instance.start_time();
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }



    public bool time_checker()
    {
        if (old_date_time != DateTime.MinValue)
        {
            int a = HourStrikesBetween(old_date_time, DateTime.Now);
            if (a > 24)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true;
        }
    }

    public static int HourStrikesBetween(DateTime from, DateTime to)
    {
        if (from > to)
        {
            throw new ArgumentException("from must not be after to");
        }

        // Trim to hours
        DateTime fromTrimmed = new DateTime(from.Year, from.Month, from.Day, from.Hour, 0, 0);
        DateTime toTrimmed = new DateTime(to.Year, to.Month, to.Day, to.Hour, 0, 0);

        int hours = (int)(toTrimmed - fromTrimmed).TotalHours;

        return hours;
    }

}
