﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using CoinPusher;
using System;
using UnityEngine.UI;

public class Redeem_api_script : MonoBehaviour
{
    [HideInInspector]
    public GameObject coupon_type_card, casout_type_card;
    [HideInInspector]
    public Transform redeem_panel_content;
    public GameObject redeem_loading_panel;
    public Coin_manager cm;
   public  string[] card_type;
   public  string[] item_type;
   public  string[] coin_type;
   public  string[] discount;
   public  string[] id;
   public  int[] coin;
   public  string[] product_id;

    string rawJson;
    private JSONNode jsonResult;
    public GameObject[] cards_list;
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine("GetData");
    }

    public void Manual_startt()
    {
        redeem_loading_panel.SetActive(true);
        StartCoroutine("GetData");
    }


    // Update is called once per frame
    
    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get("http://134.209.103.120/CoinPusher/mapi/1/redeem");
        //req_call = false;
        Debug.Log(PlayerPrefs.GetString("Token"));
        www.SetRequestHeader("Authorization", PlayerPrefs.GetString("Token"));
        yield return www.Send();
        //Debug.Log(www);
        if (www.isNetworkError)
        {
            Debug.Log(www.error);


            Check_Internet_connection();
            //req_call = true;
        }
        else
        {
            if (Notice_panel_script.Instance != null)
            {
                close_Retry_panel();
            }// Show results as text
            //Debug.Log(www.downloadHandler.text);
            rawJson = www.downloadHandler.text;
            Set_Json_data();
            //Manual_Start();
            // Or retrieve results as binary data
            // byte[] results = www.downloadHandler.data;
        }
    }

    public void Set_Json_data()
    {
        jsonResult = JSON.Parse(rawJson);
        //app_config

        //card_type = jsonResult["data"]["user"]["name"].Value;
        int m = 0;
        foreach (JSONNode message in jsonResult["data"])
        {
            m++;
        }
        card_type = new string[m];
        item_type = new string[m];
        coin_type = new string[m];
        discount = new string[m];
        id = new string[m];
        product_id = new string[m];
        coin = new int[m];

        int counter = 0;
        foreach (JSONNode message in jsonResult["data"])
        {
            card_type[counter] = message["card_type"].Value;
            item_type[counter] = message["item_type"].Value;
            coin_type[counter] = message["coin_type"].Value;
            discount[counter] = message["discount"].Value;
            id[counter] = message["id"].Value;
            product_id[counter] = message["product_id"].Value;
            coin[counter] = message["coin"].AsInt;
            counter++;
        }
        manual_start();
    }

    public void manual_start()
    {
        cards_list = new GameObject[id.Length];
        for(int i=0; i < redeem_panel_content.childCount;i++)
        {
            Destroy(redeem_panel_content.GetChild(i).transform.gameObject);
        }
        for (int i = 0; i < id.Length; i++)
        {
            if (card_type[i] == "discount")
            {
                //Debug.Log("here");
                GameObject temp = Instantiate(coupon_type_card,Vector3.zero,Quaternion.identity);
                temp.transform.SetParent(redeem_panel_content);
                temp.transform.localScale = Vector3.one;
                temp.GetComponent<redeem_card_script>()._cardtype = cardtype.coupon;
                temp.GetComponent<redeem_card_script>().b_type = set_cardtype(item_type[i]);
                temp.GetComponent<redeem_card_script>().server_coins.text = coin[i].ToString();
                temp.GetComponent<redeem_card_script>().server_coin_total = coin[i];
                temp.GetComponent<redeem_card_script>().id = id[i];
                temp.GetComponent<redeem_card_script>().coin_type.sprite = temp.GetComponent<redeem_card_script>().coin_type_sprites[set_coin_type(coin_type[i],temp)];
                temp.GetComponent<redeem_card_script>().set_card_brand_type(temp.GetComponent<redeem_card_script>().b_type, discount[i]);
                temp.GetComponent<redeem_card_script>().add_listener();
                cards_list[i] = temp;
            }
            else
            {
                GameObject temp = Instantiate(casout_type_card, Vector3.zero, Quaternion.identity);
                temp.transform.SetParent(redeem_panel_content);
                temp.transform.localScale = Vector3.one;
                temp.GetComponent<redeem_card_script>()._cardtype = cardtype.cashout;
                temp.GetComponent<redeem_card_script>().b_type = set_cardtype(item_type[i]);
                temp.GetComponent<redeem_card_script>().server_coins.text = coin[i].ToString();
                temp.GetComponent<redeem_card_script>().server_coin_total = coin[i];
                temp.GetComponent<redeem_card_script>().id = id[i];
                temp.GetComponent<redeem_card_script>().coin_type.sprite = temp.GetComponent<redeem_card_script>().coin_type_sprites[set_coin_type(coin_type[i], temp)];
                temp.GetComponent<redeem_card_script>().set_card_brand_type(temp.GetComponent<redeem_card_script>().b_type, discount[i]);
                temp.GetComponent<redeem_card_script>().add_listener();
                cards_list[i] = temp;
            }


        }
        //cards_update_ui();
        redeem_loading_panel.SetActive(false);

    }

    public brand_type set_cardtype(string a)
    {
        switch (a)
        {
            case "amazon":
                return brand_type.Amazon;
                break;
            case "paypal":
                return brand_type.Paypal;
                break;
            case "walmart":
                return brand_type.Walmart;
                break;
            case "asos":
                return brand_type.Asos;
                break;
            case "lego":
                return brand_type.Lego;
                break;
            case "mlbshop":
                return brand_type.Mlbshop;
                break;
            case "nike":
                return brand_type.Nike;
                break;
            case "primenow":
                return brand_type.Primenow;
                break;
            default:
                return brand_type.Amazon;
                break;
           
        }
    }
    public int set_coin_type(string a, GameObject tem)
    {
        switch (a)
        {

            case "gold":
                tem.GetComponent<redeem_card_script>().ctype = Coin_type.gold;
                tem.GetComponent<redeem_card_script>().player_coins.text = cm._Gold_coin_current_total.ToString();

                return 0;
                break;
            case "green":
                tem.GetComponent<redeem_card_script>().ctype = Coin_type.green;
                tem.GetComponent<redeem_card_script>().player_coins.text = cm._Green_coin_current_total.ToString();
                return 1;
                break;
            case "black":
                tem.GetComponent<redeem_card_script>().ctype = Coin_type.black;
                tem.GetComponent<redeem_card_script>().player_coins.text = cm._Black_coin_current_total.ToString();
                return 2;
                break;
            default:
                return 0;
                break;
        }
    }

    public void cards_update_ui()
    {
        foreach(GameObject a in cards_list)
        {
            a.GetComponent<redeem_card_script>().Update_ui();
        }
    }
    public void Check_Internet_connection()
    {
        StartCoroutine(Ck_net(isConnected =>
        {
            if (isConnected)
            {
                Debug.Log("Internet Available!");
                //return true;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_panel.SetActive(true);
                    Notice_panel_script.Instance.Something_went_wrong_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(true);
                }
            }
            else
            {
                Debug.Log("Internet Not Available");
                //return  false;
                if (!Notice_panel_script.Instance.Loading_panel.activeSelf)
                {
                    Notice_panel_script.Instance.notice_panel.SetActive(true);
                    Notice_panel_script.Instance.no_internet_panel.GetComponentInChildren<Button>().onClick.AddListener(() => Retry_connection());
                    Notice_panel_script.Instance.no_internet_panel.SetActive(true);
                }
            }
        }));
    }

    public IEnumerator Ck_net(Action<bool> syncResult)
    {
        const string echoServer = "http://google.com";

        bool result;
        using (var request = UnityWebRequest.Head(echoServer))
        {
            request.timeout = 0;
            yield return request.SendWebRequest();
            result = !request.isNetworkError && !request.isHttpError && request.responseCode == 200;
        }
        syncResult(result);
    }

    public void Retry_connection()
    {
        Manual_startt();

        Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(false);
        Notice_panel_script.Instance.no_internet_panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(true);
        StartCoroutine("Retry_connection_coroutine");
    }
    IEnumerator Retry_connection_coroutine()
    {
        yield return new WaitForSeconds(3f);
        //StartCoroutine("check_connection");
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_panel.SetActive(false);
        Manual_startt();
    }
    public void close_Retry_panel()
    {
        Notice_panel_script.Instance.Something_went_wrong_panel.SetActive(false);
        Notice_panel_script.Instance.no_internet_panel.SetActive(false);
        Notice_panel_script.Instance.Loading_panel.SetActive(false);
        Notice_panel_script.Instance.notice_panel.SetActive(false);
    }
}
