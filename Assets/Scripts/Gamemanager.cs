﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Analytics;
using Firebase.Messaging;
using Firebase.Extensions;
using System.Threading.Tasks;
using System;

public class Gamemanager : MonoBehaviour
{
    public static Gamemanager Instance;
    public GameObject nem_popup;
    public GameObject newstyle_popup;
    public RectTransform origin;
    public Transform canvas_content;
    public Setting_panel_script set_script;
    public int App_ver_code;
    public GameObject monster_hit_spark;
    int ads_counter = 0;


    private string topic = "FruitPusherManiaTopic";
    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
        //set_script.set_toogles();
        crashanalytics_init();
    }

    #region Firebase
    public void crashanalytics_init()
    {
        // Initialize Firebase
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                // Crashlytics will use the DefaultInstance, as well;
                // this ensures that Crashlytics is initialized.
                Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;
                InitializeFirebaseMessaging();
                // Set a flag here for indicating that your project is ready to use Firebase.
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
    }


    void InitializeFirebaseMessaging()
    {
        //FirebaseMessaging.MessageReceived += OnMessageReceived;
        //FirebaseMessaging.TokenReceived += OnTokenReceived;
        FirebaseMessaging.SubscribeAsync(topic).ContinueWithOnMainThread(task => {
            LogTaskCompletion(task, "SubscribeAsync");
        });
        Debug.Log("Firebase Messaging Initialized");

        // This will display the prompt to request permission to receive
        // notifications if the prompt has not already been displayed before. (If
        // the user already responded to the prompt, thier decision is cached by
        // the OS and can be changed in the OS settings).
        FirebaseMessaging.RequestPermissionAsync().ContinueWithOnMainThread(
          task => {
              LogTaskCompletion(task, "RequestPermissionAsync");
          }
        );
        //isFirebaseInitialized = true;
    }

    private bool LogTaskCompletion(Task task, string operation)
    {
        bool complete = false;
        if (task.IsCanceled)
        {
            Debug.Log(operation + " canceled.");
        }
        else if (task.IsFaulted)
        {
            Debug.Log(operation + " encounted an error.");
            foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
            {
                string errorCode = "";
                Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
                if (firebaseEx != null)
                {
                    errorCode = String.Format("Error.{0}: ",
                      ((Firebase.Messaging.Error)firebaseEx.ErrorCode).ToString());
                }
                Debug.Log(errorCode + exception.ToString());
            }
        }
        else if (task.IsCompleted)
        {
            Debug.Log(operation + " completed");
            complete = true;
        }
        return complete;
    }

    #endregion
    // Update is called once per frame
    void Update()
    {
        
    }

    public void nem_popup_fun(string msg)
    {
        GameObject a = Instantiate(nem_popup);
        a.GetComponent<nem_popup_script>().msg.text = msg;
        a.transform.SetParent(canvas_content);
        a.transform.position = origin.transform.position;
    }

    public void stop_time()
    {

        if(ads_counter>= Ads_initialize_API.Instance.ads_click)
        {
            Ads_priority_script.Instance.Show_interrestial();
            ads_counter = 0;
        }
        else
        {
            ads_counter++;
        }
        Time.timeScale = 0;
    }


    public void stop_time_and_show_interstatial()
    {
        Ads_priority_script.Instance.Show_interrestial();
        Time.timeScale = 0;
    }
    public void start_time()
    {
        Time.timeScale = 1;
    }
}
