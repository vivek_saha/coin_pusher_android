﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudienceNetwork;
using AudienceNetwork.Utility;


public class FB_ADS_script : MonoBehaviour
{

    private InterstitialAd interstitialAd;
    public string android_facebook_interstitial_id;
    private RewardedVideoAd rewardedVideoAd;
    public string android_facebook_reward_id;
    private bool isLoaded_interstatial;
    private bool isLoaded_rewarded;
#pragma warning disable 0414
    private bool didClose;
#pragma warning restore 0414


    //private void Awake()
    //{
    //}


    public void Manual_Start()
    {
#if UNITY_ANDROID

        android_facebook_interstitial_id = Ads_initialize_API.Instance.android_facebook_reward_id;
        android_facebook_reward_id = Ads_initialize_API.Instance.android_facebook_interstitial_id;
#elif UNITY_IOS
        android_facebook_interstitial_id = Ads_initialize_API.Instance.ios_facebook_interstitial_id;
        android_facebook_reward_id = Ads_initialize_API.Instance.ios_facebook_reward_id;
#else
        android_facebook_interstitial_id = Ads_initialize_API.Instance.android_facebook_reward_id;
        android_facebook_reward_id = Ads_initialize_API.Instance.android_facebook_interstitial_id;
#endif
        AudienceNetworkAds.Initialize();
        SettingsScene.InitializeSettings();

        Invoke("wait_for_fb_initialize",1f);

    }


    public void wait_for_fb_initialize()
    {
        if (android_facebook_interstitial_id != null)
        {
            LoadInterstitial();
        }
        if (android_facebook_reward_id != null)
        {
            LoadRewardedVideo();
        }
        Ads_initialize_API.Instance.init_done = true;
    }
    public void LoadInterstitial()
    {
        //statusLabel.text = "Loading interstitial ad...";

        // Create the interstitial unit with a placement ID (generate your own on the Facebook app settings).
        // Use different ID for each ad placement in your app.
        interstitialAd = new InterstitialAd(android_facebook_interstitial_id);

        interstitialAd.Register(gameObject);

        // Set delegates to get notified on changes or when the user interacts with the ad.
        interstitialAd.InterstitialAdDidLoad = delegate ()
        {
            Debug.Log("Interstitial ad loaded.");
            isLoaded_interstatial = true;
            didClose = false;
            string isAdValid = interstitialAd.IsValid() ? "valid" : "invalid";
            //statusLabel.text = "Ad loaded and is " + isAdValid + ". Click show to present!";
        };
        interstitialAd.InterstitialAdDidFailWithError = delegate (string error)
        {
            transform.GetComponent<Admob>().Disable_admob_bg();
            Debug.Log("Interstitial ad failed to load with error: " + error);
            //statusLabel.text = "Interstitial ad failed to load. Check console for details.";
        };
        interstitialAd.InterstitialAdWillLogImpression = delegate ()
        {
            Debug.Log("Interstitial ad logged impression.");
        };
        interstitialAd.InterstitialAdDidClick = delegate ()
        {
            Debug.Log("Interstitial ad clicked.");
        };
        interstitialAd.InterstitialAdDidClose = delegate ()
        {
            transform.GetComponent<Admob>().REquest_ads();
            transform.GetComponent<Admob>().Disable_admob_bg();
            Debug.Log("Interstitial ad did close.");
            didClose = true;
            if (interstitialAd != null)
            {
                interstitialAd.Dispose();
            }
        };

#if UNITY_ANDROID
        /*
         * Only relevant to Android.
         * This callback will only be triggered if the Interstitial activity has
         * been destroyed without being properly closed. This can happen if an
         * app with launchMode:singleTask (such as a Unity game) goes to
         * background and is then relaunched by tapping the icon.
         */
        interstitialAd.interstitialAdActivityDestroyed = delegate ()
        {
            if (!didClose)
            {
                Debug.Log("Interstitial activity destroyed without being closed first.");
                Debug.Log("Game should resume.");
            }
        };
#endif

        // Initiate the request to load the ad.
        interstitialAd.LoadAd();
    }


    public bool Check_interstatial_fb_ads()
    {
        return isLoaded_interstatial;
    }
    // Show button
    public void ShowInterstitial()
    {
        if (isLoaded_interstatial)
        {
            interstitialAd.Show();
            isLoaded_interstatial = false;
            //statusLabel.text = "";
        }
        else
        {
            //statusLabel.text = "Ad not loaded. Click load to request an ad.";
        }
    }

    void OnDestroy()
    {
        // Dispose of interstitial ad when the scene is destroyed
        if (interstitialAd != null)
        {
            interstitialAd.Dispose();
        }
        Debug.Log("InterstitialAdTest was destroyed!");
        //Dispose of rewardedVideo ad when the scene is destroyed
        if (rewardedVideoAd != null)
        {
            rewardedVideoAd.Dispose();
        }
        Debug.Log("RewardedVideoAdTest was destroyed!");
    }


    public void LoadRewardedVideo()
    {
        //statusLabel.text = "Loading rewardedVideo ad...";

        // Create the rewarded video unit with a placement ID (generate your own on the Facebook app settings).
        // Use different ID for each ad placement in your app.
        rewardedVideoAd = new RewardedVideoAd(android_facebook_reward_id);

        // For S2S validation you can create the rewarded video ad with the reward data
        // Refer to documentation here:
        // https://developers.facebook.com/docs/audience-network/android/rewarded-video#server-side-reward-validation
        // https://developers.facebook.com/docs/audience-network/ios/rewarded-video#server-side-reward-validation
        //RewardData rewardData = new RewardData
        //{
        //    UserId = "USER_ID",
        //    Currency = "REWARD_ID"
        //};
#pragma warning disable 0219
        RewardedVideoAd s2sRewardedVideoAd = new RewardedVideoAd(android_facebook_reward_id);
#pragma warning restore 0219

        rewardedVideoAd.Register(gameObject);

        // Set delegates to get notified on changes or when the user interacts with the ad.
        rewardedVideoAd.RewardedVideoAdDidLoad = delegate ()
        {
            Debug.Log("RewardedVideo ad loaded.");
            isLoaded_rewarded = true;
            didClose = false;
            string isAdValid = rewardedVideoAd.IsValid() ? "valid" : "invalid";
            //statusLabel.text = "Ad loaded and is " + isAdValid + ". Click show to present!";
        };
        rewardedVideoAd.RewardedVideoAdDidFailWithError = delegate (string error)
        {
            transform.GetComponent<Admob>().Disable_admob_bg();
            Debug.Log("RewardedVideo ad failed to load with error: " + error);
            //statusLabel.text = "RewardedVideo ad failed to load. Check console for details.";
        };
        rewardedVideoAd.RewardedVideoAdWillLogImpression = delegate ()
        {
            Debug.Log("RewardedVideo ad logged impression.");
        };
        rewardedVideoAd.RewardedVideoAdDidClick = delegate ()
        {
            Debug.Log("RewardedVideo ad clicked.");
        };

        // For S2S validation you need to register the following two callback
        // Refer to documentation here:
        // https://developers.facebook.com/docs/audience-network/android/rewarded-video#server-side-reward-validation
        // https://developers.facebook.com/docs/audience-network/ios/rewarded-video#server-side-reward-validation
        rewardedVideoAd.RewardedVideoAdDidSucceed = delegate ()
        {
            Debug.Log("Rewarded video ad validated by server");
            ///reward callback
            ///
            //transform.GetComponent<AdsController>().Reward_money_Add();
        };

        rewardedVideoAd.RewardedVideoAdDidFail = delegate ()
        {
            Debug.Log("Rewarded video ad not validated, or no response from server");
        };

        rewardedVideoAd.RewardedVideoAdDidClose = delegate ()
        {
            transform.GetComponent<Admob>().Disable_admob_bg();
            transform.GetComponent<Admob>().Reward_money_Add();
            transform.GetComponent<Admob>().REquest_ads();
            Ads_priority_script.Instance.Add_reward();
            Debug.Log("Rewarded video ad did close.");
            didClose = true;
            if (rewardedVideoAd != null)
            {
                rewardedVideoAd.Dispose();
            }
        };

#if UNITY_ANDROID
        /*
         * Only relevant to Android.
         * This callback will only be triggered if the Rewarded Video activity
         * has been destroyed without being properly closed. This can happen if
         * an app with launchMode:singleTask (such as a Unity game) goes to
         * background and is then relaunched by tapping the icon.
         */
        rewardedVideoAd.RewardedVideoAdActivityDestroyed = delegate ()
        {
            if (!didClose)
            {
                Debug.Log("Rewarded video activity destroyed without being closed first.");
                Debug.Log("Game should resume. User should not get a reward.");
            }
        };
#endif

        // Initiate the request to load the ad.
        rewardedVideoAd.LoadAd();
    }

    public bool Check_rewarded_fb_ads()
    {
        return isLoaded_rewarded;
    }

    // Show button
    public void ShowRewardedVideo()
    {
        if (isLoaded_rewarded)
        {
            rewardedVideoAd.Show();
            isLoaded_rewarded = false;
            //statusLabel.text = "";
        }
        else
        {
            //statusLabel.text = "Ad not loaded. Click load to request an ad.";
        }
    }


    //void OnDestroy()
    //{
    //    // Dispose of rewardedVideo ad when the scene is destroyed
    //    if (rewardedVideoAd != null)
    //    {
    //        rewardedVideoAd.Dispose();
    //    }
    //    Debug.Log("RewardedVideoAdTest was destroyed!");
    //}

    // Start is called before the first frame update
    //void Start()
    //{

    //}

    //// Update is called once per frame
    //void Update()
    //{

    //}
}
