﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class PowerUp_manager : MonoBehaviour
{
    [HideInInspector]
    public Button shake_button, wall_button, powerup_button;
    [HideInInspector]
    public GameObject shake_counter, wall_counter, powerup_counter;
    [HideInInspector]
    public Slider powerup_slider;
    // 3 coin, double dmg, froze coin
    [HideInInspector]
    public Sprite[] powerups_sprites;
    public Power_up_script p_script;
    public popup_panel_script popup_panel_scrript, newstyle_popup;
    float _shake_counter_float, _wall_counter_float, _powerup_counter_float;
    bool shake_bool= false, wall_bool= false, powerup_bool= false;
    float shake_counter_float
    {
        get
        {
            return _shake_counter_float;
        }
        set
        {
            if (value > 0)
            {
                shake_counter.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = ((int)value).ToString();
                _shake_counter_float = value;
            }
            else
            {
                shake_bool = false;
            }
        }
    }
    float wall_countre_float
    {
        get
        {
            return _wall_counter_float;
        }
        set
        {
            if (value > 0)
            {
                wall_counter.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = ((int)value).ToString();
                _wall_counter_float = value;
            }
            else
            {
                wall_bool = false;
            }
        }
    }
    float powerup_counter_float
    {
        get
        {
            return _powerup_counter_float;
        }
        set
        {
            if (value > 0)
            {
                powerup_counter.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = ((int)value).ToString();
                _powerup_counter_float = value;
            }
            else
            {
                powerup_bool = false;
            }
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        powerup_slider_reset();
        powerup_slider_start();
    }

    public void On_click_powerup(int pos)
    {
        switch (pos)
        {
            case 0:
                newstyle_popup.current_reward_enum = popup_enum.shake;
                newstyle_popup.active_panel_type = popuppanel_type.normal;
                break;
            case 1:
                newstyle_popup.current_reward_enum = popup_enum.wall;
                newstyle_popup.active_panel_type = popuppanel_type.normal;
                break;
            case 2:
                newstyle_popup.current_reward_enum = return_enum();
                newstyle_popup.active_panel_type = popuppanel_type.normal;
                break;
            default:
                break;
        }
        newstyle_popup.set_panel();
        newstyle_popup.set_panel_type();
        newstyle_popup.gameObject.SetActive(true);
        newstyle_popup.gameObject.transform.DOLocalMove(Vector3.zero, 0.5f).SetUpdate(true);
    }
    public void start_counter(int pos)
    {
        switch (pos)
        {
            case 0:
                shake_counter_float = 30f;
                shake_button.interactable = false;
                shake_counter.SetActive(true);
                shake_bool = true;
                Invoke("shake_available", 30f);
                break;
            case 1:
                wall_countre_float = 120f;
                wall_button.interactable = false;
                wall_counter.SetActive(true);
                wall_bool = true;
                Invoke("wall_available", 120f);
                break;
            case 2:
                powerup_counter_float = 30f;
                powerup_button.interactable = false;
                powerup_slider.gameObject.SetActive(false);
                DOTween.Kill(powerup_slider);
                powerup_counter.SetActive(true);
                powerup_bool = true;
                //powerup_activate();
                Invoke("powerup_available", 30f);
                break;
            default:
                break;
        }
    }

    public void oncounter_end()
    {

    }


    public void shake_available()
    {
        shake_counter.SetActive(false);
        shake_button.interactable = true;
    }

    public void wall_available()
    {
        wall_counter.SetActive(false);
        wall_button.interactable = true;
    }
    public void powerup_available()
    {
        powerup_counter.SetActive(false);
        powerup_button.interactable = true;
        change_powerups();
        powerup_slider.gameObject.SetActive(true);
        powerup_slider_start();
    }

    public void powerup_slider_start()
    {
        powerup_slider.DOValue(0, 30f).OnComplete(()=>change_powerups());
    }
    public void powerup_slider_reset()
    {
        powerup_slider.value = 1;
    }

    public popup_enum return_enum()
    {
        switch (powerup_button.GetComponent<Image>().sprite.name)
        {
            case "0":
                return popup_enum.triple_coin;
                break;
            case "1":
                return popup_enum.double_dmg;
                break;
            case "2":
                return popup_enum.froze;
                break;
            default:
                return popup_enum.triple_coin;
                break;
        }
    }
    public void change_powerups()
    {
        powerup_slider_reset();
        switch(powerup_button.GetComponent<Image>().sprite.name)
        {
            case "0":
                powerup_button.GetComponent<Image>().sprite = powerups_sprites[1];
                break;
            case "1":
                powerup_button.GetComponent<Image>().sprite = powerups_sprites[2];
                break;
            case "2":
                powerup_button.GetComponent<Image>().sprite = powerups_sprites[0];
                break;
        }
        powerup_slider_start();
    }

    public void powerup_activate()
    {
        switch (powerup_button.GetComponent<Image>().sprite.name)
        {
            case "0":
                //triple coin
                p_script.Triple_coin_powerup();
                break;
            case "1":
                //dounble dmg coin
                p_script.Double_dmg_coin_powerup();
                break;
            case "2":
                //froze coin
                p_script.Froze_coin_powerup();
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (shake_bool)
        {
            shake_counter_float -= Time.deltaTime;
        }
        if (wall_bool)
        {
            wall_countre_float -= Time.deltaTime;
        }
        if (powerup_bool)
        {
            powerup_counter_float -= Time.deltaTime;
        }
    }
}
