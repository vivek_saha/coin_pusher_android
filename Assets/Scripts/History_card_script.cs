﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class History_card_script : MonoBehaviour
{
    public TextMeshProUGUI code, time, discount;
    public Button store, use;
    public Image brand;
    public Sprite[] brand_sprites;


    public brand_type br;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void set_brand(brand_type ac)
    {
        switch (ac)
        {
            case brand_type.Amazon:
                brand.sprite = brand_sprites[0];
                break;
            case brand_type.Paypal:
                brand.sprite = brand_sprites[1];
                break;
            case brand_type.Walmart:
                brand.sprite = brand_sprites[2];
                break;
            case brand_type.Asos:
                brand.sprite = brand_sprites[3];
                break;
            case brand_type.Lego:
                brand.sprite = brand_sprites[4];
                break;
            case brand_type.Mlbshop:
                brand.sprite = brand_sprites[5];
                break;
            case brand_type.Nike:
                brand.sprite = brand_sprites[6];
                break;
            case brand_type.Primenow:
                brand.sprite = brand_sprites[7];
                break;
            default:
                break;
        }
    }

    public void addlistner(string str,string use_str)
    {
        store.onClick.AddListener(() => store_clicked(str));
        use.onClick.AddListener(() => use_clicked(use_str));
    }

    public void store_clicked(string a)
    {
        Application.OpenURL(a);
    }

    public void use_clicked(string a)
    {
        Application.OpenURL(a);
    }
}
