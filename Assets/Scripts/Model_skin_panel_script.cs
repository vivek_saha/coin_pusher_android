﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using CoinPusher;

public enum Model_skin_type { normal, circus };
public class Model_skin_panel_script : MonoBehaviour
{
    public Setting_api_script set_api;
    public Coin_manager c_manager;
    public ReelsScript reel_script;
    public Model_skin_type skin_type
    {
        get
        {
            if (PlayerPrefs.GetInt("Model_skin_circus", 0) == 0)
            {
                return Model_skin_type.normal;
            }
            else
            {
                return Model_skin_type.circus;
            }
        }
        set
        {
            if (value == Model_skin_type.normal)
            {
                PlayerPrefs.SetInt("Model_skin_circus", 0);
            }
            else
            {
                PlayerPrefs.SetInt("Model_skin_circus", 1);
            }
        }
    }

    public bool model_skin_bought
    {
        get
        {
            if (PlayerPrefs.GetInt("circus", 0) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        set
        {
            if (value == true)
            {
                PlayerPrefs.SetInt("circus", 1);
            }
        }
    }
    [HideInInspector]
    public Material platform, down_plat, front_side;

    [HideInInspector]
    [Header("circus tex")]
    public Texture circus_platform_tex, circus_down_plat_tex, circus_front_side_tex;

    [HideInInspector]
    [Header("Normal tex")]
    public Texture normal_platform_tex, normal_down_plat_tex, normal_front_side_tex;




    public GameObject normal_use_but, normal_selected;

    public GameObject circus_use_but, circus_selected, circus_buy_but;
    public Image coin_type_image;
    public TextMeshProUGUI Coin_value;
    //public GameObject 
    // Start is called before the first frame update
    void Start()
    {
        Change_theme_tex();
    }

    // Update is called once per frame
    void Update()
    {

    }

    
    

    public void Model_skin_but_changer()
    {
        if (skin_type == Model_skin_type.normal)
        {
            normal_selected.SetActive(true);
            normal_use_but.SetActive(false);
            if (model_skin_bought)
            {
                circus_buy_but.SetActive(false);
                circus_use_but.SetActive(true);
                circus_selected.SetActive(false);
            }
            else
            {
                circus_buy_but.SetActive(true);
                circus_use_but.SetActive(false);
                circus_selected.SetActive(false);
            }
        }
        else if (skin_type == Model_skin_type.circus)
        {
            normal_selected.SetActive(false);
            normal_use_but.SetActive(true);
            circus_buy_but.SetActive(false);
            circus_use_but.SetActive(false);
            circus_selected.SetActive(true);
        }
    }

    public void Change_theme_tex()
    {
        Platform_Manager.Instance.GetComponent<Model_skin_chnager_script>().skin_type = skin_type;
        Platform_Manager.Instance.GetComponent<Model_skin_chnager_script>().chnage_thme_models();
        //Debug.Log("heerererere");
        if (skin_type == Model_skin_type.normal)
        {
            //Debug.Log("heerererere");
            platform.mainTexture = normal_platform_tex;
            down_plat.mainTexture = normal_down_plat_tex;
            front_side.mainTexture = normal_front_side_tex;
            reel_script.Reel[0] = reel_script.Normal_reel;
        }
        else if (skin_type == Model_skin_type.circus)
        {
            platform.mainTexture = circus_platform_tex;
            down_plat.mainTexture = circus_down_plat_tex;
            front_side.mainTexture = circus_front_side_tex;
            //Debug.Log("heerererere");
            reel_script.Reel[0] = reel_script.Circus_reel;
        }
        Model_skin_but_changer();
    }

    public void purchased_skin()
    {
        c_manager.update_ui();
        model_skin_bought = true;
        skin_type = Model_skin_type.circus;
        Change_theme_tex();
    }
    public void Use(int value)
    {
        switch (value)
        {
            case 1:
                skin_type = Model_skin_type.normal;
                break;
            case 2:
                skin_type = Model_skin_type.circus;
                break;
            default:
                break;
        }
        Change_theme_tex();
    }
    public void purchase_but()
    {
        switch (set_api.model_skin_coin_type)
        {
            case Coin_type.gold:
                if (c_manager._Gold_coin_current_total >= set_api.model_skin_coin_value)
                {
                    purchased_skin();
                    c_manager._Gold_coin_current_total -= set_api.model_skin_coin_value;
                }
                else
                {
                    nem_popup();
                }
                break;
            case Coin_type.green:
                if (c_manager._Green_coin_current_total >= set_api.model_skin_coin_value)
                {
                    purchased_skin();
                    c_manager._Green_coin_current_total -= set_api.model_skin_coin_value;
                }
                else
                {
                    nem_popup();
                }
                break;
            case Coin_type.black:
                if (c_manager._Black_coin_current_total >= set_api.model_skin_coin_value)
                {
                    purchased_skin();
                    c_manager._Black_coin_current_total -= set_api.model_skin_coin_value;
                }
                else
                {
                    nem_popup();
                }
                break;
            default:
                break;
        }

    }


    public void nem_popup()
    {
        Gamemanager.Instance.nem_popup_fun("Not Enough Coins");
    }




}
