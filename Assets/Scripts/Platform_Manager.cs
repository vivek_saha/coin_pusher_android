﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using UnityEngine.UI;
using CoinPusher;

public class Platform_Manager : MonoBehaviour
{
    public static Platform_Manager Instance;
    
    [HideInInspector]
    public Coin_manager _coinManager;
    [HideInInspector]
    public Shoot_coin coin_thrower;
    [HideInInspector]
    public GameObject coin, green_coin, Coin_slider, coin_platform, front_wall, monster_reward_spawner, coin_tower, Black_hole_effect, gold_tower_reward, green_coin_tower_reward,bg_cloud, bg, boost_particle;


    public Slider Boost_slider;
    public int parts;
    public float radius;

    bool red_777 = false;
    bool green_777 = false;
    public bool generate_gold_tower = false;
    private GameObject temp_reward_ob;

    public float x, y, angle;
    public double x1, y1;
    public int coin_dmg = 1;

    [HideInInspector]
    public GameObject[] Monster;
    [HideInInspector]
    public Transform[] Pos;
    [HideInInspector]
    public GameObject[] Reward_monster;
    float engle_size;
    GameObject s_temp;
    // initial coin_platform y == -0.03021712
    //down coin_platform y == -0.0926

    private void Awake()
    {
        Instance = this;
        Vibration.Init();


    }

    void Start()
    {
        Boost_slider.value = 0;
        //onStart_coin_tower();
        engle_size = 360 / parts;
    }



    public void onStart_coin_tower()
    {
       s_temp = Instantiate(coin_tower);
        Invoke("wait_for_start", 0.5f);
    }
    public void wait_for_start()
    {
        foreach (Transform a in s_temp.transform)
        {
            a.GetComponent<Rigidbody>().isKinematic = false;
        }
    }
    public void Move_slider()
    {

        // startvalue = -0.1199  &  end value = 0.018
        Coin_slider.transform.DOLocalMoveZ(0.018f, 2).OnComplete(() =>
        {
            Coin_slider.transform.DOLocalMoveZ(-0.1199f, 2).OnComplete(() =>
            {
                Move_slider();
            });
        });

    }


    #region Front_wall
    public void Front_wall_on()
    {
        front_wall.transform.DOLocalMoveY(0.0048f, 1f);
        StartCoroutine("off_front_wall");
    }

    IEnumerator off_front_wall()
    {
        yield return new WaitForSeconds(120f);
        front_wall.transform.DOLocalMoveY(-0.299f, 1f);
    }
    #endregion


    public void Spawan_reward(int loc)
    {

        //for(int i =0;i<loc;i++)
        // {

        // }
        GameObject ab = Instantiate(Reward_monster[loc], new Vector3(1, 3f, 6f), Quaternion.identity);

        foreach (Transform a in ab.transform)
        {
            a.transform.SetParent(null);
        }

    }


    void Update()
    {

    }

    #region Big_coin_Shake
    public void Big_coin_Shake()
    {
        transform.DOShakePosition(0.5f, 0.5f, 7);
        if (AudioManager.Instance.Vibrate_bool)
        {
            //AudioManager.Vibrate(500);
            //Vibration.Vibrate(500);
            Vibration.VibratePeek();
            //Handheld.Vibrate();
        }
    }
    #endregion



    #region Gold_tower_reward
    public void Coin_platfrom_down()
    {
        coin_platform.transform.DOLocalRotate(new Vector3(0, 90f, 0), 0.5f);
        coin_platform.transform.DOLocalMoveY(-0.0926f, 0.5f).OnComplete(() => start_blackHole());

    }
    public void start_blackHole()
    {
        Black_hole_effect.SetActive(true);
        //Invoke("spawn_cointower_goldtower", 2f);
    }

    public void spawn_cointower_goldtower()
    {
        Black_hole_effect.SetActive(false);
        generate_gold_tower = false;
        temp_reward_ob = Instantiate(gold_tower_reward, new Vector3(0, -5.012f, 0), Quaternion.identity);
        temp_reward_ob.transform.SetParent(coin_platform.transform);
        Coin_platfrom_UP();
    }

    public void Coin_platfrom_UP()
    {
        coin_platform.transform.DOLocalRotate(Vector3.zero, 1f);
        coin_platform.transform.DOLocalMoveY(-0.03021712f, 1f).OnComplete(() => Turn_on_coin_collider());


    }

    public void Turn_on_coin_collider()
    {
        temp_reward_ob.transform.parent = null;
        foreach (Transform a in temp_reward_ob.transform)
        {
            a.GetComponent<Rigidbody>().isKinematic = false;
        }
        generate_round_coin();
    }


    public void generate_round_coin()
    {
        //float engle_size = 360 / parts;
        angle = 0;
        InvokeRepeating("fun_spawn_rep", 0f, 0.1f);
        Invoke("fun_spawn_stop", parts / 10);

    }

    public void fun_spawn_rep()
    {
        x1 = x + radius * Math.Cos(angle * (Math.PI / 180));
        y1 = y + radius * Math.Sin(angle * (Math.PI / 180));
        if (green_777)
        {
            GameObject a = Instantiate(green_coin, new Vector3((float)x1, -8f, (float)y1), Quaternion.identity);

        }
        else
        {
            GameObject a = Instantiate(coin, new Vector3((float)x1, -8f, (float)y1), Quaternion.identity);
        }

        angle += engle_size;
    }

    public void fun_spawn_stop()
    {
        CancelInvoke("fun_spawn_rep");
        green_777 = false;
        ReelsScript.Instance.Reserved_turn_cheker();
    }
    #endregion


    #region 777_red
    public void large_push_777_red()
    {
        red_777 = true;
        Coin_slider.GetComponent<Coin_slider_script>().IsLargePush = true;
        //Invoke("Coin_platfrom_down_777_red", 4f);
    }
    public void Coin_platfrom_down_777_red()
    {
        coin_platform.transform.DOLocalRotate(new Vector3(0, 90f, 0), 0.5f);
        coin_platform.transform.DOLocalMoveY(-0.0926f, 0.5f).OnComplete(() => spawn_cointower_777red());
        red_777 = false;
    }
    public void spawn_cointower_777red()
    {
        //Black_hole_effect.SetActive(false);
        temp_reward_ob = Instantiate(gold_tower_reward, new Vector3(0, -5.012f, 0), Quaternion.identity);
        temp_reward_ob.transform.SetParent(coin_platform.transform);
        Coin_platfrom_UP();
    }
    #endregion

    public void large_push_done()
    {
        if (red_777)
        {
            Coin_platfrom_down_777_red();
        }
        else
        {
            Coin_platfrom_down_777_green();
        }

    }

    #region 777_green
    public void large_push_777_green()
    {
        //red_777 = true;
        Coin_slider.GetComponent<Coin_slider_script>().IsLargePush = true;
        //Invoke("Coin_platfrom_down_777_red", 4f);
    }
    public void Coin_platfrom_down_777_green()
    {
        coin_platform.transform.DOLocalRotate(new Vector3(0, 90f, 0), 0.5f);
        coin_platform.transform.DOLocalMoveY(-0.0926f, 0.5f).OnComplete(() => spawn_cointower_777green());

    }
    public void spawn_cointower_777green()
    {
        //Black_hole_effect.SetActive(false);
        temp_reward_ob = Instantiate(green_coin_tower_reward, new Vector3(0, -5.012f, 0), Quaternion.identity);
        temp_reward_ob.transform.SetParent(coin_platform.transform);
        green_777 = true;
        Coin_platfrom_UP();
    }
    #endregion

    public void Add_boost_mode_exp()
    {
        if (!coin_thrower.boost_mode)
        {
            //Boost_slider.DOValue(Boost_slider.value + 0.1f, 0.1f);
            Boost_slider.value += 0.1f;
            if (Boost_slider.value>=1)
            {
                //boost mode on
                boostmode_on();
            }
        }
    }

    public void boostmode_on()
    {
        Vibration.VibratePeek();
        coin_thrower.boost_mode = true;
        bg_cloud.SetActive(false);
        boost_particle.SetActive(true);
        bg.GetComponent<SpriteRenderer>().DOColor(new Color(1f, 0.2117647f, 0.3803922f, 1f),0.2f);
        Boost_slider.DOValue(0, 17f);
        Invoke("boostmode_off",15f);
    }

    public void boostmode_off()
    {
        coin_thrower.boost_mode = false;
        bg_cloud.SetActive(true);
        boost_particle.SetActive(false);
        bg.GetComponent<SpriteRenderer>().DOColor(new Color(1f, 1f, 1f, 1f), 0.2f);
    }

    public void Add_coin_to_ui(Coin_type ct, string value)
    {
        _coinManager.set_coin_to_ui(ct, value);
    }
}
