﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace CoinPusher
{
    public class Coin_manager : MonoBehaviour
    {
        Coin_Counter_script cc_script;
        public Setting_api_script API_script;

        [Header("temp")]
        public int gold_value, big_gold_value;
        public float green_coin_value, big_green_coin_value;
        public int black_coin_value;
        public int shake_coin_value;
        [HideInInspector]
        public GameObject Floating_text;
        [HideInInspector]
        public GameObject gameplay_panel;
        [HideInInspector]
        public RectTransform[] coin_canvas_loc;
        [HideInInspector]
        public TextMeshProUGUI gold_coin_text, green_coin_text, black_coin_text;
        //[HideInInspector]
        public Sprite[] coin_type_sprites;
        public static Sprite[] static_coin_type_sprites;
        //private int gold_coin_current_total;
        public int _Gold_coin_current_total
        {
            get
            {
                return PlayerPrefs.GetInt("Gold_coin_current_total", 0);
            }
            set
            {
                PlayerPrefs.SetInt("Gold_coin_current_total", value);
            }
        }
        public float _Green_coin_current_total
        {
            get
            {
                return PlayerPrefs.GetFloat("Green_coin_current_total", 0.0f);
            }
            set
            {
                float yourFloat = Mathf.Round(value * 100f) / 100f;
                //Debug.Log("yourFloat:::" + yourFloat);
                PlayerPrefs.SetFloat("Green_coin_current_total", yourFloat);
            }
        }
        public int _Black_coin_current_total
        {
            get
            {
                return PlayerPrefs.GetInt("Black_coin_current_total", 0);
            }
            set
            {
                PlayerPrefs.SetInt("Black_coin_current_total", value);
            }
        }


        private void Awake()
        {

        }

        // Start is called before the first frame update
        void Start()
        {
            static_coin_type_sprites = new Sprite[3];
            static_coin_type_sprites[0] = coin_type_sprites[0];
            static_coin_type_sprites[1] = coin_type_sprites[1];
            static_coin_type_sprites[2] = coin_type_sprites[2];
            //test_coins();
            update_ui();
            cc_script = GetComponent<Coin_Counter_script>();
        }

        void test_coins()
        {
            _Gold_coin_current_total = 100000;
            _Green_coin_current_total = 10000;
            _Black_coin_current_total = 1000;
        }


        public void manual_start()
        {
            gold_value = API_script.gold_coin;
            green_coin_value = API_script.green_coin;
            big_gold_value = API_script.big_gold_coin;
            big_green_coin_value = API_script.big_green_coin;
            black_coin_value = API_script.black_bar_coin;
            shake_coin_value = API_script.shake_coin;
        }

        // Update is called once per frame
        void Update()
        {

        }


        public void Get_counter_coin(Coin_type type, Coin__size_type s_type, Transform _transform)
        {
            //Floating_text.GetComponent<floating_ui_script>().set_values(type, )
            switch (s_type)
            {
                case Coin__size_type.small:
                    if (type == Coin_type.gold)
                    {
                        Floating_text.GetComponent<floating_ui_script>().set_values(type, gold_value.ToString());
                        Floating_text.GetComponent<floating_ui_script>().main_location_rect = coin_canvas_loc[0];
                    }
                    else
                    {
                        Floating_text.GetComponent<floating_ui_script>().set_values(type, green_coin_value.ToString());
                        Floating_text.GetComponent<floating_ui_script>().main_location_rect = coin_canvas_loc[1];
                    }
                    break;
                case Coin__size_type.big:
                    if (type == Coin_type.gold)
                    {
                        Floating_text.GetComponent<floating_ui_script>().set_values(type, big_gold_value.ToString());
                        Floating_text.GetComponent<floating_ui_script>().main_location_rect = coin_canvas_loc[0];
                    }
                    else if (type == Coin_type.green)
                    {
                        Floating_text.GetComponent<floating_ui_script>().set_values(type, big_green_coin_value.ToString());
                        Floating_text.GetComponent<floating_ui_script>().main_location_rect = coin_canvas_loc[1];
                    }
                    else if (type == Coin_type.black)
                    {
                        Floating_text.GetComponent<floating_ui_script>().set_values(type, black_coin_value.ToString());
                        Floating_text.GetComponent<floating_ui_script>().main_location_rect = coin_canvas_loc[2];
                    }
                    else
                    {
                        Floating_text.GetComponent<floating_ui_script>().set_values(Coin_type.gold, shake_coin_value.ToString());
                        Floating_text.GetComponent<floating_ui_script>().main_location_rect = coin_canvas_loc[0];
                    }
                    break;
            }
            Vector3 pos = Camera.main.WorldToScreenPoint(_transform.position);
            //Debug.Log("pos" + pos);
            pos.z = 0;
            if (pos.y > 200f)
            {

            }
            else
            {
                pos.y = 0;
            }
            GameObject temp = Instantiate(Floating_text, pos, Quaternion.identity);
            temp.transform.SetParent(gameplay_panel.transform);

        }

        public void set_coin_to_ui(Coin_type tp, string value)
        {
            switch (tp)
            {
                case Coin_type.gold:
                    _Gold_coin_current_total += int.Parse(value);
                    //gold_coin_text.text = _Gold_coin_current_total.ToString();
                    break;
                case Coin_type.green:


                    //float a = float.Parse( Math.Round(double.Parse(value),2));
                    //a = a / 100f;
                    //Debug.Log("val which u get::"+value);
                    _Green_coin_current_total += float.Parse(value);
                    //green_coin_text.text = _Green_coin_current_total.ToString();
                    break;
                case Coin_type.black:
                    _Black_coin_current_total += int.Parse(value);
                    //black_coin_text.text = _Black_coin_current_total.ToString();
                    break;

                default:
                    break;
            }
            update_ui();
        }

        public void update_ui()
        {
            gold_coin_text.text = _Gold_coin_current_total.ToString();
            green_coin_text.text = _Green_coin_current_total.ToString();
            black_coin_text.text = _Black_coin_current_total.ToString();
        }
        public static Coin_type REturn_coin_type(string a)
        {
            switch (a)
            {
                case "Gold":
                    return Coin_type.gold;
                    break;
                case "Green":
                    return Coin_type.green;
                    break;
                case "Black":
                    return Coin_type.black;
                    break;
                case "Shake":
                    return Coin_type.shake;
                    break;
                default:
                    return Coin_type.gold;
                    break;
            }
        }

        public static string REturn_coin_type(Coin_type a)
        {
            switch (a)
            {
                case Coin_type.gold:
                    return "Gold";
                    break;
                case Coin_type.green:
                    return "Green";
                    break;
                case Coin_type.black:
                    return "Black";
                    break;
                case Coin_type.shake:
                    return "Shake";
                    break;
                default:
                    return "";
                    break;
            }

        }
        public static Sprite Return_coin_type_image(Coin_type a)
        {
            switch (a)
            {
                case Coin_type.gold:
                    return static_coin_type_sprites[0];
                    break;
                case Coin_type.green:
                    return static_coin_type_sprites[1];
                    break;
                case Coin_type.black:
                    return static_coin_type_sprites[2];
                    break;
                default:
                    return null;
                    break;
            }
        }

        public static bool coin_checker_manager(Coin_type c, string value)
        {
            bool temp_bool = false;
            switch (c)
            {
                case Coin_type.gold:
                    if(int.Parse(value)<= PlayerPrefs.GetInt("Gold_coin_current_total", 0))
                    {
                        temp_bool = true;
                    }
                    break;
                case Coin_type.green:
                    if (float.Parse(value) <= PlayerPrefs.GetFloat("Green_coin_current_total", 0.0f))
                    {
                        temp_bool = true;
                    }
                    break;
                case Coin_type.black:
                    if (int.Parse(value) <= PlayerPrefs.GetInt("Black_coin_current_total", 0))
                    {
                        temp_bool = true;
                    }
                    break;
                default:
                    break;
            }
            return temp_bool;
        }
      
    }
}