﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using CoinPusher;

public enum cardtype { coupon, cashout};
public enum brand_type { Amazon, Paypal, Walmart , Asos , Lego , Mlbshop , Nike , Primenow }
public class redeem_card_script : MonoBehaviour
{

    public cardtype _cardtype;
    public brand_type b_type;
    public Image card_bg,coin_type;
    public Coin_type ctype;
    public TextMeshProUGUI top_discount, mid_discount, player_coins, slash_, server_coins;
    [HideInInspector]
    public Sprite[] coin_type_sprites;
    [HideInInspector]
    public Sprite[] brandtype_sprite;
    //[HideInInspector]
    public Color[] top_string_text_color;
    //[HideInInspector]
    public Color[] mid_string_text_color;
    public Slider slider_;
    public int server_coin_total;
    public string id;
    GameObject API;
    // Start is called before the first frame update
    void Start()
    {
        API = GameObject.FindGameObjectWithTag("API");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void set_card_brand_type(brand_type br, string disc)
    {
        switch (br)
        {
            case brand_type.Amazon:
                card_bg.sprite = brandtype_sprite[0];
                if (_cardtype == cardtype.coupon)
                {
                    top_discount.text = "Amazon Outlet Up to " + disc + "% off";
                    mid_discount.text = "Up to " + disc + "% off";
                    top_discount.color = top_string_text_color[0];
                    //player_coins.color = top_discount.color;
                    //slash_.color = top_discount.color;
                    //server_coins.color = top_discount.color;
                    //mid_discount.color = mid_string_text_color[0];
                }
                else
                {
                    top_discount.text = server_coin_total + " coins = $ " + disc;
                    mid_discount.text = "$ "+ disc;
                    top_discount.color = top_string_text_color[0];
                    //player_coins.color = top_discount.color;
                    //slash_.color = top_discount.color;
                    //server_coins.color = top_discount.color;
                    //mid_discount.color = mid_string_text_color[0];
                }
                break;
            case brand_type.Paypal:
                card_bg.sprite = brandtype_sprite[1];
                if (_cardtype== cardtype.cashout)
                {
                    top_discount.text = server_coin_total + " coins = $ " + disc;
                    mid_discount.text = "$ "+disc;
                    top_discount.color = top_string_text_color[0];
                    //player_coins.color = top_discount.color;
                    //slash_.color = top_discount.color;
                    //server_coins.color = top_discount.color;
                    //mid_discount.color = mid_string_text_color[0];
                }
                else
                {
                    top_discount.text = server_coin_total + " coins = $ " + disc;
                    mid_discount.text = "$ " + disc;
                    top_discount.color = top_string_text_color[0];
                    //player_coins.color = top_discount.color;
                    //slash_.color = top_discount.color;
                    //server_coins.color = top_discount.color;
                    //mid_discount.color = mid_string_text_color[0];
                }
                break;
            case brand_type.Walmart:
                card_bg.sprite = brandtype_sprite[2];
                if (_cardtype == cardtype.coupon)
                {
                    top_discount.text = "Clearance Under " + disc+ "%";
                    mid_discount.text = "Clearance Under " + disc + "%";
                    top_discount.color = top_string_text_color[2];
                    player_coins.color = top_discount.color;
                    slash_.color = top_discount.color;
                    server_coins.color = top_discount.color;
                    mid_discount.color = mid_string_text_color[2];
                }
                else
                {
                    top_discount.text = server_coin_total + " coins = $ " + disc;
                    mid_discount.text = "$ " + disc;
                    top_discount.color = top_string_text_color[0];
                    player_coins.color = top_discount.color;
                    slash_.color = top_discount.color;
                    server_coins.color = top_discount.color;
                    mid_discount.color = mid_string_text_color[0];
                }
                break;
            case brand_type.Asos:
                card_bg.sprite = brandtype_sprite[3];
                if (_cardtype == cardtype.coupon)
                {
                    top_discount.text = "ASOS Womans's Clothing Up to " + disc + "% off";
                    mid_discount.text = "Up to " + disc + "% off";
                    top_discount.color = top_string_text_color[3];
                    player_coins.color = top_discount.color;
                    slash_.color = top_discount.color;
                    server_coins.color = top_discount.color;
                    mid_discount.color = mid_string_text_color[3];
                }
                else
                {
                    top_discount.text = server_coin_total + " coins = $ " + disc;
                    mid_discount.text = "$ " + disc;
                    top_discount.color = top_string_text_color[0];
                    player_coins.color = top_discount.color;
                    slash_.color = top_discount.color;
                    server_coins.color = top_discount.color;
                    mid_discount.color = mid_string_text_color[0];
                }
                break;
            case brand_type.Lego:
                card_bg.sprite = brandtype_sprite[4];
                if (_cardtype == cardtype.coupon)
                {
                    top_discount.text = "Lego Up to " + disc + "% off";
                    mid_discount.text = "Up to " + disc + "% off";
                    top_discount.color = top_string_text_color[4];
                    player_coins.color = top_discount.color;
                    slash_.color = top_discount.color;
                    server_coins.color = top_discount.color;
                    mid_discount.color = mid_string_text_color[4];
                }
                else
                {
                    top_discount.text = server_coin_total + " coins = $ " + disc;
                    mid_discount.text = "$ " + disc;
                    top_discount.color = top_string_text_color[0];
                    player_coins.color = top_discount.color;
                    slash_.color = top_discount.color;
                    server_coins.color = top_discount.color;
                    mid_discount.color = mid_string_text_color[0];
                }
                break;
            case brand_type.Mlbshop:
                card_bg.sprite = brandtype_sprite[5];
                if (_cardtype == cardtype.coupon)
                {
                    top_discount.text = " Up to " + disc + "% off";
                    mid_discount.text = "Up to " + disc + "% off";
                    top_discount.color = top_string_text_color[5];
                    player_coins.color = top_discount.color;
                    slash_.color = top_discount.color;
                    server_coins.color = top_discount.color;
                    mid_discount.color = mid_string_text_color[5];
                }
                else
                {
                    top_discount.text = server_coin_total + " coins = $ " + disc;
                    mid_discount.text = "$ " + disc;
                    top_discount.color = top_string_text_color[0];
                    player_coins.color = top_discount.color;
                    slash_.color = top_discount.color;
                    server_coins.color = top_discount.color;
                    mid_discount.color = mid_string_text_color[0];
                }
                break;
            case brand_type.Nike:
                card_bg.sprite = brandtype_sprite[6];
                if (_cardtype == cardtype.coupon)
                {
                    top_discount.text = "Amazon Outlet Up to " + disc + "% off";
                    mid_discount.text = "Up to " + disc + "% off";
                    top_discount.color = top_string_text_color[6];
                    player_coins.color = top_discount.color;
                    slash_.color = top_discount.color;
                    server_coins.color = top_discount.color;
                    mid_discount.color = mid_string_text_color[6];
                }
                else
                {
                    top_discount.text = server_coin_total + " coins = $ " + disc;
                    mid_discount.text = "$ " + disc;
                    top_discount.color = top_string_text_color[0];
                    player_coins.color = top_discount.color;
                    slash_.color = top_discount.color;
                    server_coins.color = top_discount.color;
                    mid_discount.color = mid_string_text_color[0];
                }
                break;
            case brand_type.Primenow:
                card_bg.sprite = brandtype_sprite[7];
                if (_cardtype == cardtype.coupon)
                {
                    top_discount.text = "Amazon Outlet Up to " + disc + "% off";
                    mid_discount.text = "Up to " + disc + "% off";
                    top_discount.color = top_string_text_color[7];
                    player_coins.color = top_discount.color;
                    slash_.color = top_discount.color;
                    server_coins.color = top_discount.color;
                    mid_discount.color = mid_string_text_color[7];
                }
                else
                {
                    top_discount.text = server_coin_total + " coins = $ " + disc;
                    mid_discount.text = "$ " + disc;
                    top_discount.color = top_string_text_color[0];
                    player_coins.color = top_discount.color;
                    slash_.color = top_discount.color;
                    server_coins.color = top_discount.color;
                    mid_discount.color = mid_string_text_color[0];
                }
                break;
            default:
                break;
        }
        Update_ui();
    }

    public void add_listener()
    {
        GetComponent<Button>().onClick.AddListener(() =>withdrawcall());
    }


    public void withdrawcall()
    {

        if(Coin_manager.coin_checker_manager(ctype,server_coins.text))
        {
            true_withdrawcall();
        }
        else
        {
            //not enough money
            Gamemanager.Instance.nem_popup_fun("Not Enough Coins"); ;
        }
    }
    public void true_withdrawcall()
    {
        API.GetComponent<Redeem_api_script>().redeem_loading_panel.SetActive(true);
        API.GetComponent<Withdraw_api_script>().ct = _cardtype;
        API.GetComponent<Withdraw_api_script>().redeem_id = id;
        switch (ctype)
        {
            case Coin_type.gold:
                API.GetComponent<Withdraw_api_script>().coin_type = "gold";
                break;
            case Coin_type.green:
                API.GetComponent<Withdraw_api_script>().coin_type = "green";
                break;
            case Coin_type.black:
                API.GetComponent<Withdraw_api_script>().coin_type = "black";
                break;
           
            default:
                break;
        }
        API.GetComponent<Withdraw_api_script>().coins = server_coin_total.ToString();



        //API.GetComponent<Withdraw_api_script>().coin_type
        if (_cardtype == cardtype.cashout)
        {
            API.GetComponent<Withdraw_api_script>().withdraw_panel.SetActive(true);
        }
        else
        {
            API.GetComponent<Withdraw_api_script>().start_upload();
        }
    }
    public void Update_ui()
    {
        API = GameObject.FindGameObjectWithTag("API");
        switch (ctype)
        {
            case Coin_type.gold:
                player_coins.text = API.GetComponent<Redeem_api_script>().cm._Gold_coin_current_total.ToString();
                if (API.GetComponent<Redeem_api_script>().cm._Gold_coin_current_total <= server_coin_total)
                {
                    slider_.value = API.GetComponent<Redeem_api_script>().cm._Gold_coin_current_total / server_coin_total;
                }
                else
                {
                    slider_.value = 1;
                }
                break;
            case Coin_type.green:
                player_coins.text = API.GetComponent<Redeem_api_script>().cm._Green_coin_current_total.ToString();
                if (API.GetComponent<Redeem_api_script>().cm._Green_coin_current_total <= server_coin_total)
                {
                    slider_.value = API.GetComponent<Redeem_api_script>().cm._Green_coin_current_total / server_coin_total;
                }
                else
                {
                    slider_.value = 1;
                }
                break;
            case Coin_type.black:
                player_coins.text = API.GetComponent<Redeem_api_script>().cm._Black_coin_current_total.ToString();
                if (API.GetComponent<Redeem_api_script>().cm._Black_coin_current_total <= server_coin_total)
                {
                    slider_.value = API.GetComponent<Redeem_api_script>().cm._Black_coin_current_total / server_coin_total;
                }
                else
                {
                    slider_.value = 1;
                }
                break;

            default:
                break;
        }
    }
}
