﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Shoot_coin))]
public class Shoot_coin_script_editor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        Shoot_coin abc = (Shoot_coin)target;
        if (GUILayout.Button("+10 coin"))
        {
            abc.Add_coin();
        }
    }
}