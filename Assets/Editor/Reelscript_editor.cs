﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ReelsScript))]
public class Reelscript_editor: Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ReelsScript abc = (ReelsScript)target;
       
        GUI.backgroundColor = new Color(0.3f, 1f, 0, 1);
        if (GUILayout.Button("gold_tower_reward"))
        {
            abc.gold_tower_reward();
        }
        //GUI.backgroundColor = Color.red;
        //if (GUILayout.Button("StopReels"))
        //{
            
        //    //abc.StopReels();
        //}
        GUI.backgroundColor = Color.black;
        if (GUILayout.Button("Shake_coin"))
        {

            abc.Shake_coin();
        }
        GUI.backgroundColor = Color.cyan;
        if (GUILayout.Button("auto_coin"))
        {

            abc.auto_coin();
        }
    }
}