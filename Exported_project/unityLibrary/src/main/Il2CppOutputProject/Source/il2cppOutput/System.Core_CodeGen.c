﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E (void);
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2 (void);
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 (void);
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 (void);
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000008 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000009 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000A System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000B TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000C TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000D TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000E TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000000F System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000010 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000011 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000012 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000013 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000014 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000015 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000016 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000017 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000018 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000019 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000001A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001B System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000001C System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000001D System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000001E System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001F System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000020 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000021 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000022 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000023 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000024 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000025 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000026 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000027 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000028 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000029 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000002A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002B System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000002C System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000002D System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000002E System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x0000002F System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000030 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000031 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000032 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000033 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000034 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000035 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x00000036 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000037 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000038 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000039 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000003A System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000003B System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x0000003C System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x0000003D System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000003E System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x0000003F System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000040 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000041 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000042 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000043 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000044 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000045 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000046 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000047 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000048 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000049 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000004A System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x0000004B System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000004C System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000004D System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000004E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000004F System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000050 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000051 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000052 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000053 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000054 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000055 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000056 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000057 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000058 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000059 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000005A System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000005B System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x0000005C System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x0000005D System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000005E System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000005F System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000060 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000061 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x00000062 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x00000063 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000064 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000065 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000066 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000067 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000068 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000069 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[105] = 
{
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[105] = 
{
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[29] = 
{
	{ 0x02000004, { 50, 4 } },
	{ 0x02000005, { 54, 9 } },
	{ 0x02000006, { 63, 7 } },
	{ 0x02000007, { 70, 10 } },
	{ 0x02000008, { 80, 1 } },
	{ 0x0200000A, { 81, 3 } },
	{ 0x0200000B, { 86, 5 } },
	{ 0x0200000C, { 91, 7 } },
	{ 0x0200000D, { 98, 3 } },
	{ 0x0200000E, { 101, 7 } },
	{ 0x0200000F, { 108, 4 } },
	{ 0x02000010, { 112, 34 } },
	{ 0x02000012, { 146, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 5 } },
	{ 0x06000007, { 15, 2 } },
	{ 0x06000008, { 17, 1 } },
	{ 0x06000009, { 18, 3 } },
	{ 0x0600000A, { 21, 2 } },
	{ 0x0600000B, { 23, 4 } },
	{ 0x0600000C, { 27, 4 } },
	{ 0x0600000D, { 31, 3 } },
	{ 0x0600000E, { 34, 3 } },
	{ 0x0600000F, { 37, 1 } },
	{ 0x06000010, { 38, 3 } },
	{ 0x06000011, { 41, 2 } },
	{ 0x06000012, { 43, 2 } },
	{ 0x06000013, { 45, 5 } },
	{ 0x06000031, { 84, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[148] = 
{
	{ (Il2CppRGCTXDataType)2, 11077 },
	{ (Il2CppRGCTXDataType)3, 10620 },
	{ (Il2CppRGCTXDataType)2, 11078 },
	{ (Il2CppRGCTXDataType)2, 11079 },
	{ (Il2CppRGCTXDataType)3, 10621 },
	{ (Il2CppRGCTXDataType)2, 11080 },
	{ (Il2CppRGCTXDataType)2, 11081 },
	{ (Il2CppRGCTXDataType)3, 10622 },
	{ (Il2CppRGCTXDataType)2, 11082 },
	{ (Il2CppRGCTXDataType)3, 10623 },
	{ (Il2CppRGCTXDataType)2, 11083 },
	{ (Il2CppRGCTXDataType)3, 10624 },
	{ (Il2CppRGCTXDataType)3, 10625 },
	{ (Il2CppRGCTXDataType)2, 5335 },
	{ (Il2CppRGCTXDataType)3, 10626 },
	{ (Il2CppRGCTXDataType)2, 11084 },
	{ (Il2CppRGCTXDataType)3, 10627 },
	{ (Il2CppRGCTXDataType)3, 10628 },
	{ (Il2CppRGCTXDataType)2, 11085 },
	{ (Il2CppRGCTXDataType)3, 10629 },
	{ (Il2CppRGCTXDataType)3, 10630 },
	{ (Il2CppRGCTXDataType)2, 5351 },
	{ (Il2CppRGCTXDataType)3, 10631 },
	{ (Il2CppRGCTXDataType)2, 11086 },
	{ (Il2CppRGCTXDataType)2, 11087 },
	{ (Il2CppRGCTXDataType)2, 5352 },
	{ (Il2CppRGCTXDataType)2, 11088 },
	{ (Il2CppRGCTXDataType)2, 11089 },
	{ (Il2CppRGCTXDataType)2, 11090 },
	{ (Il2CppRGCTXDataType)2, 5354 },
	{ (Il2CppRGCTXDataType)2, 11091 },
	{ (Il2CppRGCTXDataType)2, 5356 },
	{ (Il2CppRGCTXDataType)2, 11092 },
	{ (Il2CppRGCTXDataType)3, 10632 },
	{ (Il2CppRGCTXDataType)2, 11093 },
	{ (Il2CppRGCTXDataType)2, 5359 },
	{ (Il2CppRGCTXDataType)2, 11094 },
	{ (Il2CppRGCTXDataType)2, 5361 },
	{ (Il2CppRGCTXDataType)2, 5363 },
	{ (Il2CppRGCTXDataType)2, 11095 },
	{ (Il2CppRGCTXDataType)3, 10633 },
	{ (Il2CppRGCTXDataType)2, 11096 },
	{ (Il2CppRGCTXDataType)2, 5366 },
	{ (Il2CppRGCTXDataType)2, 11097 },
	{ (Il2CppRGCTXDataType)3, 10634 },
	{ (Il2CppRGCTXDataType)3, 10635 },
	{ (Il2CppRGCTXDataType)2, 11098 },
	{ (Il2CppRGCTXDataType)2, 5370 },
	{ (Il2CppRGCTXDataType)2, 11099 },
	{ (Il2CppRGCTXDataType)2, 5372 },
	{ (Il2CppRGCTXDataType)3, 10636 },
	{ (Il2CppRGCTXDataType)3, 10637 },
	{ (Il2CppRGCTXDataType)2, 5375 },
	{ (Il2CppRGCTXDataType)3, 10638 },
	{ (Il2CppRGCTXDataType)3, 10639 },
	{ (Il2CppRGCTXDataType)2, 5384 },
	{ (Il2CppRGCTXDataType)2, 11100 },
	{ (Il2CppRGCTXDataType)3, 10640 },
	{ (Il2CppRGCTXDataType)3, 10641 },
	{ (Il2CppRGCTXDataType)2, 5386 },
	{ (Il2CppRGCTXDataType)2, 10979 },
	{ (Il2CppRGCTXDataType)3, 10642 },
	{ (Il2CppRGCTXDataType)3, 10643 },
	{ (Il2CppRGCTXDataType)3, 10644 },
	{ (Il2CppRGCTXDataType)2, 5393 },
	{ (Il2CppRGCTXDataType)2, 11101 },
	{ (Il2CppRGCTXDataType)3, 10645 },
	{ (Il2CppRGCTXDataType)3, 10646 },
	{ (Il2CppRGCTXDataType)3, 10252 },
	{ (Il2CppRGCTXDataType)3, 10647 },
	{ (Il2CppRGCTXDataType)3, 10648 },
	{ (Il2CppRGCTXDataType)2, 5402 },
	{ (Il2CppRGCTXDataType)2, 11102 },
	{ (Il2CppRGCTXDataType)3, 10649 },
	{ (Il2CppRGCTXDataType)3, 10650 },
	{ (Il2CppRGCTXDataType)3, 10651 },
	{ (Il2CppRGCTXDataType)3, 10652 },
	{ (Il2CppRGCTXDataType)3, 10653 },
	{ (Il2CppRGCTXDataType)3, 10258 },
	{ (Il2CppRGCTXDataType)3, 10654 },
	{ (Il2CppRGCTXDataType)3, 10655 },
	{ (Il2CppRGCTXDataType)2, 11103 },
	{ (Il2CppRGCTXDataType)3, 10656 },
	{ (Il2CppRGCTXDataType)3, 10657 },
	{ (Il2CppRGCTXDataType)2, 11104 },
	{ (Il2CppRGCTXDataType)3, 10658 },
	{ (Il2CppRGCTXDataType)2, 11105 },
	{ (Il2CppRGCTXDataType)3, 10659 },
	{ (Il2CppRGCTXDataType)3, 10660 },
	{ (Il2CppRGCTXDataType)3, 10661 },
	{ (Il2CppRGCTXDataType)2, 5434 },
	{ (Il2CppRGCTXDataType)3, 10662 },
	{ (Il2CppRGCTXDataType)2, 5442 },
	{ (Il2CppRGCTXDataType)3, 10663 },
	{ (Il2CppRGCTXDataType)2, 11106 },
	{ (Il2CppRGCTXDataType)2, 11107 },
	{ (Il2CppRGCTXDataType)3, 10664 },
	{ (Il2CppRGCTXDataType)3, 10665 },
	{ (Il2CppRGCTXDataType)3, 10666 },
	{ (Il2CppRGCTXDataType)3, 10667 },
	{ (Il2CppRGCTXDataType)3, 10668 },
	{ (Il2CppRGCTXDataType)3, 10669 },
	{ (Il2CppRGCTXDataType)2, 5458 },
	{ (Il2CppRGCTXDataType)2, 11108 },
	{ (Il2CppRGCTXDataType)3, 10670 },
	{ (Il2CppRGCTXDataType)3, 10671 },
	{ (Il2CppRGCTXDataType)2, 5462 },
	{ (Il2CppRGCTXDataType)3, 10672 },
	{ (Il2CppRGCTXDataType)2, 11109 },
	{ (Il2CppRGCTXDataType)2, 5472 },
	{ (Il2CppRGCTXDataType)2, 5470 },
	{ (Il2CppRGCTXDataType)2, 11110 },
	{ (Il2CppRGCTXDataType)3, 10673 },
	{ (Il2CppRGCTXDataType)2, 11111 },
	{ (Il2CppRGCTXDataType)3, 10674 },
	{ (Il2CppRGCTXDataType)3, 10675 },
	{ (Il2CppRGCTXDataType)2, 5479 },
	{ (Il2CppRGCTXDataType)3, 10676 },
	{ (Il2CppRGCTXDataType)2, 5479 },
	{ (Il2CppRGCTXDataType)3, 10677 },
	{ (Il2CppRGCTXDataType)2, 5496 },
	{ (Il2CppRGCTXDataType)3, 10678 },
	{ (Il2CppRGCTXDataType)3, 10679 },
	{ (Il2CppRGCTXDataType)3, 10680 },
	{ (Il2CppRGCTXDataType)2, 11112 },
	{ (Il2CppRGCTXDataType)3, 10681 },
	{ (Il2CppRGCTXDataType)3, 10682 },
	{ (Il2CppRGCTXDataType)3, 10683 },
	{ (Il2CppRGCTXDataType)2, 5476 },
	{ (Il2CppRGCTXDataType)3, 10684 },
	{ (Il2CppRGCTXDataType)3, 10685 },
	{ (Il2CppRGCTXDataType)2, 5481 },
	{ (Il2CppRGCTXDataType)3, 10686 },
	{ (Il2CppRGCTXDataType)1, 11113 },
	{ (Il2CppRGCTXDataType)2, 5480 },
	{ (Il2CppRGCTXDataType)3, 10687 },
	{ (Il2CppRGCTXDataType)1, 5480 },
	{ (Il2CppRGCTXDataType)1, 5476 },
	{ (Il2CppRGCTXDataType)2, 11112 },
	{ (Il2CppRGCTXDataType)2, 5480 },
	{ (Il2CppRGCTXDataType)2, 5478 },
	{ (Il2CppRGCTXDataType)2, 5482 },
	{ (Il2CppRGCTXDataType)3, 10688 },
	{ (Il2CppRGCTXDataType)3, 10689 },
	{ (Il2CppRGCTXDataType)3, 10690 },
	{ (Il2CppRGCTXDataType)2, 5477 },
	{ (Il2CppRGCTXDataType)3, 10691 },
	{ (Il2CppRGCTXDataType)2, 5492 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	105,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	29,
	s_rgctxIndices,
	148,
	s_rgctxValues,
	NULL,
};
