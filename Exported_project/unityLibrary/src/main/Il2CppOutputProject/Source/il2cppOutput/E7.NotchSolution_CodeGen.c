﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void E7.NotchSolution.AdaptationBase::ResetAdaptationToCurve(UnityEngine.AnimationCurve)
extern void AdaptationBase_ResetAdaptationToCurve_m0DB3E2F55F05CC0985FB73ADB5FB5CF824E52E1E (void);
// 0x00000002 E7.NotchSolution.BlendedClipsAdaptor E7.NotchSolution.AdaptationBase::get_SelectedAdaptation()
extern void AdaptationBase_get_SelectedAdaptation_mAB19172E9F514C64643490A948AB0AAD717BE2DB (void);
// 0x00000003 UnityEngine.Animator E7.NotchSolution.AdaptationBase::get_AnimatorComponent()
extern void AdaptationBase_get_AnimatorComponent_m664E3D528CD811A9ACF493C6A96230815528E337 (void);
// 0x00000004 System.Void E7.NotchSolution.AdaptationBase::Adapt(System.Single)
extern void AdaptationBase_Adapt_m1D2C2F404AB717AC81D03326CF3EA15C04C1D8FE (void);
// 0x00000005 System.Void E7.NotchSolution.AdaptationBase::Start()
extern void AdaptationBase_Start_mEEE70073F983A0D6AB3B1C342D9E833019FF0F2F (void);
// 0x00000006 System.Void E7.NotchSolution.AdaptationBase::Update()
extern void AdaptationBase_Update_m7882EEB7AD4BB2944D16D3796AAEDD3230468004 (void);
// 0x00000007 System.Void E7.NotchSolution.AdaptationBase::Adapt()
// 0x00000008 System.Void E7.NotchSolution.AdaptationBase::E7.NotchSolution.INotchSimulatorTarget.SimulatorUpdate(UnityEngine.Rect,UnityEngine.Rect[])
extern void AdaptationBase_E7_NotchSolution_INotchSimulatorTarget_SimulatorUpdate_m89BE82FC61EACE35C4EA45DE1021463A75EB3F72 (void);
// 0x00000009 UnityEngine.Rect E7.NotchSolution.AdaptationBase::get_SafeAreaRelative()
extern void AdaptationBase_get_SafeAreaRelative_m1C2D114B34B46E26679DD3775B2BE73F002B2416 (void);
// 0x0000000A System.Void E7.NotchSolution.AdaptationBase::.ctor()
extern void AdaptationBase__ctor_m50D7CDA55A7F6682D8F780D72F758DA3CF966DF3 (void);
// 0x0000000B System.Void E7.NotchSolution.BlendedClipsAdaptor::.ctor(UnityEngine.AnimationCurve)
extern void BlendedClipsAdaptor__ctor_mFFDA7697D389E0F084755D691CB40360B8BD2E29 (void);
// 0x0000000C System.Boolean E7.NotchSolution.BlendedClipsAdaptor::get_Adaptable()
extern void BlendedClipsAdaptor_get_Adaptable_m9D2737CCB6C65D8A6FCBAA0731E9BF0FD5C820CE (void);
// 0x0000000D System.Void E7.NotchSolution.BlendedClipsAdaptor::Adapt(System.Single,UnityEngine.Animator)
extern void BlendedClipsAdaptor_Adapt_m47AA5EC9A764C6D05110D19E834B6ECB8AC7B29B (void);
// 0x0000000E System.Void E7.NotchSolution.PerEdgeAdaptations::.ctor()
extern void PerEdgeAdaptations__ctor_m83B42A2A6E7967A9CA028EDFDC8563BBD9414237 (void);
// 0x0000000F System.Void E7.NotchSolution.INotchSimulatorTarget::SimulatorUpdate(UnityEngine.Rect,UnityEngine.Rect[])
// 0x00000010 System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::UpdateRect()
// 0x00000011 UnityEngine.Rect E7.NotchSolution.NotchSolutionUIBehaviourBase::GetCanvasRect()
extern void NotchSolutionUIBehaviourBase_GetCanvasRect_m6664B2634FBD335D5350B41D855AA16F8A8A8890 (void);
// 0x00000012 System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::E7.NotchSolution.INotchSimulatorTarget.SimulatorUpdate(UnityEngine.Rect,UnityEngine.Rect[])
extern void NotchSolutionUIBehaviourBase_E7_NotchSolution_INotchSimulatorTarget_SimulatorUpdate_m89E4FE246C1D714706656787C4D67C59D8BF885F (void);
// 0x00000013 UnityEngine.Rect E7.NotchSolution.NotchSolutionUIBehaviourBase::get_SafeAreaRelative()
extern void NotchSolutionUIBehaviourBase_get_SafeAreaRelative_m4229E4426CF2727B0EC2F0E47258A1C6EEB106BD (void);
// 0x00000014 UnityEngine.RectTransform E7.NotchSolution.NotchSolutionUIBehaviourBase::get_rectTransform()
extern void NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9 (void);
// 0x00000015 System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::OnEnable()
extern void NotchSolutionUIBehaviourBase_OnEnable_mB58B0CC87D20E90186F9F4B79AB71C17DAC93D47 (void);
// 0x00000016 System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::OnDisable()
extern void NotchSolutionUIBehaviourBase_OnDisable_mF0E4327BC6B4EE827BE209E33DF2FF4DC5E06179 (void);
// 0x00000017 System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::OnRectTransformDimensionsChange()
extern void NotchSolutionUIBehaviourBase_OnRectTransformDimensionsChange_mA66CDFC9489CD86A713A890EFE360C5DEFA16884 (void);
// 0x00000018 System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::UnityEngine.UI.ILayoutController.SetLayoutHorizontal()
extern void NotchSolutionUIBehaviourBase_UnityEngine_UI_ILayoutController_SetLayoutHorizontal_m23965011DBEB718052B622CBA3FE6E7A044CEAE3 (void);
// 0x00000019 System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::UnityEngine.UI.ILayoutController.SetLayoutVertical()
extern void NotchSolutionUIBehaviourBase_UnityEngine_UI_ILayoutController_SetLayoutVertical_mF2B7807F3CB7D7914444F330681089BEC528D811 (void);
// 0x0000001A System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::UpdateRectBase()
extern void NotchSolutionUIBehaviourBase_UpdateRectBase_m1D4EAA1203356D6F91B0BCFC3947B950ECB849F9 (void);
// 0x0000001B System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::DelayedUpdate()
extern void NotchSolutionUIBehaviourBase_DelayedUpdate_m25321FD361325D51D930F734D165480207AA5957 (void);
// 0x0000001C System.Collections.IEnumerator E7.NotchSolution.NotchSolutionUIBehaviourBase::DelayedUpdateRoutine()
extern void NotchSolutionUIBehaviourBase_DelayedUpdateRoutine_mBE1625FF6D9BFE992EDD6961BB56E10DA2DB8832 (void);
// 0x0000001D System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::.ctor()
extern void NotchSolutionUIBehaviourBase__ctor_m06D19A834AF9C336013AA1322C9F9D3233D281A0 (void);
// 0x0000001E UnityEngine.Canvas E7.NotchSolution.NotchSolutionUIBehaviourBase::<GetCanvasRect>g__GetTopLevelCanvasU7C1_0()
extern void NotchSolutionUIBehaviourBase_U3CGetCanvasRectU3Eg__GetTopLevelCanvasU7C1_0_m626582600C4FD53CDEFE76B89E9759B9F4125C5D (void);
// 0x0000001F UnityEngine.ScreenOrientation E7.NotchSolution.NotchSolutionUtility::GetCurrentOrientation()
extern void NotchSolutionUtility_GetCurrentOrientation_m847578DED8F9B964755A0C64C9FADD3FDB27E3A7 (void);
// 0x00000020 System.Boolean E7.NotchSolution.NotchSolutionUtility::get_ShouldUseNotchSimulatorValue()
extern void NotchSolutionUtility_get_ShouldUseNotchSimulatorValue_m3C6D486B8587587EB12650930FF6ED47DAE7BA41 (void);
// 0x00000021 UnityEngine.Rect E7.NotchSolution.NotchSolutionUtility::get_ScreenSafeAreaRelative()
extern void NotchSolutionUtility_get_ScreenSafeAreaRelative_mA401C9F104714B1D8753556BDB9EC91DF63F76FC (void);
// 0x00000022 UnityEngine.Rect E7.NotchSolution.NotchSolutionUtility::ToScreenRelativeRect(UnityEngine.Rect)
extern void NotchSolutionUtility_ToScreenRelativeRect_m722FC3D28A744CB322BF96B19642D63B62C4166B (void);
// 0x00000023 UnityEngine.Rect[] E7.NotchSolution.NotchSolutionUtility::get_ScreenCutoutsRelative()
extern void NotchSolutionUtility_get_ScreenCutoutsRelative_m2D78B9F1EF59F96E077A72CCA9228212860D8DD9 (void);
// 0x00000024 System.Void E7.NotchSolution.NotchSolutionUtility::.cctor()
extern void NotchSolutionUtility__cctor_m7D412E25BE0C6B08A3E6E15D61D1F23A7649E6F3 (void);
// 0x00000025 System.Void E7.NotchSolution.PerEdgeEvaluationModes::.ctor()
extern void PerEdgeEvaluationModes__ctor_mC37F1C45BF291CFF2264E000D1A7565C970E3BFE (void);
// 0x00000026 System.Void E7.NotchSolution.PerEdgeValues`1::.ctor()
// 0x00000027 System.String E7.NotchSolution.SimulationDevice::ToString()
extern void SimulationDevice_ToString_mDC227D690B419A8F5C38FA8EEBD3F81D9AFB5533 (void);
// 0x00000028 System.Void E7.NotchSolution.SimulationDevice::.ctor()
extern void SimulationDevice__ctor_mC018E91064453EB925F000BCEE8926721D03CEF8 (void);
// 0x00000029 System.Void E7.NotchSolution.MetaData::.ctor()
extern void MetaData__ctor_mEF4BED7876B925356A2FC06A1381B81ABDBBC98C (void);
// 0x0000002A System.Void E7.NotchSolution.ScreenData::OnBeforeSerialize()
extern void ScreenData_OnBeforeSerialize_m7DACA6554B7A3F05E2BF3AAA612A82A21515EEE1 (void);
// 0x0000002B System.Void E7.NotchSolution.ScreenData::OnAfterDeserialize()
extern void ScreenData_OnAfterDeserialize_m481FD2919E1839DE8EA60CDB940D9F4A4C18B147 (void);
// 0x0000002C System.Void E7.NotchSolution.ScreenData::.ctor()
extern void ScreenData__ctor_m0663BE57EA610BA9471656C5297D155381AD7B65 (void);
// 0x0000002D System.Void E7.NotchSolution.OrientationDependentData::.ctor()
extern void OrientationDependentData__ctor_mE847242A14FC93440C0974CE0A80B1B563FD3F64 (void);
// 0x0000002E System.Void E7.NotchSolution.SystemInfoData::.ctor()
extern void SystemInfoData__ctor_m4BACF222E07793CA8BE503006701E10D48CE14C1 (void);
// 0x0000002F System.Void E7.NotchSolution.GraphicsDependentSystemInfoData::.ctor()
extern void GraphicsDependentSystemInfoData__ctor_m030B8A6FA78B5C8191076DA6BB9A6B4674C322B0 (void);
// 0x00000030 System.Void E7.NotchSolution.AspectRatioAdaptation::Adapt()
extern void AspectRatioAdaptation_Adapt_m22C1B967F342E974D9D50C15158F5C1CC415BEF0 (void);
// 0x00000031 System.Void E7.NotchSolution.AspectRatioAdaptation::Reset()
extern void AspectRatioAdaptation_Reset_m0B7D50FDCE4990D220F50D8E39839DF97E3167F3 (void);
// 0x00000032 System.Single E7.NotchSolution.AspectRatioAdaptation::get_AspectRatio()
extern void AspectRatioAdaptation_get_AspectRatio_m9568A2021F105A6C7B1D97DEA791C3290EDACD42 (void);
// 0x00000033 System.Void E7.NotchSolution.AspectRatioAdaptation::.ctor()
extern void AspectRatioAdaptation__ctor_mCEC39EF7263F13BF39BA057B169C0558546A7597 (void);
// 0x00000034 UnityEngine.AnimationCurve E7.NotchSolution.AspectRatioAdaptation::<Reset>g__GenDefaultCurveU7C1_0()
extern void AspectRatioAdaptation_U3CResetU3Eg__GenDefaultCurveU7C1_0_m4F275F34394578EBBBB199873FB2C391C3D3049C (void);
// 0x00000035 System.Void E7.NotchSolution.SafeAdaptation::Reset()
extern void SafeAdaptation_Reset_mBCDC4C51B720AC181883EDA727DC4BC52F30F32C (void);
// 0x00000036 System.Void E7.NotchSolution.SafeAdaptation::Adapt()
extern void SafeAdaptation_Adapt_mE5E9DEC6DB5514DDC4301EB5586EE421DC33ACDC (void);
// 0x00000037 System.Void E7.NotchSolution.SafeAdaptation::AdaptWithRelativeSafeArea(UnityEngine.Rect)
extern void SafeAdaptation_AdaptWithRelativeSafeArea_m8B4F6C7CECE7AB36DA4CA688C18A4AEFC76AC589 (void);
// 0x00000038 System.Void E7.NotchSolution.SafeAdaptation::.ctor()
extern void SafeAdaptation__ctor_mF0AA64F26F44ABD9A73AB22227EAC75CA2D84CBC (void);
// 0x00000039 System.Void E7.NotchSolution.SafePadding::UpdateRect()
extern void SafePadding_UpdateRect_m3211D09A95539B88B05450DD91319F21FD386D4C (void);
// 0x0000003A System.Void E7.NotchSolution.SafePadding::.ctor()
extern void SafePadding__ctor_m947CFAF0720337507641FAD6C3CE316E8F0F5988 (void);
// 0x0000003B System.Boolean E7.NotchSolution.SafePadding::<UpdateRect>g__LockSideU7C5_0(E7.NotchSolution.EdgeEvaluationMode)
extern void SafePadding_U3CUpdateRectU3Eg__LockSideU7C5_0_m869D49FE6FDB56D9F3675596090D0B643A82C974 (void);
// 0x0000003C System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::.ctor(System.Int32)
extern void U3CDelayedUpdateRoutineU3Ed__19__ctor_m559AAD87FE3298DA82D9DE2FB988188E7F0B4DE3 (void);
// 0x0000003D System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::System.IDisposable.Dispose()
extern void U3CDelayedUpdateRoutineU3Ed__19_System_IDisposable_Dispose_mD9FA276AA597969F911691CC52399A885EDEB678 (void);
// 0x0000003E System.Boolean E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::MoveNext()
extern void U3CDelayedUpdateRoutineU3Ed__19_MoveNext_m2279F242B9EB88A2465513F5093B4F0F69C6D323 (void);
// 0x0000003F System.Object E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedUpdateRoutineU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A01AE35B661FFFD21F0D152EB3958CCB9AA63D0 (void);
// 0x00000040 System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::System.Collections.IEnumerator.Reset()
extern void U3CDelayedUpdateRoutineU3Ed__19_System_Collections_IEnumerator_Reset_m45027201E08628547E570425DC36DE791FA5B6FB (void);
// 0x00000041 System.Object E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedUpdateRoutineU3Ed__19_System_Collections_IEnumerator_get_Current_m79AE97F34D63F7EFC36316AEC4326AE18A0DB4DF (void);
static Il2CppMethodPointer s_methodPointers[65] = 
{
	AdaptationBase_ResetAdaptationToCurve_m0DB3E2F55F05CC0985FB73ADB5FB5CF824E52E1E,
	AdaptationBase_get_SelectedAdaptation_mAB19172E9F514C64643490A948AB0AAD717BE2DB,
	AdaptationBase_get_AnimatorComponent_m664E3D528CD811A9ACF493C6A96230815528E337,
	AdaptationBase_Adapt_m1D2C2F404AB717AC81D03326CF3EA15C04C1D8FE,
	AdaptationBase_Start_mEEE70073F983A0D6AB3B1C342D9E833019FF0F2F,
	AdaptationBase_Update_m7882EEB7AD4BB2944D16D3796AAEDD3230468004,
	NULL,
	AdaptationBase_E7_NotchSolution_INotchSimulatorTarget_SimulatorUpdate_m89BE82FC61EACE35C4EA45DE1021463A75EB3F72,
	AdaptationBase_get_SafeAreaRelative_m1C2D114B34B46E26679DD3775B2BE73F002B2416,
	AdaptationBase__ctor_m50D7CDA55A7F6682D8F780D72F758DA3CF966DF3,
	BlendedClipsAdaptor__ctor_mFFDA7697D389E0F084755D691CB40360B8BD2E29,
	BlendedClipsAdaptor_get_Adaptable_m9D2737CCB6C65D8A6FCBAA0731E9BF0FD5C820CE,
	BlendedClipsAdaptor_Adapt_m47AA5EC9A764C6D05110D19E834B6ECB8AC7B29B,
	PerEdgeAdaptations__ctor_m83B42A2A6E7967A9CA028EDFDC8563BBD9414237,
	NULL,
	NULL,
	NotchSolutionUIBehaviourBase_GetCanvasRect_m6664B2634FBD335D5350B41D855AA16F8A8A8890,
	NotchSolutionUIBehaviourBase_E7_NotchSolution_INotchSimulatorTarget_SimulatorUpdate_m89E4FE246C1D714706656787C4D67C59D8BF885F,
	NotchSolutionUIBehaviourBase_get_SafeAreaRelative_m4229E4426CF2727B0EC2F0E47258A1C6EEB106BD,
	NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9,
	NotchSolutionUIBehaviourBase_OnEnable_mB58B0CC87D20E90186F9F4B79AB71C17DAC93D47,
	NotchSolutionUIBehaviourBase_OnDisable_mF0E4327BC6B4EE827BE209E33DF2FF4DC5E06179,
	NotchSolutionUIBehaviourBase_OnRectTransformDimensionsChange_mA66CDFC9489CD86A713A890EFE360C5DEFA16884,
	NotchSolutionUIBehaviourBase_UnityEngine_UI_ILayoutController_SetLayoutHorizontal_m23965011DBEB718052B622CBA3FE6E7A044CEAE3,
	NotchSolutionUIBehaviourBase_UnityEngine_UI_ILayoutController_SetLayoutVertical_mF2B7807F3CB7D7914444F330681089BEC528D811,
	NotchSolutionUIBehaviourBase_UpdateRectBase_m1D4EAA1203356D6F91B0BCFC3947B950ECB849F9,
	NotchSolutionUIBehaviourBase_DelayedUpdate_m25321FD361325D51D930F734D165480207AA5957,
	NotchSolutionUIBehaviourBase_DelayedUpdateRoutine_mBE1625FF6D9BFE992EDD6961BB56E10DA2DB8832,
	NotchSolutionUIBehaviourBase__ctor_m06D19A834AF9C336013AA1322C9F9D3233D281A0,
	NotchSolutionUIBehaviourBase_U3CGetCanvasRectU3Eg__GetTopLevelCanvasU7C1_0_m626582600C4FD53CDEFE76B89E9759B9F4125C5D,
	NotchSolutionUtility_GetCurrentOrientation_m847578DED8F9B964755A0C64C9FADD3FDB27E3A7,
	NotchSolutionUtility_get_ShouldUseNotchSimulatorValue_m3C6D486B8587587EB12650930FF6ED47DAE7BA41,
	NotchSolutionUtility_get_ScreenSafeAreaRelative_mA401C9F104714B1D8753556BDB9EC91DF63F76FC,
	NotchSolutionUtility_ToScreenRelativeRect_m722FC3D28A744CB322BF96B19642D63B62C4166B,
	NotchSolutionUtility_get_ScreenCutoutsRelative_m2D78B9F1EF59F96E077A72CCA9228212860D8DD9,
	NotchSolutionUtility__cctor_m7D412E25BE0C6B08A3E6E15D61D1F23A7649E6F3,
	PerEdgeEvaluationModes__ctor_mC37F1C45BF291CFF2264E000D1A7565C970E3BFE,
	NULL,
	SimulationDevice_ToString_mDC227D690B419A8F5C38FA8EEBD3F81D9AFB5533,
	SimulationDevice__ctor_mC018E91064453EB925F000BCEE8926721D03CEF8,
	MetaData__ctor_mEF4BED7876B925356A2FC06A1381B81ABDBBC98C,
	ScreenData_OnBeforeSerialize_m7DACA6554B7A3F05E2BF3AAA612A82A21515EEE1,
	ScreenData_OnAfterDeserialize_m481FD2919E1839DE8EA60CDB940D9F4A4C18B147,
	ScreenData__ctor_m0663BE57EA610BA9471656C5297D155381AD7B65,
	OrientationDependentData__ctor_mE847242A14FC93440C0974CE0A80B1B563FD3F64,
	SystemInfoData__ctor_m4BACF222E07793CA8BE503006701E10D48CE14C1,
	GraphicsDependentSystemInfoData__ctor_m030B8A6FA78B5C8191076DA6BB9A6B4674C322B0,
	AspectRatioAdaptation_Adapt_m22C1B967F342E974D9D50C15158F5C1CC415BEF0,
	AspectRatioAdaptation_Reset_m0B7D50FDCE4990D220F50D8E39839DF97E3167F3,
	AspectRatioAdaptation_get_AspectRatio_m9568A2021F105A6C7B1D97DEA791C3290EDACD42,
	AspectRatioAdaptation__ctor_mCEC39EF7263F13BF39BA057B169C0558546A7597,
	AspectRatioAdaptation_U3CResetU3Eg__GenDefaultCurveU7C1_0_m4F275F34394578EBBBB199873FB2C391C3D3049C,
	SafeAdaptation_Reset_mBCDC4C51B720AC181883EDA727DC4BC52F30F32C,
	SafeAdaptation_Adapt_mE5E9DEC6DB5514DDC4301EB5586EE421DC33ACDC,
	SafeAdaptation_AdaptWithRelativeSafeArea_m8B4F6C7CECE7AB36DA4CA688C18A4AEFC76AC589,
	SafeAdaptation__ctor_mF0AA64F26F44ABD9A73AB22227EAC75CA2D84CBC,
	SafePadding_UpdateRect_m3211D09A95539B88B05450DD91319F21FD386D4C,
	SafePadding__ctor_m947CFAF0720337507641FAD6C3CE316E8F0F5988,
	SafePadding_U3CUpdateRectU3Eg__LockSideU7C5_0_m869D49FE6FDB56D9F3675596090D0B643A82C974,
	U3CDelayedUpdateRoutineU3Ed__19__ctor_m559AAD87FE3298DA82D9DE2FB988188E7F0B4DE3,
	U3CDelayedUpdateRoutineU3Ed__19_System_IDisposable_Dispose_mD9FA276AA597969F911691CC52399A885EDEB678,
	U3CDelayedUpdateRoutineU3Ed__19_MoveNext_m2279F242B9EB88A2465513F5093B4F0F69C6D323,
	U3CDelayedUpdateRoutineU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A01AE35B661FFFD21F0D152EB3958CCB9AA63D0,
	U3CDelayedUpdateRoutineU3Ed__19_System_Collections_IEnumerator_Reset_m45027201E08628547E570425DC36DE791FA5B6FB,
	U3CDelayedUpdateRoutineU3Ed__19_System_Collections_IEnumerator_get_Current_m79AE97F34D63F7EFC36316AEC4326AE18A0DB4DF,
};
static const int32_t s_InvokerIndices[65] = 
{
	26,
	14,
	14,
	278,
	23,
	23,
	23,
	1921,
	1013,
	23,
	26,
	102,
	1709,
	23,
	1921,
	23,
	1013,
	1921,
	1013,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	14,
	314,
	49,
	1036,
	1042,
	4,
	3,
	23,
	-1,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	663,
	23,
	4,
	23,
	23,
	1014,
	23,
	23,
	23,
	46,
	32,
	23,
	102,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_E7_NotchSolutionCodeGenModule;
const Il2CppCodeGenModule g_E7_NotchSolutionCodeGenModule = 
{
	"E7.NotchSolution.dll",
	65,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
