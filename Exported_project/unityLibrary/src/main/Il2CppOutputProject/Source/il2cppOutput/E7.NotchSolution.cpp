﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// E7.NotchSolution.AdaptationBase
struct AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF;
// E7.NotchSolution.AspectRatioAdaptation
struct AspectRatioAdaptation_t48754AD46C3A23666F1FCD9FE921A682B598CE4D;
// E7.NotchSolution.BlendedClipsAdaptor
struct BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7;
// E7.NotchSolution.GraphicsDependentSystemInfoData
struct GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// E7.NotchSolution.MetaData
struct MetaData_t22BDEAB65B452CADFA38BB83AFD80ED804BE2828;
// E7.NotchSolution.NotchSolutionUIBehaviourBase
struct NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874;
// E7.NotchSolution.OrientationDependentData
struct OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D;
// E7.NotchSolution.PerEdgeAdaptations
struct PerEdgeAdaptations_t45C28CCF296B800024E9013701B891370CC4CF2D;
// E7.NotchSolution.PerEdgeEvaluationModes
struct PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E;
// E7.NotchSolution.SafeAdaptation
struct SafeAdaptation_t0DEF9DC095FCB9608021EA9A07ADA433CBBB6634;
// E7.NotchSolution.SafePadding
struct SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// E7.NotchSolution.ScreenData
struct ScreenData_tC4231E8FE32650020BC1AD704E046BE73CFCF56E;
// E7.NotchSolution.SimulationDevice
struct SimulationDevice_t4EC038BE01BB81A3C6AE018037486B82CE44A1F2;
// E7.NotchSolution.SystemInfoData
struct SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958;
// E7.NotchSolution.NotchSolutionUIBehaviourBase/<DelayedUpdateRoutine>d__19
struct U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;
// System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Object>
struct Dictionary_2_t7E8D40B461AB586AEA5DD75D8354C4913EEB1337;
// System.Collections.Generic.Dictionary`2<UnityEngine.ScreenOrientation,E7.NotchSolution.OrientationDependentData>
struct Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE;
// System.Collections.Generic.IEnumerable`1<System.Int32Enum>
struct IEnumerable_1_t28FB40D8E33C5846AB04F37C78130A4948569C7C;
// System.Collections.Generic.IEnumerable`1<E7.NotchSolution.OrientationDependentData>
struct IEnumerable_1_t324573AC8EBBFE4C487C1C32E9B10253586A587E;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.ScreenOrientation>
struct IEnumerable_1_tA622DCF17040D0A376ACF76DC8307B0F7E9714F5;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.ScreenOrientation>
struct IEqualityComparer_1_tAFDE3884F341ED7A973A7C9C554141667B83E5D7;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32Enum,System.Object>
struct KeyCollection_t9EE4F9D0A4F83EC4D31ABD4E20E68B4DD148ED6D;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.ScreenOrientation,E7.NotchSolution.OrientationDependentData>
struct KeyCollection_t42ABC2A1D8DF067994FAE1C29046ED791B7752E1;
// E7.NotchSolution.PerEdgeValues`1<E7.NotchSolution.BlendedClipsAdaptor>
struct PerEdgeValues_1_t1A42987AA6752FBE3EF2200F32ECE41EC4728CB1;
// E7.NotchSolution.PerEdgeValues`1<E7.NotchSolution.EdgeEvaluationMode>
struct PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE;
// E7.NotchSolution.PerEdgeValues`1<System.Int32Enum>
struct PerEdgeValues_1_tCBA78033E0D4CA83B93019350FFE465BCB2CAD14;
// E7.NotchSolution.PerEdgeValues`1<System.Object>
struct PerEdgeValues_1_tAC444F3167AA6F86918F3810645575453F524F1B;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32Enum,System.Object>
struct ValueCollection_t5373BD128F5AE1EA1689DBC1D1B58C837368E16A;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.ScreenOrientation,E7.NotchSolution.OrientationDependentData>
struct ValueCollection_t4F97D9B4470FCD9B1FB63D4852EE80A94A173F62;
// UnityEngine.AnimationClip
struct AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// System.Collections.Generic.Dictionary`2/Entry<UnityEngine.ScreenOrientation,E7.NotchSolution.OrientationDependentData>[]
struct EntryU5BU5D_t877A4E6D54CD4FD2CA0348CBEA5B8A9FCE2F03A7;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// E7.NotchSolution.GraphicsDependentSystemInfoData[]
struct GraphicsDependentSystemInfoDataU5BU5D_t9B34507900FDBB75290C1432ADBD851B3DCEBA14;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// System.Int32Enum[]
struct Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// E7.NotchSolution.OrientationDependentData[]
struct OrientationDependentDataU5BU5D_t7D9034DF05ACF0C0ED3E9D0E8A1AE964A815176E;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Rect[]
struct RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE;
// E7.NotchSolution.ScreenData[]
struct ScreenDataU5BU5D_tF5982CCA6D330EEF56AA5F4BF1170338BCF38AAB;
// UnityEngine.ScreenOrientation[]
struct ScreenOrientationU5BU5D_t7132DACB88FB07021C6A10D0EBED5B86B66BA05F;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String
struct String_t;
// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4;

IL2CPP_EXTERN_C RuntimeClass* AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LayoutRebuilder_tE88B8B9EA50644E438123BDCE2BC2A3287E07585_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral563324936E1F9C24050C5F40DA01938A4D7E2F41;
IL2CPP_EXTERN_C String_t* _stringLiteral5FF35238BBF2981025FC471BD32679FF32514282;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInParent_TisCanvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_m79D616348A09F5E2973F405F4F9D944744FAD6A5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mF1B671F6CC3E8D0F730A1BE22291122B4002B8C2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m762448D06E873D5D27DF97E793F29B6F4B825CC4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Keys_m78E2C29CA07F0853ADB4CF9D388BBA35F392C058_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Values_mF57C2399DD74B23C81A6675DB50B0669DB946C29_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisOrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D_m5DAC4A729E2BCAA0A028DAC48018784D5FD6A29C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisScreenOrientation_tDD9EF2729A0D580721770597532935B0A7ADE020_m1584F65FE299B571F83B0E1CC97141F31B02E804_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PerEdgeValues_1__ctor_mB9B72BAEB18D43AA262C35DC74889A042A1B78B2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PerEdgeValues_1__ctor_mC5C5C66BA0E9A56A888E35BA9083154E64FC61EC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayableExtensions_SetInputWeight_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mDB955B1A400A2B2FAC115FF40524DFE4F8E34652_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayableGraph_Connect_TisAnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_m51D9B475A8831D92BBA52DC12B1D1FFCFDD79451_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayableOutputExtensions_SetSourcePlayable_TisAnimationPlayableOutput_t14570F3E63619E52ABB0B0306D4F4AAA6225DE17_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mE29738BA829F39EDAFCB18988825867C07ED0B42_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CDelayedUpdateRoutineU3Ed__19_System_Collections_IEnumerator_Reset_m45027201E08628547E570425DC36DE791FA5B6FB_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t AdaptationBase_ResetAdaptationToCurve_m0DB3E2F55F05CC0985FB73ADB5FB5CF824E52E1E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptationBase__ctor_m50D7CDA55A7F6682D8F780D72F758DA3CF966DF3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptationBase_get_AnimatorComponent_m664E3D528CD811A9ACF493C6A96230815528E337_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptationBase_get_SafeAreaRelative_m1C2D114B34B46E26679DD3775B2BE73F002B2416_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AdaptationBase_get_SelectedAdaptation_mAB19172E9F514C64643490A948AB0AAD717BE2DB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AspectRatioAdaptation_U3CResetU3Eg__GenDefaultCurveU7C1_0_m4F275F34394578EBBBB199873FB2C391C3D3049C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t BlendedClipsAdaptor_Adapt_m47AA5EC9A764C6D05110D19E834B6ECB8AC7B29B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t BlendedClipsAdaptor_get_Adaptable_m9D2737CCB6C65D8A6FCBAA0731E9BF0FD5C820CE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NotchSolutionUIBehaviourBase_DelayedUpdateRoutine_mBE1625FF6D9BFE992EDD6961BB56E10DA2DB8832_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NotchSolutionUIBehaviourBase_GetCanvasRect_m6664B2634FBD335D5350B41D855AA16F8A8A8890_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NotchSolutionUIBehaviourBase_OnDisable_mF0E4327BC6B4EE827BE209E33DF2FF4DC5E06179_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NotchSolutionUIBehaviourBase_U3CGetCanvasRectU3Eg__GetTopLevelCanvasU7C1_0_m626582600C4FD53CDEFE76B89E9759B9F4125C5D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NotchSolutionUIBehaviourBase__ctor_m06D19A834AF9C336013AA1322C9F9D3233D281A0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NotchSolutionUIBehaviourBase_get_SafeAreaRelative_m4229E4426CF2727B0EC2F0E47258A1C6EEB106BD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NotchSolutionUtility__cctor_m7D412E25BE0C6B08A3E6E15D61D1F23A7649E6F3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NotchSolutionUtility_get_ScreenCutoutsRelative_m2D78B9F1EF59F96E077A72CCA9228212860D8DD9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NotchSolutionUtility_get_ScreenSafeAreaRelative_mA401C9F104714B1D8753556BDB9EC91DF63F76FC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PerEdgeAdaptations__ctor_m83B42A2A6E7967A9CA028EDFDC8563BBD9414237_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PerEdgeEvaluationModes__ctor_mC37F1C45BF291CFF2264E000D1A7565C970E3BFE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SafeAdaptation_AdaptWithRelativeSafeArea_m8B4F6C7CECE7AB36DA4CA688C18A4AEFC76AC589_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SafePadding_UpdateRect_m3211D09A95539B88B05450DD91319F21FD386D4C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ScreenData_OnAfterDeserialize_m481FD2919E1839DE8EA60CDB940D9F4A4C18B147_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ScreenData_OnBeforeSerialize_m7DACA6554B7A3F05E2BF3AAA612A82A21515EEE1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CDelayedUpdateRoutineU3Ed__19_System_Collections_IEnumerator_Reset_m45027201E08628547E570425DC36DE791FA5B6FB_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct OrientationDependentDataU5BU5D_t7D9034DF05ACF0C0ED3E9D0E8A1AE964A815176E;
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC;
struct RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE;
struct ScreenOrientationU5BU5D_t7132DACB88FB07021C6A10D0EBED5B86B66BA05F;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tA1EB567FA104EC8444150E94CCC87EE6CAC41ACF 
{
public:

public:
};


// System.Object


// E7.NotchSolution.BlendedClipsAdaptor
struct  BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7  : public RuntimeObject
{
public:
	// UnityEngine.AnimationClip E7.NotchSolution.BlendedClipsAdaptor::normalState
	AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * ___normalState_0;
	// UnityEngine.AnimationClip E7.NotchSolution.BlendedClipsAdaptor::fullyAdaptedState
	AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * ___fullyAdaptedState_1;
	// UnityEngine.AnimationCurve E7.NotchSolution.BlendedClipsAdaptor::adaptationCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___adaptationCurve_2;

public:
	inline static int32_t get_offset_of_normalState_0() { return static_cast<int32_t>(offsetof(BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7, ___normalState_0)); }
	inline AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * get_normalState_0() const { return ___normalState_0; }
	inline AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 ** get_address_of_normalState_0() { return &___normalState_0; }
	inline void set_normalState_0(AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * value)
	{
		___normalState_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___normalState_0), (void*)value);
	}

	inline static int32_t get_offset_of_fullyAdaptedState_1() { return static_cast<int32_t>(offsetof(BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7, ___fullyAdaptedState_1)); }
	inline AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * get_fullyAdaptedState_1() const { return ___fullyAdaptedState_1; }
	inline AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 ** get_address_of_fullyAdaptedState_1() { return &___fullyAdaptedState_1; }
	inline void set_fullyAdaptedState_1(AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * value)
	{
		___fullyAdaptedState_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fullyAdaptedState_1), (void*)value);
	}

	inline static int32_t get_offset_of_adaptationCurve_2() { return static_cast<int32_t>(offsetof(BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7, ___adaptationCurve_2)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_adaptationCurve_2() const { return ___adaptationCurve_2; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_adaptationCurve_2() { return &___adaptationCurve_2; }
	inline void set_adaptationCurve_2(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___adaptationCurve_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___adaptationCurve_2), (void*)value);
	}
};


// E7.NotchSolution.MetaData
struct  MetaData_t22BDEAB65B452CADFA38BB83AFD80ED804BE2828  : public RuntimeObject
{
public:
	// System.String E7.NotchSolution.MetaData::friendlyName
	String_t* ___friendlyName_0;
	// System.String E7.NotchSolution.MetaData::overlay
	String_t* ___overlay_1;

public:
	inline static int32_t get_offset_of_friendlyName_0() { return static_cast<int32_t>(offsetof(MetaData_t22BDEAB65B452CADFA38BB83AFD80ED804BE2828, ___friendlyName_0)); }
	inline String_t* get_friendlyName_0() const { return ___friendlyName_0; }
	inline String_t** get_address_of_friendlyName_0() { return &___friendlyName_0; }
	inline void set_friendlyName_0(String_t* value)
	{
		___friendlyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___friendlyName_0), (void*)value);
	}

	inline static int32_t get_offset_of_overlay_1() { return static_cast<int32_t>(offsetof(MetaData_t22BDEAB65B452CADFA38BB83AFD80ED804BE2828, ___overlay_1)); }
	inline String_t* get_overlay_1() const { return ___overlay_1; }
	inline String_t** get_address_of_overlay_1() { return &___overlay_1; }
	inline void set_overlay_1(String_t* value)
	{
		___overlay_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___overlay_1), (void*)value);
	}
};


// E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19
struct  U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F  : public RuntimeObject
{
public:
	// System.Int32 E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// E7.NotchSolution.NotchSolutionUIBehaviourBase E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::<>4__this
	NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F, ___U3CU3E4__this_2)); }
	inline NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// E7.NotchSolution.NotchSolutionUtilityEditor
struct  NotchSolutionUtilityEditor_tB3E6BE55C6E151A8DB4B86CCA4117CD3FF386ECB  : public RuntimeObject
{
public:

public:
};


// E7.NotchSolution.PerEdgeValues`1<E7.NotchSolution.BlendedClipsAdaptor>
struct  PerEdgeValues_1_t1A42987AA6752FBE3EF2200F32ECE41EC4728CB1  : public RuntimeObject
{
public:
	// T E7.NotchSolution.PerEdgeValues`1::left
	BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * ___left_0;
	// T E7.NotchSolution.PerEdgeValues`1::bottom
	BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * ___bottom_1;
	// T E7.NotchSolution.PerEdgeValues`1::top
	BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * ___top_2;
	// T E7.NotchSolution.PerEdgeValues`1::right
	BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * ___right_3;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(PerEdgeValues_1_t1A42987AA6752FBE3EF2200F32ECE41EC4728CB1, ___left_0)); }
	inline BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * get_left_0() const { return ___left_0; }
	inline BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 ** get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * value)
	{
		___left_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___left_0), (void*)value);
	}

	inline static int32_t get_offset_of_bottom_1() { return static_cast<int32_t>(offsetof(PerEdgeValues_1_t1A42987AA6752FBE3EF2200F32ECE41EC4728CB1, ___bottom_1)); }
	inline BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * get_bottom_1() const { return ___bottom_1; }
	inline BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 ** get_address_of_bottom_1() { return &___bottom_1; }
	inline void set_bottom_1(BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * value)
	{
		___bottom_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bottom_1), (void*)value);
	}

	inline static int32_t get_offset_of_top_2() { return static_cast<int32_t>(offsetof(PerEdgeValues_1_t1A42987AA6752FBE3EF2200F32ECE41EC4728CB1, ___top_2)); }
	inline BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * get_top_2() const { return ___top_2; }
	inline BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 ** get_address_of_top_2() { return &___top_2; }
	inline void set_top_2(BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * value)
	{
		___top_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___top_2), (void*)value);
	}

	inline static int32_t get_offset_of_right_3() { return static_cast<int32_t>(offsetof(PerEdgeValues_1_t1A42987AA6752FBE3EF2200F32ECE41EC4728CB1, ___right_3)); }
	inline BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * get_right_3() const { return ___right_3; }
	inline BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 ** get_address_of_right_3() { return &___right_3; }
	inline void set_right_3(BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * value)
	{
		___right_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___right_3), (void*)value);
	}
};


// E7.NotchSolution.ScreenData
struct  ScreenData_tC4231E8FE32650020BC1AD704E046BE73CFCF56E  : public RuntimeObject
{
public:
	// System.Int32 E7.NotchSolution.ScreenData::width
	int32_t ___width_0;
	// System.Int32 E7.NotchSolution.ScreenData::height
	int32_t ___height_1;
	// System.Int32 E7.NotchSolution.ScreenData::navigationBarHeight
	int32_t ___navigationBarHeight_2;
	// System.Single E7.NotchSolution.ScreenData::dpi
	float ___dpi_3;
	// System.Collections.Generic.Dictionary`2<UnityEngine.ScreenOrientation,E7.NotchSolution.OrientationDependentData> E7.NotchSolution.ScreenData::orientations
	Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * ___orientations_4;
	// UnityEngine.ScreenOrientation[] E7.NotchSolution.ScreenData::orientationKeys
	ScreenOrientationU5BU5D_t7132DACB88FB07021C6A10D0EBED5B86B66BA05F* ___orientationKeys_5;
	// E7.NotchSolution.OrientationDependentData[] E7.NotchSolution.ScreenData::orientationValues
	OrientationDependentDataU5BU5D_t7D9034DF05ACF0C0ED3E9D0E8A1AE964A815176E* ___orientationValues_6;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(ScreenData_tC4231E8FE32650020BC1AD704E046BE73CFCF56E, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(ScreenData_tC4231E8FE32650020BC1AD704E046BE73CFCF56E, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_navigationBarHeight_2() { return static_cast<int32_t>(offsetof(ScreenData_tC4231E8FE32650020BC1AD704E046BE73CFCF56E, ___navigationBarHeight_2)); }
	inline int32_t get_navigationBarHeight_2() const { return ___navigationBarHeight_2; }
	inline int32_t* get_address_of_navigationBarHeight_2() { return &___navigationBarHeight_2; }
	inline void set_navigationBarHeight_2(int32_t value)
	{
		___navigationBarHeight_2 = value;
	}

	inline static int32_t get_offset_of_dpi_3() { return static_cast<int32_t>(offsetof(ScreenData_tC4231E8FE32650020BC1AD704E046BE73CFCF56E, ___dpi_3)); }
	inline float get_dpi_3() const { return ___dpi_3; }
	inline float* get_address_of_dpi_3() { return &___dpi_3; }
	inline void set_dpi_3(float value)
	{
		___dpi_3 = value;
	}

	inline static int32_t get_offset_of_orientations_4() { return static_cast<int32_t>(offsetof(ScreenData_tC4231E8FE32650020BC1AD704E046BE73CFCF56E, ___orientations_4)); }
	inline Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * get_orientations_4() const { return ___orientations_4; }
	inline Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE ** get_address_of_orientations_4() { return &___orientations_4; }
	inline void set_orientations_4(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * value)
	{
		___orientations_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___orientations_4), (void*)value);
	}

	inline static int32_t get_offset_of_orientationKeys_5() { return static_cast<int32_t>(offsetof(ScreenData_tC4231E8FE32650020BC1AD704E046BE73CFCF56E, ___orientationKeys_5)); }
	inline ScreenOrientationU5BU5D_t7132DACB88FB07021C6A10D0EBED5B86B66BA05F* get_orientationKeys_5() const { return ___orientationKeys_5; }
	inline ScreenOrientationU5BU5D_t7132DACB88FB07021C6A10D0EBED5B86B66BA05F** get_address_of_orientationKeys_5() { return &___orientationKeys_5; }
	inline void set_orientationKeys_5(ScreenOrientationU5BU5D_t7132DACB88FB07021C6A10D0EBED5B86B66BA05F* value)
	{
		___orientationKeys_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___orientationKeys_5), (void*)value);
	}

	inline static int32_t get_offset_of_orientationValues_6() { return static_cast<int32_t>(offsetof(ScreenData_tC4231E8FE32650020BC1AD704E046BE73CFCF56E, ___orientationValues_6)); }
	inline OrientationDependentDataU5BU5D_t7D9034DF05ACF0C0ED3E9D0E8A1AE964A815176E* get_orientationValues_6() const { return ___orientationValues_6; }
	inline OrientationDependentDataU5BU5D_t7D9034DF05ACF0C0ED3E9D0E8A1AE964A815176E** get_address_of_orientationValues_6() { return &___orientationValues_6; }
	inline void set_orientationValues_6(OrientationDependentDataU5BU5D_t7D9034DF05ACF0C0ED3E9D0E8A1AE964A815176E* value)
	{
		___orientationValues_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___orientationValues_6), (void*)value);
	}
};


// E7.NotchSolution.SimulationDevice
struct  SimulationDevice_t4EC038BE01BB81A3C6AE018037486B82CE44A1F2  : public RuntimeObject
{
public:
	// E7.NotchSolution.MetaData E7.NotchSolution.SimulationDevice::Meta
	MetaData_t22BDEAB65B452CADFA38BB83AFD80ED804BE2828 * ___Meta_0;
	// E7.NotchSolution.ScreenData[] E7.NotchSolution.SimulationDevice::Screens
	ScreenDataU5BU5D_tF5982CCA6D330EEF56AA5F4BF1170338BCF38AAB* ___Screens_1;
	// E7.NotchSolution.SystemInfoData E7.NotchSolution.SimulationDevice::SystemInfo
	SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A * ___SystemInfo_2;

public:
	inline static int32_t get_offset_of_Meta_0() { return static_cast<int32_t>(offsetof(SimulationDevice_t4EC038BE01BB81A3C6AE018037486B82CE44A1F2, ___Meta_0)); }
	inline MetaData_t22BDEAB65B452CADFA38BB83AFD80ED804BE2828 * get_Meta_0() const { return ___Meta_0; }
	inline MetaData_t22BDEAB65B452CADFA38BB83AFD80ED804BE2828 ** get_address_of_Meta_0() { return &___Meta_0; }
	inline void set_Meta_0(MetaData_t22BDEAB65B452CADFA38BB83AFD80ED804BE2828 * value)
	{
		___Meta_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Meta_0), (void*)value);
	}

	inline static int32_t get_offset_of_Screens_1() { return static_cast<int32_t>(offsetof(SimulationDevice_t4EC038BE01BB81A3C6AE018037486B82CE44A1F2, ___Screens_1)); }
	inline ScreenDataU5BU5D_tF5982CCA6D330EEF56AA5F4BF1170338BCF38AAB* get_Screens_1() const { return ___Screens_1; }
	inline ScreenDataU5BU5D_tF5982CCA6D330EEF56AA5F4BF1170338BCF38AAB** get_address_of_Screens_1() { return &___Screens_1; }
	inline void set_Screens_1(ScreenDataU5BU5D_tF5982CCA6D330EEF56AA5F4BF1170338BCF38AAB* value)
	{
		___Screens_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Screens_1), (void*)value);
	}

	inline static int32_t get_offset_of_SystemInfo_2() { return static_cast<int32_t>(offsetof(SimulationDevice_t4EC038BE01BB81A3C6AE018037486B82CE44A1F2, ___SystemInfo_2)); }
	inline SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A * get_SystemInfo_2() const { return ___SystemInfo_2; }
	inline SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A ** get_address_of_SystemInfo_2() { return &___SystemInfo_2; }
	inline void set_SystemInfo_2(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A * value)
	{
		___SystemInfo_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SystemInfo_2), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.Dictionary`2_KeyCollection<UnityEngine.ScreenOrientation,E7.NotchSolution.OrientationDependentData>
struct  KeyCollection_t42ABC2A1D8DF067994FAE1C29046ED791B7752E1  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2_KeyCollection::dictionary
	Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(KeyCollection_t42ABC2A1D8DF067994FAE1C29046ED791B7752E1, ___dictionary_0)); }
	inline Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2_ValueCollection<UnityEngine.ScreenOrientation,E7.NotchSolution.OrientationDependentData>
struct  ValueCollection_t4F97D9B4470FCD9B1FB63D4852EE80A94A173F62  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2_ValueCollection::dictionary
	Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * ___dictionary_0;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(ValueCollection_t4F97D9B4470FCD9B1FB63D4852EE80A94A173F62, ___dictionary_0)); }
	inline Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictionary_0), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2<UnityEngine.ScreenOrientation,E7.NotchSolution.OrientationDependentData>
struct  Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t877A4E6D54CD4FD2CA0348CBEA5B8A9FCE2F03A7* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t42ABC2A1D8DF067994FAE1C29046ED791B7752E1 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t4F97D9B4470FCD9B1FB63D4852EE80A94A173F62 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE, ___entries_1)); }
	inline EntryU5BU5D_t877A4E6D54CD4FD2CA0348CBEA5B8A9FCE2F03A7* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t877A4E6D54CD4FD2CA0348CBEA5B8A9FCE2F03A7** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t877A4E6D54CD4FD2CA0348CBEA5B8A9FCE2F03A7* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE, ___keys_7)); }
	inline KeyCollection_t42ABC2A1D8DF067994FAE1C29046ED791B7752E1 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t42ABC2A1D8DF067994FAE1C29046ED791B7752E1 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t42ABC2A1D8DF067994FAE1C29046ED791B7752E1 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE, ___values_8)); }
	inline ValueCollection_t4F97D9B4470FCD9B1FB63D4852EE80A94A173F62 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t4F97D9B4470FCD9B1FB63D4852EE80A94A173F62 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t4F97D9B4470FCD9B1FB63D4852EE80A94A173F62 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct  YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// E7.NotchSolution.PerEdgeAdaptations
struct  PerEdgeAdaptations_t45C28CCF296B800024E9013701B891370CC4CF2D  : public PerEdgeValues_1_t1A42987AA6752FBE3EF2200F32ECE41EC4728CB1
{
public:

public:
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2__padding[1];
	};

public:
};


// UnityEngine.Keyframe
struct  Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;
	// System.Int32 UnityEngine.Keyframe::m_WeightedMode
	int32_t ___m_WeightedMode_4;
	// System.Single UnityEngine.Keyframe::m_InWeight
	float ___m_InWeight_5;
	// System.Single UnityEngine.Keyframe::m_OutWeight
	float ___m_OutWeight_6;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}

	inline static int32_t get_offset_of_m_WeightedMode_4() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_WeightedMode_4)); }
	inline int32_t get_m_WeightedMode_4() const { return ___m_WeightedMode_4; }
	inline int32_t* get_address_of_m_WeightedMode_4() { return &___m_WeightedMode_4; }
	inline void set_m_WeightedMode_4(int32_t value)
	{
		___m_WeightedMode_4 = value;
	}

	inline static int32_t get_offset_of_m_InWeight_5() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_InWeight_5)); }
	inline float get_m_InWeight_5() const { return ___m_InWeight_5; }
	inline float* get_address_of_m_InWeight_5() { return &___m_InWeight_5; }
	inline void set_m_InWeight_5(float value)
	{
		___m_InWeight_5 = value;
	}

	inline static int32_t get_offset_of_m_OutWeight_6() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_OutWeight_6)); }
	inline float get_m_OutWeight_6() const { return ___m_OutWeight_6; }
	inline float* get_address_of_m_OutWeight_6() { return &___m_OutWeight_6; }
	inline void set_m_OutWeight_6(float value)
	{
		___m_OutWeight_6 = value;
	}
};


// UnityEngine.Rect
struct  Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.Resolution
struct  Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 
{
public:
	// System.Int32 UnityEngine.Resolution::m_Width
	int32_t ___m_Width_0;
	// System.Int32 UnityEngine.Resolution::m_Height
	int32_t ___m_Height_1;
	// System.Int32 UnityEngine.Resolution::m_RefreshRate
	int32_t ___m_RefreshRate_2;

public:
	inline static int32_t get_offset_of_m_Width_0() { return static_cast<int32_t>(offsetof(Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767, ___m_Width_0)); }
	inline int32_t get_m_Width_0() const { return ___m_Width_0; }
	inline int32_t* get_address_of_m_Width_0() { return &___m_Width_0; }
	inline void set_m_Width_0(int32_t value)
	{
		___m_Width_0 = value;
	}

	inline static int32_t get_offset_of_m_Height_1() { return static_cast<int32_t>(offsetof(Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767, ___m_Height_1)); }
	inline int32_t get_m_Height_1() const { return ___m_Height_1; }
	inline int32_t* get_address_of_m_Height_1() { return &___m_Height_1; }
	inline void set_m_Height_1(int32_t value)
	{
		___m_Height_1 = value;
	}

	inline static int32_t get_offset_of_m_RefreshRate_2() { return static_cast<int32_t>(offsetof(Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767, ___m_RefreshRate_2)); }
	inline int32_t get_m_RefreshRate_2() const { return ___m_RefreshRate_2; }
	inline int32_t* get_address_of_m_RefreshRate_2() { return &___m_RefreshRate_2; }
	inline void set_m_RefreshRate_2(int32_t value)
	{
		___m_RefreshRate_2 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.WaitForEndOfFrame
struct  WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:

public:
};


// E7.NotchSolution.EdgeEvaluationMode
struct  EdgeEvaluationMode_t553381A5C9E321F705994215BD1DC114EBABE925 
{
public:
	// System.Int32 E7.NotchSolution.EdgeEvaluationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EdgeEvaluationMode_t553381A5C9E321F705994215BD1DC114EBABE925, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// E7.NotchSolution.NotchSolutionUtility
struct  NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55  : public RuntimeObject
{
public:

public:
};

struct NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields
{
public:
	// UnityEngine.Rect E7.NotchSolution.NotchSolutionUtility::defaultSafeArea
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___defaultSafeArea_0;
	// UnityEngine.Rect[] E7.NotchSolution.NotchSolutionUtility::defaultCutouts
	RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* ___defaultCutouts_1;
	// UnityEngine.Rect E7.NotchSolution.NotchSolutionUtility::cachedScreenSafeArea
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___cachedScreenSafeArea_2;
	// UnityEngine.Rect E7.NotchSolution.NotchSolutionUtility::cachedScreenSafeAreaRelative
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___cachedScreenSafeAreaRelative_3;
	// System.Boolean E7.NotchSolution.NotchSolutionUtility::safeAreaRelativeCached
	bool ___safeAreaRelativeCached_4;
	// UnityEngine.Rect[] E7.NotchSolution.NotchSolutionUtility::cachedScreenCutouts
	RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* ___cachedScreenCutouts_5;
	// UnityEngine.Rect[] E7.NotchSolution.NotchSolutionUtility::cachedScreenCutoutsRelative
	RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* ___cachedScreenCutoutsRelative_6;
	// System.Boolean E7.NotchSolution.NotchSolutionUtility::cutoutsRelativeCached
	bool ___cutoutsRelativeCached_7;

public:
	inline static int32_t get_offset_of_defaultSafeArea_0() { return static_cast<int32_t>(offsetof(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields, ___defaultSafeArea_0)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_defaultSafeArea_0() const { return ___defaultSafeArea_0; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_defaultSafeArea_0() { return &___defaultSafeArea_0; }
	inline void set_defaultSafeArea_0(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___defaultSafeArea_0 = value;
	}

	inline static int32_t get_offset_of_defaultCutouts_1() { return static_cast<int32_t>(offsetof(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields, ___defaultCutouts_1)); }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* get_defaultCutouts_1() const { return ___defaultCutouts_1; }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE** get_address_of_defaultCutouts_1() { return &___defaultCutouts_1; }
	inline void set_defaultCutouts_1(RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* value)
	{
		___defaultCutouts_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultCutouts_1), (void*)value);
	}

	inline static int32_t get_offset_of_cachedScreenSafeArea_2() { return static_cast<int32_t>(offsetof(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields, ___cachedScreenSafeArea_2)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_cachedScreenSafeArea_2() const { return ___cachedScreenSafeArea_2; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_cachedScreenSafeArea_2() { return &___cachedScreenSafeArea_2; }
	inline void set_cachedScreenSafeArea_2(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___cachedScreenSafeArea_2 = value;
	}

	inline static int32_t get_offset_of_cachedScreenSafeAreaRelative_3() { return static_cast<int32_t>(offsetof(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields, ___cachedScreenSafeAreaRelative_3)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_cachedScreenSafeAreaRelative_3() const { return ___cachedScreenSafeAreaRelative_3; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_cachedScreenSafeAreaRelative_3() { return &___cachedScreenSafeAreaRelative_3; }
	inline void set_cachedScreenSafeAreaRelative_3(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___cachedScreenSafeAreaRelative_3 = value;
	}

	inline static int32_t get_offset_of_safeAreaRelativeCached_4() { return static_cast<int32_t>(offsetof(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields, ___safeAreaRelativeCached_4)); }
	inline bool get_safeAreaRelativeCached_4() const { return ___safeAreaRelativeCached_4; }
	inline bool* get_address_of_safeAreaRelativeCached_4() { return &___safeAreaRelativeCached_4; }
	inline void set_safeAreaRelativeCached_4(bool value)
	{
		___safeAreaRelativeCached_4 = value;
	}

	inline static int32_t get_offset_of_cachedScreenCutouts_5() { return static_cast<int32_t>(offsetof(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields, ___cachedScreenCutouts_5)); }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* get_cachedScreenCutouts_5() const { return ___cachedScreenCutouts_5; }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE** get_address_of_cachedScreenCutouts_5() { return &___cachedScreenCutouts_5; }
	inline void set_cachedScreenCutouts_5(RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* value)
	{
		___cachedScreenCutouts_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cachedScreenCutouts_5), (void*)value);
	}

	inline static int32_t get_offset_of_cachedScreenCutoutsRelative_6() { return static_cast<int32_t>(offsetof(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields, ___cachedScreenCutoutsRelative_6)); }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* get_cachedScreenCutoutsRelative_6() const { return ___cachedScreenCutoutsRelative_6; }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE** get_address_of_cachedScreenCutoutsRelative_6() { return &___cachedScreenCutoutsRelative_6; }
	inline void set_cachedScreenCutoutsRelative_6(RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* value)
	{
		___cachedScreenCutoutsRelative_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cachedScreenCutoutsRelative_6), (void*)value);
	}

	inline static int32_t get_offset_of_cutoutsRelativeCached_7() { return static_cast<int32_t>(offsetof(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields, ___cutoutsRelativeCached_7)); }
	inline bool get_cutoutsRelativeCached_7() const { return ___cutoutsRelativeCached_7; }
	inline bool* get_address_of_cutoutsRelativeCached_7() { return &___cutoutsRelativeCached_7; }
	inline void set_cutoutsRelativeCached_7(bool value)
	{
		___cutoutsRelativeCached_7 = value;
	}
};


// E7.NotchSolution.OrientationDependentData
struct  OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D  : public RuntimeObject
{
public:
	// UnityEngine.Rect E7.NotchSolution.OrientationDependentData::safeArea
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___safeArea_0;
	// UnityEngine.Rect[] E7.NotchSolution.OrientationDependentData::cutouts
	RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* ___cutouts_1;

public:
	inline static int32_t get_offset_of_safeArea_0() { return static_cast<int32_t>(offsetof(OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D, ___safeArea_0)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_safeArea_0() const { return ___safeArea_0; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_safeArea_0() { return &___safeArea_0; }
	inline void set_safeArea_0(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___safeArea_0 = value;
	}

	inline static int32_t get_offset_of_cutouts_1() { return static_cast<int32_t>(offsetof(OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D, ___cutouts_1)); }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* get_cutouts_1() const { return ___cutouts_1; }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE** get_address_of_cutouts_1() { return &___cutouts_1; }
	inline void set_cutouts_1(RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* value)
	{
		___cutouts_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cutouts_1), (void*)value);
	}
};


// E7.NotchSolution.SupportedOrientations
struct  SupportedOrientations_t323A67083594549B1CDBD44D0FAC396592705032 
{
public:
	// System.Int32 E7.NotchSolution.SupportedOrientations::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SupportedOrientations_t323A67083594549B1CDBD44D0FAC396592705032, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Int32Enum
struct  Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AnimationCurve
struct  AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Coroutine
struct  Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.DeviceType
struct  DeviceType_tBE460E2D86295964F19F6708DBDC3568ECCBE9DF 
{
public:
	// System.Int32 UnityEngine.DeviceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeviceType_tBE460E2D86295964F19F6708DBDC3568ECCBE9DF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.DrivenTransformProperties
struct  DrivenTransformProperties_t3AD3E95057A9FBFD9600C7C8F2F446D93250DF62 
{
public:
	// System.Int32 UnityEngine.DrivenTransformProperties::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DrivenTransformProperties_t3AD3E95057A9FBFD9600C7C8F2F446D93250DF62, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.NPOTSupport
struct  NPOTSupport_t0952D210286E1764231FF4C622F7AA8C412559DC 
{
public:
	// System.Int32 UnityEngine.NPOTSupport::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NPOTSupport_t0952D210286E1764231FF4C622F7AA8C412559DC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.OperatingSystemFamily
struct  OperatingSystemFamily_tA0F8964A9E51797792B4FCD070B5501858BEFC33 
{
public:
	// System.Int32 UnityEngine.OperatingSystemFamily::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OperatingSystemFamily_tA0F8964A9E51797792B4FCD070B5501858BEFC33, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Playables.DirectorUpdateMode
struct  DirectorUpdateMode_tB24F8E33907264F52253ECB157109A9E456B25DE 
{
public:
	// System.Int32 UnityEngine.Playables.DirectorUpdateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DirectorUpdateMode_tB24F8E33907264F52253ECB157109A9E456B25DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Playables.PlayableGraph
struct  PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableGraph::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableGraph::m_Version
	uint32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A, ___m_Version_1)); }
	inline uint32_t get_m_Version_1() const { return ___m_Version_1; }
	inline uint32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(uint32_t value)
	{
		___m_Version_1 = value;
	}
};


// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableHandle::m_Version
	uint32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A, ___m_Version_1)); }
	inline uint32_t get_m_Version_1() const { return ___m_Version_1; }
	inline uint32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(uint32_t value)
	{
		___m_Version_1 = value;
	}
};

struct PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A_StaticFields
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::m_Null
	PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  ___m_Null_2;

public:
	inline static int32_t get_offset_of_m_Null_2() { return static_cast<int32_t>(offsetof(PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A_StaticFields, ___m_Null_2)); }
	inline PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  get_m_Null_2() const { return ___m_Null_2; }
	inline PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A * get_address_of_m_Null_2() { return &___m_Null_2; }
	inline void set_m_Null_2(PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  value)
	{
		___m_Null_2 = value;
	}
};


// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t8C84BCDB2AECFEDBCF0E7CC7CDBADD517D148CD1 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	uint32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t8C84BCDB2AECFEDBCF0E7CC7CDBADD517D148CD1, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t8C84BCDB2AECFEDBCF0E7CC7CDBADD517D148CD1, ___m_Version_1)); }
	inline uint32_t get_m_Version_1() const { return ___m_Version_1; }
	inline uint32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(uint32_t value)
	{
		___m_Version_1 = value;
	}
};

struct PlayableOutputHandle_t8C84BCDB2AECFEDBCF0E7CC7CDBADD517D148CD1_StaticFields
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::m_Null
	PlayableOutputHandle_t8C84BCDB2AECFEDBCF0E7CC7CDBADD517D148CD1  ___m_Null_2;

public:
	inline static int32_t get_offset_of_m_Null_2() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t8C84BCDB2AECFEDBCF0E7CC7CDBADD517D148CD1_StaticFields, ___m_Null_2)); }
	inline PlayableOutputHandle_t8C84BCDB2AECFEDBCF0E7CC7CDBADD517D148CD1  get_m_Null_2() const { return ___m_Null_2; }
	inline PlayableOutputHandle_t8C84BCDB2AECFEDBCF0E7CC7CDBADD517D148CD1 * get_address_of_m_Null_2() { return &___m_Null_2; }
	inline void set_m_Null_2(PlayableOutputHandle_t8C84BCDB2AECFEDBCF0E7CC7CDBADD517D148CD1  value)
	{
		___m_Null_2 = value;
	}
};


// UnityEngine.RectTransform_Edge
struct  Edge_tC1CFEDE8D0D04FFA0E8F261E6DFEB1F22831F364 
{
public:
	// System.Int32 UnityEngine.RectTransform_Edge::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Edge_tC1CFEDE8D0D04FFA0E8F261E6DFEB1F22831F364, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Rendering.CopyTextureSupport
struct  CopyTextureSupport_t538543181A38D77492629B23A4321BFCD11A7AED 
{
public:
	// System.Int32 UnityEngine.Rendering.CopyTextureSupport::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CopyTextureSupport_t538543181A38D77492629B23A4321BFCD11A7AED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Rendering.GraphicsDeviceType
struct  GraphicsDeviceType_t531071CD9311C868D1279D2550F83670D18FB779 
{
public:
	// System.Int32 UnityEngine.Rendering.GraphicsDeviceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GraphicsDeviceType_t531071CD9311C868D1279D2550F83670D18FB779, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ScreenOrientation
struct  ScreenOrientation_tDD9EF2729A0D580721770597532935B0A7ADE020 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenOrientation_tDD9EF2729A0D580721770597532935B0A7ADE020, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// E7.NotchSolution.GraphicsDependentSystemInfoData
struct  GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912  : public RuntimeObject
{
public:
	// UnityEngine.Rendering.GraphicsDeviceType E7.NotchSolution.GraphicsDependentSystemInfoData::graphicsDeviceType
	int32_t ___graphicsDeviceType_0;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::graphicsMemorySize
	int32_t ___graphicsMemorySize_1;
	// System.String E7.NotchSolution.GraphicsDependentSystemInfoData::graphicsDeviceName
	String_t* ___graphicsDeviceName_2;
	// System.String E7.NotchSolution.GraphicsDependentSystemInfoData::graphicsDeviceVendor
	String_t* ___graphicsDeviceVendor_3;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::graphicsDeviceID
	int32_t ___graphicsDeviceID_4;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::graphicsDeviceVendorID
	int32_t ___graphicsDeviceVendorID_5;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::graphicsUVStartsAtTop
	bool ___graphicsUVStartsAtTop_6;
	// System.String E7.NotchSolution.GraphicsDependentSystemInfoData::graphicsDeviceVersion
	String_t* ___graphicsDeviceVersion_7;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::graphicsShaderLevel
	int32_t ___graphicsShaderLevel_8;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::graphicsMultiThreaded
	bool ___graphicsMultiThreaded_9;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::hasHiddenSurfaceRemovalOnGPU
	bool ___hasHiddenSurfaceRemovalOnGPU_10;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::hasDynamicUniformArrayIndexingInFragmentShaders
	bool ___hasDynamicUniformArrayIndexingInFragmentShaders_11;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsShadows
	bool ___supportsShadows_12;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsRawShadowDepthSampling
	bool ___supportsRawShadowDepthSampling_13;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsMotionVectors
	bool ___supportsMotionVectors_14;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supports3DTextures
	bool ___supports3DTextures_15;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supports2DArrayTextures
	bool ___supports2DArrayTextures_16;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supports3DRenderTextures
	bool ___supports3DRenderTextures_17;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsCubemapArrayTextures
	bool ___supportsCubemapArrayTextures_18;
	// UnityEngine.Rendering.CopyTextureSupport E7.NotchSolution.GraphicsDependentSystemInfoData::copyTextureSupport
	int32_t ___copyTextureSupport_19;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsComputeShaders
	bool ___supportsComputeShaders_20;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsGeometryShaders
	bool ___supportsGeometryShaders_21;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsTessellationShaders
	bool ___supportsTessellationShaders_22;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsInstancing
	bool ___supportsInstancing_23;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsHardwareQuadTopology
	bool ___supportsHardwareQuadTopology_24;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supports32bitsIndexBuffer
	bool ___supports32bitsIndexBuffer_25;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsSparseTextures
	bool ___supportsSparseTextures_26;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::supportedRenderTargetCount
	int32_t ___supportedRenderTargetCount_27;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsSeparatedRenderTargetsBlend
	bool ___supportsSeparatedRenderTargetsBlend_28;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::supportedRandomWriteTargetCount
	int32_t ___supportedRandomWriteTargetCount_29;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::supportsMultisampledTextures
	int32_t ___supportsMultisampledTextures_30;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsMultisampleAutoResolve
	bool ___supportsMultisampleAutoResolve_31;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::supportsTextureWrapMirrorOnce
	int32_t ___supportsTextureWrapMirrorOnce_32;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::usesReversedZBuffer
	bool ___usesReversedZBuffer_33;
	// UnityEngine.NPOTSupport E7.NotchSolution.GraphicsDependentSystemInfoData::npotSupport
	int32_t ___npotSupport_34;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::maxTextureSize
	int32_t ___maxTextureSize_35;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::maxCubemapSize
	int32_t ___maxCubemapSize_36;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::maxComputeBufferInputsVertex
	int32_t ___maxComputeBufferInputsVertex_37;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::maxComputeBufferInputsFragment
	int32_t ___maxComputeBufferInputsFragment_38;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::maxComputeBufferInputsGeometry
	int32_t ___maxComputeBufferInputsGeometry_39;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::maxComputeBufferInputsDomain
	int32_t ___maxComputeBufferInputsDomain_40;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::maxComputeBufferInputsHull
	int32_t ___maxComputeBufferInputsHull_41;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::maxComputeBufferInputsCompute
	int32_t ___maxComputeBufferInputsCompute_42;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::maxComputeWorkGroupSize
	int32_t ___maxComputeWorkGroupSize_43;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::maxComputeWorkGroupSizeX
	int32_t ___maxComputeWorkGroupSizeX_44;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::maxComputeWorkGroupSizeY
	int32_t ___maxComputeWorkGroupSizeY_45;
	// System.Int32 E7.NotchSolution.GraphicsDependentSystemInfoData::maxComputeWorkGroupSizeZ
	int32_t ___maxComputeWorkGroupSizeZ_46;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsAsyncCompute
	bool ___supportsAsyncCompute_47;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsGraphicsFence
	bool ___supportsGraphicsFence_48;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsAsyncGPUReadback
	bool ___supportsAsyncGPUReadback_49;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsRayTracing
	bool ___supportsRayTracing_50;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsSetConstantBuffer
	bool ___supportsSetConstantBuffer_51;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::minConstantBufferOffsetAlignment
	bool ___minConstantBufferOffsetAlignment_52;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::hasMipMaxLevel
	bool ___hasMipMaxLevel_53;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::supportsMipStreaming
	bool ___supportsMipStreaming_54;
	// System.Boolean E7.NotchSolution.GraphicsDependentSystemInfoData::usesLoadStoreActions
	bool ___usesLoadStoreActions_55;

public:
	inline static int32_t get_offset_of_graphicsDeviceType_0() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___graphicsDeviceType_0)); }
	inline int32_t get_graphicsDeviceType_0() const { return ___graphicsDeviceType_0; }
	inline int32_t* get_address_of_graphicsDeviceType_0() { return &___graphicsDeviceType_0; }
	inline void set_graphicsDeviceType_0(int32_t value)
	{
		___graphicsDeviceType_0 = value;
	}

	inline static int32_t get_offset_of_graphicsMemorySize_1() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___graphicsMemorySize_1)); }
	inline int32_t get_graphicsMemorySize_1() const { return ___graphicsMemorySize_1; }
	inline int32_t* get_address_of_graphicsMemorySize_1() { return &___graphicsMemorySize_1; }
	inline void set_graphicsMemorySize_1(int32_t value)
	{
		___graphicsMemorySize_1 = value;
	}

	inline static int32_t get_offset_of_graphicsDeviceName_2() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___graphicsDeviceName_2)); }
	inline String_t* get_graphicsDeviceName_2() const { return ___graphicsDeviceName_2; }
	inline String_t** get_address_of_graphicsDeviceName_2() { return &___graphicsDeviceName_2; }
	inline void set_graphicsDeviceName_2(String_t* value)
	{
		___graphicsDeviceName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___graphicsDeviceName_2), (void*)value);
	}

	inline static int32_t get_offset_of_graphicsDeviceVendor_3() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___graphicsDeviceVendor_3)); }
	inline String_t* get_graphicsDeviceVendor_3() const { return ___graphicsDeviceVendor_3; }
	inline String_t** get_address_of_graphicsDeviceVendor_3() { return &___graphicsDeviceVendor_3; }
	inline void set_graphicsDeviceVendor_3(String_t* value)
	{
		___graphicsDeviceVendor_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___graphicsDeviceVendor_3), (void*)value);
	}

	inline static int32_t get_offset_of_graphicsDeviceID_4() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___graphicsDeviceID_4)); }
	inline int32_t get_graphicsDeviceID_4() const { return ___graphicsDeviceID_4; }
	inline int32_t* get_address_of_graphicsDeviceID_4() { return &___graphicsDeviceID_4; }
	inline void set_graphicsDeviceID_4(int32_t value)
	{
		___graphicsDeviceID_4 = value;
	}

	inline static int32_t get_offset_of_graphicsDeviceVendorID_5() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___graphicsDeviceVendorID_5)); }
	inline int32_t get_graphicsDeviceVendorID_5() const { return ___graphicsDeviceVendorID_5; }
	inline int32_t* get_address_of_graphicsDeviceVendorID_5() { return &___graphicsDeviceVendorID_5; }
	inline void set_graphicsDeviceVendorID_5(int32_t value)
	{
		___graphicsDeviceVendorID_5 = value;
	}

	inline static int32_t get_offset_of_graphicsUVStartsAtTop_6() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___graphicsUVStartsAtTop_6)); }
	inline bool get_graphicsUVStartsAtTop_6() const { return ___graphicsUVStartsAtTop_6; }
	inline bool* get_address_of_graphicsUVStartsAtTop_6() { return &___graphicsUVStartsAtTop_6; }
	inline void set_graphicsUVStartsAtTop_6(bool value)
	{
		___graphicsUVStartsAtTop_6 = value;
	}

	inline static int32_t get_offset_of_graphicsDeviceVersion_7() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___graphicsDeviceVersion_7)); }
	inline String_t* get_graphicsDeviceVersion_7() const { return ___graphicsDeviceVersion_7; }
	inline String_t** get_address_of_graphicsDeviceVersion_7() { return &___graphicsDeviceVersion_7; }
	inline void set_graphicsDeviceVersion_7(String_t* value)
	{
		___graphicsDeviceVersion_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___graphicsDeviceVersion_7), (void*)value);
	}

	inline static int32_t get_offset_of_graphicsShaderLevel_8() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___graphicsShaderLevel_8)); }
	inline int32_t get_graphicsShaderLevel_8() const { return ___graphicsShaderLevel_8; }
	inline int32_t* get_address_of_graphicsShaderLevel_8() { return &___graphicsShaderLevel_8; }
	inline void set_graphicsShaderLevel_8(int32_t value)
	{
		___graphicsShaderLevel_8 = value;
	}

	inline static int32_t get_offset_of_graphicsMultiThreaded_9() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___graphicsMultiThreaded_9)); }
	inline bool get_graphicsMultiThreaded_9() const { return ___graphicsMultiThreaded_9; }
	inline bool* get_address_of_graphicsMultiThreaded_9() { return &___graphicsMultiThreaded_9; }
	inline void set_graphicsMultiThreaded_9(bool value)
	{
		___graphicsMultiThreaded_9 = value;
	}

	inline static int32_t get_offset_of_hasHiddenSurfaceRemovalOnGPU_10() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___hasHiddenSurfaceRemovalOnGPU_10)); }
	inline bool get_hasHiddenSurfaceRemovalOnGPU_10() const { return ___hasHiddenSurfaceRemovalOnGPU_10; }
	inline bool* get_address_of_hasHiddenSurfaceRemovalOnGPU_10() { return &___hasHiddenSurfaceRemovalOnGPU_10; }
	inline void set_hasHiddenSurfaceRemovalOnGPU_10(bool value)
	{
		___hasHiddenSurfaceRemovalOnGPU_10 = value;
	}

	inline static int32_t get_offset_of_hasDynamicUniformArrayIndexingInFragmentShaders_11() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___hasDynamicUniformArrayIndexingInFragmentShaders_11)); }
	inline bool get_hasDynamicUniformArrayIndexingInFragmentShaders_11() const { return ___hasDynamicUniformArrayIndexingInFragmentShaders_11; }
	inline bool* get_address_of_hasDynamicUniformArrayIndexingInFragmentShaders_11() { return &___hasDynamicUniformArrayIndexingInFragmentShaders_11; }
	inline void set_hasDynamicUniformArrayIndexingInFragmentShaders_11(bool value)
	{
		___hasDynamicUniformArrayIndexingInFragmentShaders_11 = value;
	}

	inline static int32_t get_offset_of_supportsShadows_12() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsShadows_12)); }
	inline bool get_supportsShadows_12() const { return ___supportsShadows_12; }
	inline bool* get_address_of_supportsShadows_12() { return &___supportsShadows_12; }
	inline void set_supportsShadows_12(bool value)
	{
		___supportsShadows_12 = value;
	}

	inline static int32_t get_offset_of_supportsRawShadowDepthSampling_13() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsRawShadowDepthSampling_13)); }
	inline bool get_supportsRawShadowDepthSampling_13() const { return ___supportsRawShadowDepthSampling_13; }
	inline bool* get_address_of_supportsRawShadowDepthSampling_13() { return &___supportsRawShadowDepthSampling_13; }
	inline void set_supportsRawShadowDepthSampling_13(bool value)
	{
		___supportsRawShadowDepthSampling_13 = value;
	}

	inline static int32_t get_offset_of_supportsMotionVectors_14() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsMotionVectors_14)); }
	inline bool get_supportsMotionVectors_14() const { return ___supportsMotionVectors_14; }
	inline bool* get_address_of_supportsMotionVectors_14() { return &___supportsMotionVectors_14; }
	inline void set_supportsMotionVectors_14(bool value)
	{
		___supportsMotionVectors_14 = value;
	}

	inline static int32_t get_offset_of_supports3DTextures_15() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supports3DTextures_15)); }
	inline bool get_supports3DTextures_15() const { return ___supports3DTextures_15; }
	inline bool* get_address_of_supports3DTextures_15() { return &___supports3DTextures_15; }
	inline void set_supports3DTextures_15(bool value)
	{
		___supports3DTextures_15 = value;
	}

	inline static int32_t get_offset_of_supports2DArrayTextures_16() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supports2DArrayTextures_16)); }
	inline bool get_supports2DArrayTextures_16() const { return ___supports2DArrayTextures_16; }
	inline bool* get_address_of_supports2DArrayTextures_16() { return &___supports2DArrayTextures_16; }
	inline void set_supports2DArrayTextures_16(bool value)
	{
		___supports2DArrayTextures_16 = value;
	}

	inline static int32_t get_offset_of_supports3DRenderTextures_17() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supports3DRenderTextures_17)); }
	inline bool get_supports3DRenderTextures_17() const { return ___supports3DRenderTextures_17; }
	inline bool* get_address_of_supports3DRenderTextures_17() { return &___supports3DRenderTextures_17; }
	inline void set_supports3DRenderTextures_17(bool value)
	{
		___supports3DRenderTextures_17 = value;
	}

	inline static int32_t get_offset_of_supportsCubemapArrayTextures_18() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsCubemapArrayTextures_18)); }
	inline bool get_supportsCubemapArrayTextures_18() const { return ___supportsCubemapArrayTextures_18; }
	inline bool* get_address_of_supportsCubemapArrayTextures_18() { return &___supportsCubemapArrayTextures_18; }
	inline void set_supportsCubemapArrayTextures_18(bool value)
	{
		___supportsCubemapArrayTextures_18 = value;
	}

	inline static int32_t get_offset_of_copyTextureSupport_19() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___copyTextureSupport_19)); }
	inline int32_t get_copyTextureSupport_19() const { return ___copyTextureSupport_19; }
	inline int32_t* get_address_of_copyTextureSupport_19() { return &___copyTextureSupport_19; }
	inline void set_copyTextureSupport_19(int32_t value)
	{
		___copyTextureSupport_19 = value;
	}

	inline static int32_t get_offset_of_supportsComputeShaders_20() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsComputeShaders_20)); }
	inline bool get_supportsComputeShaders_20() const { return ___supportsComputeShaders_20; }
	inline bool* get_address_of_supportsComputeShaders_20() { return &___supportsComputeShaders_20; }
	inline void set_supportsComputeShaders_20(bool value)
	{
		___supportsComputeShaders_20 = value;
	}

	inline static int32_t get_offset_of_supportsGeometryShaders_21() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsGeometryShaders_21)); }
	inline bool get_supportsGeometryShaders_21() const { return ___supportsGeometryShaders_21; }
	inline bool* get_address_of_supportsGeometryShaders_21() { return &___supportsGeometryShaders_21; }
	inline void set_supportsGeometryShaders_21(bool value)
	{
		___supportsGeometryShaders_21 = value;
	}

	inline static int32_t get_offset_of_supportsTessellationShaders_22() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsTessellationShaders_22)); }
	inline bool get_supportsTessellationShaders_22() const { return ___supportsTessellationShaders_22; }
	inline bool* get_address_of_supportsTessellationShaders_22() { return &___supportsTessellationShaders_22; }
	inline void set_supportsTessellationShaders_22(bool value)
	{
		___supportsTessellationShaders_22 = value;
	}

	inline static int32_t get_offset_of_supportsInstancing_23() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsInstancing_23)); }
	inline bool get_supportsInstancing_23() const { return ___supportsInstancing_23; }
	inline bool* get_address_of_supportsInstancing_23() { return &___supportsInstancing_23; }
	inline void set_supportsInstancing_23(bool value)
	{
		___supportsInstancing_23 = value;
	}

	inline static int32_t get_offset_of_supportsHardwareQuadTopology_24() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsHardwareQuadTopology_24)); }
	inline bool get_supportsHardwareQuadTopology_24() const { return ___supportsHardwareQuadTopology_24; }
	inline bool* get_address_of_supportsHardwareQuadTopology_24() { return &___supportsHardwareQuadTopology_24; }
	inline void set_supportsHardwareQuadTopology_24(bool value)
	{
		___supportsHardwareQuadTopology_24 = value;
	}

	inline static int32_t get_offset_of_supports32bitsIndexBuffer_25() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supports32bitsIndexBuffer_25)); }
	inline bool get_supports32bitsIndexBuffer_25() const { return ___supports32bitsIndexBuffer_25; }
	inline bool* get_address_of_supports32bitsIndexBuffer_25() { return &___supports32bitsIndexBuffer_25; }
	inline void set_supports32bitsIndexBuffer_25(bool value)
	{
		___supports32bitsIndexBuffer_25 = value;
	}

	inline static int32_t get_offset_of_supportsSparseTextures_26() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsSparseTextures_26)); }
	inline bool get_supportsSparseTextures_26() const { return ___supportsSparseTextures_26; }
	inline bool* get_address_of_supportsSparseTextures_26() { return &___supportsSparseTextures_26; }
	inline void set_supportsSparseTextures_26(bool value)
	{
		___supportsSparseTextures_26 = value;
	}

	inline static int32_t get_offset_of_supportedRenderTargetCount_27() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportedRenderTargetCount_27)); }
	inline int32_t get_supportedRenderTargetCount_27() const { return ___supportedRenderTargetCount_27; }
	inline int32_t* get_address_of_supportedRenderTargetCount_27() { return &___supportedRenderTargetCount_27; }
	inline void set_supportedRenderTargetCount_27(int32_t value)
	{
		___supportedRenderTargetCount_27 = value;
	}

	inline static int32_t get_offset_of_supportsSeparatedRenderTargetsBlend_28() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsSeparatedRenderTargetsBlend_28)); }
	inline bool get_supportsSeparatedRenderTargetsBlend_28() const { return ___supportsSeparatedRenderTargetsBlend_28; }
	inline bool* get_address_of_supportsSeparatedRenderTargetsBlend_28() { return &___supportsSeparatedRenderTargetsBlend_28; }
	inline void set_supportsSeparatedRenderTargetsBlend_28(bool value)
	{
		___supportsSeparatedRenderTargetsBlend_28 = value;
	}

	inline static int32_t get_offset_of_supportedRandomWriteTargetCount_29() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportedRandomWriteTargetCount_29)); }
	inline int32_t get_supportedRandomWriteTargetCount_29() const { return ___supportedRandomWriteTargetCount_29; }
	inline int32_t* get_address_of_supportedRandomWriteTargetCount_29() { return &___supportedRandomWriteTargetCount_29; }
	inline void set_supportedRandomWriteTargetCount_29(int32_t value)
	{
		___supportedRandomWriteTargetCount_29 = value;
	}

	inline static int32_t get_offset_of_supportsMultisampledTextures_30() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsMultisampledTextures_30)); }
	inline int32_t get_supportsMultisampledTextures_30() const { return ___supportsMultisampledTextures_30; }
	inline int32_t* get_address_of_supportsMultisampledTextures_30() { return &___supportsMultisampledTextures_30; }
	inline void set_supportsMultisampledTextures_30(int32_t value)
	{
		___supportsMultisampledTextures_30 = value;
	}

	inline static int32_t get_offset_of_supportsMultisampleAutoResolve_31() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsMultisampleAutoResolve_31)); }
	inline bool get_supportsMultisampleAutoResolve_31() const { return ___supportsMultisampleAutoResolve_31; }
	inline bool* get_address_of_supportsMultisampleAutoResolve_31() { return &___supportsMultisampleAutoResolve_31; }
	inline void set_supportsMultisampleAutoResolve_31(bool value)
	{
		___supportsMultisampleAutoResolve_31 = value;
	}

	inline static int32_t get_offset_of_supportsTextureWrapMirrorOnce_32() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsTextureWrapMirrorOnce_32)); }
	inline int32_t get_supportsTextureWrapMirrorOnce_32() const { return ___supportsTextureWrapMirrorOnce_32; }
	inline int32_t* get_address_of_supportsTextureWrapMirrorOnce_32() { return &___supportsTextureWrapMirrorOnce_32; }
	inline void set_supportsTextureWrapMirrorOnce_32(int32_t value)
	{
		___supportsTextureWrapMirrorOnce_32 = value;
	}

	inline static int32_t get_offset_of_usesReversedZBuffer_33() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___usesReversedZBuffer_33)); }
	inline bool get_usesReversedZBuffer_33() const { return ___usesReversedZBuffer_33; }
	inline bool* get_address_of_usesReversedZBuffer_33() { return &___usesReversedZBuffer_33; }
	inline void set_usesReversedZBuffer_33(bool value)
	{
		___usesReversedZBuffer_33 = value;
	}

	inline static int32_t get_offset_of_npotSupport_34() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___npotSupport_34)); }
	inline int32_t get_npotSupport_34() const { return ___npotSupport_34; }
	inline int32_t* get_address_of_npotSupport_34() { return &___npotSupport_34; }
	inline void set_npotSupport_34(int32_t value)
	{
		___npotSupport_34 = value;
	}

	inline static int32_t get_offset_of_maxTextureSize_35() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___maxTextureSize_35)); }
	inline int32_t get_maxTextureSize_35() const { return ___maxTextureSize_35; }
	inline int32_t* get_address_of_maxTextureSize_35() { return &___maxTextureSize_35; }
	inline void set_maxTextureSize_35(int32_t value)
	{
		___maxTextureSize_35 = value;
	}

	inline static int32_t get_offset_of_maxCubemapSize_36() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___maxCubemapSize_36)); }
	inline int32_t get_maxCubemapSize_36() const { return ___maxCubemapSize_36; }
	inline int32_t* get_address_of_maxCubemapSize_36() { return &___maxCubemapSize_36; }
	inline void set_maxCubemapSize_36(int32_t value)
	{
		___maxCubemapSize_36 = value;
	}

	inline static int32_t get_offset_of_maxComputeBufferInputsVertex_37() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___maxComputeBufferInputsVertex_37)); }
	inline int32_t get_maxComputeBufferInputsVertex_37() const { return ___maxComputeBufferInputsVertex_37; }
	inline int32_t* get_address_of_maxComputeBufferInputsVertex_37() { return &___maxComputeBufferInputsVertex_37; }
	inline void set_maxComputeBufferInputsVertex_37(int32_t value)
	{
		___maxComputeBufferInputsVertex_37 = value;
	}

	inline static int32_t get_offset_of_maxComputeBufferInputsFragment_38() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___maxComputeBufferInputsFragment_38)); }
	inline int32_t get_maxComputeBufferInputsFragment_38() const { return ___maxComputeBufferInputsFragment_38; }
	inline int32_t* get_address_of_maxComputeBufferInputsFragment_38() { return &___maxComputeBufferInputsFragment_38; }
	inline void set_maxComputeBufferInputsFragment_38(int32_t value)
	{
		___maxComputeBufferInputsFragment_38 = value;
	}

	inline static int32_t get_offset_of_maxComputeBufferInputsGeometry_39() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___maxComputeBufferInputsGeometry_39)); }
	inline int32_t get_maxComputeBufferInputsGeometry_39() const { return ___maxComputeBufferInputsGeometry_39; }
	inline int32_t* get_address_of_maxComputeBufferInputsGeometry_39() { return &___maxComputeBufferInputsGeometry_39; }
	inline void set_maxComputeBufferInputsGeometry_39(int32_t value)
	{
		___maxComputeBufferInputsGeometry_39 = value;
	}

	inline static int32_t get_offset_of_maxComputeBufferInputsDomain_40() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___maxComputeBufferInputsDomain_40)); }
	inline int32_t get_maxComputeBufferInputsDomain_40() const { return ___maxComputeBufferInputsDomain_40; }
	inline int32_t* get_address_of_maxComputeBufferInputsDomain_40() { return &___maxComputeBufferInputsDomain_40; }
	inline void set_maxComputeBufferInputsDomain_40(int32_t value)
	{
		___maxComputeBufferInputsDomain_40 = value;
	}

	inline static int32_t get_offset_of_maxComputeBufferInputsHull_41() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___maxComputeBufferInputsHull_41)); }
	inline int32_t get_maxComputeBufferInputsHull_41() const { return ___maxComputeBufferInputsHull_41; }
	inline int32_t* get_address_of_maxComputeBufferInputsHull_41() { return &___maxComputeBufferInputsHull_41; }
	inline void set_maxComputeBufferInputsHull_41(int32_t value)
	{
		___maxComputeBufferInputsHull_41 = value;
	}

	inline static int32_t get_offset_of_maxComputeBufferInputsCompute_42() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___maxComputeBufferInputsCompute_42)); }
	inline int32_t get_maxComputeBufferInputsCompute_42() const { return ___maxComputeBufferInputsCompute_42; }
	inline int32_t* get_address_of_maxComputeBufferInputsCompute_42() { return &___maxComputeBufferInputsCompute_42; }
	inline void set_maxComputeBufferInputsCompute_42(int32_t value)
	{
		___maxComputeBufferInputsCompute_42 = value;
	}

	inline static int32_t get_offset_of_maxComputeWorkGroupSize_43() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___maxComputeWorkGroupSize_43)); }
	inline int32_t get_maxComputeWorkGroupSize_43() const { return ___maxComputeWorkGroupSize_43; }
	inline int32_t* get_address_of_maxComputeWorkGroupSize_43() { return &___maxComputeWorkGroupSize_43; }
	inline void set_maxComputeWorkGroupSize_43(int32_t value)
	{
		___maxComputeWorkGroupSize_43 = value;
	}

	inline static int32_t get_offset_of_maxComputeWorkGroupSizeX_44() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___maxComputeWorkGroupSizeX_44)); }
	inline int32_t get_maxComputeWorkGroupSizeX_44() const { return ___maxComputeWorkGroupSizeX_44; }
	inline int32_t* get_address_of_maxComputeWorkGroupSizeX_44() { return &___maxComputeWorkGroupSizeX_44; }
	inline void set_maxComputeWorkGroupSizeX_44(int32_t value)
	{
		___maxComputeWorkGroupSizeX_44 = value;
	}

	inline static int32_t get_offset_of_maxComputeWorkGroupSizeY_45() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___maxComputeWorkGroupSizeY_45)); }
	inline int32_t get_maxComputeWorkGroupSizeY_45() const { return ___maxComputeWorkGroupSizeY_45; }
	inline int32_t* get_address_of_maxComputeWorkGroupSizeY_45() { return &___maxComputeWorkGroupSizeY_45; }
	inline void set_maxComputeWorkGroupSizeY_45(int32_t value)
	{
		___maxComputeWorkGroupSizeY_45 = value;
	}

	inline static int32_t get_offset_of_maxComputeWorkGroupSizeZ_46() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___maxComputeWorkGroupSizeZ_46)); }
	inline int32_t get_maxComputeWorkGroupSizeZ_46() const { return ___maxComputeWorkGroupSizeZ_46; }
	inline int32_t* get_address_of_maxComputeWorkGroupSizeZ_46() { return &___maxComputeWorkGroupSizeZ_46; }
	inline void set_maxComputeWorkGroupSizeZ_46(int32_t value)
	{
		___maxComputeWorkGroupSizeZ_46 = value;
	}

	inline static int32_t get_offset_of_supportsAsyncCompute_47() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsAsyncCompute_47)); }
	inline bool get_supportsAsyncCompute_47() const { return ___supportsAsyncCompute_47; }
	inline bool* get_address_of_supportsAsyncCompute_47() { return &___supportsAsyncCompute_47; }
	inline void set_supportsAsyncCompute_47(bool value)
	{
		___supportsAsyncCompute_47 = value;
	}

	inline static int32_t get_offset_of_supportsGraphicsFence_48() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsGraphicsFence_48)); }
	inline bool get_supportsGraphicsFence_48() const { return ___supportsGraphicsFence_48; }
	inline bool* get_address_of_supportsGraphicsFence_48() { return &___supportsGraphicsFence_48; }
	inline void set_supportsGraphicsFence_48(bool value)
	{
		___supportsGraphicsFence_48 = value;
	}

	inline static int32_t get_offset_of_supportsAsyncGPUReadback_49() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsAsyncGPUReadback_49)); }
	inline bool get_supportsAsyncGPUReadback_49() const { return ___supportsAsyncGPUReadback_49; }
	inline bool* get_address_of_supportsAsyncGPUReadback_49() { return &___supportsAsyncGPUReadback_49; }
	inline void set_supportsAsyncGPUReadback_49(bool value)
	{
		___supportsAsyncGPUReadback_49 = value;
	}

	inline static int32_t get_offset_of_supportsRayTracing_50() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsRayTracing_50)); }
	inline bool get_supportsRayTracing_50() const { return ___supportsRayTracing_50; }
	inline bool* get_address_of_supportsRayTracing_50() { return &___supportsRayTracing_50; }
	inline void set_supportsRayTracing_50(bool value)
	{
		___supportsRayTracing_50 = value;
	}

	inline static int32_t get_offset_of_supportsSetConstantBuffer_51() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsSetConstantBuffer_51)); }
	inline bool get_supportsSetConstantBuffer_51() const { return ___supportsSetConstantBuffer_51; }
	inline bool* get_address_of_supportsSetConstantBuffer_51() { return &___supportsSetConstantBuffer_51; }
	inline void set_supportsSetConstantBuffer_51(bool value)
	{
		___supportsSetConstantBuffer_51 = value;
	}

	inline static int32_t get_offset_of_minConstantBufferOffsetAlignment_52() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___minConstantBufferOffsetAlignment_52)); }
	inline bool get_minConstantBufferOffsetAlignment_52() const { return ___minConstantBufferOffsetAlignment_52; }
	inline bool* get_address_of_minConstantBufferOffsetAlignment_52() { return &___minConstantBufferOffsetAlignment_52; }
	inline void set_minConstantBufferOffsetAlignment_52(bool value)
	{
		___minConstantBufferOffsetAlignment_52 = value;
	}

	inline static int32_t get_offset_of_hasMipMaxLevel_53() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___hasMipMaxLevel_53)); }
	inline bool get_hasMipMaxLevel_53() const { return ___hasMipMaxLevel_53; }
	inline bool* get_address_of_hasMipMaxLevel_53() { return &___hasMipMaxLevel_53; }
	inline void set_hasMipMaxLevel_53(bool value)
	{
		___hasMipMaxLevel_53 = value;
	}

	inline static int32_t get_offset_of_supportsMipStreaming_54() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___supportsMipStreaming_54)); }
	inline bool get_supportsMipStreaming_54() const { return ___supportsMipStreaming_54; }
	inline bool* get_address_of_supportsMipStreaming_54() { return &___supportsMipStreaming_54; }
	inline void set_supportsMipStreaming_54(bool value)
	{
		___supportsMipStreaming_54 = value;
	}

	inline static int32_t get_offset_of_usesLoadStoreActions_55() { return static_cast<int32_t>(offsetof(GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912, ___usesLoadStoreActions_55)); }
	inline bool get_usesLoadStoreActions_55() const { return ___usesLoadStoreActions_55; }
	inline bool* get_address_of_usesLoadStoreActions_55() { return &___usesLoadStoreActions_55; }
	inline void set_usesLoadStoreActions_55(bool value)
	{
		___usesLoadStoreActions_55 = value;
	}
};


// E7.NotchSolution.PerEdgeValues`1<E7.NotchSolution.EdgeEvaluationMode>
struct  PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE  : public RuntimeObject
{
public:
	// T E7.NotchSolution.PerEdgeValues`1::left
	int32_t ___left_0;
	// T E7.NotchSolution.PerEdgeValues`1::bottom
	int32_t ___bottom_1;
	// T E7.NotchSolution.PerEdgeValues`1::top
	int32_t ___top_2;
	// T E7.NotchSolution.PerEdgeValues`1::right
	int32_t ___right_3;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE, ___left_0)); }
	inline int32_t get_left_0() const { return ___left_0; }
	inline int32_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int32_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_bottom_1() { return static_cast<int32_t>(offsetof(PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE, ___bottom_1)); }
	inline int32_t get_bottom_1() const { return ___bottom_1; }
	inline int32_t* get_address_of_bottom_1() { return &___bottom_1; }
	inline void set_bottom_1(int32_t value)
	{
		___bottom_1 = value;
	}

	inline static int32_t get_offset_of_top_2() { return static_cast<int32_t>(offsetof(PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE, ___top_2)); }
	inline int32_t get_top_2() const { return ___top_2; }
	inline int32_t* get_address_of_top_2() { return &___top_2; }
	inline void set_top_2(int32_t value)
	{
		___top_2 = value;
	}

	inline static int32_t get_offset_of_right_3() { return static_cast<int32_t>(offsetof(PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE, ___right_3)); }
	inline int32_t get_right_3() const { return ___right_3; }
	inline int32_t* get_address_of_right_3() { return &___right_3; }
	inline void set_right_3(int32_t value)
	{
		___right_3 = value;
	}
};


// E7.NotchSolution.SystemInfoData
struct  SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A  : public RuntimeObject
{
public:
	// System.String E7.NotchSolution.SystemInfoData::deviceModel
	String_t* ___deviceModel_0;
	// UnityEngine.DeviceType E7.NotchSolution.SystemInfoData::deviceType
	int32_t ___deviceType_1;
	// System.String E7.NotchSolution.SystemInfoData::operatingSystem
	String_t* ___operatingSystem_2;
	// UnityEngine.OperatingSystemFamily E7.NotchSolution.SystemInfoData::operatingSystemFamily
	int32_t ___operatingSystemFamily_3;
	// System.Int32 E7.NotchSolution.SystemInfoData::processorCount
	int32_t ___processorCount_4;
	// System.Int32 E7.NotchSolution.SystemInfoData::processorFrequency
	int32_t ___processorFrequency_5;
	// System.String E7.NotchSolution.SystemInfoData::processorType
	String_t* ___processorType_6;
	// System.Boolean E7.NotchSolution.SystemInfoData::supportsAccelerometer
	bool ___supportsAccelerometer_7;
	// System.Boolean E7.NotchSolution.SystemInfoData::supportsAudio
	bool ___supportsAudio_8;
	// System.Boolean E7.NotchSolution.SystemInfoData::supportsGyroscope
	bool ___supportsGyroscope_9;
	// System.Boolean E7.NotchSolution.SystemInfoData::supportsLocationService
	bool ___supportsLocationService_10;
	// System.Boolean E7.NotchSolution.SystemInfoData::supportsVibration
	bool ___supportsVibration_11;
	// System.Int32 E7.NotchSolution.SystemInfoData::systemMemorySize
	int32_t ___systemMemorySize_12;
	// System.String E7.NotchSolution.SystemInfoData::unsupportedIdentifier
	String_t* ___unsupportedIdentifier_13;
	// E7.NotchSolution.GraphicsDependentSystemInfoData[] E7.NotchSolution.SystemInfoData::GraphicsDependentData
	GraphicsDependentSystemInfoDataU5BU5D_t9B34507900FDBB75290C1432ADBD851B3DCEBA14* ___GraphicsDependentData_14;

public:
	inline static int32_t get_offset_of_deviceModel_0() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___deviceModel_0)); }
	inline String_t* get_deviceModel_0() const { return ___deviceModel_0; }
	inline String_t** get_address_of_deviceModel_0() { return &___deviceModel_0; }
	inline void set_deviceModel_0(String_t* value)
	{
		___deviceModel_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deviceModel_0), (void*)value);
	}

	inline static int32_t get_offset_of_deviceType_1() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___deviceType_1)); }
	inline int32_t get_deviceType_1() const { return ___deviceType_1; }
	inline int32_t* get_address_of_deviceType_1() { return &___deviceType_1; }
	inline void set_deviceType_1(int32_t value)
	{
		___deviceType_1 = value;
	}

	inline static int32_t get_offset_of_operatingSystem_2() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___operatingSystem_2)); }
	inline String_t* get_operatingSystem_2() const { return ___operatingSystem_2; }
	inline String_t** get_address_of_operatingSystem_2() { return &___operatingSystem_2; }
	inline void set_operatingSystem_2(String_t* value)
	{
		___operatingSystem_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___operatingSystem_2), (void*)value);
	}

	inline static int32_t get_offset_of_operatingSystemFamily_3() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___operatingSystemFamily_3)); }
	inline int32_t get_operatingSystemFamily_3() const { return ___operatingSystemFamily_3; }
	inline int32_t* get_address_of_operatingSystemFamily_3() { return &___operatingSystemFamily_3; }
	inline void set_operatingSystemFamily_3(int32_t value)
	{
		___operatingSystemFamily_3 = value;
	}

	inline static int32_t get_offset_of_processorCount_4() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___processorCount_4)); }
	inline int32_t get_processorCount_4() const { return ___processorCount_4; }
	inline int32_t* get_address_of_processorCount_4() { return &___processorCount_4; }
	inline void set_processorCount_4(int32_t value)
	{
		___processorCount_4 = value;
	}

	inline static int32_t get_offset_of_processorFrequency_5() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___processorFrequency_5)); }
	inline int32_t get_processorFrequency_5() const { return ___processorFrequency_5; }
	inline int32_t* get_address_of_processorFrequency_5() { return &___processorFrequency_5; }
	inline void set_processorFrequency_5(int32_t value)
	{
		___processorFrequency_5 = value;
	}

	inline static int32_t get_offset_of_processorType_6() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___processorType_6)); }
	inline String_t* get_processorType_6() const { return ___processorType_6; }
	inline String_t** get_address_of_processorType_6() { return &___processorType_6; }
	inline void set_processorType_6(String_t* value)
	{
		___processorType_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___processorType_6), (void*)value);
	}

	inline static int32_t get_offset_of_supportsAccelerometer_7() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___supportsAccelerometer_7)); }
	inline bool get_supportsAccelerometer_7() const { return ___supportsAccelerometer_7; }
	inline bool* get_address_of_supportsAccelerometer_7() { return &___supportsAccelerometer_7; }
	inline void set_supportsAccelerometer_7(bool value)
	{
		___supportsAccelerometer_7 = value;
	}

	inline static int32_t get_offset_of_supportsAudio_8() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___supportsAudio_8)); }
	inline bool get_supportsAudio_8() const { return ___supportsAudio_8; }
	inline bool* get_address_of_supportsAudio_8() { return &___supportsAudio_8; }
	inline void set_supportsAudio_8(bool value)
	{
		___supportsAudio_8 = value;
	}

	inline static int32_t get_offset_of_supportsGyroscope_9() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___supportsGyroscope_9)); }
	inline bool get_supportsGyroscope_9() const { return ___supportsGyroscope_9; }
	inline bool* get_address_of_supportsGyroscope_9() { return &___supportsGyroscope_9; }
	inline void set_supportsGyroscope_9(bool value)
	{
		___supportsGyroscope_9 = value;
	}

	inline static int32_t get_offset_of_supportsLocationService_10() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___supportsLocationService_10)); }
	inline bool get_supportsLocationService_10() const { return ___supportsLocationService_10; }
	inline bool* get_address_of_supportsLocationService_10() { return &___supportsLocationService_10; }
	inline void set_supportsLocationService_10(bool value)
	{
		___supportsLocationService_10 = value;
	}

	inline static int32_t get_offset_of_supportsVibration_11() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___supportsVibration_11)); }
	inline bool get_supportsVibration_11() const { return ___supportsVibration_11; }
	inline bool* get_address_of_supportsVibration_11() { return &___supportsVibration_11; }
	inline void set_supportsVibration_11(bool value)
	{
		___supportsVibration_11 = value;
	}

	inline static int32_t get_offset_of_systemMemorySize_12() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___systemMemorySize_12)); }
	inline int32_t get_systemMemorySize_12() const { return ___systemMemorySize_12; }
	inline int32_t* get_address_of_systemMemorySize_12() { return &___systemMemorySize_12; }
	inline void set_systemMemorySize_12(int32_t value)
	{
		___systemMemorySize_12 = value;
	}

	inline static int32_t get_offset_of_unsupportedIdentifier_13() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___unsupportedIdentifier_13)); }
	inline String_t* get_unsupportedIdentifier_13() const { return ___unsupportedIdentifier_13; }
	inline String_t** get_address_of_unsupportedIdentifier_13() { return &___unsupportedIdentifier_13; }
	inline void set_unsupportedIdentifier_13(String_t* value)
	{
		___unsupportedIdentifier_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___unsupportedIdentifier_13), (void*)value);
	}

	inline static int32_t get_offset_of_GraphicsDependentData_14() { return static_cast<int32_t>(offsetof(SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A, ___GraphicsDependentData_14)); }
	inline GraphicsDependentSystemInfoDataU5BU5D_t9B34507900FDBB75290C1432ADBD851B3DCEBA14* get_GraphicsDependentData_14() const { return ___GraphicsDependentData_14; }
	inline GraphicsDependentSystemInfoDataU5BU5D_t9B34507900FDBB75290C1432ADBD851B3DCEBA14** get_address_of_GraphicsDependentData_14() { return &___GraphicsDependentData_14; }
	inline void set_GraphicsDependentData_14(GraphicsDependentSystemInfoDataU5BU5D_t9B34507900FDBB75290C1432ADBD851B3DCEBA14* value)
	{
		___GraphicsDependentData_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GraphicsDependentData_14), (void*)value);
	}
};


// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Animations.AnimationClipPlayable
struct  AnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationClipPlayable::m_Handle
	PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953, ___m_Handle_0)); }
	inline PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  value)
	{
		___m_Handle_0 = value;
	}
};


// UnityEngine.Animations.AnimationMixerPlayable
struct  AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimationMixerPlayable::m_Handle
	PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741, ___m_Handle_0)); }
	inline PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_StaticFields
{
public:
	// UnityEngine.Animations.AnimationMixerPlayable UnityEngine.Animations.AnimationMixerPlayable::m_NullPlayable
	AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_StaticFields, ___m_NullPlayable_1)); }
	inline AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  value)
	{
		___m_NullPlayable_1 = value;
	}
};


// UnityEngine.Animations.AnimationPlayableOutput
struct  AnimationPlayableOutput_t14570F3E63619E52ABB0B0306D4F4AAA6225DE17 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Animations.AnimationPlayableOutput::m_Handle
	PlayableOutputHandle_t8C84BCDB2AECFEDBCF0E7CC7CDBADD517D148CD1  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimationPlayableOutput_t14570F3E63619E52ABB0B0306D4F4AAA6225DE17, ___m_Handle_0)); }
	inline PlayableOutputHandle_t8C84BCDB2AECFEDBCF0E7CC7CDBADD517D148CD1  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t8C84BCDB2AECFEDBCF0E7CC7CDBADD517D148CD1 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t8C84BCDB2AECFEDBCF0E7CC7CDBADD517D148CD1  value)
	{
		___m_Handle_0 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Motion
struct  Motion_t3EAEF01D52B05F10A21CC9B54A35C8F3F6BA3A67  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:
	// System.Boolean UnityEngine.Motion::<isAnimatorMotion>k__BackingField
	bool ___U3CisAnimatorMotionU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CisAnimatorMotionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Motion_t3EAEF01D52B05F10A21CC9B54A35C8F3F6BA3A67, ___U3CisAnimatorMotionU3Ek__BackingField_4)); }
	inline bool get_U3CisAnimatorMotionU3Ek__BackingField_4() const { return ___U3CisAnimatorMotionU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CisAnimatorMotionU3Ek__BackingField_4() { return &___U3CisAnimatorMotionU3Ek__BackingField_4; }
	inline void set_U3CisAnimatorMotionU3Ek__BackingField_4(bool value)
	{
		___U3CisAnimatorMotionU3Ek__BackingField_4 = value;
	}
};


// E7.NotchSolution.PerEdgeEvaluationModes
struct  PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E  : public PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE
{
public:

public:
};


// System.NotSupportedException
struct  NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.AnimationClip
struct  AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178  : public Motion_t3EAEF01D52B05F10A21CC9B54A35C8F3F6BA3A67
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Animator
struct  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Canvas
struct  Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_StaticFields
{
public:
	// UnityEngine.Canvas_WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * ___willRenderCanvases_4;

public:
	inline static int32_t get_offset_of_willRenderCanvases_4() { return static_cast<int32_t>(offsetof(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_StaticFields, ___willRenderCanvases_4)); }
	inline WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * get_willRenderCanvases_4() const { return ___willRenderCanvases_4; }
	inline WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 ** get_address_of_willRenderCanvases_4() { return &___willRenderCanvases_4; }
	inline void set_willRenderCanvases_4(WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * value)
	{
		___willRenderCanvases_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___willRenderCanvases_4), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct  RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform_ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// E7.NotchSolution.AdaptationBase
struct  AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// E7.NotchSolution.SupportedOrientations E7.NotchSolution.AdaptationBase::supportedOrientations
	int32_t ___supportedOrientations_4;
	// E7.NotchSolution.BlendedClipsAdaptor E7.NotchSolution.AdaptationBase::portraitOrDefaultAdaptation
	BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * ___portraitOrDefaultAdaptation_5;
	// E7.NotchSolution.BlendedClipsAdaptor E7.NotchSolution.AdaptationBase::landscapeAdaptation
	BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * ___landscapeAdaptation_6;
	// UnityEngine.Rect E7.NotchSolution.AdaptationBase::storedSimulatedSafeAreaRelative
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___storedSimulatedSafeAreaRelative_7;
	// UnityEngine.Rect[] E7.NotchSolution.AdaptationBase::storedSimulatedCutoutsRelative
	RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* ___storedSimulatedCutoutsRelative_8;

public:
	inline static int32_t get_offset_of_supportedOrientations_4() { return static_cast<int32_t>(offsetof(AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF, ___supportedOrientations_4)); }
	inline int32_t get_supportedOrientations_4() const { return ___supportedOrientations_4; }
	inline int32_t* get_address_of_supportedOrientations_4() { return &___supportedOrientations_4; }
	inline void set_supportedOrientations_4(int32_t value)
	{
		___supportedOrientations_4 = value;
	}

	inline static int32_t get_offset_of_portraitOrDefaultAdaptation_5() { return static_cast<int32_t>(offsetof(AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF, ___portraitOrDefaultAdaptation_5)); }
	inline BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * get_portraitOrDefaultAdaptation_5() const { return ___portraitOrDefaultAdaptation_5; }
	inline BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 ** get_address_of_portraitOrDefaultAdaptation_5() { return &___portraitOrDefaultAdaptation_5; }
	inline void set_portraitOrDefaultAdaptation_5(BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * value)
	{
		___portraitOrDefaultAdaptation_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___portraitOrDefaultAdaptation_5), (void*)value);
	}

	inline static int32_t get_offset_of_landscapeAdaptation_6() { return static_cast<int32_t>(offsetof(AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF, ___landscapeAdaptation_6)); }
	inline BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * get_landscapeAdaptation_6() const { return ___landscapeAdaptation_6; }
	inline BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 ** get_address_of_landscapeAdaptation_6() { return &___landscapeAdaptation_6; }
	inline void set_landscapeAdaptation_6(BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * value)
	{
		___landscapeAdaptation_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___landscapeAdaptation_6), (void*)value);
	}

	inline static int32_t get_offset_of_storedSimulatedSafeAreaRelative_7() { return static_cast<int32_t>(offsetof(AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF, ___storedSimulatedSafeAreaRelative_7)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_storedSimulatedSafeAreaRelative_7() const { return ___storedSimulatedSafeAreaRelative_7; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_storedSimulatedSafeAreaRelative_7() { return &___storedSimulatedSafeAreaRelative_7; }
	inline void set_storedSimulatedSafeAreaRelative_7(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___storedSimulatedSafeAreaRelative_7 = value;
	}

	inline static int32_t get_offset_of_storedSimulatedCutoutsRelative_8() { return static_cast<int32_t>(offsetof(AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF, ___storedSimulatedCutoutsRelative_8)); }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* get_storedSimulatedCutoutsRelative_8() const { return ___storedSimulatedCutoutsRelative_8; }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE** get_address_of_storedSimulatedCutoutsRelative_8() { return &___storedSimulatedCutoutsRelative_8; }
	inline void set_storedSimulatedCutoutsRelative_8(RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* value)
	{
		___storedSimulatedCutoutsRelative_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___storedSimulatedCutoutsRelative_8), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// E7.NotchSolution.AspectRatioAdaptation
struct  AspectRatioAdaptation_t48754AD46C3A23666F1FCD9FE921A682B598CE4D  : public AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF
{
public:

public:
};


// E7.NotchSolution.NotchSolutionUIBehaviourBase
struct  NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Rect E7.NotchSolution.NotchSolutionUIBehaviourBase::storedSimulatedSafeAreaRelative
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___storedSimulatedSafeAreaRelative_4;
	// UnityEngine.Rect[] E7.NotchSolution.NotchSolutionUIBehaviourBase::storedSimulatedCutoutsRelative
	RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* ___storedSimulatedCutoutsRelative_5;
	// UnityEngine.RectTransform E7.NotchSolution.NotchSolutionUIBehaviourBase::m_Rect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_Rect_6;
	// UnityEngine.DrivenRectTransformTracker E7.NotchSolution.NotchSolutionUIBehaviourBase::m_Tracker
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  ___m_Tracker_7;
	// UnityEngine.WaitForEndOfFrame E7.NotchSolution.NotchSolutionUIBehaviourBase::eofWait
	WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * ___eofWait_8;

public:
	inline static int32_t get_offset_of_storedSimulatedSafeAreaRelative_4() { return static_cast<int32_t>(offsetof(NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874, ___storedSimulatedSafeAreaRelative_4)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_storedSimulatedSafeAreaRelative_4() const { return ___storedSimulatedSafeAreaRelative_4; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_storedSimulatedSafeAreaRelative_4() { return &___storedSimulatedSafeAreaRelative_4; }
	inline void set_storedSimulatedSafeAreaRelative_4(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___storedSimulatedSafeAreaRelative_4 = value;
	}

	inline static int32_t get_offset_of_storedSimulatedCutoutsRelative_5() { return static_cast<int32_t>(offsetof(NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874, ___storedSimulatedCutoutsRelative_5)); }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* get_storedSimulatedCutoutsRelative_5() const { return ___storedSimulatedCutoutsRelative_5; }
	inline RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE** get_address_of_storedSimulatedCutoutsRelative_5() { return &___storedSimulatedCutoutsRelative_5; }
	inline void set_storedSimulatedCutoutsRelative_5(RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* value)
	{
		___storedSimulatedCutoutsRelative_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___storedSimulatedCutoutsRelative_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874, ___m_Rect_6)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Rect_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tracker_7() { return static_cast<int32_t>(offsetof(NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874, ___m_Tracker_7)); }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  get_m_Tracker_7() const { return ___m_Tracker_7; }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * get_address_of_m_Tracker_7() { return &___m_Tracker_7; }
	inline void set_m_Tracker_7(DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  value)
	{
		___m_Tracker_7 = value;
	}

	inline static int32_t get_offset_of_eofWait_8() { return static_cast<int32_t>(offsetof(NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874, ___eofWait_8)); }
	inline WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * get_eofWait_8() const { return ___eofWait_8; }
	inline WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 ** get_address_of_eofWait_8() { return &___eofWait_8; }
	inline void set_eofWait_8(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * value)
	{
		___eofWait_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eofWait_8), (void*)value);
	}
};


// E7.NotchSolution.SafeAdaptation
struct  SafeAdaptation_t0DEF9DC095FCB9608021EA9A07ADA433CBBB6634  : public AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF
{
public:
	// UnityEngine.RectTransform_Edge E7.NotchSolution.SafeAdaptation::adaptToEdge
	int32_t ___adaptToEdge_9;
	// E7.NotchSolution.EdgeEvaluationMode E7.NotchSolution.SafeAdaptation::evaluationMode
	int32_t ___evaluationMode_10;

public:
	inline static int32_t get_offset_of_adaptToEdge_9() { return static_cast<int32_t>(offsetof(SafeAdaptation_t0DEF9DC095FCB9608021EA9A07ADA433CBBB6634, ___adaptToEdge_9)); }
	inline int32_t get_adaptToEdge_9() const { return ___adaptToEdge_9; }
	inline int32_t* get_address_of_adaptToEdge_9() { return &___adaptToEdge_9; }
	inline void set_adaptToEdge_9(int32_t value)
	{
		___adaptToEdge_9 = value;
	}

	inline static int32_t get_offset_of_evaluationMode_10() { return static_cast<int32_t>(offsetof(SafeAdaptation_t0DEF9DC095FCB9608021EA9A07ADA433CBBB6634, ___evaluationMode_10)); }
	inline int32_t get_evaluationMode_10() const { return ___evaluationMode_10; }
	inline int32_t* get_address_of_evaluationMode_10() { return &___evaluationMode_10; }
	inline void set_evaluationMode_10(int32_t value)
	{
		___evaluationMode_10 = value;
	}
};


// E7.NotchSolution.SafePadding
struct  SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE  : public NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874
{
public:
	// E7.NotchSolution.SupportedOrientations E7.NotchSolution.SafePadding::orientationType
	int32_t ___orientationType_9;
	// E7.NotchSolution.PerEdgeEvaluationModes E7.NotchSolution.SafePadding::portraitOrDefaultPaddings
	PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * ___portraitOrDefaultPaddings_10;
	// E7.NotchSolution.PerEdgeEvaluationModes E7.NotchSolution.SafePadding::landscapePaddings
	PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * ___landscapePaddings_11;
	// System.Single E7.NotchSolution.SafePadding::influence
	float ___influence_12;
	// System.Boolean E7.NotchSolution.SafePadding::flipPadding
	bool ___flipPadding_13;

public:
	inline static int32_t get_offset_of_orientationType_9() { return static_cast<int32_t>(offsetof(SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE, ___orientationType_9)); }
	inline int32_t get_orientationType_9() const { return ___orientationType_9; }
	inline int32_t* get_address_of_orientationType_9() { return &___orientationType_9; }
	inline void set_orientationType_9(int32_t value)
	{
		___orientationType_9 = value;
	}

	inline static int32_t get_offset_of_portraitOrDefaultPaddings_10() { return static_cast<int32_t>(offsetof(SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE, ___portraitOrDefaultPaddings_10)); }
	inline PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * get_portraitOrDefaultPaddings_10() const { return ___portraitOrDefaultPaddings_10; }
	inline PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E ** get_address_of_portraitOrDefaultPaddings_10() { return &___portraitOrDefaultPaddings_10; }
	inline void set_portraitOrDefaultPaddings_10(PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * value)
	{
		___portraitOrDefaultPaddings_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___portraitOrDefaultPaddings_10), (void*)value);
	}

	inline static int32_t get_offset_of_landscapePaddings_11() { return static_cast<int32_t>(offsetof(SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE, ___landscapePaddings_11)); }
	inline PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * get_landscapePaddings_11() const { return ___landscapePaddings_11; }
	inline PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E ** get_address_of_landscapePaddings_11() { return &___landscapePaddings_11; }
	inline void set_landscapePaddings_11(PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * value)
	{
		___landscapePaddings_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___landscapePaddings_11), (void*)value);
	}

	inline static int32_t get_offset_of_influence_12() { return static_cast<int32_t>(offsetof(SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE, ___influence_12)); }
	inline float get_influence_12() const { return ___influence_12; }
	inline float* get_address_of_influence_12() { return &___influence_12; }
	inline void set_influence_12(float value)
	{
		___influence_12 = value;
	}

	inline static int32_t get_offset_of_flipPadding_13() { return static_cast<int32_t>(offsetof(SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE, ___flipPadding_13)); }
	inline bool get_flipPadding_13() const { return ___flipPadding_13; }
	inline bool* get_address_of_flipPadding_13() { return &___flipPadding_13; }
	inline void set_flipPadding_13(bool value)
	{
		___flipPadding_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Rect[]
struct RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  m_Items[1];

public:
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  m_Items[1];

public:
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.ScreenOrientation[]
struct ScreenOrientationU5BU5D_t7132DACB88FB07021C6A10D0EBED5B86B66BA05F  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// E7.NotchSolution.OrientationDependentData[]
struct OrientationDependentDataU5BU5D_t7D9034DF05ACF0C0ED3E9D0E8A1AE964A815176E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D * m_Items[1];

public:
	inline OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableExtensions::SetInputWeight<UnityEngine.Animations.AnimationMixerPlayable>(!!0,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayableExtensions_SetInputWeight_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mDB955B1A400A2B2FAC115FF40524DFE4F8E34652_gshared (AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  ___playable0, int32_t ___inputIndex1, float ___weight2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableGraph::Connect<UnityEngine.Animations.AnimationClipPlayable,UnityEngine.Animations.AnimationMixerPlayable>(!!0,System.Int32,!!1,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayableGraph_Connect_TisAnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_m51D9B475A8831D92BBA52DC12B1D1FFCFDD79451_gshared (PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A * __this, AnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953  ___source0, int32_t ___sourceOutputPort1, AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  ___destination2, int32_t ___destinationInputPort3, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableOutputExtensions::SetSourcePlayable<UnityEngine.Animations.AnimationPlayableOutput,UnityEngine.Animations.AnimationMixerPlayable>(!!0,!!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayableOutputExtensions_SetSourcePlayable_TisAnimationPlayableOutput_t14570F3E63619E52ABB0B0306D4F4AAA6225DE17_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mE29738BA829F39EDAFCB18988825867C07ED0B42_gshared (AnimationPlayableOutput_t14570F3E63619E52ABB0B0306D4F4AAA6225DE17  ___output0, AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  ___value1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInParent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponentInParent_TisRuntimeObject_mADA186D1675BEA6779C469918206294354385EC3_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void E7.NotchSolution.PerEdgeValues`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerEdgeValues_1__ctor_m21C9D595E4D12E51118E3AC003884749A9B86A53_gshared (PerEdgeValues_1_tAC444F3167AA6F86918F3810645575453F524F1B * __this, const RuntimeMethod* method);
// System.Void E7.NotchSolution.PerEdgeValues`1<System.Int32Enum>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerEdgeValues_1__ctor_mB579A4F8D7DB9058A0F33EC590E87A07180B1538_gshared (PerEdgeValues_1_tCBA78033E0D4CA83B93019350FFE465BCB2CAD14 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/KeyCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Object>::get_Keys()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR KeyCollection_t9EE4F9D0A4F83EC4D31ABD4E20E68B4DD148ED6D * Dictionary_2_get_Keys_m19022C12B8702F3A79910A4E45A1F4215E5FE16F_gshared (Dictionary_2_t7E8D40B461AB586AEA5DD75D8354C4913EEB1337 * __this, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Int32Enum>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD* Enumerable_ToArray_TisInt32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C_mF08E57F5EA873295A63AF0479B7991393205E4E4_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Object>::get_Values()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ValueCollection_t5373BD128F5AE1EA1689DBC1D1B58C837368E16A * Dictionary_2_get_Values_mCE8DFA32761CC8C69390885B07388FB6B221F055_gshared (Dictionary_2_t7E8D40B461AB586AEA5DD75D8354C4913EEB1337 * __this, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Enumerable_ToArray_TisRuntimeObject_m21E15191FE8BDBAE753CC592A1DB55EA3BCE7B5B_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_mF42565DC9AD476065ED33869AD6DC710F775F641_gshared (Dictionary_2_t7E8D40B461AB586AEA5DD75D8354C4913EEB1337 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Object>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_mEA7DC2B06A480A5EC7DE49B6E83C2D121D1962EF_gshared (Dictionary_2_t7E8D40B461AB586AEA5DD75D8354C4913EEB1337 * __this, int32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);

// System.Void E7.NotchSolution.BlendedClipsAdaptor::.ctor(UnityEngine.AnimationCurve)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlendedClipsAdaptor__ctor_mFFDA7697D389E0F084755D691CB40360B8BD2E29 (BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___defaultCurve0, const RuntimeMethod* method);
// UnityEngine.ScreenOrientation E7.NotchSolution.NotchSolutionUtility::GetCurrentOrientation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NotchSolutionUtility_GetCurrentOrientation_m847578DED8F9B964755A0C64C9FADD3FDB27E3A7 (const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// E7.NotchSolution.BlendedClipsAdaptor E7.NotchSolution.AdaptationBase::get_SelectedAdaptation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * AdaptationBase_get_SelectedAdaptation_mAB19172E9F514C64643490A948AB0AAD717BE2DB (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, const RuntimeMethod* method);
// UnityEngine.Animator E7.NotchSolution.AdaptationBase::get_AnimatorComponent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * AdaptationBase_get_AnimatorComponent_m664E3D528CD811A9ACF493C6A96230815528E337 (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, const RuntimeMethod* method);
// System.Void E7.NotchSolution.BlendedClipsAdaptor::Adapt(System.Single,UnityEngine.Animator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlendedClipsAdaptor_Adapt_m47AA5EC9A764C6D05110D19E834B6ECB8AC7B29B (BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * __this, float ___valueForAdaptationCurve0, Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___animator1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Application::get_isPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567 (const RuntimeMethod* method);
// System.Boolean E7.NotchSolution.NotchSolutionUtility::get_ShouldUseNotchSimulatorValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NotchSolutionUtility_get_ShouldUseNotchSimulatorValue_m3C6D486B8587587EB12650930FF6ED47DAE7BA41 (const RuntimeMethod* method);
// UnityEngine.Rect E7.NotchSolution.NotchSolutionUtility::get_ScreenSafeAreaRelative()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  NotchSolutionUtility_get_ScreenSafeAreaRelative_mA401C9F104714B1D8753556BDB9EC91DF63F76FC (const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Single E7.NotchSolution.AspectRatioAdaptation::get_AspectRatio()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AspectRatioAdaptation_get_AspectRatio_m9568A2021F105A6C7B1D97DEA791C3290EDACD42 (AspectRatioAdaptation_t48754AD46C3A23666F1FCD9FE921A682B598CE4D * __this, const RuntimeMethod* method);
// System.Void E7.NotchSolution.AdaptationBase::Adapt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptationBase_Adapt_m1D2C2F404AB717AC81D03326CF3EA15C04C1D8FE (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, float ___valueForAdaptationCurve0, const RuntimeMethod* method);
// UnityEngine.AnimationCurve E7.NotchSolution.AspectRatioAdaptation::<Reset>g__GenDefaultCurve|1_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * AspectRatioAdaptation_U3CResetU3Eg__GenDefaultCurveU7C1_0_m4F275F34394578EBBBB199873FB2C391C3D3049C (const RuntimeMethod* method);
// System.Void E7.NotchSolution.AdaptationBase::ResetAdaptationToCurve(UnityEngine.AnimationCurve)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptationBase_ResetAdaptationToCurve_m0DB3E2F55F05CC0985FB73ADB5FB5CF824E52E1E (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___curve0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C (const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE (const RuntimeMethod* method);
// System.Void E7.NotchSolution.AdaptationBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptationBase__ctor_m50D7CDA55A7F6682D8F780D72F758DA3CF966DF3 (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Keyframe__ctor_m572CCEE06F612003F939F3FF439B15F89E8C1D54 (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * __this, float ___time0, float ___value1, float ___inTangent2, float ___outTangent3, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_InverseLerp_mCD2E6F9ADCFFB40EB7D3086E444DF2C702F9C29B (float ___a0, float ___b1, float ___value2, const RuntimeMethod* method);
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * __this, KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* ___keys0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Boolean E7.NotchSolution.BlendedClipsAdaptor::get_Adaptable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BlendedClipsAdaptor_get_Adaptable_m9D2737CCB6C65D8A6FCBAA0731E9BF0FD5C820CE (BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * __this, float ___time0, const RuntimeMethod* method);
// UnityEngine.Playables.PlayableGraph UnityEngine.Playables.PlayableGraph::Create(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A  PlayableGraph_Create_mA33157C9AB7F53A7850C1427EFB11773ACA74872 (String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableGraph::SetTimeUpdateMode(UnityEngine.Playables.DirectorUpdateMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayableGraph_SetTimeUpdateMode_mF78E1782C4707F10170370446FF3959DAA6CF844 (PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A * __this, int32_t ___value0, const RuntimeMethod* method);
// UnityEngine.Animations.AnimationMixerPlayable UnityEngine.Animations.AnimationMixerPlayable::Create(UnityEngine.Playables.PlayableGraph,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  AnimationMixerPlayable_Create_mA961D86F5F73ADCA1F0650088B6214CEB5A86F1F (PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A  ___graph0, int32_t ___inputCount1, bool ___normalizeWeights2, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableExtensions::SetInputWeight<UnityEngine.Animations.AnimationMixerPlayable>(!!0,System.Int32,System.Single)
inline void PlayableExtensions_SetInputWeight_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mDB955B1A400A2B2FAC115FF40524DFE4F8E34652 (AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  ___playable0, int32_t ___inputIndex1, float ___weight2, const RuntimeMethod* method)
{
	((  void (*) (AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741 , int32_t, float, const RuntimeMethod*))PlayableExtensions_SetInputWeight_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mDB955B1A400A2B2FAC115FF40524DFE4F8E34652_gshared)(___playable0, ___inputIndex1, ___weight2, method);
}
// UnityEngine.Animations.AnimationClipPlayable UnityEngine.Animations.AnimationClipPlayable::Create(UnityEngine.Playables.PlayableGraph,UnityEngine.AnimationClip)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953  AnimationClipPlayable_Create_mEB660B81F10778CE974F987D2C42C9921FB6A468 (PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A  ___graph0, AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * ___clip1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Playables.PlayableGraph::Connect<UnityEngine.Animations.AnimationClipPlayable,UnityEngine.Animations.AnimationMixerPlayable>(!!0,System.Int32,!!1,System.Int32)
inline bool PlayableGraph_Connect_TisAnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_m51D9B475A8831D92BBA52DC12B1D1FFCFDD79451 (PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A * __this, AnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953  ___source0, int32_t ___sourceOutputPort1, AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  ___destination2, int32_t ___destinationInputPort3, const RuntimeMethod* method)
{
	return ((  bool (*) (PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A *, AnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953 , int32_t, AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741 , int32_t, const RuntimeMethod*))PlayableGraph_Connect_TisAnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_m51D9B475A8831D92BBA52DC12B1D1FFCFDD79451_gshared)(__this, ___source0, ___sourceOutputPort1, ___destination2, ___destinationInputPort3, method);
}
// UnityEngine.Animations.AnimationPlayableOutput UnityEngine.Animations.AnimationPlayableOutput::Create(UnityEngine.Playables.PlayableGraph,System.String,UnityEngine.Animator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AnimationPlayableOutput_t14570F3E63619E52ABB0B0306D4F4AAA6225DE17  AnimationPlayableOutput_Create_m421CC6D39B79DD5C1E2A88EA4B7F98E433AE8AFC (PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A  ___graph0, String_t* ___name1, Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___target2, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableOutputExtensions::SetSourcePlayable<UnityEngine.Animations.AnimationPlayableOutput,UnityEngine.Animations.AnimationMixerPlayable>(!!0,!!1)
inline void PlayableOutputExtensions_SetSourcePlayable_TisAnimationPlayableOutput_t14570F3E63619E52ABB0B0306D4F4AAA6225DE17_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mE29738BA829F39EDAFCB18988825867C07ED0B42 (AnimationPlayableOutput_t14570F3E63619E52ABB0B0306D4F4AAA6225DE17  ___output0, AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  ___value1, const RuntimeMethod* method)
{
	((  void (*) (AnimationPlayableOutput_t14570F3E63619E52ABB0B0306D4F4AAA6225DE17 , AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741 , const RuntimeMethod*))PlayableOutputExtensions_SetSourcePlayable_TisAnimationPlayableOutput_t14570F3E63619E52ABB0B0306D4F4AAA6225DE17_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mE29738BA829F39EDAFCB18988825867C07ED0B42_gshared)(___output0, ___value1, method);
}
// System.Void UnityEngine.Playables.PlayableGraph::Evaluate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayableGraph_Evaluate_m57E3E70BCBB2C87C131D840170E66F168BA5A4F3 (PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Playables.PlayableGraph::Destroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayableGraph_Destroy_mD237E1367E2E370C77D16564BA85668E8FCFF03F (PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A * __this, const RuntimeMethod* method);
// UnityEngine.Canvas E7.NotchSolution.NotchSolutionUIBehaviourBase::<GetCanvasRect>g__GetTopLevelCanvas|1_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * NotchSolutionUIBehaviourBase_U3CGetCanvasRectU3Eg__GetTopLevelCanvasU7C1_0_m626582600C4FD53CDEFE76B89E9759B9F4125C5D (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  RectTransform_get_sizeDelta_mCFAE8C916280C173AB79BE32B910376E310D1C50 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828 (const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m00C682F84642AE657D7EBB0D5BC6E8F3CA4D1E82 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___position0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___size1, const RuntimeMethod* method);
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::UpdateRectBase()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotchSolutionUIBehaviourBase_UpdateRectBase_m1D4EAA1203356D6F91B0BCFC3947B950ECB849F9 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.EventSystems.UIBehaviour::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIBehaviour_OnEnable_m9BE8F521B232703E4A0EF14EA43F264EDAF3B3F0 (UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E * __this, const RuntimeMethod* method);
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::DelayedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotchSolutionUIBehaviourBase_DelayedUpdate_m25321FD361325D51D930F734D165480207AA5957 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrivenRectTransformTracker_Clear_m41F9B0AA2025AF5B76D38E68B08C111D7D8EB845 (DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * __this, const RuntimeMethod* method);
// UnityEngine.RectTransform E7.NotchSolution.NotchSolutionUIBehaviourBase::get_rectTransform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutForRebuild(UnityEngine.RectTransform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LayoutRebuilder_MarkLayoutForRebuild_m1BDFA10259B85AEBD3A758B78EF4702BE014D1FE (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___rect0, const RuntimeMethod* method);
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIBehaviour_OnDisable_m7D3E0D1AC43330C5A50B17DD296D2CB84994CA23 (UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Behaviour::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeInHierarchy_mA3990AC5F61BB35283188E925C2BE7F7BF67734B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator E7.NotchSolution.NotchSolutionUIBehaviourBase::DelayedUpdateRoutine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NotchSolutionUIBehaviourBase_DelayedUpdateRoutine_mBE1625FF6D9BFE992EDD6961BB56E10DA2DB8832 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase/<DelayedUpdateRoutine>d__19::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayedUpdateRoutineU3Ed__19__ctor_m559AAD87FE3298DA82D9DE2FB988188E7F0B4DE3 (U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.EventSystems.UIBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIBehaviour__ctor_m869436738107AF382FD4D10DE9641F8241B323C7 (UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInParent<UnityEngine.Canvas>()
inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * Component_GetComponentInParent_TisCanvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_m79D616348A09F5E2973F405F4F9D944744FAD6A5 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentInParent_TisRuntimeObject_mADA186D1675BEA6779C469918206294354385EC3_gshared)(__this, method);
}
// UnityEngine.Canvas UnityEngine.Canvas::get_rootCanvas()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * Canvas_get_rootCanvas_mB1C93410A4AA793D88130FD08C05D71327641DC5 (Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * __this, const RuntimeMethod* method);
// UnityEngine.Rect UnityEngine.Screen::get_safeArea()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  Screen_get_safeArea_m47E3A61627ECEAC2336C1FBFBF67A6C7503569F5 (const RuntimeMethod* method);
// UnityEngine.Rect E7.NotchSolution.NotchSolutionUtility::ToScreenRelativeRect(UnityEngine.Rect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  NotchSolutionUtility_ToScreenRelativeRect_m722FC3D28A744CB322BF96B19642D63B62C4166B (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___absoluteRect0, const RuntimeMethod* method);
// UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  Screen_get_currentResolution_mE1A3C7E9603FA56B539FDDA1F602C66732EFD17B (const RuntimeMethod* method);
// System.Int32 UnityEngine.Resolution::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Resolution_get_width_mDD9DCC89D65057B64C413AF15BEE2E37E9892065 (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Resolution::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Resolution_get_height_mB90F24337D7B96A288F8BE1D0F2F3599B785AD27 (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_x()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_x_mA61220F6F26ECD6951B779FFA7CAD7ECE11D6987 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_y()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_y_m4E1AAD20D167085FF4F9E9C86EF34689F5770A74 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method);
// UnityEngine.Rect[] UnityEngine.Screen::get_cutouts()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* Screen_get_cutouts_m3EFA8304695B6B5134F51A696D3A173CC7B114A0 (const RuntimeMethod* method);
// System.Void E7.NotchSolution.PerEdgeValues`1<E7.NotchSolution.BlendedClipsAdaptor>::.ctor()
inline void PerEdgeValues_1__ctor_mC5C5C66BA0E9A56A888E35BA9083154E64FC61EC (PerEdgeValues_1_t1A42987AA6752FBE3EF2200F32ECE41EC4728CB1 * __this, const RuntimeMethod* method)
{
	((  void (*) (PerEdgeValues_1_t1A42987AA6752FBE3EF2200F32ECE41EC4728CB1 *, const RuntimeMethod*))PerEdgeValues_1__ctor_m21C9D595E4D12E51118E3AC003884749A9B86A53_gshared)(__this, method);
}
// System.Void E7.NotchSolution.PerEdgeValues`1<E7.NotchSolution.EdgeEvaluationMode>::.ctor()
inline void PerEdgeValues_1__ctor_mB9B72BAEB18D43AA262C35DC74889A042A1B78B2 (PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE * __this, const RuntimeMethod* method)
{
	((  void (*) (PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE *, const RuntimeMethod*))PerEdgeValues_1__ctor_mB579A4F8D7DB9058A0F33EC590E87A07180B1538_gshared)(__this, method);
}
// UnityEngine.AnimationCurve UnityEngine.AnimationCurve::Linear(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * AnimationCurve_Linear_mE61D9A684B81B47365C299B6B9C5D744FC9DFBFA (float ___timeStart0, float ___valueStart1, float ___timeEnd2, float ___valueEnd3, const RuntimeMethod* method);
// UnityEngine.Rect E7.NotchSolution.AdaptationBase::get_SafeAreaRelative()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  AdaptationBase_get_SafeAreaRelative_m1C2D114B34B46E26679DD3775B2BE73F002B2416 (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, const RuntimeMethod* method);
// System.Void E7.NotchSolution.SafeAdaptation::AdaptWithRelativeSafeArea(UnityEngine.Rect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SafeAdaptation_AdaptWithRelativeSafeArea_m8B4F6C7CECE7AB36DA4CA688C18A4AEFC76AC589 (SafeAdaptation_t0DEF9DC095FCB9608021EA9A07ADA433CBBB6634 * __this, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___relativeSafeArea0, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_xMin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_xMax()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_xMax_m174FFAACE6F19A59AA793B3D507BE70116E27DE5 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_yMax()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_yMax_m9685BF55B44C51FF9BA080F9995073E458E1CDC3 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_yMin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775 (float ___a0, float ___b1, const RuntimeMethod* method);
// System.Boolean E7.NotchSolution.SafePadding::<UpdateRect>g__LockSide|5_0(E7.NotchSolution.EdgeEvaluationMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SafePadding_U3CUpdateRectU3Eg__LockSideU7C5_0_m869D49FE6FDB56D9F3675596090D0B643A82C974 (int32_t ___saem0, const RuntimeMethod* method);
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DrivenRectTransformTracker_Add_m65814604ABCE8B9F81270F3C2E1632CCB9E9A5E7 (DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * __this, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___driver0, RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___rectTransform1, int32_t ___drivenProperties2, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchorMin_mD9E6E95890B701A5190C12F5AE42E622246AF798 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_one_m9B2AFD26404B6DD0F520D19FC7F79371C5C18B42 (const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchorMax_m67E04F54B5122804E32019D5FAE50C21CC67651D (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// UnityEngine.Rect E7.NotchSolution.NotchSolutionUIBehaviourBase::GetCanvasRect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  NotchSolutionUIBehaviourBase_GetCanvasRect_m6664B2634FBD335D5350B41D855AA16F8A8A8890 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method);
// UnityEngine.Rect E7.NotchSolution.NotchSolutionUIBehaviourBase::get_SafeAreaRelative()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  NotchSolutionUIBehaviourBase_get_SafeAreaRelative_m4229E4426CF2727B0EC2F0E47258A1C6EEB106BD (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method);
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  RectTransform_get_rect_m7B24A1D6E0CB87F3481DDD2584C82C97025404E2 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_sizeDelta_m61943618442E31C6FF0556CDFC70940AE7AD04D0 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  RectTransform_get_pivot_m146F0BB5D3873FCEF3606DAFB8994BFA978095EE (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.RectTransform::get_anchoredPosition3D()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  RectTransform_get_anchoredPosition3D_mA9E9CCB2E97E4DCE93CF7194856F831E48F678A2 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.RectTransform::set_anchoredPosition3D(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchoredPosition3D_mD232BFB736C35B6F3367E1D63BBA6FAE098DA761 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotchSolutionUIBehaviourBase__ctor_m06D19A834AF9C336013AA1322C9F9D3233D281A0 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/KeyCollection<!0,!1> System.Collections.Generic.Dictionary`2<UnityEngine.ScreenOrientation,E7.NotchSolution.OrientationDependentData>::get_Keys()
inline KeyCollection_t42ABC2A1D8DF067994FAE1C29046ED791B7752E1 * Dictionary_2_get_Keys_m78E2C29CA07F0853ADB4CF9D388BBA35F392C058 (Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * __this, const RuntimeMethod* method)
{
	return ((  KeyCollection_t42ABC2A1D8DF067994FAE1C29046ED791B7752E1 * (*) (Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE *, const RuntimeMethod*))Dictionary_2_get_Keys_m19022C12B8702F3A79910A4E45A1F4215E5FE16F_gshared)(__this, method);
}
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.ScreenOrientation>(System.Collections.Generic.IEnumerable`1<!!0>)
inline ScreenOrientationU5BU5D_t7132DACB88FB07021C6A10D0EBED5B86B66BA05F* Enumerable_ToArray_TisScreenOrientation_tDD9EF2729A0D580721770597532935B0A7ADE020_m1584F65FE299B571F83B0E1CC97141F31B02E804 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  ScreenOrientationU5BU5D_t7132DACB88FB07021C6A10D0EBED5B86B66BA05F* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisInt32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C_mF08E57F5EA873295A63AF0479B7991393205E4E4_gshared)(___source0, method);
}
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<UnityEngine.ScreenOrientation,E7.NotchSolution.OrientationDependentData>::get_Values()
inline ValueCollection_t4F97D9B4470FCD9B1FB63D4852EE80A94A173F62 * Dictionary_2_get_Values_mF57C2399DD74B23C81A6675DB50B0669DB946C29 (Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * __this, const RuntimeMethod* method)
{
	return ((  ValueCollection_t4F97D9B4470FCD9B1FB63D4852EE80A94A173F62 * (*) (Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE *, const RuntimeMethod*))Dictionary_2_get_Values_mCE8DFA32761CC8C69390885B07388FB6B221F055_gshared)(__this, method);
}
// !!0[] System.Linq.Enumerable::ToArray<E7.NotchSolution.OrientationDependentData>(System.Collections.Generic.IEnumerable`1<!!0>)
inline OrientationDependentDataU5BU5D_t7D9034DF05ACF0C0ED3E9D0E8A1AE964A815176E* Enumerable_ToArray_TisOrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D_m5DAC4A729E2BCAA0A028DAC48018784D5FD6A29C (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  OrientationDependentDataU5BU5D_t7D9034DF05ACF0C0ED3E9D0E8A1AE964A815176E* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_m21E15191FE8BDBAE753CC592A1DB55EA3BCE7B5B_gshared)(___source0, method);
}
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.ScreenOrientation,E7.NotchSolution.OrientationDependentData>::.ctor()
inline void Dictionary_2__ctor_m762448D06E873D5D27DF97E793F29B6F4B825CC4 (Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE *, const RuntimeMethod*))Dictionary_2__ctor_mF42565DC9AD476065ED33869AD6DC710F775F641_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.ScreenOrientation,E7.NotchSolution.OrientationDependentData>::Add(!0,!1)
inline void Dictionary_2_Add_mF1B671F6CC3E8D0F730A1BE22291122B4002B8C2 (Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * __this, int32_t ___key0, OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D * ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE *, int32_t, OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D *, const RuntimeMethod*))Dictionary_2_Add_mEA7DC2B06A480A5EC7DE49B6E83C2D121D1962EF_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void E7.NotchSolution.AdaptationBase::ResetAdaptationToCurve(UnityEngine.AnimationCurve)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptationBase_ResetAdaptationToCurve_m0DB3E2F55F05CC0985FB73ADB5FB5CF824E52E1E (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___curve0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptationBase_ResetAdaptationToCurve_m0DB3E2F55F05CC0985FB73ADB5FB5CF824E52E1E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// portraitOrDefaultAdaptation = new BlendedClipsAdaptor(curve);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_0 = ___curve0;
		BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * L_1 = (BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 *)il2cpp_codegen_object_new(BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7_il2cpp_TypeInfo_var);
		BlendedClipsAdaptor__ctor_mFFDA7697D389E0F084755D691CB40360B8BD2E29(L_1, L_0, /*hidden argument*/NULL);
		__this->set_portraitOrDefaultAdaptation_5(L_1);
		// landscapeAdaptation = new BlendedClipsAdaptor(curve);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_2 = ___curve0;
		BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * L_3 = (BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 *)il2cpp_codegen_object_new(BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7_il2cpp_TypeInfo_var);
		BlendedClipsAdaptor__ctor_mFFDA7697D389E0F084755D691CB40360B8BD2E29(L_3, L_2, /*hidden argument*/NULL);
		__this->set_landscapeAdaptation_6(L_3);
		// }
		return;
	}
}
// E7.NotchSolution.BlendedClipsAdaptor E7.NotchSolution.AdaptationBase::get_SelectedAdaptation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * AdaptationBase_get_SelectedAdaptation_mAB19172E9F514C64643490A948AB0AAD717BE2DB (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptationBase_get_SelectedAdaptation_mAB19172E9F514C64643490A948AB0AAD717BE2DB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// supportedOrientations == SupportedOrientations.Dual ?
		//     NotchSolutionUtility.GetCurrentOrientation() == ScreenOrientation.Landscape ?
		//         landscapeAdaptation : portraitOrDefaultAdaptation
		// : portraitOrDefaultAdaptation;
		int32_t L_0 = __this->get_supportedOrientations_4();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0010;
		}
	}
	{
		BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * L_1 = __this->get_portraitOrDefaultAdaptation_5();
		return L_1;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var);
		int32_t L_2 = NotchSolutionUtility_GetCurrentOrientation_m847578DED8F9B964755A0C64C9FADD3FDB27E3A7(/*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_001f;
		}
	}
	{
		BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * L_3 = __this->get_portraitOrDefaultAdaptation_5();
		return L_3;
	}

IL_001f:
	{
		BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * L_4 = __this->get_landscapeAdaptation_6();
		return L_4;
	}
}
// UnityEngine.Animator E7.NotchSolution.AdaptationBase::get_AnimatorComponent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * AdaptationBase_get_AnimatorComponent_m664E3D528CD811A9ACF493C6A96230815528E337 (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptationBase_get_AnimatorComponent_m664E3D528CD811A9ACF493C6A96230815528E337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private Animator AnimatorComponent => GetComponent<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		return L_0;
	}
}
// System.Void E7.NotchSolution.AdaptationBase::Adapt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptationBase_Adapt_m1D2C2F404AB717AC81D03326CF3EA15C04C1D8FE (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, float ___valueForAdaptationCurve0, const RuntimeMethod* method)
{
	{
		// protected void Adapt(float valueForAdaptationCurve) => SelectedAdaptation.Adapt(valueForAdaptationCurve, AnimatorComponent);
		BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * L_0 = AdaptationBase_get_SelectedAdaptation_mAB19172E9F514C64643490A948AB0AAD717BE2DB(__this, /*hidden argument*/NULL);
		float L_1 = ___valueForAdaptationCurve0;
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_2 = AdaptationBase_get_AnimatorComponent_m664E3D528CD811A9ACF493C6A96230815528E337(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		BlendedClipsAdaptor_Adapt_m47AA5EC9A764C6D05110D19E834B6ECB8AC7B29B(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void E7.NotchSolution.AdaptationBase::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptationBase_Start_mEEE70073F983A0D6AB3B1C342D9E833019FF0F2F (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, const RuntimeMethod* method)
{
	{
		// void Start() => Adapt();
		VirtActionInvoker0::Invoke(5 /* System.Void E7.NotchSolution.AdaptationBase::Adapt() */, __this);
		return;
	}
}
// System.Void E7.NotchSolution.AdaptationBase::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptationBase_Update_m7882EEB7AD4BB2944D16D3796AAEDD3230468004 (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, const RuntimeMethod* method)
{
	{
		// if(Application.isPlaying == false)
		bool L_0 = Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		// Adapt();
		VirtActionInvoker0::Invoke(5 /* System.Void E7.NotchSolution.AdaptationBase::Adapt() */, __this);
	}

IL_000d:
	{
		// }
		return;
	}
}
// System.Void E7.NotchSolution.AdaptationBase::E7.NotchSolution.INotchSimulatorTarget.SimulatorUpdate(UnityEngine.Rect,UnityEngine.Rect[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptationBase_E7_NotchSolution_INotchSimulatorTarget_SimulatorUpdate_m89BE82FC61EACE35C4EA45DE1021463A75EB3F72 (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___simulatedSafeAreaRelative0, RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* ___simulatedCutoutsRelative1, const RuntimeMethod* method)
{
	{
		// this.storedSimulatedSafeAreaRelative = simulatedSafeAreaRelative;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_0 = ___simulatedSafeAreaRelative0;
		__this->set_storedSimulatedSafeAreaRelative_7(L_0);
		// this.storedSimulatedCutoutsRelative = simulatedCutoutsRelative;
		RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* L_1 = ___simulatedCutoutsRelative1;
		__this->set_storedSimulatedCutoutsRelative_8(L_1);
		// Adapt();
		VirtActionInvoker0::Invoke(5 /* System.Void E7.NotchSolution.AdaptationBase::Adapt() */, __this);
		// }
		return;
	}
}
// UnityEngine.Rect E7.NotchSolution.AdaptationBase::get_SafeAreaRelative()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  AdaptationBase_get_SafeAreaRelative_m1C2D114B34B46E26679DD3775B2BE73F002B2416 (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptationBase_get_SafeAreaRelative_m1C2D114B34B46E26679DD3775B2BE73F002B2416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// => NotchSolutionUtility.ShouldUseNotchSimulatorValue ? storedSimulatedSafeAreaRelative : NotchSolutionUtility.ScreenSafeAreaRelative;
		IL2CPP_RUNTIME_CLASS_INIT(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var);
		bool L_0 = NotchSolutionUtility_get_ShouldUseNotchSimulatorValue_m3C6D486B8587587EB12650930FF6ED47DAE7BA41(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_1 = NotchSolutionUtility_get_ScreenSafeAreaRelative_mA401C9F104714B1D8753556BDB9EC91DF63F76FC(/*hidden argument*/NULL);
		return L_1;
	}

IL_000d:
	{
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_2 = __this->get_storedSimulatedSafeAreaRelative_7();
		return L_2;
	}
}
// System.Void E7.NotchSolution.AdaptationBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AdaptationBase__ctor_m50D7CDA55A7F6682D8F780D72F758DA3CF966DF3 (AdaptationBase_t162568CF16939D2C21E471F8506DE0F4EAD158EF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AdaptationBase__ctor_m50D7CDA55A7F6682D8F780D72F758DA3CF966DF3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private Rect storedSimulatedSafeAreaRelative = NotchSolutionUtility.defaultSafeArea;
		IL2CPP_RUNTIME_CLASS_INIT(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_0 = ((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->get_defaultSafeArea_0();
		__this->set_storedSimulatedSafeAreaRelative_7(L_0);
		// private Rect[] storedSimulatedCutoutsRelative = NotchSolutionUtility.defaultCutouts;
		RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* L_1 = ((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->get_defaultCutouts_1();
		__this->set_storedSimulatedCutoutsRelative_8(L_1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void E7.NotchSolution.AspectRatioAdaptation::Adapt()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspectRatioAdaptation_Adapt_m22C1B967F342E974D9D50C15158F5C1CC415BEF0 (AspectRatioAdaptation_t48754AD46C3A23666F1FCD9FE921A682B598CE4D * __this, const RuntimeMethod* method)
{
	{
		// public override void Adapt() => base.Adapt(valueForAdaptationCurve: AspectRatio);
		float L_0 = AspectRatioAdaptation_get_AspectRatio_m9568A2021F105A6C7B1D97DEA791C3290EDACD42(__this, /*hidden argument*/NULL);
		AdaptationBase_Adapt_m1D2C2F404AB717AC81D03326CF3EA15C04C1D8FE(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void E7.NotchSolution.AspectRatioAdaptation::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspectRatioAdaptation_Reset_m0B7D50FDCE4990D220F50D8E39839DF97E3167F3 (AspectRatioAdaptation_t48754AD46C3A23666F1FCD9FE921A682B598CE4D * __this, const RuntimeMethod* method)
{
	{
		// ResetAdaptationToCurve(GenDefaultCurve());
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_0 = AspectRatioAdaptation_U3CResetU3Eg__GenDefaultCurveU7C1_0_m4F275F34394578EBBBB199873FB2C391C3D3049C(/*hidden argument*/NULL);
		AdaptationBase_ResetAdaptationToCurve_m0DB3E2F55F05CC0985FB73ADB5FB5CF824E52E1E(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Single E7.NotchSolution.AspectRatioAdaptation::get_AspectRatio()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AspectRatioAdaptation_get_AspectRatio_m9568A2021F105A6C7B1D97DEA791C3290EDACD42 (AspectRatioAdaptation_t48754AD46C3A23666F1FCD9FE921A682B598CE4D * __this, const RuntimeMethod* method)
{
	{
		// bool landscape = Screen.width > Screen.height;
		int32_t L_0 = Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C(/*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE(/*hidden argument*/NULL);
		// return landscape ? Screen.width / (float)Screen.height : Screen.height / (float)Screen.width;
		if (((((int32_t)L_0) > ((int32_t)L_1))? 1 : 0))
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE(/*hidden argument*/NULL);
		int32_t L_3 = Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C(/*hidden argument*/NULL);
		return ((float)((float)(((float)((float)L_2)))/(float)(((float)((float)L_3)))));
	}

IL_001c:
	{
		int32_t L_4 = Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C(/*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE(/*hidden argument*/NULL);
		return ((float)((float)(((float)((float)L_4)))/(float)(((float)((float)L_5)))));
	}
}
// System.Void E7.NotchSolution.AspectRatioAdaptation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AspectRatioAdaptation__ctor_mCEC39EF7263F13BF39BA057B169C0558546A7597 (AspectRatioAdaptation_t48754AD46C3A23666F1FCD9FE921A682B598CE4D * __this, const RuntimeMethod* method)
{
	{
		AdaptationBase__ctor_m50D7CDA55A7F6682D8F780D72F758DA3CF966DF3(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AnimationCurve E7.NotchSolution.AspectRatioAdaptation::<Reset>g__GenDefaultCurveU7C1_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * AspectRatioAdaptation_U3CResetU3Eg__GenDefaultCurveU7C1_0_m4F275F34394578EBBBB199873FB2C391C3D3049C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AspectRatioAdaptation_U3CResetU3Eg__GenDefaultCurveU7C1_0_m4F275F34394578EBBBB199873FB2C391C3D3049C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		//  new AnimationCurve(new Keyframe[]
		// {
		//     new Keyframe( 4f/3f, 0,1.2f,1.2f), 
		//     new Keyframe( 16f/9f, Mathf.InverseLerp(4/3f, 19.5f/9f, 16/9f), 1.2f, 1.2f), 
		//     new Keyframe( 19.5f/9f, 1, 1.2f,1.2f), 
		// });
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_m572CCEE06F612003F939F3FF439B15F89E8C1D54((&L_2), (1.33333337f), (0.0f), (1.20000005f), (1.20000005f), /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var);
		float L_4 = Mathf_InverseLerp_mCD2E6F9ADCFFB40EB7D3086E444DF2C702F9C29B((1.33333337f), (2.16666675f), (1.77777779f), /*hidden argument*/NULL);
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Keyframe__ctor_m572CCEE06F612003F939F3FF439B15F89E8C1D54((&L_5), (1.77777779f), L_4, (1.20000005f), (1.20000005f), /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_5);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_6 = L_3;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_7;
		memset((&L_7), 0, sizeof(L_7));
		Keyframe__ctor_m572CCEE06F612003F939F3FF439B15F89E8C1D54((&L_7), (2.16666675f), (1.0f), (1.20000005f), (1.20000005f), /*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_7);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_8 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_8, L_6, /*hidden argument*/NULL);
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void E7.NotchSolution.BlendedClipsAdaptor::.ctor(UnityEngine.AnimationCurve)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlendedClipsAdaptor__ctor_mFFDA7697D389E0F084755D691CB40360B8BD2E29 (BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___defaultCurve0, const RuntimeMethod* method)
{
	{
		// public BlendedClipsAdaptor(AnimationCurve defaultCurve)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// this.adaptationCurve = defaultCurve;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_0 = ___defaultCurve0;
		__this->set_adaptationCurve_2(L_0);
		// }
		return;
	}
}
// System.Boolean E7.NotchSolution.BlendedClipsAdaptor::get_Adaptable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BlendedClipsAdaptor_get_Adaptable_m9D2737CCB6C65D8A6FCBAA0731E9BF0FD5C820CE (BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlendedClipsAdaptor_get_Adaptable_m9D2737CCB6C65D8A6FCBAA0731E9BF0FD5C820CE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal bool Adaptable => !(adaptationCurve == null || normalState == null || fullyAdaptedState == null);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_0 = __this->get_adaptationCurve_2();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * L_1 = __this->get_normalState_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0026;
		}
	}
	{
		AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * L_3 = __this->get_fullyAdaptedState_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}

IL_0026:
	{
		return (bool)0;
	}
}
// System.Void E7.NotchSolution.BlendedClipsAdaptor::Adapt(System.Single,UnityEngine.Animator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BlendedClipsAdaptor_Adapt_m47AA5EC9A764C6D05110D19E834B6ECB8AC7B29B (BlendedClipsAdaptor_t203B0BC38D4CC97E22E88FFC74EDEE06A1604DF7 * __this, float ___valueForAdaptationCurve0, Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___animator1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BlendedClipsAdaptor_Adapt_m47AA5EC9A764C6D05110D19E834B6ECB8AC7B29B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A  V_1;
	memset((&V_1), 0, sizeof(V_1));
	AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  V_2;
	memset((&V_2), 0, sizeof(V_2));
	AnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953  V_3;
	memset((&V_3), 0, sizeof(V_3));
	AnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// if (!Adaptable) return;
		bool L_0 = BlendedClipsAdaptor_get_Adaptable_m9D2737CCB6C65D8A6FCBAA0731E9BF0FD5C820CE(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (!Adaptable) return;
		return;
	}

IL_0009:
	{
		// float blend = adaptationCurve.Evaluate(valueForAdaptationCurve);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_1 = __this->get_adaptationCurve_2();
		float L_2 = ___valueForAdaptationCurve0;
		NullCheck(L_1);
		float L_3 = AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// PlayableGraph pg = PlayableGraph.Create("AdaptationGraph");
		PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A  L_4 = PlayableGraph_Create_mA33157C9AB7F53A7850C1427EFB11773ACA74872(_stringLiteral563324936E1F9C24050C5F40DA01938A4D7E2F41, /*hidden argument*/NULL);
		V_1 = L_4;
		// pg.SetTimeUpdateMode(DirectorUpdateMode.Manual);
		PlayableGraph_SetTimeUpdateMode_mF78E1782C4707F10170370446FF3959DAA6CF844((PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A *)(&V_1), 3, /*hidden argument*/NULL);
		// var mixer = AnimationMixerPlayable.Create(pg, 2, normalizeWeights: true);
		PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A  L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_il2cpp_TypeInfo_var);
		AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  L_6 = AnimationMixerPlayable_Create_mA961D86F5F73ADCA1F0650088B6214CEB5A86F1F(L_5, 2, (bool)1, /*hidden argument*/NULL);
		V_2 = L_6;
		// mixer.SetInputWeight(inputIndex: 0, weight: 1 - blend);
		AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  L_7 = V_2;
		float L_8 = V_0;
		PlayableExtensions_SetInputWeight_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mDB955B1A400A2B2FAC115FF40524DFE4F8E34652(L_7, 0, ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_8)), /*hidden argument*/PlayableExtensions_SetInputWeight_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mDB955B1A400A2B2FAC115FF40524DFE4F8E34652_RuntimeMethod_var);
		// mixer.SetInputWeight(inputIndex: 1, weight: blend);
		AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  L_9 = V_2;
		float L_10 = V_0;
		PlayableExtensions_SetInputWeight_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mDB955B1A400A2B2FAC115FF40524DFE4F8E34652(L_9, 1, L_10, /*hidden argument*/PlayableExtensions_SetInputWeight_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mDB955B1A400A2B2FAC115FF40524DFE4F8E34652_RuntimeMethod_var);
		// var normalStateAcp = AnimationClipPlayable.Create(pg, normalState);
		PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A  L_11 = V_1;
		AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * L_12 = __this->get_normalState_0();
		AnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953  L_13 = AnimationClipPlayable_Create_mEB660B81F10778CE974F987D2C42C9921FB6A468(L_11, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		// var fullyAdaptedStateAcp = AnimationClipPlayable.Create(pg, fullyAdaptedState);
		PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A  L_14 = V_1;
		AnimationClip_tD9BFD73D43793BA608D5C0B46BE29EB59E40D178 * L_15 = __this->get_fullyAdaptedState_1();
		AnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953  L_16 = AnimationClipPlayable_Create_mEB660B81F10778CE974F987D2C42C9921FB6A468(L_14, L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		// pg.Connect(normalStateAcp, sourceOutputPort: 0, mixer, destinationInputPort: 0);
		AnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953  L_17 = V_3;
		AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  L_18 = V_2;
		PlayableGraph_Connect_TisAnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_m51D9B475A8831D92BBA52DC12B1D1FFCFDD79451((PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A *)(&V_1), L_17, 0, L_18, 0, /*hidden argument*/PlayableGraph_Connect_TisAnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_m51D9B475A8831D92BBA52DC12B1D1FFCFDD79451_RuntimeMethod_var);
		// pg.Connect(fullyAdaptedStateAcp, sourceOutputPort: 0, mixer, destinationInputPort: 1);
		AnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953  L_19 = V_4;
		AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  L_20 = V_2;
		PlayableGraph_Connect_TisAnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_m51D9B475A8831D92BBA52DC12B1D1FFCFDD79451((PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A *)(&V_1), L_19, 0, L_20, 1, /*hidden argument*/PlayableGraph_Connect_TisAnimationClipPlayable_t6386488B0C0300A21A352B4C17B9E6D5D38DF953_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_m51D9B475A8831D92BBA52DC12B1D1FFCFDD79451_RuntimeMethod_var);
		// var output = AnimationPlayableOutput.Create(pg, "AdaptationGraphOutput", animator);
		PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A  L_21 = V_1;
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_22 = ___animator1;
		AnimationPlayableOutput_t14570F3E63619E52ABB0B0306D4F4AAA6225DE17  L_23 = AnimationPlayableOutput_Create_m421CC6D39B79DD5C1E2A88EA4B7F98E433AE8AFC(L_21, _stringLiteral5FF35238BBF2981025FC471BD32679FF32514282, L_22, /*hidden argument*/NULL);
		// output.SetSourcePlayable(mixer);
		AnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741  L_24 = V_2;
		PlayableOutputExtensions_SetSourcePlayable_TisAnimationPlayableOutput_t14570F3E63619E52ABB0B0306D4F4AAA6225DE17_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mE29738BA829F39EDAFCB18988825867C07ED0B42(L_23, L_24, /*hidden argument*/PlayableOutputExtensions_SetSourcePlayable_TisAnimationPlayableOutput_t14570F3E63619E52ABB0B0306D4F4AAA6225DE17_TisAnimationMixerPlayable_t7C6D407FE0D55712B07081F8114CFA347F124741_mE29738BA829F39EDAFCB18988825867C07ED0B42_RuntimeMethod_var);
		// pg.Evaluate();
		PlayableGraph_Evaluate_m57E3E70BCBB2C87C131D840170E66F168BA5A4F3((PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A *)(&V_1), /*hidden argument*/NULL);
		// pg.Destroy();
		PlayableGraph_Destroy_mD237E1367E2E370C77D16564BA85668E8FCFF03F((PlayableGraph_t2D5083CFACB413FA1BB13FF054BE09A5A55A205A *)(&V_1), /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void E7.NotchSolution.GraphicsDependentSystemInfoData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicsDependentSystemInfoData__ctor_m030B8A6FA78B5C8191076DA6BB9A6B4674C322B0 (GraphicsDependentSystemInfoData_tE5BF49387F34A4CDF9863F044F1524BE6E2C9912 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void E7.NotchSolution.MetaData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaData__ctor_mEF4BED7876B925356A2FC06A1381B81ABDBBC98C (MetaData_t22BDEAB65B452CADFA38BB83AFD80ED804BE2828 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Rect E7.NotchSolution.NotchSolutionUIBehaviourBase::GetCanvasRect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  NotchSolutionUIBehaviourBase_GetCanvasRect_m6664B2634FBD335D5350B41D855AA16F8A8A8890 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NotchSolutionUIBehaviourBase_GetCanvasRect_m6664B2634FBD335D5350B41D855AA16F8A8A8890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var topLevelCanvas = GetTopLevelCanvas();
		Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_0 = NotchSolutionUIBehaviourBase_U3CGetCanvasRectU3Eg__GetTopLevelCanvasU7C1_0_m626582600C4FD53CDEFE76B89E9759B9F4125C5D(__this, /*hidden argument*/NULL);
		// Vector2 topRectSize = topLevelCanvas.GetComponent<RectTransform>().sizeDelta;
		NullCheck(L_0);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_1 = Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79(L_0, /*hidden argument*/Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79_RuntimeMethod_var);
		NullCheck(L_1);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = RectTransform_get_sizeDelta_mCFAE8C916280C173AB79BE32B910376E310D1C50(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// return new Rect(Vector2.zero, topRectSize);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_il2cpp_TypeInfo_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828(/*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = V_0;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Rect__ctor_m00C682F84642AE657D7EBB0D5BC6E8F3CA4D1E82((&L_5), L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::E7.NotchSolution.INotchSimulatorTarget.SimulatorUpdate(UnityEngine.Rect,UnityEngine.Rect[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotchSolutionUIBehaviourBase_E7_NotchSolution_INotchSimulatorTarget_SimulatorUpdate_m89E4FE246C1D714706656787C4D67C59D8BF885F (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___simulatedSafeAreaRelative0, RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* ___simulatedCutoutsRelative1, const RuntimeMethod* method)
{
	{
		// this.storedSimulatedSafeAreaRelative = simulatedSafeAreaRelative;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_0 = ___simulatedSafeAreaRelative0;
		__this->set_storedSimulatedSafeAreaRelative_4(L_0);
		// this.storedSimulatedCutoutsRelative = simulatedCutoutsRelative;
		RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* L_1 = ___simulatedCutoutsRelative1;
		__this->set_storedSimulatedCutoutsRelative_5(L_1);
		// UpdateRectBase();
		NotchSolutionUIBehaviourBase_UpdateRectBase_m1D4EAA1203356D6F91B0BCFC3947B950ECB849F9(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Rect E7.NotchSolution.NotchSolutionUIBehaviourBase::get_SafeAreaRelative()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  NotchSolutionUIBehaviourBase_get_SafeAreaRelative_m4229E4426CF2727B0EC2F0E47258A1C6EEB106BD (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NotchSolutionUIBehaviourBase_get_SafeAreaRelative_m4229E4426CF2727B0EC2F0E47258A1C6EEB106BD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// => NotchSolutionUtility.ShouldUseNotchSimulatorValue ? storedSimulatedSafeAreaRelative : NotchSolutionUtility.ScreenSafeAreaRelative;
		IL2CPP_RUNTIME_CLASS_INIT(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var);
		bool L_0 = NotchSolutionUtility_get_ShouldUseNotchSimulatorValue_m3C6D486B8587587EB12650930FF6ED47DAE7BA41(/*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_1 = NotchSolutionUtility_get_ScreenSafeAreaRelative_mA401C9F104714B1D8753556BDB9EC91DF63F76FC(/*hidden argument*/NULL);
		return L_1;
	}

IL_000d:
	{
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_2 = __this->get_storedSimulatedSafeAreaRelative_4();
		return L_2;
	}
}
// UnityEngine.RectTransform E7.NotchSolution.NotchSolutionUIBehaviourBase::get_rectTransform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_Rect == null)
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_0 = __this->get_m_Rect_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// m_Rect = GetComponent<RectTransform>();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_2 = Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79_RuntimeMethod_var);
		__this->set_m_Rect_6(L_2);
	}

IL_001a:
	{
		// return m_Rect;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_3 = __this->get_m_Rect_6();
		return L_3;
	}
}
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotchSolutionUIBehaviourBase_OnEnable_mB58B0CC87D20E90186F9F4B79AB71C17DAC93D47 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method)
{
	{
		// base.OnEnable();
		UIBehaviour_OnEnable_m9BE8F521B232703E4A0EF14EA43F264EDAF3B3F0(__this, /*hidden argument*/NULL);
		// DelayedUpdate();
		NotchSolutionUIBehaviourBase_DelayedUpdate_m25321FD361325D51D930F734D165480207AA5957(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotchSolutionUIBehaviourBase_OnDisable_mF0E4327BC6B4EE827BE209E33DF2FF4DC5E06179 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NotchSolutionUIBehaviourBase_OnDisable_mF0E4327BC6B4EE827BE209E33DF2FF4DC5E06179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_Tracker.Clear();
		DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * L_0 = __this->get_address_of_m_Tracker_7();
		DrivenRectTransformTracker_Clear_m41F9B0AA2025AF5B76D38E68B08C111D7D8EB845((DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 *)L_0, /*hidden argument*/NULL);
		// LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_1 = NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LayoutRebuilder_tE88B8B9EA50644E438123BDCE2BC2A3287E07585_il2cpp_TypeInfo_var);
		LayoutRebuilder_MarkLayoutForRebuild_m1BDFA10259B85AEBD3A758B78EF4702BE014D1FE(L_1, /*hidden argument*/NULL);
		// base.OnDisable();
		UIBehaviour_OnDisable_m7D3E0D1AC43330C5A50B17DD296D2CB84994CA23(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::OnRectTransformDimensionsChange()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotchSolutionUIBehaviourBase_OnRectTransformDimensionsChange_mA66CDFC9489CD86A713A890EFE360C5DEFA16884 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method)
{
	{
		// UpdateRectBase();
		NotchSolutionUIBehaviourBase_UpdateRectBase_m1D4EAA1203356D6F91B0BCFC3947B950ECB849F9(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::UnityEngine.UI.ILayoutController.SetLayoutHorizontal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotchSolutionUIBehaviourBase_UnityEngine_UI_ILayoutController_SetLayoutHorizontal_m23965011DBEB718052B622CBA3FE6E7A044CEAE3 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method)
{
	{
		// UpdateRectBase();
		NotchSolutionUIBehaviourBase_UpdateRectBase_m1D4EAA1203356D6F91B0BCFC3947B950ECB849F9(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::UnityEngine.UI.ILayoutController.SetLayoutVertical()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotchSolutionUIBehaviourBase_UnityEngine_UI_ILayoutController_SetLayoutVertical_mF2B7807F3CB7D7914444F330681089BEC528D811 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::UpdateRectBase()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotchSolutionUIBehaviourBase_UpdateRectBase_m1D4EAA1203356D6F91B0BCFC3947B950ECB849F9 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method)
{
	{
		// if (!(enabled && gameObject.activeInHierarchy)) return;
		bool L_0 = Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = GameObject_get_activeInHierarchy_mA3990AC5F61BB35283188E925C2BE7F7BF67734B(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0016;
		}
	}

IL_0015:
	{
		// if (!(enabled && gameObject.activeInHierarchy)) return;
		return;
	}

IL_0016:
	{
		// UpdateRect();
		VirtActionInvoker0::Invoke(20 /* System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::UpdateRect() */, __this);
		// }
		return;
	}
}
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::DelayedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotchSolutionUIBehaviourBase_DelayedUpdate_m25321FD361325D51D930F734D165480207AA5957 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method)
{
	{
		// private void DelayedUpdate() => StartCoroutine(DelayedUpdateRoutine());
		RuntimeObject* L_0 = NotchSolutionUIBehaviourBase_DelayedUpdateRoutine_mBE1625FF6D9BFE992EDD6961BB56E10DA2DB8832(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator E7.NotchSolution.NotchSolutionUIBehaviourBase::DelayedUpdateRoutine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NotchSolutionUIBehaviourBase_DelayedUpdateRoutine_mBE1625FF6D9BFE992EDD6961BB56E10DA2DB8832 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NotchSolutionUIBehaviourBase_DelayedUpdateRoutine_mBE1625FF6D9BFE992EDD6961BB56E10DA2DB8832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F * L_0 = (U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F *)il2cpp_codegen_object_new(U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F_il2cpp_TypeInfo_var);
		U3CDelayedUpdateRoutineU3Ed__19__ctor_m559AAD87FE3298DA82D9DE2FB988188E7F0B4DE3(L_0, 0, /*hidden argument*/NULL);
		U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotchSolutionUIBehaviourBase__ctor_m06D19A834AF9C336013AA1322C9F9D3233D281A0 (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NotchSolutionUIBehaviourBase__ctor_m06D19A834AF9C336013AA1322C9F9D3233D281A0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private Rect storedSimulatedSafeAreaRelative = NotchSolutionUtility.defaultSafeArea;
		IL2CPP_RUNTIME_CLASS_INIT(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_0 = ((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->get_defaultSafeArea_0();
		__this->set_storedSimulatedSafeAreaRelative_4(L_0);
		// private Rect[] storedSimulatedCutoutsRelative = NotchSolutionUtility.defaultCutouts;
		RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* L_1 = ((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->get_defaultCutouts_1();
		__this->set_storedSimulatedCutoutsRelative_5(L_1);
		// private WaitForEndOfFrame eofWait = new WaitForEndOfFrame();
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_2 = (WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA(L_2, /*hidden argument*/NULL);
		__this->set_eofWait_8(L_2);
		UIBehaviour__ctor_m869436738107AF382FD4D10DE9641F8241B323C7(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Canvas E7.NotchSolution.NotchSolutionUIBehaviourBase::<GetCanvasRect>g__GetTopLevelCanvasU7C1_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * NotchSolutionUIBehaviourBase_U3CGetCanvasRectU3Eg__GetTopLevelCanvasU7C1_0_m626582600C4FD53CDEFE76B89E9759B9F4125C5D (NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NotchSolutionUIBehaviourBase_U3CGetCanvasRectU3Eg__GetTopLevelCanvasU7C1_0_m626582600C4FD53CDEFE76B89E9759B9F4125C5D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var canvas = this.GetComponentInParent<Canvas>();
		Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_0 = Component_GetComponentInParent_TisCanvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_m79D616348A09F5E2973F405F4F9D944744FAD6A5(__this, /*hidden argument*/Component_GetComponentInParent_TisCanvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_m79D616348A09F5E2973F405F4F9D944744FAD6A5_RuntimeMethod_var);
		// var rootCanvas = canvas.rootCanvas;
		NullCheck(L_0);
		Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * L_1 = Canvas_get_rootCanvas_mB1C93410A4AA793D88130FD08C05D71327641DC5(L_0, /*hidden argument*/NULL);
		// return rootCanvas;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.ScreenOrientation E7.NotchSolution.NotchSolutionUtility::GetCurrentOrientation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NotchSolutionUtility_GetCurrentOrientation_m847578DED8F9B964755A0C64C9FADD3FDB27E3A7 (const RuntimeMethod* method)
{
	{
		// => Screen.width > Screen.height ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;
		int32_t L_0 = Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C(/*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE(/*hidden argument*/NULL);
		if ((((int32_t)L_0) > ((int32_t)L_1)))
		{
			goto IL_000e;
		}
	}
	{
		return (int32_t)(1);
	}

IL_000e:
	{
		return (int32_t)(3);
	}
}
// System.Boolean E7.NotchSolution.NotchSolutionUtility::get_ShouldUseNotchSimulatorValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NotchSolutionUtility_get_ShouldUseNotchSimulatorValue_m3C6D486B8587587EB12650930FF6ED47DAE7BA41 (const RuntimeMethod* method)
{
	{
		// return false;
		return (bool)0;
	}
}
// UnityEngine.Rect E7.NotchSolution.NotchSolutionUtility::get_ScreenSafeAreaRelative()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  NotchSolutionUtility_get_ScreenSafeAreaRelative_mA401C9F104714B1D8753556BDB9EC91DF63F76FC (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NotchSolutionUtility_get_ScreenSafeAreaRelative_mA401C9F104714B1D8753556BDB9EC91DF63F76FC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Rect absolutePaddings = Screen.safeArea;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_0 = Screen_get_safeArea_m47E3A61627ECEAC2336C1FBFBF67A6C7503569F5(/*hidden argument*/NULL);
		// cachedScreenSafeAreaRelative = ToScreenRelativeRect(absolutePaddings);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_1 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_2 = NotchSolutionUtility_ToScreenRelativeRect_m722FC3D28A744CB322BF96B19642D63B62C4166B(L_1, /*hidden argument*/NULL);
		((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->set_cachedScreenSafeAreaRelative_3(L_2);
		// cachedScreenSafeArea = absolutePaddings;
		((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->set_cachedScreenSafeArea_2(L_1);
		// safeAreaRelativeCached = true;
		((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->set_safeAreaRelativeCached_4((bool)1);
		// return cachedScreenSafeAreaRelative;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_3 = ((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->get_cachedScreenSafeAreaRelative_3();
		return L_3;
	}
}
// UnityEngine.Rect E7.NotchSolution.NotchSolutionUtility::ToScreenRelativeRect(UnityEngine.Rect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  NotchSolutionUtility_ToScreenRelativeRect_m722FC3D28A744CB322BF96B19642D63B62C4166B (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___absoluteRect0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// int w = Screen.currentResolution.width;
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_0 = Screen_get_currentResolution_mE1A3C7E9603FA56B539FDDA1F602C66732EFD17B(/*hidden argument*/NULL);
		V_2 = L_0;
		int32_t L_1 = Resolution_get_width_mDD9DCC89D65057B64C413AF15BEE2E37E9892065((Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 *)(&V_2), /*hidden argument*/NULL);
		V_0 = L_1;
		// int h = Screen.currentResolution.height;
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_2 = Screen_get_currentResolution_mE1A3C7E9603FA56B539FDDA1F602C66732EFD17B(/*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = Resolution_get_height_mB90F24337D7B96A288F8BE1D0F2F3599B785AD27((Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 *)(&V_2), /*hidden argument*/NULL);
		V_1 = L_3;
		// return new Rect(
		//     absoluteRect.x / w,
		//     absoluteRect.y / h,
		//     absoluteRect.width / w,
		//     absoluteRect.height / h
		// );
		float L_4 = Rect_get_x_mA61220F6F26ECD6951B779FFA7CAD7ECE11D6987((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___absoluteRect0), /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		float L_6 = Rect_get_y_m4E1AAD20D167085FF4F9E9C86EF34689F5770A74((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___absoluteRect0), /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		float L_8 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___absoluteRect0), /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		float L_10 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___absoluteRect0), /*hidden argument*/NULL);
		int32_t L_11 = V_1;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70((&L_12), ((float)((float)L_4/(float)(((float)((float)L_5))))), ((float)((float)L_6/(float)(((float)((float)L_7))))), ((float)((float)L_8/(float)(((float)((float)L_9))))), ((float)((float)L_10/(float)(((float)((float)L_11))))), /*hidden argument*/NULL);
		return L_12;
	}
}
// UnityEngine.Rect[] E7.NotchSolution.NotchSolutionUtility::get_ScreenCutoutsRelative()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* NotchSolutionUtility_get_ScreenCutoutsRelative_m2D78B9F1EF59F96E077A72CCA9228212860D8DD9 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NotchSolutionUtility_get_ScreenCutoutsRelative_m2D78B9F1EF59F96E077A72CCA9228212860D8DD9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// Rect[] absoluteCutouts = Screen.cutouts;
		RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* L_0 = Screen_get_cutouts_m3EFA8304695B6B5134F51A696D3A173CC7B114A0(/*hidden argument*/NULL);
		V_0 = L_0;
		// cachedScreenCutoutsRelative = new Rect[absoluteCutouts.Length];
		RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* L_1 = V_0;
		NullCheck(L_1);
		RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* L_2 = (RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE*)(RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE*)SZArrayNew(RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length)))));
		IL2CPP_RUNTIME_CLASS_INIT(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var);
		((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->set_cachedScreenCutoutsRelative_6(L_2);
		// for(int i = 0; i < absoluteCutouts.Length; i ++)
		V_1 = 0;
		goto IL_0032;
	}

IL_0017:
	{
		// cachedScreenCutoutsRelative[i] = ToScreenRelativeRect(absoluteCutouts[i]);
		IL2CPP_RUNTIME_CLASS_INIT(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var);
		RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* L_3 = ((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->get_cachedScreenCutoutsRelative_6();
		int32_t L_4 = V_1;
		RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_9 = NotchSolutionUtility_ToScreenRelativeRect_m722FC3D28A744CB322BF96B19642D63B62C4166B(L_8, /*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 )L_9);
		// for(int i = 0; i < absoluteCutouts.Length; i ++)
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0032:
	{
		// for(int i = 0; i < absoluteCutouts.Length; i ++)
		int32_t L_11 = V_1;
		RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* L_12 = V_0;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length)))))))
		{
			goto IL_0017;
		}
	}
	{
		// cachedScreenCutouts = absoluteCutouts;
		RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var);
		((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->set_cachedScreenCutouts_5(L_13);
		// cutoutsRelativeCached = true;
		((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->set_cutoutsRelativeCached_7((bool)1);
		// return cachedScreenCutoutsRelative;
		RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* L_14 = ((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->get_cachedScreenCutoutsRelative_6();
		return L_14;
	}
}
// System.Void E7.NotchSolution.NotchSolutionUtility::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotchSolutionUtility__cctor_m7D412E25BE0C6B08A3E6E15D61D1F23A7649E6F3 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NotchSolutionUtility__cctor_m7D412E25BE0C6B08A3E6E15D61D1F23A7649E6F3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static Rect defaultSafeArea = new Rect(0, 0, 1, 1);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70((&L_0), (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->set_defaultSafeArea_0(L_0);
		// internal static Rect[] defaultCutouts = new Rect[0];
		RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE* L_1 = (RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE*)(RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE*)SZArrayNew(RectU5BU5D_tD4F5052A6F89820365269FF4CA7C3EB1ACD4B1EE_il2cpp_TypeInfo_var, (uint32_t)0);
		((NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_StaticFields*)il2cpp_codegen_static_fields_for(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var))->set_defaultCutouts_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void E7.NotchSolution.OrientationDependentData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OrientationDependentData__ctor_mE847242A14FC93440C0974CE0A80B1B563FD3F64 (OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void E7.NotchSolution.PerEdgeAdaptations::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerEdgeAdaptations__ctor_m83B42A2A6E7967A9CA028EDFDC8563BBD9414237 (PerEdgeAdaptations_t45C28CCF296B800024E9013701B891370CC4CF2D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PerEdgeAdaptations__ctor_m83B42A2A6E7967A9CA028EDFDC8563BBD9414237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PerEdgeValues_1__ctor_mC5C5C66BA0E9A56A888E35BA9083154E64FC61EC(__this, /*hidden argument*/PerEdgeValues_1__ctor_mC5C5C66BA0E9A56A888E35BA9083154E64FC61EC_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void E7.NotchSolution.PerEdgeEvaluationModes::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PerEdgeEvaluationModes__ctor_mC37F1C45BF291CFF2264E000D1A7565C970E3BFE (PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PerEdgeEvaluationModes__ctor_mC37F1C45BF291CFF2264E000D1A7565C970E3BFE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PerEdgeValues_1__ctor_mB9B72BAEB18D43AA262C35DC74889A042A1B78B2(__this, /*hidden argument*/PerEdgeValues_1__ctor_mB9B72BAEB18D43AA262C35DC74889A042A1B78B2_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void E7.NotchSolution.SafeAdaptation::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SafeAdaptation_Reset_mBCDC4C51B720AC181883EDA727DC4BC52F30F32C (SafeAdaptation_t0DEF9DC095FCB9608021EA9A07ADA433CBBB6634 * __this, const RuntimeMethod* method)
{
	{
		// ResetAdaptationToCurve(AnimationCurve.Linear(0, 0, iPhoneXNotchHeightRelative, 1));
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_0 = AnimationCurve_Linear_mE61D9A684B81B47365C299B6B9C5D744FC9DFBFA((0.0f), (0.0f), (0.0541871786f), (1.0f), /*hidden argument*/NULL);
		AdaptationBase_ResetAdaptationToCurve_m0DB3E2F55F05CC0985FB73ADB5FB5CF824E52E1E(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void E7.NotchSolution.SafeAdaptation::Adapt()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SafeAdaptation_Adapt_mE5E9DEC6DB5514DDC4301EB5586EE421DC33ACDC (SafeAdaptation_t0DEF9DC095FCB9608021EA9A07ADA433CBBB6634 * __this, const RuntimeMethod* method)
{
	{
		// public override void Adapt() => AdaptWithRelativeSafeArea(SafeAreaRelative);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_0 = AdaptationBase_get_SafeAreaRelative_m1C2D114B34B46E26679DD3775B2BE73F002B2416(__this, /*hidden argument*/NULL);
		SafeAdaptation_AdaptWithRelativeSafeArea_m8B4F6C7CECE7AB36DA4CA688C18A4AEFC76AC589(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void E7.NotchSolution.SafeAdaptation::AdaptWithRelativeSafeArea(UnityEngine.Rect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SafeAdaptation_AdaptWithRelativeSafeArea_m8B4F6C7CECE7AB36DA4CA688C18A4AEFC76AC589 (SafeAdaptation_t0DEF9DC095FCB9608021EA9A07ADA433CBBB6634 * __this, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___relativeSafeArea0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafeAdaptation_AdaptWithRelativeSafeArea_m8B4F6C7CECE7AB36DA4CA688C18A4AEFC76AC589_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		// float spaceTakenRelative = 0;
		V_0 = (0.0f);
		// if (evaluationMode != EdgeEvaluationMode.Off)
		int32_t L_0 = __this->get_evaluationMode_10();
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_00d5;
		}
	}
	{
		// switch (adaptToEdge)
		int32_t L_1 = __this->get_adaptToEdge_9();
		V_1 = L_1;
		int32_t L_2 = V_1;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0031;
			}
			case 1:
			{
				goto IL_003b;
			}
			case 2:
			{
				goto IL_004b;
			}
			case 3:
			{
				goto IL_005b;
			}
		}
	}
	{
		goto IL_0063;
	}

IL_0031:
	{
		// case RectTransform.Edge.Left: spaceTakenRelative = relativeSafeArea.xMin; break;
		float L_3 = Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___relativeSafeArea0), /*hidden argument*/NULL);
		V_0 = L_3;
		// case RectTransform.Edge.Left: spaceTakenRelative = relativeSafeArea.xMin; break;
		goto IL_0063;
	}

IL_003b:
	{
		// case RectTransform.Edge.Right: spaceTakenRelative = 1 - relativeSafeArea.xMax; break;
		float L_4 = Rect_get_xMax_m174FFAACE6F19A59AA793B3D507BE70116E27DE5((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___relativeSafeArea0), /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_4));
		// case RectTransform.Edge.Right: spaceTakenRelative = 1 - relativeSafeArea.xMax; break;
		goto IL_0063;
	}

IL_004b:
	{
		// case RectTransform.Edge.Top: spaceTakenRelative = 1 - relativeSafeArea.yMax; break;
		float L_5 = Rect_get_yMax_m9685BF55B44C51FF9BA080F9995073E458E1CDC3((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___relativeSafeArea0), /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_5));
		// case RectTransform.Edge.Top: spaceTakenRelative = 1 - relativeSafeArea.yMax; break;
		goto IL_0063;
	}

IL_005b:
	{
		// case RectTransform.Edge.Bottom: spaceTakenRelative = relativeSafeArea.yMin; break;
		float L_6 = Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___relativeSafeArea0), /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0063:
	{
		// if (evaluationMode == EdgeEvaluationMode.Balanced)
		int32_t L_7 = __this->get_evaluationMode_10();
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_00d5;
		}
	}
	{
		// switch (adaptToEdge)
		int32_t L_8 = __this->get_adaptToEdge_9();
		V_1 = L_8;
		int32_t L_9 = V_1;
		switch (L_9)
		{
			case 0:
			{
				goto IL_008b;
			}
			case 1:
			{
				goto IL_00a1;
			}
			case 2:
			{
				goto IL_00b1;
			}
			case 3:
			{
				goto IL_00c1;
			}
		}
	}
	{
		goto IL_00d5;
	}

IL_008b:
	{
		// case RectTransform.Edge.Left: spaceTakenRelative = Mathf.Max(spaceTakenRelative, 1 - relativeSafeArea.xMax); break;
		float L_10 = V_0;
		float L_11 = Rect_get_xMax_m174FFAACE6F19A59AA793B3D507BE70116E27DE5((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___relativeSafeArea0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_10, ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		// case RectTransform.Edge.Left: spaceTakenRelative = Mathf.Max(spaceTakenRelative, 1 - relativeSafeArea.xMax); break;
		goto IL_00d5;
	}

IL_00a1:
	{
		// case RectTransform.Edge.Right: spaceTakenRelative = Mathf.Max(spaceTakenRelative, relativeSafeArea.xMin); break;
		float L_13 = V_0;
		float L_14 = Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___relativeSafeArea0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var);
		float L_15 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_13, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		// case RectTransform.Edge.Right: spaceTakenRelative = Mathf.Max(spaceTakenRelative, relativeSafeArea.xMin); break;
		goto IL_00d5;
	}

IL_00b1:
	{
		// case RectTransform.Edge.Top: spaceTakenRelative = Mathf.Max(spaceTakenRelative, relativeSafeArea.yMin); break;
		float L_16 = V_0;
		float L_17 = Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___relativeSafeArea0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var);
		float L_18 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_16, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		// case RectTransform.Edge.Top: spaceTakenRelative = Mathf.Max(spaceTakenRelative, relativeSafeArea.yMin); break;
		goto IL_00d5;
	}

IL_00c1:
	{
		// case RectTransform.Edge.Bottom: spaceTakenRelative = Mathf.Max(spaceTakenRelative, 1 - relativeSafeArea.yMax); break;
		float L_19 = V_0;
		float L_20 = Rect_get_yMax_m9685BF55B44C51FF9BA080F9995073E458E1CDC3((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___relativeSafeArea0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4D4AC358D24F6DDC32EC291DDE1DF2C3B752A194_il2cpp_TypeInfo_var);
		float L_21 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_19, ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_20)), /*hidden argument*/NULL);
		V_0 = L_21;
	}

IL_00d5:
	{
		// base.Adapt(valueForAdaptationCurve: spaceTakenRelative);
		float L_22 = V_0;
		AdaptationBase_Adapt_m1D2C2F404AB717AC81D03326CF3EA15C04C1D8FE(__this, L_22, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void E7.NotchSolution.SafeAdaptation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SafeAdaptation__ctor_mF0AA64F26F44ABD9A73AB22227EAC75CA2D84CBC (SafeAdaptation_t0DEF9DC095FCB9608021EA9A07ADA433CBBB6634 * __this, const RuntimeMethod* method)
{
	{
		AdaptationBase__ctor_m50D7CDA55A7F6682D8F780D72F758DA3CF966DF3(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void E7.NotchSolution.SafePadding::UpdateRect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SafePadding_UpdateRect_m3211D09A95539B88B05450DD91319F21FD386D4C (SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SafePadding_UpdateRect_m3211D09A95539B88B05450DD91319F21FD386D4C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * V_0 = NULL;
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  V_2;
	memset((&V_2), 0, sizeof(V_2));
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* V_3 = NULL;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* V_4 = NULL;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_6;
	memset((&V_6), 0, sizeof(V_6));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_7;
	memset((&V_7), 0, sizeof(V_7));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_8;
	memset((&V_8), 0, sizeof(V_8));
	int32_t V_9 = 0;
	float V_10 = 0.0f;
	PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * G_B5_0 = NULL;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B7_0 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B7_1 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B7_2 = NULL;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B6_0 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B6_1 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B6_2 = NULL;
	int32_t G_B8_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B8_1 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B8_2 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B8_3 = NULL;
	int32_t G_B10_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B10_1 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B10_2 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B10_3 = NULL;
	int32_t G_B9_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B9_1 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B9_2 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B9_3 = NULL;
	int32_t G_B11_0 = 0;
	int32_t G_B11_1 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B11_2 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B11_3 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B11_4 = NULL;
	int32_t G_B13_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B13_1 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B13_2 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B13_3 = NULL;
	int32_t G_B12_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B12_1 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B12_2 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B12_3 = NULL;
	int32_t G_B14_0 = 0;
	int32_t G_B14_1 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B14_2 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B14_3 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B14_4 = NULL;
	int32_t G_B16_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B16_1 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B16_2 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B16_3 = NULL;
	int32_t G_B15_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B15_1 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B15_2 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B15_3 = NULL;
	int32_t G_B17_0 = 0;
	int32_t G_B17_1 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B17_2 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B17_3 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B17_4 = NULL;
	int32_t G_B19_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B19_1 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B19_2 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B19_3 = NULL;
	int32_t G_B18_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B18_1 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B18_2 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B18_3 = NULL;
	int32_t G_B20_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B20_1 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B20_2 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B20_3 = NULL;
	int32_t G_B21_0 = 0;
	int32_t G_B21_1 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B21_2 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B21_3 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B21_4 = NULL;
	int32_t G_B23_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B23_1 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B23_2 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B23_3 = NULL;
	int32_t G_B22_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B22_1 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B22_2 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B22_3 = NULL;
	int32_t G_B24_0 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B24_1 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B24_2 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B24_3 = NULL;
	int32_t G_B25_0 = 0;
	int32_t G_B25_1 = 0;
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * G_B25_2 = NULL;
	SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * G_B25_3 = NULL;
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * G_B25_4 = NULL;
	int32_t G_B31_0 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* G_B31_1 = NULL;
	int32_t G_B30_0 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* G_B30_1 = NULL;
	float G_B32_0 = 0.0f;
	int32_t G_B32_1 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* G_B32_2 = NULL;
	int32_t G_B39_0 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* G_B39_1 = NULL;
	int32_t G_B38_0 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* G_B38_1 = NULL;
	float G_B40_0 = 0.0f;
	int32_t G_B40_1 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* G_B40_2 = NULL;
	int32_t G_B47_0 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* G_B47_1 = NULL;
	int32_t G_B46_0 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* G_B46_1 = NULL;
	float G_B48_0 = 0.0f;
	int32_t G_B48_1 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* G_B48_2 = NULL;
	int32_t G_B55_0 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* G_B55_1 = NULL;
	int32_t G_B54_0 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* G_B54_1 = NULL;
	float G_B56_0 = 0.0f;
	int32_t G_B56_1 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* G_B56_2 = NULL;
	{
		// PerEdgeEvaluationModes selectedOrientation =
		// orientationType == SupportedOrientations.Dual ?
		// NotchSolutionUtility.GetCurrentOrientation() == ScreenOrientation.Landscape ?
		// landscapePaddings : portraitOrDefaultPaddings
		// : portraitOrDefaultPaddings;
		int32_t L_0 = __this->get_orientationType_9();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_1 = __this->get_portraitOrDefaultPaddings_10();
		G_B5_0 = L_1;
		goto IL_0027;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NotchSolutionUtility_t1BF9A5D2EF30B3BA5A4143DC15A51DDDFB84FB55_il2cpp_TypeInfo_var);
		int32_t L_2 = NotchSolutionUtility_GetCurrentOrientation_m847578DED8F9B964755A0C64C9FADD3FDB27E3A7(/*hidden argument*/NULL);
		if ((((int32_t)L_2) == ((int32_t)3)))
		{
			goto IL_0021;
		}
	}
	{
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_3 = __this->get_portraitOrDefaultPaddings_10();
		G_B5_0 = L_3;
		goto IL_0027;
	}

IL_0021:
	{
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_4 = __this->get_landscapePaddings_11();
		G_B5_0 = L_4;
	}

IL_0027:
	{
		V_0 = G_B5_0;
		// m_Tracker.Clear();
		DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * L_5 = ((NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 *)__this)->get_address_of_m_Tracker_7();
		DrivenRectTransformTracker_Clear_m41F9B0AA2025AF5B76D38E68B08C111D7D8EB845((DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 *)L_5, /*hidden argument*/NULL);
		// m_Tracker.Add(this, rectTransform,
		//     (LockSide(selectedOrientation.left) ? DrivenTransformProperties.AnchorMinX : 0) |
		//     (LockSide(selectedOrientation.right) ? DrivenTransformProperties.AnchorMaxX : 0) |
		//     (LockSide(selectedOrientation.bottom) ? DrivenTransformProperties.AnchorMinY : 0) |
		//     (LockSide(selectedOrientation.top) ? DrivenTransformProperties.AnchorMaxY : 0) |
		//     (LockSide(selectedOrientation.left) && LockSide(selectedOrientation.right) ? (DrivenTransformProperties.SizeDeltaX | DrivenTransformProperties.AnchoredPositionX) : 0) |
		//     (LockSide(selectedOrientation.top) && LockSide(selectedOrientation.bottom) ? (DrivenTransformProperties.SizeDeltaY | DrivenTransformProperties.AnchoredPositionY) : 0)
		// );
		DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * L_6 = ((NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 *)__this)->get_address_of_m_Tracker_7();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_7 = NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9(__this, /*hidden argument*/NULL);
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = ((PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE *)L_8)->get_left_0();
		bool L_10 = SafePadding_U3CUpdateRectU3Eg__LockSideU7C5_0_m869D49FE6FDB56D9F3675596090D0B643A82C974(L_9, /*hidden argument*/NULL);
		G_B6_0 = L_7;
		G_B6_1 = __this;
		G_B6_2 = L_6;
		if (L_10)
		{
			G_B7_0 = L_7;
			G_B7_1 = __this;
			G_B7_2 = L_6;
			goto IL_0050;
		}
	}
	{
		G_B8_0 = 0;
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		goto IL_0055;
	}

IL_0050:
	{
		G_B8_0 = ((int32_t)256);
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
	}

IL_0055:
	{
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = ((PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE *)L_11)->get_right_3();
		bool L_13 = SafePadding_U3CUpdateRectU3Eg__LockSideU7C5_0_m869D49FE6FDB56D9F3675596090D0B643A82C974(L_12, /*hidden argument*/NULL);
		G_B9_0 = G_B8_0;
		G_B9_1 = G_B8_1;
		G_B9_2 = G_B8_2;
		G_B9_3 = G_B8_3;
		if (L_13)
		{
			G_B10_0 = G_B8_0;
			G_B10_1 = G_B8_1;
			G_B10_2 = G_B8_2;
			G_B10_3 = G_B8_3;
			goto IL_0065;
		}
	}
	{
		G_B11_0 = 0;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		G_B11_4 = G_B9_3;
		goto IL_006a;
	}

IL_0065:
	{
		G_B11_0 = ((int32_t)1024);
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
		G_B11_4 = G_B10_3;
	}

IL_006a:
	{
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = ((PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE *)L_14)->get_bottom_1();
		bool L_16 = SafePadding_U3CUpdateRectU3Eg__LockSideU7C5_0_m869D49FE6FDB56D9F3675596090D0B643A82C974(L_15, /*hidden argument*/NULL);
		G_B12_0 = ((int32_t)((int32_t)G_B11_1|(int32_t)G_B11_0));
		G_B12_1 = G_B11_2;
		G_B12_2 = G_B11_3;
		G_B12_3 = G_B11_4;
		if (L_16)
		{
			G_B13_0 = ((int32_t)((int32_t)G_B11_1|(int32_t)G_B11_0));
			G_B13_1 = G_B11_2;
			G_B13_2 = G_B11_3;
			G_B13_3 = G_B11_4;
			goto IL_007b;
		}
	}
	{
		G_B14_0 = 0;
		G_B14_1 = G_B12_0;
		G_B14_2 = G_B12_1;
		G_B14_3 = G_B12_2;
		G_B14_4 = G_B12_3;
		goto IL_0080;
	}

IL_007b:
	{
		G_B14_0 = ((int32_t)512);
		G_B14_1 = G_B13_0;
		G_B14_2 = G_B13_1;
		G_B14_3 = G_B13_2;
		G_B14_4 = G_B13_3;
	}

IL_0080:
	{
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_17 = V_0;
		NullCheck(L_17);
		int32_t L_18 = ((PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE *)L_17)->get_top_2();
		bool L_19 = SafePadding_U3CUpdateRectU3Eg__LockSideU7C5_0_m869D49FE6FDB56D9F3675596090D0B643A82C974(L_18, /*hidden argument*/NULL);
		G_B15_0 = ((int32_t)((int32_t)G_B14_1|(int32_t)G_B14_0));
		G_B15_1 = G_B14_2;
		G_B15_2 = G_B14_3;
		G_B15_3 = G_B14_4;
		if (L_19)
		{
			G_B16_0 = ((int32_t)((int32_t)G_B14_1|(int32_t)G_B14_0));
			G_B16_1 = G_B14_2;
			G_B16_2 = G_B14_3;
			G_B16_3 = G_B14_4;
			goto IL_0091;
		}
	}
	{
		G_B17_0 = 0;
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		G_B17_3 = G_B15_2;
		G_B17_4 = G_B15_3;
		goto IL_0096;
	}

IL_0091:
	{
		G_B17_0 = ((int32_t)2048);
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
		G_B17_4 = G_B16_3;
	}

IL_0096:
	{
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_20 = V_0;
		NullCheck(L_20);
		int32_t L_21 = ((PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE *)L_20)->get_left_0();
		bool L_22 = SafePadding_U3CUpdateRectU3Eg__LockSideU7C5_0_m869D49FE6FDB56D9F3675596090D0B643A82C974(L_21, /*hidden argument*/NULL);
		G_B18_0 = ((int32_t)((int32_t)G_B17_1|(int32_t)G_B17_0));
		G_B18_1 = G_B17_2;
		G_B18_2 = G_B17_3;
		G_B18_3 = G_B17_4;
		if (!L_22)
		{
			G_B19_0 = ((int32_t)((int32_t)G_B17_1|(int32_t)G_B17_0));
			G_B19_1 = G_B17_2;
			G_B19_2 = G_B17_3;
			G_B19_3 = G_B17_4;
			goto IL_00b1;
		}
	}
	{
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_23 = V_0;
		NullCheck(L_23);
		int32_t L_24 = ((PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE *)L_23)->get_right_3();
		bool L_25 = SafePadding_U3CUpdateRectU3Eg__LockSideU7C5_0_m869D49FE6FDB56D9F3675596090D0B643A82C974(L_24, /*hidden argument*/NULL);
		G_B19_0 = G_B18_0;
		G_B19_1 = G_B18_1;
		G_B19_2 = G_B18_2;
		G_B19_3 = G_B18_3;
		if (L_25)
		{
			G_B20_0 = G_B18_0;
			G_B20_1 = G_B18_1;
			G_B20_2 = G_B18_2;
			G_B20_3 = G_B18_3;
			goto IL_00b4;
		}
	}

IL_00b1:
	{
		G_B21_0 = 0;
		G_B21_1 = G_B19_0;
		G_B21_2 = G_B19_1;
		G_B21_3 = G_B19_2;
		G_B21_4 = G_B19_3;
		goto IL_00b9;
	}

IL_00b4:
	{
		G_B21_0 = ((int32_t)4098);
		G_B21_1 = G_B20_0;
		G_B21_2 = G_B20_1;
		G_B21_3 = G_B20_2;
		G_B21_4 = G_B20_3;
	}

IL_00b9:
	{
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27 = ((PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE *)L_26)->get_top_2();
		bool L_28 = SafePadding_U3CUpdateRectU3Eg__LockSideU7C5_0_m869D49FE6FDB56D9F3675596090D0B643A82C974(L_27, /*hidden argument*/NULL);
		G_B22_0 = ((int32_t)((int32_t)G_B21_1|(int32_t)G_B21_0));
		G_B22_1 = G_B21_2;
		G_B22_2 = G_B21_3;
		G_B22_3 = G_B21_4;
		if (!L_28)
		{
			G_B23_0 = ((int32_t)((int32_t)G_B21_1|(int32_t)G_B21_0));
			G_B23_1 = G_B21_2;
			G_B23_2 = G_B21_3;
			G_B23_3 = G_B21_4;
			goto IL_00d4;
		}
	}
	{
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_29 = V_0;
		NullCheck(L_29);
		int32_t L_30 = ((PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE *)L_29)->get_bottom_1();
		bool L_31 = SafePadding_U3CUpdateRectU3Eg__LockSideU7C5_0_m869D49FE6FDB56D9F3675596090D0B643A82C974(L_30, /*hidden argument*/NULL);
		G_B23_0 = G_B22_0;
		G_B23_1 = G_B22_1;
		G_B23_2 = G_B22_2;
		G_B23_3 = G_B22_3;
		if (L_31)
		{
			G_B24_0 = G_B22_0;
			G_B24_1 = G_B22_1;
			G_B24_2 = G_B22_2;
			G_B24_3 = G_B22_3;
			goto IL_00d7;
		}
	}

IL_00d4:
	{
		G_B25_0 = 0;
		G_B25_1 = G_B23_0;
		G_B25_2 = G_B23_1;
		G_B25_3 = G_B23_2;
		G_B25_4 = G_B23_3;
		goto IL_00dc;
	}

IL_00d7:
	{
		G_B25_0 = ((int32_t)8196);
		G_B25_1 = G_B24_0;
		G_B25_2 = G_B24_1;
		G_B25_3 = G_B24_2;
		G_B25_4 = G_B24_3;
	}

IL_00dc:
	{
		DrivenRectTransformTracker_Add_m65814604ABCE8B9F81270F3C2E1632CCB9E9A5E7((DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 *)G_B25_4, G_B25_3, G_B25_2, ((int32_t)((int32_t)G_B25_1|(int32_t)G_B25_0)), /*hidden argument*/NULL);
		// rectTransform.anchorMin = Vector2.zero;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_32 = NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_il2cpp_TypeInfo_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_33 = Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828(/*hidden argument*/NULL);
		NullCheck(L_32);
		RectTransform_set_anchorMin_mD9E6E95890B701A5190C12F5AE42E622246AF798(L_32, L_33, /*hidden argument*/NULL);
		// rectTransform.anchorMax = Vector2.one;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_34 = NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9(__this, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_35 = Vector2_get_one_m9B2AFD26404B6DD0F520D19FC7F79371C5C18B42(/*hidden argument*/NULL);
		NullCheck(L_34);
		RectTransform_set_anchorMax_m67E04F54B5122804E32019D5FAE50C21CC67651D(L_34, L_35, /*hidden argument*/NULL);
		// var topRect = GetCanvasRect();
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_36 = NotchSolutionUIBehaviourBase_GetCanvasRect_m6664B2634FBD335D5350B41D855AA16F8A8A8890(__this, /*hidden argument*/NULL);
		V_1 = L_36;
		// var safeAreaRelative = SafeAreaRelative;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_37 = NotchSolutionUIBehaviourBase_get_SafeAreaRelative_m4229E4426CF2727B0EC2F0E47258A1C6EEB106BD(__this, /*hidden argument*/NULL);
		V_2 = L_37;
		// var relativeLDUR = new float[4]
		// {
		//     safeAreaRelative.xMin,
		//     safeAreaRelative.yMin,
		//     1 - (safeAreaRelative.yMin + safeAreaRelative.height),
		//     1 - (safeAreaRelative.xMin + safeAreaRelative.width),
		// };
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_38 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)4);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_39 = L_38;
		float L_40 = Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_2), /*hidden argument*/NULL);
		NullCheck(L_39);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_40);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_41 = L_39;
		float L_42 = Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_2), /*hidden argument*/NULL);
		NullCheck(L_41);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_42);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_43 = L_41;
		float L_44 = Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_2), /*hidden argument*/NULL);
		float L_45 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_2), /*hidden argument*/NULL);
		NullCheck(L_43);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_add((float)L_44, (float)L_45)))));
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_46 = L_43;
		float L_47 = Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_2), /*hidden argument*/NULL);
		float L_48 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_2), /*hidden argument*/NULL);
		NullCheck(L_46);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_add((float)L_47, (float)L_48)))));
		V_3 = L_46;
		// var currentRect = rectTransform.rect;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_49 = NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9(__this, /*hidden argument*/NULL);
		NullCheck(L_49);
		RectTransform_get_rect_m7B24A1D6E0CB87F3481DDD2584C82C97025404E2(L_49, /*hidden argument*/NULL);
		// var finalPaddingsLDUR = new float[4]
		// {
		//     0,0,0,0
		// };
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_50 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)4);
		V_4 = L_50;
		// switch (selectedOrientation.left)
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_51 = V_0;
		NullCheck(L_51);
		int32_t L_52 = ((PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE *)L_51)->get_left_0();
		V_9 = L_52;
		int32_t L_53 = V_9;
		if (!L_53)
		{
			goto IL_0182;
		}
	}
	{
		int32_t L_54 = V_9;
		if ((((int32_t)L_54) == ((int32_t)1)))
		{
			goto IL_0193;
		}
	}
	{
		goto IL_01b7;
	}

IL_0182:
	{
		// finalPaddingsLDUR[0] = topRect.width * relativeLDUR[0];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_55 = V_4;
		float L_56 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_57 = V_3;
		NullCheck(L_57);
		int32_t L_58 = 0;
		float L_59 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		NullCheck(L_55);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)((float)il2cpp_codegen_multiply((float)L_56, (float)L_59)));
		// break;
		goto IL_01b7;
	}

IL_0193:
	{
		// finalPaddingsLDUR[0] = relativeLDUR[3] > relativeLDUR[0] ?
		//     topRect.width * relativeLDUR[3] :
		//     topRect.width * relativeLDUR[0];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_60 = V_4;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_61 = V_3;
		NullCheck(L_61);
		int32_t L_62 = 3;
		float L_63 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_62));
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_64 = V_3;
		NullCheck(L_64);
		int32_t L_65 = 0;
		float L_66 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		G_B30_0 = 0;
		G_B30_1 = L_60;
		if ((((float)L_63) > ((float)L_66)))
		{
			G_B31_0 = 0;
			G_B31_1 = L_60;
			goto IL_01ab;
		}
	}
	{
		float L_67 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_68 = V_3;
		NullCheck(L_68);
		int32_t L_69 = 0;
		float L_70 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_69));
		G_B32_0 = ((float)il2cpp_codegen_multiply((float)L_67, (float)L_70));
		G_B32_1 = G_B30_0;
		G_B32_2 = G_B30_1;
		goto IL_01b6;
	}

IL_01ab:
	{
		float L_71 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_72 = V_3;
		NullCheck(L_72);
		int32_t L_73 = 3;
		float L_74 = (L_72)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		G_B32_0 = ((float)il2cpp_codegen_multiply((float)L_71, (float)L_74));
		G_B32_1 = G_B31_0;
		G_B32_2 = G_B31_1;
	}

IL_01b6:
	{
		NullCheck(G_B32_2);
		(G_B32_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B32_1), (float)G_B32_0);
	}

IL_01b7:
	{
		// switch (selectedOrientation.right)
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_75 = V_0;
		NullCheck(L_75);
		int32_t L_76 = ((PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE *)L_75)->get_right_3();
		V_9 = L_76;
		int32_t L_77 = V_9;
		if (!L_77)
		{
			goto IL_01ca;
		}
	}
	{
		int32_t L_78 = V_9;
		if ((((int32_t)L_78) == ((int32_t)1)))
		{
			goto IL_01db;
		}
	}
	{
		goto IL_01ff;
	}

IL_01ca:
	{
		// finalPaddingsLDUR[3] = topRect.width * relativeLDUR[3];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_79 = V_4;
		float L_80 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_81 = V_3;
		NullCheck(L_81);
		int32_t L_82 = 3;
		float L_83 = (L_81)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		NullCheck(L_79);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)((float)il2cpp_codegen_multiply((float)L_80, (float)L_83)));
		// break;
		goto IL_01ff;
	}

IL_01db:
	{
		// finalPaddingsLDUR[3] = relativeLDUR[0] > relativeLDUR[3] ?
		//     topRect.width * relativeLDUR[0] :
		//     topRect.width * relativeLDUR[3];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_84 = V_4;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_85 = V_3;
		NullCheck(L_85);
		int32_t L_86 = 0;
		float L_87 = (L_85)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_88 = V_3;
		NullCheck(L_88);
		int32_t L_89 = 3;
		float L_90 = (L_88)->GetAt(static_cast<il2cpp_array_size_t>(L_89));
		G_B38_0 = 3;
		G_B38_1 = L_84;
		if ((((float)L_87) > ((float)L_90)))
		{
			G_B39_0 = 3;
			G_B39_1 = L_84;
			goto IL_01f3;
		}
	}
	{
		float L_91 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_92 = V_3;
		NullCheck(L_92);
		int32_t L_93 = 3;
		float L_94 = (L_92)->GetAt(static_cast<il2cpp_array_size_t>(L_93));
		G_B40_0 = ((float)il2cpp_codegen_multiply((float)L_91, (float)L_94));
		G_B40_1 = G_B38_0;
		G_B40_2 = G_B38_1;
		goto IL_01fe;
	}

IL_01f3:
	{
		float L_95 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_96 = V_3;
		NullCheck(L_96);
		int32_t L_97 = 0;
		float L_98 = (L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_97));
		G_B40_0 = ((float)il2cpp_codegen_multiply((float)L_95, (float)L_98));
		G_B40_1 = G_B39_0;
		G_B40_2 = G_B39_1;
	}

IL_01fe:
	{
		NullCheck(G_B40_2);
		(G_B40_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B40_1), (float)G_B40_0);
	}

IL_01ff:
	{
		// switch (selectedOrientation.bottom)
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_99 = V_0;
		NullCheck(L_99);
		int32_t L_100 = ((PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE *)L_99)->get_bottom_1();
		V_9 = L_100;
		int32_t L_101 = V_9;
		if (!L_101)
		{
			goto IL_0212;
		}
	}
	{
		int32_t L_102 = V_9;
		if ((((int32_t)L_102) == ((int32_t)1)))
		{
			goto IL_0223;
		}
	}
	{
		goto IL_0247;
	}

IL_0212:
	{
		// finalPaddingsLDUR[1] = topRect.height * relativeLDUR[1];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_103 = V_4;
		float L_104 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_105 = V_3;
		NullCheck(L_105);
		int32_t L_106 = 1;
		float L_107 = (L_105)->GetAt(static_cast<il2cpp_array_size_t>(L_106));
		NullCheck(L_103);
		(L_103)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)((float)il2cpp_codegen_multiply((float)L_104, (float)L_107)));
		// break;
		goto IL_0247;
	}

IL_0223:
	{
		// finalPaddingsLDUR[1] = relativeLDUR[2] > relativeLDUR[1] ?
		//     topRect.height * relativeLDUR[2] :
		//     topRect.height * relativeLDUR[1];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_108 = V_4;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_109 = V_3;
		NullCheck(L_109);
		int32_t L_110 = 2;
		float L_111 = (L_109)->GetAt(static_cast<il2cpp_array_size_t>(L_110));
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_112 = V_3;
		NullCheck(L_112);
		int32_t L_113 = 1;
		float L_114 = (L_112)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		G_B46_0 = 1;
		G_B46_1 = L_108;
		if ((((float)L_111) > ((float)L_114)))
		{
			G_B47_0 = 1;
			G_B47_1 = L_108;
			goto IL_023b;
		}
	}
	{
		float L_115 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_116 = V_3;
		NullCheck(L_116);
		int32_t L_117 = 1;
		float L_118 = (L_116)->GetAt(static_cast<il2cpp_array_size_t>(L_117));
		G_B48_0 = ((float)il2cpp_codegen_multiply((float)L_115, (float)L_118));
		G_B48_1 = G_B46_0;
		G_B48_2 = G_B46_1;
		goto IL_0246;
	}

IL_023b:
	{
		float L_119 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_120 = V_3;
		NullCheck(L_120);
		int32_t L_121 = 2;
		float L_122 = (L_120)->GetAt(static_cast<il2cpp_array_size_t>(L_121));
		G_B48_0 = ((float)il2cpp_codegen_multiply((float)L_119, (float)L_122));
		G_B48_1 = G_B47_0;
		G_B48_2 = G_B47_1;
	}

IL_0246:
	{
		NullCheck(G_B48_2);
		(G_B48_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B48_1), (float)G_B48_0);
	}

IL_0247:
	{
		// switch (selectedOrientation.top)
		PerEdgeEvaluationModes_t0D11880FFEE6D06E9866C92E6D956D4B3D99737E * L_123 = V_0;
		NullCheck(L_123);
		int32_t L_124 = ((PerEdgeValues_1_t06B3B53F39A5725AA29F949EC7185887B1339EDE *)L_123)->get_top_2();
		V_9 = L_124;
		int32_t L_125 = V_9;
		if (!L_125)
		{
			goto IL_025a;
		}
	}
	{
		int32_t L_126 = V_9;
		if ((((int32_t)L_126) == ((int32_t)1)))
		{
			goto IL_026b;
		}
	}
	{
		goto IL_028f;
	}

IL_025a:
	{
		// finalPaddingsLDUR[2] = topRect.height * relativeLDUR[2];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_127 = V_4;
		float L_128 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_129 = V_3;
		NullCheck(L_129);
		int32_t L_130 = 2;
		float L_131 = (L_129)->GetAt(static_cast<il2cpp_array_size_t>(L_130));
		NullCheck(L_127);
		(L_127)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)((float)il2cpp_codegen_multiply((float)L_128, (float)L_131)));
		// break;
		goto IL_028f;
	}

IL_026b:
	{
		// finalPaddingsLDUR[2] = relativeLDUR[1] > relativeLDUR[2] ?
		//     topRect.height * relativeLDUR[1] :
		//     topRect.height * relativeLDUR[2];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_132 = V_4;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_133 = V_3;
		NullCheck(L_133);
		int32_t L_134 = 1;
		float L_135 = (L_133)->GetAt(static_cast<il2cpp_array_size_t>(L_134));
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_136 = V_3;
		NullCheck(L_136);
		int32_t L_137 = 2;
		float L_138 = (L_136)->GetAt(static_cast<il2cpp_array_size_t>(L_137));
		G_B54_0 = 2;
		G_B54_1 = L_132;
		if ((((float)L_135) > ((float)L_138)))
		{
			G_B55_0 = 2;
			G_B55_1 = L_132;
			goto IL_0283;
		}
	}
	{
		float L_139 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_140 = V_3;
		NullCheck(L_140);
		int32_t L_141 = 2;
		float L_142 = (L_140)->GetAt(static_cast<il2cpp_array_size_t>(L_141));
		G_B56_0 = ((float)il2cpp_codegen_multiply((float)L_139, (float)L_142));
		G_B56_1 = G_B54_0;
		G_B56_2 = G_B54_1;
		goto IL_028e;
	}

IL_0283:
	{
		float L_143 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_144 = V_3;
		NullCheck(L_144);
		int32_t L_145 = 1;
		float L_146 = (L_144)->GetAt(static_cast<il2cpp_array_size_t>(L_145));
		G_B56_0 = ((float)il2cpp_codegen_multiply((float)L_143, (float)L_146));
		G_B56_1 = G_B55_0;
		G_B56_2 = G_B55_1;
	}

IL_028e:
	{
		NullCheck(G_B56_2);
		(G_B56_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B56_1), (float)G_B56_0);
	}

IL_028f:
	{
		// finalPaddingsLDUR[0] *= influence;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_147 = V_4;
		NullCheck(L_147);
		float* L_148 = ((L_147)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)));
		float L_149 = *((float*)L_148);
		float L_150 = __this->get_influence_12();
		*((float*)L_148) = (float)((float)il2cpp_codegen_multiply((float)L_149, (float)L_150));
		// finalPaddingsLDUR[1] *= influence;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_151 = V_4;
		NullCheck(L_151);
		float* L_152 = ((L_151)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)));
		float L_153 = *((float*)L_152);
		float L_154 = __this->get_influence_12();
		*((float*)L_152) = (float)((float)il2cpp_codegen_multiply((float)L_153, (float)L_154));
		// finalPaddingsLDUR[2] *= influence;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_155 = V_4;
		NullCheck(L_155);
		float* L_156 = ((L_155)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)));
		float L_157 = *((float*)L_156);
		float L_158 = __this->get_influence_12();
		*((float*)L_156) = (float)((float)il2cpp_codegen_multiply((float)L_157, (float)L_158));
		// finalPaddingsLDUR[3] *= influence;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_159 = V_4;
		NullCheck(L_159);
		float* L_160 = ((L_159)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)));
		float L_161 = *((float*)L_160);
		float L_162 = __this->get_influence_12();
		*((float*)L_160) = (float)((float)il2cpp_codegen_multiply((float)L_161, (float)L_162));
		// if (flipPadding)
		bool L_163 = __this->get_flipPadding_13();
		if (!L_163)
		{
			goto IL_030e;
		}
	}
	{
		// float remember = 0;
		V_10 = (0.0f);
		// finalPaddingsLDUR[0] = remember;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_164 = V_4;
		float L_165 = V_10;
		NullCheck(L_164);
		(L_164)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_165);
		// finalPaddingsLDUR[0] = finalPaddingsLDUR[3];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_166 = V_4;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_167 = V_4;
		NullCheck(L_167);
		int32_t L_168 = 3;
		float L_169 = (L_167)->GetAt(static_cast<il2cpp_array_size_t>(L_168));
		NullCheck(L_166);
		(L_166)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_169);
		// finalPaddingsLDUR[3] = remember;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_170 = V_4;
		float L_171 = V_10;
		NullCheck(L_170);
		(L_170)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)L_171);
		// finalPaddingsLDUR[1] = remember;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_172 = V_4;
		float L_173 = V_10;
		NullCheck(L_172);
		(L_172)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_173);
		// finalPaddingsLDUR[1] = finalPaddingsLDUR[2];
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_174 = V_4;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_175 = V_4;
		NullCheck(L_175);
		int32_t L_176 = 2;
		float L_177 = (L_175)->GetAt(static_cast<il2cpp_array_size_t>(L_176));
		NullCheck(L_174);
		(L_174)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_177);
		// finalPaddingsLDUR[2] = remember;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_178 = V_4;
		float L_179 = V_10;
		NullCheck(L_178);
		(L_178)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_179);
	}

IL_030e:
	{
		// var sizeDelta = rectTransform.sizeDelta;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_180 = NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9(__this, /*hidden argument*/NULL);
		NullCheck(L_180);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_181 = RectTransform_get_sizeDelta_mCFAE8C916280C173AB79BE32B910376E310D1C50(L_180, /*hidden argument*/NULL);
		V_5 = L_181;
		// sizeDelta.x = -(finalPaddingsLDUR[0] + finalPaddingsLDUR[3]);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_182 = V_4;
		NullCheck(L_182);
		int32_t L_183 = 0;
		float L_184 = (L_182)->GetAt(static_cast<il2cpp_array_size_t>(L_183));
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_185 = V_4;
		NullCheck(L_185);
		int32_t L_186 = 3;
		float L_187 = (L_185)->GetAt(static_cast<il2cpp_array_size_t>(L_186));
		(&V_5)->set_x_0(((-((float)il2cpp_codegen_add((float)L_184, (float)L_187)))));
		// sizeDelta.y = -(finalPaddingsLDUR[1] + finalPaddingsLDUR[2]);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_188 = V_4;
		NullCheck(L_188);
		int32_t L_189 = 1;
		float L_190 = (L_188)->GetAt(static_cast<il2cpp_array_size_t>(L_189));
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_191 = V_4;
		NullCheck(L_191);
		int32_t L_192 = 2;
		float L_193 = (L_191)->GetAt(static_cast<il2cpp_array_size_t>(L_192));
		(&V_5)->set_y_1(((-((float)il2cpp_codegen_add((float)L_190, (float)L_193)))));
		// rectTransform.sizeDelta = sizeDelta;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_194 = NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9(__this, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_195 = V_5;
		NullCheck(L_194);
		RectTransform_set_sizeDelta_m61943618442E31C6FF0556CDFC70940AE7AD04D0(L_194, L_195, /*hidden argument*/NULL);
		// Vector2 rectWidthHeight = new Vector2(topRect.width + sizeDelta.x, topRect.height + sizeDelta.y);
		float L_196 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_197 = V_5;
		float L_198 = L_197.get_x_0();
		float L_199 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_200 = V_5;
		float L_201 = L_200.get_y_1();
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_6), ((float)il2cpp_codegen_add((float)L_196, (float)L_198)), ((float)il2cpp_codegen_add((float)L_199, (float)L_201)), /*hidden argument*/NULL);
		// Vector2 zeroPosition = new Vector2(rectTransform.pivot.x * topRect.width, rectTransform.pivot.y * topRect.height);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_202 = NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9(__this, /*hidden argument*/NULL);
		NullCheck(L_202);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_203 = RectTransform_get_pivot_m146F0BB5D3873FCEF3606DAFB8994BFA978095EE(L_202, /*hidden argument*/NULL);
		float L_204 = L_203.get_x_0();
		float L_205 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_206 = NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9(__this, /*hidden argument*/NULL);
		NullCheck(L_206);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_207 = RectTransform_get_pivot_m146F0BB5D3873FCEF3606DAFB8994BFA978095EE(L_206, /*hidden argument*/NULL);
		float L_208 = L_207.get_y_1();
		float L_209 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_7), ((float)il2cpp_codegen_multiply((float)L_204, (float)L_205)), ((float)il2cpp_codegen_multiply((float)L_208, (float)L_209)), /*hidden argument*/NULL);
		// Vector2 pivotInRect = new Vector2(rectTransform.pivot.x * rectWidthHeight.x, rectTransform.pivot.y * rectWidthHeight.y);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_210 = NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9(__this, /*hidden argument*/NULL);
		NullCheck(L_210);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_211 = RectTransform_get_pivot_m146F0BB5D3873FCEF3606DAFB8994BFA978095EE(L_210, /*hidden argument*/NULL);
		float L_212 = L_211.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_213 = V_6;
		float L_214 = L_213.get_x_0();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_215 = NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9(__this, /*hidden argument*/NULL);
		NullCheck(L_215);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_216 = RectTransform_get_pivot_m146F0BB5D3873FCEF3606DAFB8994BFA978095EE(L_215, /*hidden argument*/NULL);
		float L_217 = L_216.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_218 = V_6;
		float L_219 = L_218.get_y_1();
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_8), ((float)il2cpp_codegen_multiply((float)L_212, (float)L_214)), ((float)il2cpp_codegen_multiply((float)L_217, (float)L_219)), /*hidden argument*/NULL);
		// rectTransform.anchoredPosition3D = new Vector3(
		//     finalPaddingsLDUR[0] + pivotInRect.x - zeroPosition.x,
		//     finalPaddingsLDUR[1] + pivotInRect.y - zeroPosition.y,
		// rectTransform.anchoredPosition3D.z);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_220 = NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_221 = V_4;
		NullCheck(L_221);
		int32_t L_222 = 0;
		float L_223 = (L_221)->GetAt(static_cast<il2cpp_array_size_t>(L_222));
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_224 = V_8;
		float L_225 = L_224.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_226 = V_7;
		float L_227 = L_226.get_x_0();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_228 = V_4;
		NullCheck(L_228);
		int32_t L_229 = 1;
		float L_230 = (L_228)->GetAt(static_cast<il2cpp_array_size_t>(L_229));
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_231 = V_8;
		float L_232 = L_231.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_233 = V_7;
		float L_234 = L_233.get_y_1();
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_235 = NotchSolutionUIBehaviourBase_get_rectTransform_m630002926E26908CBBCDC35799E05A0A7FEC8CB9(__this, /*hidden argument*/NULL);
		NullCheck(L_235);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_236 = RectTransform_get_anchoredPosition3D_mA9E9CCB2E97E4DCE93CF7194856F831E48F678A2(L_235, /*hidden argument*/NULL);
		float L_237 = L_236.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_238;
		memset((&L_238), 0, sizeof(L_238));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_238), ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_add((float)L_223, (float)L_225)), (float)L_227)), ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_add((float)L_230, (float)L_232)), (float)L_234)), L_237, /*hidden argument*/NULL);
		NullCheck(L_220);
		RectTransform_set_anchoredPosition3D_mD232BFB736C35B6F3367E1D63BBA6FAE098DA761(L_220, L_238, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void E7.NotchSolution.SafePadding::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SafePadding__ctor_m947CFAF0720337507641FAD6C3CE316E8F0F5988 (SafePadding_t9B39E1921C1FDC3E2384D993DC698F618291D2CE * __this, const RuntimeMethod* method)
{
	{
		// [SerializeField] [Range(0f, 1f)] float influence = 1;
		__this->set_influence_12((1.0f));
		NotchSolutionUIBehaviourBase__ctor_m06D19A834AF9C336013AA1322C9F9D3233D281A0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean E7.NotchSolution.SafePadding::<UpdateRect>g__LockSideU7C5_0(E7.NotchSolution.EdgeEvaluationMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SafePadding_U3CUpdateRectU3Eg__LockSideU7C5_0_m869D49FE6FDB56D9F3675596090D0B643A82C974 (int32_t ___saem0, const RuntimeMethod* method)
{
	{
		// switch (saem)
		int32_t L_0 = ___saem0;
		if ((!(((uint32_t)L_0) <= ((uint32_t)2))))
		{
			goto IL_0006;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0006:
	{
		// return false;
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void E7.NotchSolution.ScreenData::OnBeforeSerialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScreenData_OnBeforeSerialize_m7DACA6554B7A3F05E2BF3AAA612A82A21515EEE1 (ScreenData_tC4231E8FE32650020BC1AD704E046BE73CFCF56E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScreenData_OnBeforeSerialize_m7DACA6554B7A3F05E2BF3AAA612A82A21515EEE1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (orientations == null) return;
		Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * L_0 = __this->get_orientations_4();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (orientations == null) return;
		return;
	}

IL_0009:
	{
		// orientationKeys = orientations.Keys.ToArray();
		Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * L_1 = __this->get_orientations_4();
		NullCheck(L_1);
		KeyCollection_t42ABC2A1D8DF067994FAE1C29046ED791B7752E1 * L_2 = Dictionary_2_get_Keys_m78E2C29CA07F0853ADB4CF9D388BBA35F392C058(L_1, /*hidden argument*/Dictionary_2_get_Keys_m78E2C29CA07F0853ADB4CF9D388BBA35F392C058_RuntimeMethod_var);
		ScreenOrientationU5BU5D_t7132DACB88FB07021C6A10D0EBED5B86B66BA05F* L_3 = Enumerable_ToArray_TisScreenOrientation_tDD9EF2729A0D580721770597532935B0A7ADE020_m1584F65FE299B571F83B0E1CC97141F31B02E804(L_2, /*hidden argument*/Enumerable_ToArray_TisScreenOrientation_tDD9EF2729A0D580721770597532935B0A7ADE020_m1584F65FE299B571F83B0E1CC97141F31B02E804_RuntimeMethod_var);
		__this->set_orientationKeys_5(L_3);
		// orientationValues = orientations.Values.ToArray();
		Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * L_4 = __this->get_orientations_4();
		NullCheck(L_4);
		ValueCollection_t4F97D9B4470FCD9B1FB63D4852EE80A94A173F62 * L_5 = Dictionary_2_get_Values_mF57C2399DD74B23C81A6675DB50B0669DB946C29(L_4, /*hidden argument*/Dictionary_2_get_Values_mF57C2399DD74B23C81A6675DB50B0669DB946C29_RuntimeMethod_var);
		OrientationDependentDataU5BU5D_t7D9034DF05ACF0C0ED3E9D0E8A1AE964A815176E* L_6 = Enumerable_ToArray_TisOrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D_m5DAC4A729E2BCAA0A028DAC48018784D5FD6A29C(L_5, /*hidden argument*/Enumerable_ToArray_TisOrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D_m5DAC4A729E2BCAA0A028DAC48018784D5FD6A29C_RuntimeMethod_var);
		__this->set_orientationValues_6(L_6);
		// }
		return;
	}
}
// System.Void E7.NotchSolution.ScreenData::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScreenData_OnAfterDeserialize_m481FD2919E1839DE8EA60CDB940D9F4A4C18B147 (ScreenData_tC4231E8FE32650020BC1AD704E046BE73CFCF56E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScreenData_OnAfterDeserialize_m481FD2919E1839DE8EA60CDB940D9F4A4C18B147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// orientations = new Dictionary<ScreenOrientation, OrientationDependentData>();
		Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * L_0 = (Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE *)il2cpp_codegen_object_new(Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m762448D06E873D5D27DF97E793F29B6F4B825CC4(L_0, /*hidden argument*/Dictionary_2__ctor_m762448D06E873D5D27DF97E793F29B6F4B825CC4_RuntimeMethod_var);
		__this->set_orientations_4(L_0);
		// for (int i = 0; i < orientationKeys.Length; i++)
		V_0 = 0;
		goto IL_002e;
	}

IL_000f:
	{
		// orientations.Add(orientationKeys[i], orientationValues[i]);
		Dictionary_2_tC1C4E3AB9032A1F22D0163C128BD2BA339A7F7BE * L_1 = __this->get_orientations_4();
		ScreenOrientationU5BU5D_t7132DACB88FB07021C6A10D0EBED5B86B66BA05F* L_2 = __this->get_orientationKeys_5();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		int32_t L_5 = (int32_t)(L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		OrientationDependentDataU5BU5D_t7D9034DF05ACF0C0ED3E9D0E8A1AE964A815176E* L_6 = __this->get_orientationValues_6();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		OrientationDependentData_tC5A927A27C4E58A619C09F31B1A476B19F97B23D * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_1);
		Dictionary_2_Add_mF1B671F6CC3E8D0F730A1BE22291122B4002B8C2(L_1, L_5, L_9, /*hidden argument*/Dictionary_2_Add_mF1B671F6CC3E8D0F730A1BE22291122B4002B8C2_RuntimeMethod_var);
		// for (int i = 0; i < orientationKeys.Length; i++)
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_002e:
	{
		// for (int i = 0; i < orientationKeys.Length; i++)
		int32_t L_11 = V_0;
		ScreenOrientationU5BU5D_t7132DACB88FB07021C6A10D0EBED5B86B66BA05F* L_12 = __this->get_orientationKeys_5();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length)))))))
		{
			goto IL_000f;
		}
	}
	{
		// }
		return;
	}
}
// System.Void E7.NotchSolution.ScreenData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScreenData__ctor_m0663BE57EA610BA9471656C5297D155381AD7B65 (ScreenData_tC4231E8FE32650020BC1AD704E046BE73CFCF56E * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String E7.NotchSolution.SimulationDevice::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SimulationDevice_ToString_mDC227D690B419A8F5C38FA8EEBD3F81D9AFB5533 (SimulationDevice_t4EC038BE01BB81A3C6AE018037486B82CE44A1F2 * __this, const RuntimeMethod* method)
{
	{
		// return Meta.friendlyName;
		MetaData_t22BDEAB65B452CADFA38BB83AFD80ED804BE2828 * L_0 = __this->get_Meta_0();
		NullCheck(L_0);
		String_t* L_1 = L_0->get_friendlyName_0();
		return L_1;
	}
}
// System.Void E7.NotchSolution.SimulationDevice::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimulationDevice__ctor_mC018E91064453EB925F000BCEE8926721D03CEF8 (SimulationDevice_t4EC038BE01BB81A3C6AE018037486B82CE44A1F2 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void E7.NotchSolution.SystemInfoData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SystemInfoData__ctor_m4BACF222E07793CA8BE503006701E10D48CE14C1 (SystemInfoData_tD08E62B54CB99C80509FB60B5D31D5D14E9BBE7A * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayedUpdateRoutineU3Ed__19__ctor_m559AAD87FE3298DA82D9DE2FB988188E7F0B4DE3 (U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayedUpdateRoutineU3Ed__19_System_IDisposable_Dispose_mD9FA276AA597969F911691CC52399A885EDEB678 (U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CDelayedUpdateRoutineU3Ed__19_MoveNext_m2279F242B9EB88A2465513F5093B4F0F69C6D323 (U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0033;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return eofWait;
		NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * L_4 = V_1;
		NullCheck(L_4);
		WaitForEndOfFrame_t082FDFEAAFF92937632C357C39E55C84B8FD06D4 * L_5 = L_4->get_eofWait_8();
		__this->set_U3CU3E2__current_1(L_5);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0033:
	{
		__this->set_U3CU3E1__state_0((-1));
		// UpdateRectBase();
		NotchSolutionUIBehaviourBase_tDA50E0671C5ACD0E536E5D9386EE0051FB2CF874 * L_6 = V_1;
		NullCheck(L_6);
		NotchSolutionUIBehaviourBase_UpdateRectBase_m1D4EAA1203356D6F91B0BCFC3947B950ECB849F9(L_6, /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CDelayedUpdateRoutineU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7A01AE35B661FFFD21F0D152EB3958CCB9AA63D0 (U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayedUpdateRoutineU3Ed__19_System_Collections_IEnumerator_Reset_m45027201E08628547E570425DC36DE791FA5B6FB (U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDelayedUpdateRoutineU3Ed__19_System_Collections_IEnumerator_Reset_m45027201E08628547E570425DC36DE791FA5B6FB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, U3CDelayedUpdateRoutineU3Ed__19_System_Collections_IEnumerator_Reset_m45027201E08628547E570425DC36DE791FA5B6FB_RuntimeMethod_var);
	}
}
// System.Object E7.NotchSolution.NotchSolutionUIBehaviourBase_<DelayedUpdateRoutine>d__19::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CDelayedUpdateRoutineU3Ed__19_System_Collections_IEnumerator_get_Current_m79AE97F34D63F7EFC36316AEC4326AE18A0DB4DF (U3CDelayedUpdateRoutineU3Ed__19_tF633C0CEFB87A74C7B4BBCE923ECFE2FB49EDD5F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
