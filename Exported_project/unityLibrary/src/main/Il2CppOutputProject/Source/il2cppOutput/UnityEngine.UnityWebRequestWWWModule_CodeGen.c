﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String UnityEngine.WWW::EscapeURL(System.String)
extern void WWW_EscapeURL_m6BE746EF27C0DB6DED0EE26EC2BD1FD6B422A4AC (void);
// 0x00000002 System.String UnityEngine.WWW::EscapeURL(System.String,System.Text.Encoding)
extern void WWW_EscapeURL_m26FFA65AB485ABD0F58782DA88BFAF022704687B (void);
// 0x00000003 System.Boolean UnityEngine.WWW::get_keepWaiting()
extern void WWW_get_keepWaiting_m231A6A7A835610182D78FC414665CC75195ABD70 (void);
static Il2CppMethodPointer s_methodPointers[3] = 
{
	WWW_EscapeURL_m6BE746EF27C0DB6DED0EE26EC2BD1FD6B422A4AC,
	WWW_EscapeURL_m26FFA65AB485ABD0F58782DA88BFAF022704687B,
	WWW_get_keepWaiting_m231A6A7A835610182D78FC414665CC75195ABD70,
};
static const int32_t s_InvokerIndices[3] = 
{
	0,
	1,
	102,
};
extern const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule = 
{
	"UnityEngine.UnityWebRequestWWWModule.dll",
	3,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
