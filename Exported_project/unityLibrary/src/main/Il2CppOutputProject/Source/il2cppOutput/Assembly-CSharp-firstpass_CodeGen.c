﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 SimpleJSON.JSONNodeType SimpleJSON.JSONNode::get_Tag()
// 0x00000002 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_m68FCA7088A6690FB632145068B7E8B373A8DB4ED (void);
// 0x00000003 System.Void SimpleJSON.JSONNode::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m25F2BA097890C323C4B283AE4ED5995BB103F021 (void);
// 0x00000004 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_mE4DA966D936ACB13C37763CDCE5EC2AF5729D205 (void);
// 0x00000005 System.Void SimpleJSON.JSONNode::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m29B408C6CAB5AA50B520C24A19563CE2C8E0F9DE (void);
// 0x00000006 System.String SimpleJSON.JSONNode::get_Value()
extern void JSONNode_get_Value_m9B82BB86D4FD6E7CC92BFB773E9459329816CE9C (void);
// 0x00000007 System.Void SimpleJSON.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_m831EF2FF049F37126F9915D07AD0408400B1C72A (void);
// 0x00000008 System.Int32 SimpleJSON.JSONNode::get_Count()
extern void JSONNode_get_Count_m7AD24AA531038210738C18D8A25190B3BA76FD2D (void);
// 0x00000009 System.Boolean SimpleJSON.JSONNode::get_IsNumber()
extern void JSONNode_get_IsNumber_m3189BF7AD2A6D5D70797D383EDCF74ACEBBBCE5C (void);
// 0x0000000A System.Boolean SimpleJSON.JSONNode::get_IsString()
extern void JSONNode_get_IsString_mAA1EB2A7163F1F1CECB249CC01967F58C792D10F (void);
// 0x0000000B System.Boolean SimpleJSON.JSONNode::get_IsBoolean()
extern void JSONNode_get_IsBoolean_mAEAD2EF66A8E96031237FB0C24A3EB7678D9A67C (void);
// 0x0000000C System.Boolean SimpleJSON.JSONNode::get_IsNull()
extern void JSONNode_get_IsNull_mA9E1B086C9708E0DBA88B8D46C12145F350DD103 (void);
// 0x0000000D System.Boolean SimpleJSON.JSONNode::get_IsArray()
extern void JSONNode_get_IsArray_m1B11D601CC7BCB5FCBF86ABD789B500240A63A67 (void);
// 0x0000000E System.Boolean SimpleJSON.JSONNode::get_IsObject()
extern void JSONNode_get_IsObject_mFC432CA1A043D3EE786300135BCEDB8F60F004D7 (void);
// 0x0000000F System.Boolean SimpleJSON.JSONNode::get_Inline()
extern void JSONNode_get_Inline_m2F25D4A17A00109F0FC4E333DD0992D8F8B3E3E9 (void);
// 0x00000010 System.Void SimpleJSON.JSONNode::set_Inline(System.Boolean)
extern void JSONNode_set_Inline_m929B6EBB35EA276CF12E8A90FFCA2A8B330342A0 (void);
// 0x00000011 System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode)
extern void JSONNode_Add_mDD2480A1E9A657DD80FACB63418B7D3EB928FC8C (void);
// 0x00000012 System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode)
extern void JSONNode_Add_m31E333F3FF2EA38F6E41E269B55CA1573F6BB85F (void);
// 0x00000013 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.String)
extern void JSONNode_Remove_mF7243F4578DB464584758182E06AB93679B3783F (void);
// 0x00000014 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m7241A96E1AE22CAE87DE961567091BBFE1F5A360 (void);
// 0x00000015 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(SimpleJSON.JSONNode)
extern void JSONNode_Remove_mF567CA468968F37D6A0BC5E8A30D5F2C4A0F8EB5 (void);
// 0x00000016 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_Children()
extern void JSONNode_get_Children_m93AC18CC13651ADE3DB09B101FCB8E0B489B299F (void);
// 0x00000017 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_DeepChildren()
extern void JSONNode_get_DeepChildren_mF66414308628678DEA3A73F7B6841D82D99020CE (void);
// 0x00000018 System.String SimpleJSON.JSONNode::ToString()
extern void JSONNode_ToString_m6C2211311DCA4723345DF3832693184CB3E4554D (void);
// 0x00000019 System.String SimpleJSON.JSONNode::ToString(System.Int32)
extern void JSONNode_ToString_mC000EFD570BBC997597C47F38395D1B780063CCC (void);
// 0x0000001A System.Void SimpleJSON.JSONNode::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
// 0x0000001B SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONNode::GetEnumerator()
// 0x0000001C System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode::get_Linq()
extern void JSONNode_get_Linq_m90FBEF6424C5C7C4A0DD9489C0638F6905803DA5 (void);
// 0x0000001D SimpleJSON.JSONNode_KeyEnumerator SimpleJSON.JSONNode::get_Keys()
extern void JSONNode_get_Keys_m4D9FD5091E68ADC0A6A621818E27CD2CD1DDE7C0 (void);
// 0x0000001E SimpleJSON.JSONNode_ValueEnumerator SimpleJSON.JSONNode::get_Values()
extern void JSONNode_get_Values_mC225711E45284FEC77A340E3D7686B2F667EBE6A (void);
// 0x0000001F System.Double SimpleJSON.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_mDB5FD20F057B328E4A8D2A6E0344D712EA5222CD (void);
// 0x00000020 System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mC5255237373D9BFBB462959043D6A230D5E52642 (void);
// 0x00000021 System.Int32 SimpleJSON.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_m3F0B6AC95E43D28183E6FF0E6680E8AEBFF5614D (void);
// 0x00000022 System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m19378B69037D4A783749882479D66937E3942CE2 (void);
// 0x00000023 System.Single SimpleJSON.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_mA5E740EE81C49B0A2B3653DD2D91EAC4D0DA38B2 (void);
// 0x00000024 System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_m7389EDD5E4CAFCBF1B5FD2BC91E492736E0ECA65 (void);
// 0x00000025 System.Boolean SimpleJSON.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_m468F905B830A77EDC94036AECAE06CCB2062D2F0 (void);
// 0x00000026 System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_mAF476F53D7ED6FAE388588E1C1DD2B4EF980F5EE (void);
// 0x00000027 SimpleJSON.JSONArray SimpleJSON.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_mD6FAE94576DA50BCAF8E3EEF81CAB62C89966D3F (void);
// 0x00000028 SimpleJSON.JSONObject SimpleJSON.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_m96C974C159160E812077411341FFA273B3BD4216 (void);
// 0x00000029 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_mC6392F9282360F9ABD3AC734B18BF94C1FB7F107 (void);
// 0x0000002A System.String SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mE11B102085A3EDECED8D7593CE898C27BC363AAF (void);
// 0x0000002B SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Double)
extern void JSONNode_op_Implicit_mDB9E40DDE6449122804576F1F4FC2D1BD9FE9721 (void);
// 0x0000002C System.Double SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m96884B0F42B9282CF3B94DF70EA790591CE5D70A (void);
// 0x0000002D SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Single)
extern void JSONNode_op_Implicit_m15B5C6A8F83AEFCA8E2422039495626A6B5BA9AD (void);
// 0x0000002E System.Single SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m1C2AE3220662C5E56E16142EFD9391B7C50A318C (void);
// 0x0000002F SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Int32)
extern void JSONNode_op_Implicit_m04F10331C13E2A8FC82650B6B15E1187B045F3A7 (void);
// 0x00000030 System.Int32 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m9A07E4AE48A99860EC3766183C68D53FBD909A50 (void);
// 0x00000031 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Boolean)
extern void JSONNode_op_Implicit_m04174CC463B2FAAA3893DA0E1BC5F0EB0280C3B5 (void);
// 0x00000032 System.Boolean SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m56F05EC7453355A68B50E4933297958F8D60701C (void);
// 0x00000033 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void JSONNode_op_Implicit_m757755AFE21B579A47662DAA9BD32620FC7BC7F2 (void);
// 0x00000034 System.Boolean SimpleJSON.JSONNode::op_Equality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Equality_mEB349D8644B8E3F87CD33A35457A00480EFB329A (void);
// 0x00000035 System.Boolean SimpleJSON.JSONNode::op_Inequality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m22851EA8F1ED30D4A1B977CD92AFEEB246DD098A (void);
// 0x00000036 System.Boolean SimpleJSON.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_mBFF7A822B266FD96F6A6B54433B3F9A00CE06AA0 (void);
// 0x00000037 System.Int32 SimpleJSON.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_mA0F3A3B708D8EAABFB5AAF1A3FE8EB86A5CC90BA (void);
// 0x00000038 System.Text.StringBuilder SimpleJSON.JSONNode::get_EscapeBuilder()
extern void JSONNode_get_EscapeBuilder_m0ACB6810C49FC1E2246978F2E5C66F7344422458 (void);
// 0x00000039 System.String SimpleJSON.JSONNode::Escape(System.String)
extern void JSONNode_Escape_m71E3F923CC34FB0961ACD615E516B61AB60BBE23 (void);
// 0x0000003A System.Void SimpleJSON.JSONNode::ParseElement(SimpleJSON.JSONNode,System.String,System.String,System.Boolean)
extern void JSONNode_ParseElement_m78C5E1232F0AA824915F86ACA6114BD584BB1F71 (void);
// 0x0000003B SimpleJSON.JSONNode SimpleJSON.JSONNode::Parse(System.String)
extern void JSONNode_Parse_m69AEF21E17F84854BF203846254C8B943E9FEF21 (void);
// 0x0000003C System.Void SimpleJSON.JSONNode::.ctor()
extern void JSONNode__ctor_m1CBF4F8B6E1DECA2CC6252F7DE0BEF1D11A18A91 (void);
// 0x0000003D System.Void SimpleJSON.JSONNode::.cctor()
extern void JSONNode__cctor_mD5545294D80A8A7EC2E166FDDF8C71A4EC3F09D0 (void);
// 0x0000003E System.Boolean SimpleJSON.JSONArray::get_Inline()
extern void JSONArray_get_Inline_mC00FE5764DBE022CB2A6BF50824C467CF01FCB01 (void);
// 0x0000003F System.Void SimpleJSON.JSONArray::set_Inline(System.Boolean)
extern void JSONArray_set_Inline_mCBDA88B1041B9DDFB22CF1F95D2C9AF533C11231 (void);
// 0x00000040 SimpleJSON.JSONNodeType SimpleJSON.JSONArray::get_Tag()
extern void JSONArray_get_Tag_mBA7D2708AFD83F55024FEE237CB4CC2B53ECFDDA (void);
// 0x00000041 System.Boolean SimpleJSON.JSONArray::get_IsArray()
extern void JSONArray_get_IsArray_m194C15DF5096C82E67284D79ECC9D52A783A5B21 (void);
// 0x00000042 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_m0B2C42AB0E7CD1C885EE73FFBC8A67964C58B18D (void);
// 0x00000043 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_mB91E086B47603F6EB9E27B95FCAD0E696B2D6F35 (void);
// 0x00000044 System.Void SimpleJSON.JSONArray::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_mF4A3E324D160BF0AD1427B88A2B47F700EB69696 (void);
// 0x00000045 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_m411ED75FE2A99BEA55FE44AB9C844D971A4BC815 (void);
// 0x00000046 System.Void SimpleJSON.JSONArray::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_mF61DBA24AB5A0323BDBA8F38F1C4504FC7A7D3E6 (void);
// 0x00000047 System.Int32 SimpleJSON.JSONArray::get_Count()
extern void JSONArray_get_Count_m9468CAC9D288419DE8A7CA235B32EBC9FBF558B7 (void);
// 0x00000048 System.Void SimpleJSON.JSONArray::Add(System.String,SimpleJSON.JSONNode)
extern void JSONArray_Add_mFDEF8511E0C68436BDFEF07162C4F0AF4EAA2970 (void);
// 0x00000049 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_mD3958262883D23A7E689E01AEF74DF589AD323FE (void);
// 0x0000004A SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(SimpleJSON.JSONNode)
extern void JSONArray_Remove_m7142B83C03C15E06352847A651143A30DDD53E28 (void);
// 0x0000004B System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::get_Children()
extern void JSONArray_get_Children_mFF20142C5D77D438F218F14C2C7241E9083B15D8 (void);
// 0x0000004C System.Void SimpleJSON.JSONArray::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONArray_WriteToStringBuilder_m80DDB88D53978AE9F8BC8CB95D98F10B92E5D64B (void);
// 0x0000004D System.Void SimpleJSON.JSONArray::.ctor()
extern void JSONArray__ctor_m5F3CE693516DF2278031527B3BEB830D430F78B1 (void);
// 0x0000004E System.Boolean SimpleJSON.JSONObject::get_Inline()
extern void JSONObject_get_Inline_m359D85BE012171E4E002285F20BC429E1A6BFA98 (void);
// 0x0000004F System.Void SimpleJSON.JSONObject::set_Inline(System.Boolean)
extern void JSONObject_set_Inline_m0AEACDF3C151A715C9F63679BDFAC03171D47810 (void);
// 0x00000050 SimpleJSON.JSONNodeType SimpleJSON.JSONObject::get_Tag()
extern void JSONObject_get_Tag_m511E4A135B46CF2AFFB7509BBFBF6ABAF93603E7 (void);
// 0x00000051 System.Boolean SimpleJSON.JSONObject::get_IsObject()
extern void JSONObject_get_IsObject_m8DA1E1498537E1E1C66EB54EF30C3C60C6C0D3CB (void);
// 0x00000052 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONObject::GetEnumerator()
extern void JSONObject_GetEnumerator_m1AF735552661D6EEA2C54C3F190230835E407236 (void);
// 0x00000053 SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.String)
extern void JSONObject_get_Item_m1A6BE59A0A8BAB7FE28A6C0BC87FA4BE32D4018B (void);
// 0x00000054 System.Void SimpleJSON.JSONObject::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_mB3A6227B0FEFE0A516EE7E2F694042FE168156EB (void);
// 0x00000055 SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.Int32)
extern void JSONObject_get_Item_m76D60CDACB0991A590F72FF75D1828EA2B39F39F (void);
// 0x00000056 System.Void SimpleJSON.JSONObject::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_m3CB015E02CB932589E57C00F6C3E27322293B3A0 (void);
// 0x00000057 System.Int32 SimpleJSON.JSONObject::get_Count()
extern void JSONObject_get_Count_m19CC7B95F788DFF0CE6917F06CFDFE0062FBDB96 (void);
// 0x00000058 System.Void SimpleJSON.JSONObject::Add(System.String,SimpleJSON.JSONNode)
extern void JSONObject_Add_m0268BD87069B407DE0F601028CFBE6A52F22D594 (void);
// 0x00000059 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.String)
extern void JSONObject_Remove_mE20DCDECF7FF984A389891EF2F08DC2088E7D8C6 (void);
// 0x0000005A SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.Int32)
extern void JSONObject_Remove_mD648238FD174CC258C46E70A36BDFDBE3CC53EE8 (void);
// 0x0000005B SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(SimpleJSON.JSONNode)
extern void JSONObject_Remove_m0F1ECF30C2B6C5607AFD8C72E37E87FE5D572EDB (void);
// 0x0000005C System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject::get_Children()
extern void JSONObject_get_Children_mD36A022F70752842908E542DF8A88E827A43153C (void);
// 0x0000005D System.Void SimpleJSON.JSONObject::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONObject_WriteToStringBuilder_mC9B657B6950EAD6BFADD62F0EFCFACC8E1E25DB4 (void);
// 0x0000005E System.Void SimpleJSON.JSONObject::.ctor()
extern void JSONObject__ctor_mDC880B59ED4D826E15F67A626326822D1CD1F52C (void);
// 0x0000005F SimpleJSON.JSONNodeType SimpleJSON.JSONString::get_Tag()
extern void JSONString_get_Tag_m7E09C62F10964D4D27854A83E0860A5AC985AFE3 (void);
// 0x00000060 System.Boolean SimpleJSON.JSONString::get_IsString()
extern void JSONString_get_IsString_m5BF6795E2EC537F95E28F159B6760962D9F8CA4E (void);
// 0x00000061 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONString::GetEnumerator()
extern void JSONString_GetEnumerator_m69464F52BB0DA089F154DCB55F5ED7302310CB9D (void);
// 0x00000062 System.String SimpleJSON.JSONString::get_Value()
extern void JSONString_get_Value_m4731F37BD04F956DABFB67BA0C421211C6C97F59 (void);
// 0x00000063 System.Void SimpleJSON.JSONString::set_Value(System.String)
extern void JSONString_set_Value_m2670EB54A9F28DF3F09476CA0AEF5A74F83EF45B (void);
// 0x00000064 System.Void SimpleJSON.JSONString::.ctor(System.String)
extern void JSONString__ctor_m7E8C037643D81531C83ED7103B3B2E2847454981 (void);
// 0x00000065 System.Void SimpleJSON.JSONString::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONString_WriteToStringBuilder_mD04D1D9ECF6524AFE0BFFA950C5E28C6CCA58A81 (void);
// 0x00000066 System.Boolean SimpleJSON.JSONString::Equals(System.Object)
extern void JSONString_Equals_mBF62E77E4EA113962F09D5EF5AAB519F0C5DBE96 (void);
// 0x00000067 System.Int32 SimpleJSON.JSONString::GetHashCode()
extern void JSONString_GetHashCode_m0D03E5BEB56AD43BDEFC55A105FB80C808C22017 (void);
// 0x00000068 SimpleJSON.JSONNodeType SimpleJSON.JSONNumber::get_Tag()
extern void JSONNumber_get_Tag_m6B46BC7A9FAB14E69076796A4EC0EA692AE868F8 (void);
// 0x00000069 System.Boolean SimpleJSON.JSONNumber::get_IsNumber()
extern void JSONNumber_get_IsNumber_m4EA2B0AAFDABFB616DC3F2F5BF2432E5FBC7DE1C (void);
// 0x0000006A SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONNumber::GetEnumerator()
extern void JSONNumber_GetEnumerator_m401DDA65F1DA380E495054994C9AABEAAD9E1DA3 (void);
// 0x0000006B System.String SimpleJSON.JSONNumber::get_Value()
extern void JSONNumber_get_Value_mF47E99756A3D325CF16BE8BFD6FF7148DFCAB81D (void);
// 0x0000006C System.Void SimpleJSON.JSONNumber::set_Value(System.String)
extern void JSONNumber_set_Value_mA2DD9A59619C9FD69ED20EAD8EE931E4506589A1 (void);
// 0x0000006D System.Double SimpleJSON.JSONNumber::get_AsDouble()
extern void JSONNumber_get_AsDouble_m71742FA87FBA9CEC1B3CD7797BB12D940AA8C3D5 (void);
// 0x0000006E System.Void SimpleJSON.JSONNumber::set_AsDouble(System.Double)
extern void JSONNumber_set_AsDouble_m01EF626FFB8BCB29DDDB4AB4B6A6FD4973C0395E (void);
// 0x0000006F System.Void SimpleJSON.JSONNumber::.ctor(System.Double)
extern void JSONNumber__ctor_m540460B21D5ED37F9529F1734B24B6E868D432B8 (void);
// 0x00000070 System.Void SimpleJSON.JSONNumber::.ctor(System.String)
extern void JSONNumber__ctor_mAAF7F464DDCB064C83D2898FE50CCE90F77E4808 (void);
// 0x00000071 System.Void SimpleJSON.JSONNumber::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNumber_WriteToStringBuilder_m918A4406F272535FBDEC2E67DA558DF0B3AB2264 (void);
// 0x00000072 System.Boolean SimpleJSON.JSONNumber::IsNumeric(System.Object)
extern void JSONNumber_IsNumeric_mFAC1B74A6BD2705D6B2952D74DA6DF6D38C4F268 (void);
// 0x00000073 System.Boolean SimpleJSON.JSONNumber::Equals(System.Object)
extern void JSONNumber_Equals_mA8EBF84AAB284A4FF0E308F193B06A812E038A3F (void);
// 0x00000074 System.Int32 SimpleJSON.JSONNumber::GetHashCode()
extern void JSONNumber_GetHashCode_m886C693586191D3BE953E1E1CC7D5548B1E737D3 (void);
// 0x00000075 SimpleJSON.JSONNodeType SimpleJSON.JSONBool::get_Tag()
extern void JSONBool_get_Tag_mB9CA91991F00F4B74A0B8506DF446B77AF7872AB (void);
// 0x00000076 System.Boolean SimpleJSON.JSONBool::get_IsBoolean()
extern void JSONBool_get_IsBoolean_m4C847D82501287231AD2E08CBB6CADA94D980599 (void);
// 0x00000077 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONBool::GetEnumerator()
extern void JSONBool_GetEnumerator_mB707A772B8651C727BABE7657335A6EFCFD2D407 (void);
// 0x00000078 System.String SimpleJSON.JSONBool::get_Value()
extern void JSONBool_get_Value_m7EDACE20FE016532F6714D1311EC851635DD4C53 (void);
// 0x00000079 System.Void SimpleJSON.JSONBool::set_Value(System.String)
extern void JSONBool_set_Value_mE4AC78CF051938BCFBD45858D58A260709A210BC (void);
// 0x0000007A System.Boolean SimpleJSON.JSONBool::get_AsBool()
extern void JSONBool_get_AsBool_m2BB0201AE8B6118A20FCF2727F3F8D235505A552 (void);
// 0x0000007B System.Void SimpleJSON.JSONBool::set_AsBool(System.Boolean)
extern void JSONBool_set_AsBool_m3D364B4168376B1F5312DCF0F4B771C278BF014F (void);
// 0x0000007C System.Void SimpleJSON.JSONBool::.ctor(System.Boolean)
extern void JSONBool__ctor_mBFA987A0D1492AFBC458BB89C88E7EC4AA2BE007 (void);
// 0x0000007D System.Void SimpleJSON.JSONBool::.ctor(System.String)
extern void JSONBool__ctor_m9AAE9CA0B6044181BAF839B3CB43F9EB05DB2F8A (void);
// 0x0000007E System.Void SimpleJSON.JSONBool::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONBool_WriteToStringBuilder_m421FFE2D4A595460C5B0AA59C83E6EF5DECCC4E0 (void);
// 0x0000007F System.Boolean SimpleJSON.JSONBool::Equals(System.Object)
extern void JSONBool_Equals_mEEF0FFBE41AB3814546ABA212A53AA57517D84B1 (void);
// 0x00000080 System.Int32 SimpleJSON.JSONBool::GetHashCode()
extern void JSONBool_GetHashCode_m59F19469C64DA65D9B03D4BE6FBCBAAE2762C05E (void);
// 0x00000081 SimpleJSON.JSONNull SimpleJSON.JSONNull::CreateOrGet()
extern void JSONNull_CreateOrGet_mE2E06026E04958D2795026FBF38A47FBE14A7AEF (void);
// 0x00000082 System.Void SimpleJSON.JSONNull::.ctor()
extern void JSONNull__ctor_m774EAA6C8365C47B27BDB3FAD8C28686A1105033 (void);
// 0x00000083 SimpleJSON.JSONNodeType SimpleJSON.JSONNull::get_Tag()
extern void JSONNull_get_Tag_m8CC0AC532BEF769781F75F62AC52009E154FB105 (void);
// 0x00000084 System.Boolean SimpleJSON.JSONNull::get_IsNull()
extern void JSONNull_get_IsNull_m3B07A0182B1672877098FDC068A5ABA1AE336099 (void);
// 0x00000085 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONNull::GetEnumerator()
extern void JSONNull_GetEnumerator_m4654CE7479238B4E8CB6219F9D859CBD7784AB18 (void);
// 0x00000086 System.String SimpleJSON.JSONNull::get_Value()
extern void JSONNull_get_Value_m553A353F6BB70066CDFC90AE29595B789CE3C0ED (void);
// 0x00000087 System.Void SimpleJSON.JSONNull::set_Value(System.String)
extern void JSONNull_set_Value_mBBFC09F30AC48C4DF47BF6FCA1F271A6C6AD814A (void);
// 0x00000088 System.Boolean SimpleJSON.JSONNull::get_AsBool()
extern void JSONNull_get_AsBool_m21B6CD1890428C491DBDC195ABD86C7A9B2CB520 (void);
// 0x00000089 System.Void SimpleJSON.JSONNull::set_AsBool(System.Boolean)
extern void JSONNull_set_AsBool_m9C680F040C87C7617AC04D1CCEC4DF04B3A062B1 (void);
// 0x0000008A System.Boolean SimpleJSON.JSONNull::Equals(System.Object)
extern void JSONNull_Equals_m281EA55980AE7E71755B38A70914EDB521651CDE (void);
// 0x0000008B System.Int32 SimpleJSON.JSONNull::GetHashCode()
extern void JSONNull_GetHashCode_m11ACC117024154A6425EC0A92F92BE0850AB9AC8 (void);
// 0x0000008C System.Void SimpleJSON.JSONNull::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNull_WriteToStringBuilder_mA85B44E1E280A51A0E09A296D80CDF5735E8E4A6 (void);
// 0x0000008D System.Void SimpleJSON.JSONNull::.cctor()
extern void JSONNull__cctor_mFBF013316CC0BC72EEF846918C4C18C8EDD2C75A (void);
// 0x0000008E SimpleJSON.JSONNodeType SimpleJSON.JSONLazyCreator::get_Tag()
extern void JSONLazyCreator_get_Tag_m3A6D074EB93F58B7070B64703D0ABCF7F75B1954 (void);
// 0x0000008F SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONLazyCreator::GetEnumerator()
extern void JSONLazyCreator_GetEnumerator_m1E8AD0743755E20E6E32B8B53643063DC5F3C13B (void);
// 0x00000090 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode)
extern void JSONLazyCreator__ctor_m0B79861AA63A30F62B32C3A2F3A87F152C1015F6 (void);
// 0x00000091 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode,System.String)
extern void JSONLazyCreator__ctor_m6C390D7570282C672EB980117A97087DE5E763DF (void);
// 0x00000092 System.Void SimpleJSON.JSONLazyCreator::Set(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Set_mC0B81E1660108B5070C659C1E3D6E8E715F5BA90 (void);
// 0x00000093 SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_m5679E58AD4E2A877E1B0DFDEE1966C91F22F6873 (void);
// 0x00000094 System.Void SimpleJSON.JSONLazyCreator::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_mE8980B3490249E8442E0BBA0FFAF7A25579659FB (void);
// 0x00000095 SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_m1E8CE199AA17884522F27832C69277BB00BABF07 (void);
// 0x00000096 System.Void SimpleJSON.JSONLazyCreator::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m0D0E3BF1D133B3E0DA46F6A127350770F4837A91 (void);
// 0x00000097 System.Void SimpleJSON.JSONLazyCreator::Add(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m6AA4B279C70E27ED1B18AF98E92783CD97EA0889 (void);
// 0x00000098 System.Void SimpleJSON.JSONLazyCreator::Add(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_mA541314CB35828119821DF9D32C06C02E103D097 (void);
// 0x00000099 System.Boolean SimpleJSON.JSONLazyCreator::op_Equality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_mCB25438D736BAEC78588A1C203AE79D31064898C (void);
// 0x0000009A System.Boolean SimpleJSON.JSONLazyCreator::op_Inequality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_m02BA9367F2E9AB8C9BCD5669C6BA9DE076178218 (void);
// 0x0000009B System.Boolean SimpleJSON.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_m14E80E99AF8A7CCD7EB9991FFABF3DB01980894F (void);
// 0x0000009C System.Int32 SimpleJSON.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_m20B43AF6AED7123FA9CA49F2019A4B214F523DF9 (void);
// 0x0000009D System.Int32 SimpleJSON.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_m42A06E76316F1243A40AD6FD3C664575D8988758 (void);
// 0x0000009E System.Void SimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_mD3942EF6D8F7D250F63D8358B626E3233E4E6D9A (void);
// 0x0000009F System.Single SimpleJSON.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_m89017807E84A7B04CD9B072858E4B2780928CECF (void);
// 0x000000A0 System.Void SimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_m74C138C110E58F0F84C3E63844C9D10694D1BF9F (void);
// 0x000000A1 System.Double SimpleJSON.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_mBD1C53F2A1C5BEA7D0F16FC4427B361325D1DFF8 (void);
// 0x000000A2 System.Void SimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_m490FE6CA5F4FB2BF477934745D8D184779BC746D (void);
// 0x000000A3 System.Boolean SimpleJSON.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_mF27B6C9FC704065CDB2B178AED35069212EDDFCC (void);
// 0x000000A4 System.Void SimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_m58268C31926C3B396083BF888747BC1F5E4BCBCB (void);
// 0x000000A5 SimpleJSON.JSONArray SimpleJSON.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_m7774E52E019503D6042D0AB8021466B5E617DFBC (void);
// 0x000000A6 SimpleJSON.JSONObject SimpleJSON.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_m258758F521BE31F4BA01AD8E3D769761D320CFCC (void);
// 0x000000A7 System.Void SimpleJSON.JSONLazyCreator::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONLazyCreator_WriteToStringBuilder_m95D334FA623EB56E6BD461C32EA5CD864662F05B (void);
// 0x000000A8 SimpleJSON.JSONNode SimpleJSON.JSON::Parse(System.String)
extern void JSON_Parse_mE96FEEA722459A42C836CCF97AE3D01A1912D85D (void);
// 0x000000A9 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOFade(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOFade_mB0A1DEFE27E8F72B69B139BC8DC38C6FEA3E4BF9 (void);
// 0x000000AA DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOPitch(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOPitch_mED79FD413B765CF753901D63A15E3BAB4F136BF5 (void);
// 0x000000AB DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOSetFloat(UnityEngine.Audio.AudioMixer,System.String,System.Single,System.Single)
extern void DOTweenModuleAudio_DOSetFloat_mF674349FF317034791BEAE9809B40CCFA1869B48 (void);
// 0x000000AC System.Int32 DG.Tweening.DOTweenModuleAudio::DOComplete(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOComplete_mE45F3CE362B143FEE103CCF4529EB3F3618E0374 (void);
// 0x000000AD System.Int32 DG.Tweening.DOTweenModuleAudio::DOKill(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOKill_m9F3A656D282397AE4D0E8C922EACA496F97451B9 (void);
// 0x000000AE System.Int32 DG.Tweening.DOTweenModuleAudio::DOFlip(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOFlip_mEC014BB3042476E266602A94C3A8D7B0F68F360F (void);
// 0x000000AF System.Int32 DG.Tweening.DOTweenModuleAudio::DOGoto(UnityEngine.Audio.AudioMixer,System.Single,System.Boolean)
extern void DOTweenModuleAudio_DOGoto_m08C8ECCEE66D3351AA564933F5E3524FDADA3321 (void);
// 0x000000B0 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPause_m0CEEBD646BE6B1E365BFFC1A721AB1EC725C79F5 (void);
// 0x000000B1 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlay(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlay_m36D439A7E650CF52D9FA10268BE2D2B051457CAB (void);
// 0x000000B2 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayBackwards(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayBackwards_mB87CC3019D26370C14B31E5CD685EAD9B4A6C807 (void);
// 0x000000B3 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayForward(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayForward_m3F98B1B4E7FF894DECC8BDD7BFCFBEE05611D236 (void);
// 0x000000B4 System.Int32 DG.Tweening.DOTweenModuleAudio::DORestart(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORestart_mBBE976782787A691CACE2AF3F608061EE84066EA (void);
// 0x000000B5 System.Int32 DG.Tweening.DOTweenModuleAudio::DORewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORewind_mAC2F193E7AE357B7262B943BBE8B3E90AE234E43 (void);
// 0x000000B6 System.Int32 DG.Tweening.DOTweenModuleAudio::DOSmoothRewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOSmoothRewind_mE0ECA143026AE852C008B8F72830B1A54288712F (void);
// 0x000000B7 System.Int32 DG.Tweening.DOTweenModuleAudio::DOTogglePause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOTogglePause_m50DDFEC4D606FEE2F10D06B2F14CB754130CBF43 (void);
// 0x000000B8 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMove(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMove_m7C104B86167199AAADD840A0F20230E771E23BAC (void);
// 0x000000B9 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveX(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveX_mBF3A0D598A2B21C4A33BF6B57C693A80BA11F04E (void);
// 0x000000BA DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveY(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveY_mE9A0F966009FF670A280698CABC105F2DEC53785 (void);
// 0x000000BB DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveZ(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveZ_m74E324975AD7F83B146499FBA679D2E1CE2954F9 (void);
// 0x000000BC DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DORotate(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void DOTweenModulePhysics_DORotate_mF410C6772EB409A68E6321B0FE25DB47D07E7ACB (void);
// 0x000000BD DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DOLookAt(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.AxisConstraint,System.Nullable`1<UnityEngine.Vector3>)
extern void DOTweenModulePhysics_DOLookAt_m9538B48E2E89A7D56026F886AF566CE3F54D4FBB (void);
// 0x000000BE DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics::DOJump(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOJump_m0BB90FDEDE9EA110CC979A9DDDE549A893E1048F (void);
// 0x000000BF DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOPath_m052A26EE319E0FDFE96993555AD285A3FDFFE130 (void);
// 0x000000C0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOLocalPath_mF29800CD9C3B067EBDD1038CA785AEA459FE3951 (void);
// 0x000000C1 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOPath_mA689B9526195B1CC1ADC554D30F73A4BAAC31DCE (void);
// 0x000000C2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOLocalPath_m9F7958E2826A48DA8B593A5980D22755B00635C3 (void);
// 0x000000C3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMove_m987F6ECB82AAE0D4DB0B99E601383B9485112B5A (void);
// 0x000000C4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveX(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveX_mF18F09118B41EB441A9087FB0104331E3396DEDE (void);
// 0x000000C5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveY(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveY_m004E0B3A3AC6801B41D99E2EA737D89A61014075 (void);
// 0x000000C6 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModulePhysics2D::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
extern void DOTweenModulePhysics2D_DORotate_m3F2AAC844CABD53CA1E2F86D86CC76AFC8DDB0F0 (void);
// 0x000000C7 DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D::DOJump(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOJump_m0A7F94644F5BEFC8CE5043F90F3A165AEA3233FB (void);
// 0x000000C8 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::DOPath(UnityEngine.Rigidbody2D,UnityEngine.Vector2[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics2D_DOPath_m6FD2B95C52CE291024FB70DD01519F55F95E8E0B (void);
// 0x000000C9 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics2D::DOLocalPath(UnityEngine.Rigidbody2D,UnityEngine.Vector2[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics2D_DOLocalPath_mC425FC389DC5F01DA97D1974116ED8A30AF8A57B (void);
// 0x000000CA DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOColor_mC5483201DA04BD9EF3744EE9BB12039148EFDEB8 (void);
// 0x000000CB DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
extern void DOTweenModuleSprite_DOFade_mEC831B3B29368AFAF17BE5192BC298A5291E429A (void);
// 0x000000CC DG.Tweening.Sequence DG.Tweening.DOTweenModuleSprite::DOGradientColor(UnityEngine.SpriteRenderer,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleSprite_DOGradientColor_m78FE9EFA7085410CE51EBA5E5A40B1DA89A923D7 (void);
// 0x000000CD DG.Tweening.Tweener DG.Tweening.DOTweenModuleSprite::DOBlendableColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOBlendableColor_m811FFD010A4748CE26380DED4333685B6B34BB8F (void);
// 0x000000CE DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mBE8F5B25D6DAB090178C1AE0A97896F09D42136C (void);
// 0x000000CF DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mB297E6DBECD16B7917BEC5C66841B7F9A97EC1D2 (void);
// 0x000000D0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Graphic,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m8684861AE6E5AC93CFDA8C324F1127994EFE1D21 (void);
// 0x000000D1 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mFB279C1658A0C33E18B893B56EB5918AF6B197E2 (void);
// 0x000000D2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mC5103EC42ADFFC9414B8124AE06A78EEC6CB15E8 (void);
// 0x000000D3 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFillAmount(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFillAmount_m94B9C82653880C547041682B5584297E764D0FFB (void);
// 0x000000D4 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOGradientColor(UnityEngine.UI.Image,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUI_DOGradientColor_m7E77041E72693BD8665A823E9F240C9D3412A37E (void);
// 0x000000D5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOFlexibleSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOFlexibleSize_mAE13B0912532252843908941352094EF2CC51A62 (void);
// 0x000000D6 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOMinSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOMinSize_mFA6A60AD7883E2F33B2C43180CDF505DD77BB3A1 (void);
// 0x000000D7 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPreferredSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPreferredSize_mA76609A9F081BD2654D4806EC926BD9A22BE821A (void);
// 0x000000D8 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Outline,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mAB679CEC795E637B1C5EEAD7D8587FD585038685 (void);
// 0x000000D9 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Outline,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mA29DCB435C24F977562C78B3F10DAA3CA4002BEA (void);
// 0x000000DA DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOScale(UnityEngine.UI.Outline,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOScale_m12677AAAF1D3E459962C2AC5822490CCC4462510 (void);
// 0x000000DB DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos_mF3F0D7B55596FAC36C1626BFE3AFBE8B62D8CEE7 (void);
// 0x000000DC DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosX_m2AF646AA8418FC9FA314040BF6ACEF0203C86885 (void);
// 0x000000DD DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosY_mC469FB1A568CA7D1AF1962779DE37AF4435EFDAE (void);
// 0x000000DE DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3D_m1E6E6DBDFAA9075D543B38E51897E7D3A1ECB6C3 (void);
// 0x000000DF DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DX_m4289CECA46B7121C85F77C1DE799F7299E6DF1CF (void);
// 0x000000E0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DY_m5D91164E28D3F06B217F5E58DEA39377575B8B07 (void);
// 0x000000E1 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DZ(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DZ_mD88C597A518B248666A0D48B64BC1B58B56C9703 (void);
// 0x000000E2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMax(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMax_m197F8BF707AA13F292821A6ECB7F28C4FF8785C0 (void);
// 0x000000E3 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMin(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMin_m35F8E0D8D4D25AF1267E6881F330375A2B8EEE42 (void);
// 0x000000E4 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivot(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOPivot_mD53B5CF825615686B097049CE62AA8E3491125A8 (void);
// 0x000000E5 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotX(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotX_m1C0A12505CD967E3972BED5D67BF43E6E921AF79 (void);
// 0x000000E6 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotY(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotY_mB5F9616AF3C60BE5C100098D6C036C7D5E7CB5AD (void);
// 0x000000E7 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOSizeDelta_m78D3A33AECD966A309AF8C208D3D6B9314D38F32 (void);
// 0x000000E8 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPunchAnchorPos_mD2BB31D6D9298D4E8495F5E1D10FC7C0B87F10EA (void);
// 0x000000E9 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,System.Single,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_mC1E8246B2F64FEABE7E605708E7F0698BA618D6A (void);
// 0x000000EA DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m2D790EA998982A8D6DCEFA40FEBF70AE38C0AD5A (void);
// 0x000000EB DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOJumpAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOJumpAnchorPos_m2A51D08D3FB82651DB54F793EB4AD73421D13324 (void);
// 0x000000EC DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DONormalizedPos(UnityEngine.UI.ScrollRect,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DONormalizedPos_m939397030AB516930DBFD9C8D45A3AEDE321405C (void);
// 0x000000ED DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOHorizontalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOHorizontalNormalizedPos_mFACB6552CDAA4D2446AF2A9415FE05BDE892BE7A (void);
// 0x000000EE DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOVerticalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOVerticalNormalizedPos_m57EFC967C31D427F90F677DA374751C0CB2956DA (void);
// 0x000000EF DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOValue(UnityEngine.UI.Slider,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOValue_m9907FC2755FEE780DBAAF0452BD4FC6A5CC328EA (void);
// 0x000000F0 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mD7B708C07D203365A38EF934AFACD21C24B1C165 (void);
// 0x000000F1 DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions> DG.Tweening.DOTweenModuleUI::DOCounter(UnityEngine.UI.Text,System.Int32,System.Int32,System.Single,System.Boolean,System.Globalization.CultureInfo)
extern void DOTweenModuleUI_DOCounter_m5FCE25042F0583B46E7E02B9C3C0A325CF115BC6 (void);
// 0x000000F2 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m8486AF190AF2A12A2447B9F151B26A6DFA15D712 (void);
// 0x000000F3 DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.DOTweenModuleUI::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern void DOTweenModuleUI_DOText_m492C7500E6933DC63649880C1109F5069F091704 (void);
// 0x000000F4 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_mB08C0D34D2A3FEFF0467BF3D89D2205182AB8035 (void);
// 0x000000F5 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m1C41D6AF610C8794B0478893E179B99EFB1C4F04 (void);
// 0x000000F6 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m960BF5A6F36E365EC0772648264F4AB1528F7D1D (void);
// 0x000000F7 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_m2481629D3A7249FF88BBA02967ACC65DE4B3BF1A (void);
// 0x000000F8 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.String,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_m442F630721A1EB3B66DF29C18A2AEAF3B521D7D4 (void);
// 0x000000F9 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForCompletion(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForCompletion_m088FEB04A09709E31A69211AB9DE0AEC43B95543 (void);
// 0x000000FA UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForRewind(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForRewind_mD4A412B8E08D883BE8B5B92987FA63E06ABD0526 (void);
// 0x000000FB UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForKill(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForKill_m1569FF0CBF305C3C6EC2C68716FBF38B88FF459F (void);
// 0x000000FC UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForElapsedLoops_m1778D8A9ADF3F4FFA79E3EBAB6435C641A78E72D (void);
// 0x000000FD UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForPosition(DG.Tweening.Tween,System.Single,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForPosition_mED12B668D115E6AD9AFDBB6105F79EF7ECA81940 (void);
// 0x000000FE UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForStart(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForStart_mDF0DA79B71A0C48B7B9AB4357B7972EBA0A16342 (void);
// 0x000000FF DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOOffset_mA0198BD6CDD74693047812B24DE55E63AC62AEE3 (void);
// 0x00000100 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOTiling_mCECBD1E9F943E2044524759B9C3701C07139BC60 (void);
// 0x00000101 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForCompletion(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForCompletion_mB902946F8997751FD34B45C02072CD521D2B488D (void);
// 0x00000102 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForRewind(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForRewind_m0A51B9394F968A1AC37304A430D241358916C7A9 (void);
// 0x00000103 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForKill(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForKill_mC24606201524AB6EEDA13517713DCCDEE84B683A (void);
// 0x00000104 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForElapsedLoops(DG.Tweening.Tween,System.Int32)
extern void DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_mCC2A02BF06D5EBFD0BFE2CD8904D218375DBBBE2 (void);
// 0x00000105 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForPosition(DG.Tweening.Tween,System.Single)
extern void DOTweenModuleUnityVersion_AsyncWaitForPosition_m8ACE33C300180234AA183ACC96518425BD77E3F8 (void);
// 0x00000106 System.Threading.Tasks.Task DG.Tweening.DOTweenModuleUnityVersion::AsyncWaitForStart(DG.Tweening.Tween)
extern void DOTweenModuleUnityVersion_AsyncWaitForStart_m5C7F5659DDC019416014E896836394EDE186F2E2 (void);
// 0x00000107 System.Void DG.Tweening.DOTweenModuleUtils::Init()
extern void DOTweenModuleUtils_Init_mD1FDBB7E353A91D49F518C9E053F0CD77814D0CD (void);
// 0x00000108 System.Void DG.Tweening.DOTweenModuleUtils::Preserver()
extern void DOTweenModuleUtils_Preserver_mA1D92B9161C90E3558CF28A7AC5DB75098605201 (void);
// 0x00000109 System.Boolean SimpleJSON.JSONNode_Enumerator::get_IsValid()
extern void Enumerator_get_IsValid_mFA3BB4B7F43B1005535936519FD3A42F5B23981E_AdjustorThunk (void);
// 0x0000010A System.Void SimpleJSON.JSONNode_Enumerator::.ctor(System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>)
extern void Enumerator__ctor_m057901299D85978F34E86C2B99997599C904516F_AdjustorThunk (void);
// 0x0000010B System.Void SimpleJSON.JSONNode_Enumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>)
extern void Enumerator__ctor_m1DBE7DBD0628D4C87A31D88096548AF2917CC576_AdjustorThunk (void);
// 0x0000010C System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode_Enumerator::get_Current()
extern void Enumerator_get_Current_mF9E718C3795EB611071FF790A62BB5CAFD377FA9_AdjustorThunk (void);
// 0x0000010D System.Boolean SimpleJSON.JSONNode_Enumerator::MoveNext()
extern void Enumerator_MoveNext_m580CDFBD1C55B6794F5567E544DE9857A5618A6E_AdjustorThunk (void);
// 0x0000010E System.Void SimpleJSON.JSONNode_ValueEnumerator::.ctor(System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m9E8A702BF30F549C55B441B43EBBF823D1F8C3AF_AdjustorThunk (void);
// 0x0000010F System.Void SimpleJSON.JSONNode_ValueEnumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_mD13470952E4791B2DC48D9A64AD1735248EC0AF2_AdjustorThunk (void);
// 0x00000110 System.Void SimpleJSON.JSONNode_ValueEnumerator::.ctor(SimpleJSON.JSONNode_Enumerator)
extern void ValueEnumerator__ctor_mCB3648E562A32EEA21FD3F4DB8C20160ED03722E_AdjustorThunk (void);
// 0x00000111 SimpleJSON.JSONNode SimpleJSON.JSONNode_ValueEnumerator::get_Current()
extern void ValueEnumerator_get_Current_mF69BBD9003C127D62E50E3A2AB4179EA3863B710_AdjustorThunk (void);
// 0x00000112 System.Boolean SimpleJSON.JSONNode_ValueEnumerator::MoveNext()
extern void ValueEnumerator_MoveNext_mFBBE4BD2BD4BEDC52A09992FFB2BE22E61571C4B_AdjustorThunk (void);
// 0x00000113 SimpleJSON.JSONNode_ValueEnumerator SimpleJSON.JSONNode_ValueEnumerator::GetEnumerator()
extern void ValueEnumerator_GetEnumerator_mB4E3F0A6AC7AA6140B861775571A15036BECB254_AdjustorThunk (void);
// 0x00000114 System.Void SimpleJSON.JSONNode_KeyEnumerator::.ctor(System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_m13B6087FA46C7C03A3471C28B3FBC22DD4171FC7_AdjustorThunk (void);
// 0x00000115 System.Void SimpleJSON.JSONNode_KeyEnumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_mD48489EDADC6546248A26244922162520AD16C3B_AdjustorThunk (void);
// 0x00000116 System.Void SimpleJSON.JSONNode_KeyEnumerator::.ctor(SimpleJSON.JSONNode_Enumerator)
extern void KeyEnumerator__ctor_m0747B3AC824951C3C8B818496CEDD6D88B9E7E8A_AdjustorThunk (void);
// 0x00000117 SimpleJSON.JSONNode SimpleJSON.JSONNode_KeyEnumerator::get_Current()
extern void KeyEnumerator_get_Current_m1E30F213CEB7C4567654D28C37E2841B4A81FAE0_AdjustorThunk (void);
// 0x00000118 System.Boolean SimpleJSON.JSONNode_KeyEnumerator::MoveNext()
extern void KeyEnumerator_MoveNext_m99B9BF2D4062F287B8D3A55C2C691537CAFE6F98_AdjustorThunk (void);
// 0x00000119 SimpleJSON.JSONNode_KeyEnumerator SimpleJSON.JSONNode_KeyEnumerator::GetEnumerator()
extern void KeyEnumerator_GetEnumerator_m0243DD23679D98AE2F30AD67FFFD4E27A16AB170_AdjustorThunk (void);
// 0x0000011A System.Void SimpleJSON.JSONNode_LinqEnumerator::.ctor(SimpleJSON.JSONNode)
extern void LinqEnumerator__ctor_m3B100A94EC3DF14B4A94AE12D32050F84E72F65B (void);
// 0x0000011B System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode_LinqEnumerator::get_Current()
extern void LinqEnumerator_get_Current_m414843B6351637AAAF627AAD81880DE4BFD61353 (void);
// 0x0000011C System.Object SimpleJSON.JSONNode_LinqEnumerator::System.Collections.IEnumerator.get_Current()
extern void LinqEnumerator_System_Collections_IEnumerator_get_Current_m7F2D3716F0A2DAB0E37902FCC792A31BF9063758 (void);
// 0x0000011D System.Boolean SimpleJSON.JSONNode_LinqEnumerator::MoveNext()
extern void LinqEnumerator_MoveNext_mC600DAAECE627F2900FA1975C41E3D95F79E30F5 (void);
// 0x0000011E System.Void SimpleJSON.JSONNode_LinqEnumerator::Dispose()
extern void LinqEnumerator_Dispose_m1E091AD169C66B9DA526BA37FFC6DC662ECE3272 (void);
// 0x0000011F System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode_LinqEnumerator::GetEnumerator()
extern void LinqEnumerator_GetEnumerator_m8EE24F869680079C3280C231400F84085414E93E (void);
// 0x00000120 System.Void SimpleJSON.JSONNode_LinqEnumerator::Reset()
extern void LinqEnumerator_Reset_m5412BB4216090D65D10EE8E9E3D3006CAC45C43C (void);
// 0x00000121 System.Collections.IEnumerator SimpleJSON.JSONNode_LinqEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m74AF45D293634ECACC82EE9CB8987984DB4E6968 (void);
// 0x00000122 System.Void SimpleJSON.JSONNode_<get_Children>d__39::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__39__ctor_m63F0D06333FBB49218C0B5014FECFBC5175BD0A5 (void);
// 0x00000123 System.Void SimpleJSON.JSONNode_<get_Children>d__39::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mE120FE95B41F93A499A4D5DC467888E726B2F1E3 (void);
// 0x00000124 System.Boolean SimpleJSON.JSONNode_<get_Children>d__39::MoveNext()
extern void U3Cget_ChildrenU3Ed__39_MoveNext_m79E16CADBE6A9ADCE0E05EC107BBE0DCFA4FC44D (void);
// 0x00000125 SimpleJSON.JSONNode SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m5092BD25A20414E5F6EB32077296D953F0D581B6 (void);
// 0x00000126 System.Void SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_m27A1C27A483D81AA0440A12DD10FE9AD9E3A6ACC (void);
// 0x00000127 System.Object SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_mAD0C2CEA294864931CA571CB5D7EFBCD0C670BE6 (void);
// 0x00000128 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mA647F5A5EEA340473000B2E0E7F4B82337322ED6 (void);
// 0x00000129 System.Collections.IEnumerator SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_m3B6F1A44882D66A956E6B435D261558891C87CE5 (void);
// 0x0000012A System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::.ctor(System.Int32)
extern void U3Cget_DeepChildrenU3Ed__41__ctor_m0C4C7559A4F8423F94519BECED7E7461F0C6C59C (void);
// 0x0000012B System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.IDisposable.Dispose()
extern void U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_mE9B558A92556F9BC9B1DAE775781708631B23773 (void);
// 0x0000012C System.Boolean SimpleJSON.JSONNode_<get_DeepChildren>d__41::MoveNext()
extern void U3Cget_DeepChildrenU3Ed__41_MoveNext_mF6F808F7064BD1D12BE84B35B6DB06C667F5B202 (void);
// 0x0000012D System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::<>m__Finally1()
extern void U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally1_mFBED011F823B343DF2B3F5C760644DD0C9F476BE (void);
// 0x0000012E System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::<>m__Finally2()
extern void U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally2_m2C4BE4DFF37F16458DDF888AED1FA19576F422CC (void);
// 0x0000012F SimpleJSON.JSONNode SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mD96920D0B8440B39B9A5FB193E20F50C798CE42A (void);
// 0x00000130 System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_mC12AE3C207E5BE2C29D113F7524B3DF55C709B66 (void);
// 0x00000131 System.Object SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_m8BB67D7F47460E168E96A9A294806D0A725D8B19 (void);
// 0x00000132 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBB66C6FD838719E9C3F687CE7E9E37C206DE6460 (void);
// 0x00000133 System.Collections.IEnumerator SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m8F9A41F8EC3D989146B598EBFB8AF6F63FF9F014 (void);
// 0x00000134 System.Void SimpleJSON.JSONArray_<get_Children>d__22::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__22__ctor_mFDAB07A9A4574778F9BE79CB24F5D25268081AEC (void);
// 0x00000135 System.Void SimpleJSON.JSONArray_<get_Children>d__22::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_m27D90B6AD18FAE9F1C3491048B45920A3221EC50 (void);
// 0x00000136 System.Boolean SimpleJSON.JSONArray_<get_Children>d__22::MoveNext()
extern void U3Cget_ChildrenU3Ed__22_MoveNext_m71D465B34326827965E057A2D08CE6A07D4FB0B5 (void);
// 0x00000137 System.Void SimpleJSON.JSONArray_<get_Children>d__22::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_mE203663469D7E7DCC70B97ABC4E7DF17CE3AFD14 (void);
// 0x00000138 SimpleJSON.JSONNode SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m31F5F2527DFDC39F0317D8236755CA263E0A6051 (void);
// 0x00000139 System.Void SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_mEFE1CB55600EC3E065CBE2DA326B800C5A6B3DF7 (void);
// 0x0000013A System.Object SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m078E049917A635A9534AD2BBC91AE3F7C507F558 (void);
// 0x0000013B System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF48300F880C2258127A0D59E244B611816290D87 (void);
// 0x0000013C System.Collections.IEnumerator SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m02F5A03E66FDD6D8FBDD6731827D6490F4A04B29 (void);
// 0x0000013D System.Void SimpleJSON.JSONObject_<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mE55EB0815ED94FB7DA4223EB2B1D83A792D49444 (void);
// 0x0000013E System.Boolean SimpleJSON.JSONObject_<>c__DisplayClass21_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m32C80D793500A7AD9747C4B9BEF7E5902322DC8D (void);
// 0x0000013F System.Void SimpleJSON.JSONObject_<get_Children>d__23::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__23__ctor_mAAFF58023C08EF32D6720ED98EAAEBDDEC41A5D3 (void);
// 0x00000140 System.Void SimpleJSON.JSONObject_<get_Children>d__23::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m181A10417CF3D7FB11597D6BC8980F1041538D6C (void);
// 0x00000141 System.Boolean SimpleJSON.JSONObject_<get_Children>d__23::MoveNext()
extern void U3Cget_ChildrenU3Ed__23_MoveNext_m73899F4BB42E92BEEDCC9E6DE93498B3E5FA12E8 (void);
// 0x00000142 System.Void SimpleJSON.JSONObject_<get_Children>d__23::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m5675C62761DE690A974233757AFF7ACA2F6588C5 (void);
// 0x00000143 SimpleJSON.JSONNode SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mEE728FF36A84F0406C9961667F0CF83A1D076EF8 (void);
// 0x00000144 System.Void SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_m1878F871DF3F1DECBC29D1EA12F302E7C7D913C0 (void);
// 0x00000145 System.Object SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_mEDC188741AB55EAA55CD475E71338FF617C4DB81 (void);
// 0x00000146 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m7F30C3A17067EAEBE1F620E688233DDA9E058C00 (void);
// 0x00000147 System.Collections.IEnumerator SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m0435461165875637BB8E9DDD819E8C18BC824AA0 (void);
// 0x00000148 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mC9A0CAB6C9AE11C4941DFA9D053070CD2FD516BC (void);
// 0x00000149 System.Single DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m49FE38C026CC5480F88654B30AFC76E56349778C (void);
// 0x0000014A System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m08E2A8A3819BE9AE4A4C1AFAE018E27119B7DB13 (void);
// 0x0000014B System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m0DDEA1A43F1A3843AA5C62C248793853ACB56FB8 (void);
// 0x0000014C System.Single DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::<DOPitch>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_m58DB59F47188A28A5127A320B556E44BA6EB22D1 (void);
// 0x0000014D System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::<DOPitch>b__1(System.Single)
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_m5DB4AFB51E6AAD83942F9E6DFB17DFE6C75E0F74 (void);
// 0x0000014E System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m98D11882C35266F66659B1EE1BF86BD1A10B5682 (void);
// 0x0000014F System.Single DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::<DOSetFloat>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_mDF96B328362E424BCFAC0784FE3878D968810737 (void);
// 0x00000150 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::<DOSetFloat>b__1(System.Single)
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_mEB3F4011A9EC3F36357547FE0C0DCF2B0657601B (void);
// 0x00000151 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m63ADD398E427E85420399A7A291C8AB3E114E802 (void);
// 0x00000152 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_m9CCE2617F2DF17E4C81D16589545B91914B6E14F (void);
// 0x00000153 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m118BC92350F85BB6B7D0E4DFD001AC7F14087869 (void);
// 0x00000154 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_mDAB4C01153C8CB75453B44B664EB12A541F3E8AC (void);
// 0x00000155 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m89CE3B2AD0685D6644943F7F0F11C7A7E6833B50 (void);
// 0x00000156 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_mF843B9D21CD4FB10E190912B502BCE5936E62BA9 (void);
// 0x00000157 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB16E21526E8B098B45C43A2E3CB12BC02845FEC0 (void);
// 0x00000158 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass3_0::<DOMoveZ>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_mE6BE0287D941A3382FE35DBA638FFE636B4BD5B4 (void);
// 0x00000159 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m4494A87C8737B4326800E02CC5E66162DCACEBA4 (void);
// 0x0000015A UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass4_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_mF2DACA96604E7E3FA529BB1F3BC1B8D1229D29B0 (void);
// 0x0000015B System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m61B7AF03993F87E42AAF65A8EC56F6D595BF15FA (void);
// 0x0000015C UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass5_0::<DOLookAt>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_m35E9F4DC31C1B39AE7AD17C7FD5494F86986983D (void);
// 0x0000015D System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m136EBFF341B7C238E9517606B9CAC933AF4DAE3A (void);
// 0x0000015E UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_mAB30C048A7DCC1D1B9656E86507A956EE374ACE5 (void);
// 0x0000015F System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__1()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_m62C7B6DAAEC853E2C48E2F56A0918FC39DDA2E40 (void);
// 0x00000160 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_mC30FDCDD0BE010B84AD37D5F54585F3A77EC4AC5 (void);
// 0x00000161 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_m305EA5A202C011BB907DA1768C055BBF76C938D9 (void);
// 0x00000162 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__4()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_m39B304CB49D3BC14B2C9A7F94EF46A28E45E7089 (void);
// 0x00000163 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m7EB4080EE45F71C7B5C7FC8C4397C300F5ACBDD8 (void);
// 0x00000164 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass7_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_m124D52228901D80260472F63AA26FD57C4D1055C (void);
// 0x00000165 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m01CA622748CDB6ACEC270067D893562F467C794C (void);
// 0x00000166 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_m14FC5E33F0A124AB11E785DA497603548DBA0C58 (void);
// 0x00000167 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m87A4D3BD60A9AE2ACF56C51350139E47D971B5FD (void);
// 0x00000168 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m825F7F53873EAA6CE53B2CB914B3D2F51F87100F (void);
// 0x00000169 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass9_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mE6BEB9A34A2ACA95E5E4789FB0A41AD09144D6CC (void);
// 0x0000016A System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mD65EDC783B8DA7F8CE6DC579F670BE939068A9FB (void);
// 0x0000016B UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m4AB566ED4A6AD33D97665AEDAF614628027AF26F (void);
// 0x0000016C System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_m19F363524123912BC25BEBEBBFB3B5406B699158 (void);
// 0x0000016D System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mD5121B8CCB444BDD80CF1A9E3757B55A9ABB9C12 (void);
// 0x0000016E UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mCFD816BA19520401F2B169ED5775476BB24DCC5D (void);
// 0x0000016F System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mBC28699A1759EF11EC626AECEB04BCA8E3A29E21 (void);
// 0x00000170 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_mFC38E73FD38BE1442DB5F8C6BE0642367BE20B73 (void);
// 0x00000171 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m99DCD72B6EBC0698DE92E5360DE96C3B77A08760 (void);
// 0x00000172 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_mB98366A51D3C8AAA935B99D11DE9CACF8BADF093 (void);
// 0x00000173 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m6284017530E3CAE718B57CE0B1A4B5538AE67A0B (void);
// 0x00000174 System.Single DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass3_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_m30816E46E9AE2707BA6A40BB14AB1CC9FED758E0 (void);
// 0x00000175 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mCACB56B32C32686E61CBAF0DB42451D8E3C9D2D6 (void);
// 0x00000176 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_m07B218C5A79A8036DBC75F17CF4E719D58F515FF (void);
// 0x00000177 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_mFA4C0E124D639B83AC861602527C4674B32BE5ED (void);
// 0x00000178 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_mDE18B38CBAF272EC44518040B4ED92CBAAEBDB5D (void);
// 0x00000179 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_mB6C41172DBCDE9EC8F7C7AC9FCCA12237257CF3F (void);
// 0x0000017A System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_m8F939BEEC32E261FC859B4001E445BF37B3C4480 (void);
// 0x0000017B System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__5()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_m8256969EBC92F019390147EAF159D5D031B5D12C (void);
// 0x0000017C System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mAA50C827E27780810FD49A9186C8FE9730A8050D (void);
// 0x0000017D UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass5_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__0_m60FACA3EED8EF251E50C4AF2359626EF8CBC7558 (void);
// 0x0000017E System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass5_0::<DOPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__1_mB0F5F2CC8D39790B79757A33CE1DCD4547E7EB50 (void);
// 0x0000017F System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m00FAAED8DFB2827420F97A994E63F3BD6082BD3B (void);
// 0x00000180 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass6_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__0_m99C1C98AA603CEA2F8466485D1F1FC02933A7F93 (void);
// 0x00000181 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass6_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__1_m869C353E4D757757FF7EAA0086FB9A049CFB7B86 (void);
// 0x00000182 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m6CAF206E9568D6336FBD31D538A6A0FA34E81EF0 (void);
// 0x00000183 UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m7DFAA0E76AA73088F8301C0887A6CA9FA7A393DD (void);
// 0x00000184 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m4526CF01023DFFE3F5728C31BDAC0F90A6C9EBEC (void);
// 0x00000185 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mBC0D7BDCA11CE5E38E24A61DE8168DD96AC1A840 (void);
// 0x00000186 UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m9D7DE4E2377E078233BB9FF0DF4E3E39759A892B (void);
// 0x00000187 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_m0EE03E1FA86827EC879708FEEC4D68BFF4AD0837 (void);
// 0x00000188 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m708AF2802A87C338E12EA52A4DA08D97BBE16375 (void);
// 0x00000189 UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_mD3C2065A33BBBA7103E37DBB8B08FB825B1FF49A (void);
// 0x0000018A System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_m8A9D459501EE0E9844F947EE642963955F7E785F (void);
// 0x0000018B UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_Utils::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern void Utils_SwitchToRectTransform_m03DE2A203997D266A289C397CCA1CD2FF868F224 (void);
// 0x0000018C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m141C1A36E04A1010B221331919A2DD643737E1F1 (void);
// 0x0000018D System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m4DC14E1110C69A8007F90B04434A9667CDB71347 (void);
// 0x0000018E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m07B0D13C8F401F39AF1CAA6E711DAEB2631A0C6E (void);
// 0x0000018F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mF08B02DF314825A9F3252AA0DB3FBA0438499B71 (void);
// 0x00000190 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_m4EB29EC972AC3CFE8D785A37D3EA0FE5919104FF (void);
// 0x00000191 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m1C6DB448A2B33AE11FE43DC4D4D978802A846D08 (void);
// 0x00000192 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m940E80A7BD91EC634A3063A81705C1CA897441BA (void);
// 0x00000193 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_mCEAD71FEAA2ABFC92DC1E9F2CF424E926C4B3C9E (void);
// 0x00000194 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m5215F02965D28B7BE17F609BD81F4C94E34D9967 (void);
// 0x00000195 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mFC1D1AFA961EE54EC3AC45CBD6ACBB2198F9F4D1 (void);
// 0x00000196 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m0CD987CF99960944A1615486EBD5DECF4DC14263 (void);
// 0x00000197 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mFA47EF72CB09FD706B512F128D84F6FC5CEB06E7 (void);
// 0x00000198 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mAB4D14EE7D6F085ADAC49C1D4084D8E410904ADD (void);
// 0x00000199 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m27D07F05CFF165E3EC68A3C4A9C313DF097CFE64 (void);
// 0x0000019A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mAADA4AEF9FAD900BE9DF963E3D9FD4D1A390FFEB (void);
// 0x0000019B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mA716A695CEF34C1DD9DDBCF87A32B3EA6AC56DEE (void);
// 0x0000019C System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::<DOFillAmount>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m17681A9FFE53F44E317BB2D74151638AE6D6CA10 (void);
// 0x0000019D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::<DOFillAmount>b__1(System.Single)
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m98974BD33AD9B1EE1225CF9425A693E9E9A95BF9 (void);
// 0x0000019E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mC33276AAC40B1986BD7D786A13A75BE46D98E575 (void);
// 0x0000019F UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::<DOFlexibleSize>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m0E752801FD65C3CECA2CBA2B0B41F1121273B7DF (void);
// 0x000001A0 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::<DOFlexibleSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m16B62B9FCEDD3323BB0E707581DCD1095A971E35 (void);
// 0x000001A1 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mBFB31B6BD9C48D8BBBF9A246C359E814B2F1AA2A (void);
// 0x000001A2 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::<DOMinSize>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m4A4A66B3A15F7DFF6001C080DA64A84D26AA98E2 (void);
// 0x000001A3 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::<DOMinSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m3F88457C1647900570A9BC38EFE371F5A3446061 (void);
// 0x000001A4 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m77573B5C974D508FD7B51B458EACEF80C8E97C5A (void);
// 0x000001A5 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::<DOPreferredSize>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mA5D6160C3BBF351768B63AE9547F3C39D878FE24 (void);
// 0x000001A6 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::<DOPreferredSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m197C519DB62AF13D4B15A85F4C3B1F01A47930E0 (void);
// 0x000001A7 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mC7ABBF2015432FC179739C7796F200D2A6B624CF (void);
// 0x000001A8 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_m902BA935FEFB26FB6274C2CFBFB54600CA093E04 (void);
// 0x000001A9 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m443BDCBF61E18262396FD7CCCAB8EA5224C653DC (void);
// 0x000001AA System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mFF37C7CB9DDFF2D79FEDE2C307994557E7403CC7 (void);
// 0x000001AB UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_m24EC0BECBC9CE42D544B0DA1A2610A5DF5AE8CB6 (void);
// 0x000001AC System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_m580E11AA158861453455CEE077B169160022344A (void);
// 0x000001AD System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m37609BBBCE7805314FEA1780512A34444EFFAC30 (void);
// 0x000001AE UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::<DOScale>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_m154033992493DB776542F0C4D9DF31201DB230FD (void);
// 0x000001AF System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::<DOScale>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m6E6608DBFE9AB8B89C150C2FEECABEDC067EB020 (void);
// 0x000001B0 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mEC3B6B763DF4E0887CCFE5EB7FE3F483F8C07979 (void);
// 0x000001B1 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::<DOAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m74C05A6B7DA6E7E588B7D66913B49B7AB3041FB3 (void);
// 0x000001B2 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::<DOAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_mC775BAB0715629CA4D07582CC2CF19A1BC8FFA0B (void);
// 0x000001B3 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m67EA9DE8A36ECA66FEE7C3C548E501E250BFDC46 (void);
// 0x000001B4 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::<DOAnchorPosX>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_mD702D92407483FB14C76144B9087419DCBE562EA (void);
// 0x000001B5 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::<DOAnchorPosX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m8769407CA79C59C2CBD9E162CA195DCCAC96DB1D (void);
// 0x000001B6 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m279D9D9F290E99D443091BEA0CEA8AEC6B00B095 (void);
// 0x000001B7 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::<DOAnchorPosY>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mE6323DCC5517513C1D0B773E7D9C165CAFEE3CD2 (void);
// 0x000001B8 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::<DOAnchorPosY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m6D806F973A634624EDCC3FFB4D19ACEE8379767B (void);
// 0x000001B9 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mE670638665FE55574F902F01FCE17088CC9B51F7 (void);
// 0x000001BA UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_mD8F4CC891541EC9D948A06094D8261EA8AD116BC (void);
// 0x000001BB System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m7B125E2E19261A5A069B51EE5E94702F21A80507 (void);
// 0x000001BC System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mCD8F6C7BF95D50D747AEE173EB435EF65A689609 (void);
// 0x000001BD UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::<DOAnchorPos3DX>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_m4419AAF2DAA16C2B4BAB3ECDC0DE54FB382CA62D (void);
// 0x000001BE System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::<DOAnchorPos3DX>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_m6C5D7A6C16983482442E32A16A033B0150FE0CC6 (void);
// 0x000001BF System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m86AF170A0925E08D0A65541F5FFDF4677DC26993 (void);
// 0x000001C0 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::<DOAnchorPos3DY>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m36926693350BD10E98C0FAB23A42BAAAD36A2493 (void);
// 0x000001C1 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::<DOAnchorPos3DY>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_mA1047AB2DA90FDA989A954328B6F1CB245632267 (void);
// 0x000001C2 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_mA57B296946A78BDE4C0996F6620A6BEA7560B293 (void);
// 0x000001C3 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mF1D595979B05ADF33616C34C354882C41849573A (void);
// 0x000001C4 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_m807CF2B0A4B3E80FBB9AF6B1EB052C81287C638A (void);
// 0x000001C5 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_mDF4289D1B1D2AE224E8B9C38ACE5AC948B7E02FC (void);
// 0x000001C6 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::<DOAnchorMax>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_m4793787A220BB6883365AAB5D49C15FCC3603437 (void);
// 0x000001C7 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::<DOAnchorMax>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_mD4F8D97C6800C59B301EEF002B8AD3E159800DC7 (void);
// 0x000001C8 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mCF9593E6690F3231C205ED9D783D2A7B7FDCDC39 (void);
// 0x000001C9 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::<DOAnchorMin>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_mD3E3F3E0E5AF777235F92F8E889792A48F7D6D39 (void);
// 0x000001CA System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::<DOAnchorMin>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m3FCAFBC63C0B808C424CBDEFBCA43E384278F078 (void);
// 0x000001CB System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_mA36790CF12C557DCAE53E28D7F414FEC023EAE31 (void);
// 0x000001CC UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::<DOPivot>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_m657FF3EF015F8C24EB07CAD829F7D7FDC79FE16F (void);
// 0x000001CD System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::<DOPivot>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_mE61802FBC7AFA9C5F4C096B11AFE938EBDDE8FAA (void);
// 0x000001CE System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mA35A93D158559A282A5188A97BCCA9220AF67370 (void);
// 0x000001CF UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::<DOPivotX>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_m4083146BB0967165276D99BCF35C2E61CACE3E8B (void);
// 0x000001D0 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::<DOPivotX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m6190029053544AA7B28529A6D76199BEB5013BBA (void);
// 0x000001D1 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_mEF252993D9AFCE312A8CD692AB8B341B0581798F (void);
// 0x000001D2 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::<DOPivotY>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_m6E0C3C507A99950035716FB3D920DA38FD6A52AB (void);
// 0x000001D3 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::<DOPivotY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_m5C473C0B5F6E0D644CBD57C2F92031C6144E4C95 (void);
// 0x000001D4 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_mADF6707C33AE95BE5459C995658C8CB66D3B5E68 (void);
// 0x000001D5 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::<DOSizeDelta>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_mAB34EEC0EF3CB109FC204F04F56B9EE5ED83EA7C (void);
// 0x000001D6 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::<DOSizeDelta>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m8B501473D4692E82386DBB0B1177501BC7B2CE26 (void);
// 0x000001D7 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m739B5C4A5F9226F99D697145E1264BEB7C51E3FE (void);
// 0x000001D8 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::<DOPunchAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_mF3376BA94ED3378565749B29617B355427895913 (void);
// 0x000001D9 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_mD796519764D5AFA3E5E3ADF9DEE0CCB1E038314A (void);
// 0x000001DA System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m27FE3BB51DEF6D4EF9440F1784A6223FDEEFBE09 (void);
// 0x000001DB UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_mF0A138BBFFEBD350A7A798C3CE1A6BAC8A12AAEC (void);
// 0x000001DC System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mD7C8B1865C3264C5A26A46DC1D818EAAD3811F5C (void);
// 0x000001DD System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m0749B705541B7643446DDC13757AB711FD6BA4EE (void);
// 0x000001DE UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_m937214C17C3B8994C19B4BF1FA11854C23805FE1 (void);
// 0x000001DF System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_m8ECF7A3C7EEB609D3120E803190A59D791FEAABA (void);
// 0x000001E0 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m947221A2C6C3E7E7B6CF8BD5049E6F6156A220DE (void);
// 0x000001E1 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_m666972BFD02D183E9CD1A2802C99C8191CBBA172 (void);
// 0x000001E2 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_m2E0B34BA143754440C4D00A44A562E0081EE8E89 (void);
// 0x000001E3 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__2()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_m3B1ED4F1B580A89FD256913A9E8ADB05D37A1F59 (void);
// 0x000001E4 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__3()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_mD88B3091CB8ABE147ABBD65A5C76D4786611715E (void);
// 0x000001E5 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_m3D79AB9382A23E9675B92DEADD0EF7380D4DD3D4 (void);
// 0x000001E6 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__5()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m3A622CEE4C7EE2DD110F96D1D729246009B9C85B (void);
// 0x000001E7 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m03D1ABAB0A38CCE0780692DE6D93AF76F9C1EE44 (void);
// 0x000001E8 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::<DONormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_m761B696496A585F9B0EA0A5D15A68CDF0F42E2F0 (void);
// 0x000001E9 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::<DONormalizedPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_m0CDB4E0C9F474A8EAA81F963149AF56DC17BD6BF (void);
// 0x000001EA System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m2F12269007BF6FE3291F4FEF4A2FEB1976F3BB62 (void);
// 0x000001EB System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_mF10881E3C73606BC0CC8612F300827EFA2F9421A (void);
// 0x000001EC System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mB241BF3812019CDF5065B8EFF948E109024906E5 (void);
// 0x000001ED System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_mAEFE6D86AB20D7486EDFA2FBC5AED13570E20F43 (void);
// 0x000001EE System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_mEA15A9439D9B20F844F994FB19D718EE44E37429 (void);
// 0x000001EF System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_m21343C52B802215FED725F28ABDB0CE1B95CB0AC (void);
// 0x000001F0 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m0696F3B811BADDC612F4542E58E1E66D128D50F1 (void);
// 0x000001F1 System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::<DOValue>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_mDF51D8475A640B2843B6560D25B5D0DA6D00AF9E (void);
// 0x000001F2 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::<DOValue>b__1(System.Single)
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_m5BBCAC5F25C4D6080CFCF57EB6674FB5D1D4A4DD (void);
// 0x000001F3 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mEDF045706162E16509229F389B46C7BA17394D9C (void);
// 0x000001F4 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_m84CB93B80C9F53FB9D43DF6B432D6F1A706B5DCB (void);
// 0x000001F5 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m788D00617ABD0E5DFB574A47BB3AD963FEE9F16E (void);
// 0x000001F6 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m9FD740E362DCC48E7BD9D7AE5ECBCC3E43388C9D (void);
// 0x000001F7 System.Int32 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::<DOCounter>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__0_m8E958A032ED76D1ABFBB0CC1ADA05E5E0F04FC20 (void);
// 0x000001F8 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::<DOCounter>b__1(System.Int32)
extern void U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__1_m1229BCB1349621EBDF1E85929F5834E27D72EBCD (void);
// 0x000001F9 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m1C6DBE0C109255EC8DB7EF1D9DC96AFC25EE1534 (void);
// 0x000001FA UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_m0E974078AA51392F9B2F13D291E5E57BA14188CC (void);
// 0x000001FB System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m794BE13000F3747BBE5C768CFD317242C614DDB7 (void);
// 0x000001FC System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_mA6A78E6BA75EC11AF74C18ED8E5C9565575031F7 (void);
// 0x000001FD System.String DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::<DOText>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__0_mD143685572AC22B0B4C884C29F440FD7F76D18C1 (void);
// 0x000001FE System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::<DOText>b__1(System.String)
extern void U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__1_m78502B08AADA48FCA7C6791831DE20C2EB1B3B9D (void);
// 0x000001FF System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m66A09CEBC52DE07C1D50D7B88D64AD0E268CA295 (void);
// 0x00000200 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_m5F023AA6E390243271D28465DBA5C741CE266450 (void);
// 0x00000201 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_m713281AFFE3CA99B69CAE844B7EEB8883DDBE8EB (void);
// 0x00000202 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_mB38B5B1876CD03E6025C16B11EAD4B328BB07E59 (void);
// 0x00000203 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_m038DCA3479DD16455C79432B77A7044FBDC39948 (void);
// 0x00000204 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_mB09861A4718E374CC6758B7C65213814FF5610E4 (void);
// 0x00000205 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_mD773616857E0E577F59FFDC4E6118336C27AF59E (void);
// 0x00000206 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass40_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__0_m3B35E4644C4114D2A02539738B653A8FDA09212B (void);
// 0x00000207 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass40_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__1_m590C89402F7307E38B48CF20810CD1CE043E7CD1 (void);
// 0x00000208 System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m6F9D103276662BC49FE1ED157BE9C581A9363319 (void);
// 0x00000209 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::<DOOffset>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m8C3BD8E42C1FDD4F8C07E91DBA4E0EF235953798 (void);
// 0x0000020A System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::<DOOffset>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m35102A9B07D6AA99A40902529D951CEFDFE5FA55 (void);
// 0x0000020B System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mC70A530B3CAA981F3C397D1542383389796A3422 (void);
// 0x0000020C UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::<DOTiling>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_mD661636986CCD0D6637C7954BE9011DFCC662DCE (void);
// 0x0000020D System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::<DOTiling>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_mE5BB70F0F913CEF02333C55808F5FB16BAB60CF1 (void);
// 0x0000020E System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForCompletion>d__10::MoveNext()
extern void U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m19986616949ED62423EDBE99F4DAEB70FDD70C3D_AdjustorThunk (void);
// 0x0000020F System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForCompletion>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_mFA48688CDBEE844B795981A009A54F2B38F136F9_AdjustorThunk (void);
// 0x00000210 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForRewind>d__11::MoveNext()
extern void U3CAsyncWaitForRewindU3Ed__11_MoveNext_mEF162B37908B99CFF948BD2617E73A362A6A2004_AdjustorThunk (void);
// 0x00000211 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForRewind>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mDD173C965929C8DC333851E28ADCAAB305681CBA_AdjustorThunk (void);
// 0x00000212 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForKill>d__12::MoveNext()
extern void U3CAsyncWaitForKillU3Ed__12_MoveNext_m05EA91DC717412C1F74FC0A58120B189AB135C4A_AdjustorThunk (void);
// 0x00000213 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForKill>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m765F310D1CF6A33B443185D95138C5C3E6798C6A_AdjustorThunk (void);
// 0x00000214 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForElapsedLoops>d__13::MoveNext()
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_m09697E4CFC135C583A20921B72A654D2554C62E9_AdjustorThunk (void);
// 0x00000215 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForElapsedLoops>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m68993F95BF124F3B6F3ACF8C16F50F0EEB4EEA26_AdjustorThunk (void);
// 0x00000216 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForPosition>d__14::MoveNext()
extern void U3CAsyncWaitForPositionU3Ed__14_MoveNext_m13A4D7405F3CEFA4E0A6CCCD3C3B4A51968F5718_AdjustorThunk (void);
// 0x00000217 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForPosition>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_m0B4D683822C29D1CF7A86766AA82B51C5DA55D0C_AdjustorThunk (void);
// 0x00000218 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForStart>d__15::MoveNext()
extern void U3CAsyncWaitForStartU3Ed__15_MoveNext_m791B08379C3D728FE1118238797F3CB237C1D1B2_AdjustorThunk (void);
// 0x00000219 System.Void DG.Tweening.DOTweenModuleUnityVersion_<AsyncWaitForStart>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CAsyncWaitForStartU3Ed__15_SetStateMachine_m0AA0EC0DA8DE0C4C885AFCF3414300244E13CD6B_AdjustorThunk (void);
// 0x0000021A System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForCompletion::get_keepWaiting()
extern void WaitForCompletion_get_keepWaiting_mF0E6ED9B79F7B3137E81D73BDCB871A7FFDD427E (void);
// 0x0000021B System.Void DG.Tweening.DOTweenCYInstruction_WaitForCompletion::.ctor(DG.Tweening.Tween)
extern void WaitForCompletion__ctor_m7776A76A217F26D17792AAD45969CDBC5664CD94 (void);
// 0x0000021C System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForRewind::get_keepWaiting()
extern void WaitForRewind_get_keepWaiting_m72AB45283B51A6B583C8297262FD6D93E380B729 (void);
// 0x0000021D System.Void DG.Tweening.DOTweenCYInstruction_WaitForRewind::.ctor(DG.Tweening.Tween)
extern void WaitForRewind__ctor_mA04F7E1D6B5FD87FE5E5CD1706B199C371BB6444 (void);
// 0x0000021E System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForKill::get_keepWaiting()
extern void WaitForKill_get_keepWaiting_m9D5736D3C4B308CC106649597BBF5B591C5128FA (void);
// 0x0000021F System.Void DG.Tweening.DOTweenCYInstruction_WaitForKill::.ctor(DG.Tweening.Tween)
extern void WaitForKill__ctor_m06E1809436484E10B0B514B8F593D8AD5FD1288E (void);
// 0x00000220 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::get_keepWaiting()
extern void WaitForElapsedLoops_get_keepWaiting_mBADBF1056A0555A2002FEE28B0F8C11081D4CD17 (void);
// 0x00000221 System.Void DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::.ctor(DG.Tweening.Tween,System.Int32)
extern void WaitForElapsedLoops__ctor_mB2E48FE099278BC6035ECDDFF7058119A1CBB692 (void);
// 0x00000222 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForPosition::get_keepWaiting()
extern void WaitForPosition_get_keepWaiting_m1EDE94387F740F202869EB4B0F5A26E0A6942CE7 (void);
// 0x00000223 System.Void DG.Tweening.DOTweenCYInstruction_WaitForPosition::.ctor(DG.Tweening.Tween,System.Single)
extern void WaitForPosition__ctor_m91EA36AE55FB50AA7424FF345FCECD9B145EAC53 (void);
// 0x00000224 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForStart::get_keepWaiting()
extern void WaitForStart_get_keepWaiting_mB883320436C331DFF16A1C44A7F15DD913F3A276 (void);
// 0x00000225 System.Void DG.Tweening.DOTweenCYInstruction_WaitForStart::.ctor(DG.Tweening.Tween)
extern void WaitForStart__ctor_m14BC2358012D948605484214A1C77409082EBBF5 (void);
// 0x00000226 System.Void DG.Tweening.DOTweenModuleUtils_Physics::SetOrientationOnPath(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Physics_SetOrientationOnPath_mDB3D488E218E259EFF57A11D1BE4EBAC6DD81C8B (void);
// 0x00000227 System.Boolean DG.Tweening.DOTweenModuleUtils_Physics::HasRigidbody2D(UnityEngine.Component)
extern void Physics_HasRigidbody2D_mC466D2DF937DC5188F923D116D8BBA0DF9C9B256 (void);
// 0x00000228 System.Boolean DG.Tweening.DOTweenModuleUtils_Physics::HasRigidbody(UnityEngine.Component)
extern void Physics_HasRigidbody_m8BE432ECF97A79843D7D4969D4FF4AFE2D291100 (void);
// 0x00000229 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModuleUtils_Physics::CreateDOTweenPathTween(UnityEngine.MonoBehaviour,System.Boolean,System.Boolean,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void Physics_CreateDOTweenPathTween_mA90F09CAFFEFF7F4B9B1A756179E89EBFBA70E5D (void);
static Il2CppMethodPointer s_methodPointers[553] = 
{
	NULL,
	JSONNode_get_Item_m68FCA7088A6690FB632145068B7E8B373A8DB4ED,
	JSONNode_set_Item_m25F2BA097890C323C4B283AE4ED5995BB103F021,
	JSONNode_get_Item_mE4DA966D936ACB13C37763CDCE5EC2AF5729D205,
	JSONNode_set_Item_m29B408C6CAB5AA50B520C24A19563CE2C8E0F9DE,
	JSONNode_get_Value_m9B82BB86D4FD6E7CC92BFB773E9459329816CE9C,
	JSONNode_set_Value_m831EF2FF049F37126F9915D07AD0408400B1C72A,
	JSONNode_get_Count_m7AD24AA531038210738C18D8A25190B3BA76FD2D,
	JSONNode_get_IsNumber_m3189BF7AD2A6D5D70797D383EDCF74ACEBBBCE5C,
	JSONNode_get_IsString_mAA1EB2A7163F1F1CECB249CC01967F58C792D10F,
	JSONNode_get_IsBoolean_mAEAD2EF66A8E96031237FB0C24A3EB7678D9A67C,
	JSONNode_get_IsNull_mA9E1B086C9708E0DBA88B8D46C12145F350DD103,
	JSONNode_get_IsArray_m1B11D601CC7BCB5FCBF86ABD789B500240A63A67,
	JSONNode_get_IsObject_mFC432CA1A043D3EE786300135BCEDB8F60F004D7,
	JSONNode_get_Inline_m2F25D4A17A00109F0FC4E333DD0992D8F8B3E3E9,
	JSONNode_set_Inline_m929B6EBB35EA276CF12E8A90FFCA2A8B330342A0,
	JSONNode_Add_mDD2480A1E9A657DD80FACB63418B7D3EB928FC8C,
	JSONNode_Add_m31E333F3FF2EA38F6E41E269B55CA1573F6BB85F,
	JSONNode_Remove_mF7243F4578DB464584758182E06AB93679B3783F,
	JSONNode_Remove_m7241A96E1AE22CAE87DE961567091BBFE1F5A360,
	JSONNode_Remove_mF567CA468968F37D6A0BC5E8A30D5F2C4A0F8EB5,
	JSONNode_get_Children_m93AC18CC13651ADE3DB09B101FCB8E0B489B299F,
	JSONNode_get_DeepChildren_mF66414308628678DEA3A73F7B6841D82D99020CE,
	JSONNode_ToString_m6C2211311DCA4723345DF3832693184CB3E4554D,
	JSONNode_ToString_mC000EFD570BBC997597C47F38395D1B780063CCC,
	NULL,
	NULL,
	JSONNode_get_Linq_m90FBEF6424C5C7C4A0DD9489C0638F6905803DA5,
	JSONNode_get_Keys_m4D9FD5091E68ADC0A6A621818E27CD2CD1DDE7C0,
	JSONNode_get_Values_mC225711E45284FEC77A340E3D7686B2F667EBE6A,
	JSONNode_get_AsDouble_mDB5FD20F057B328E4A8D2A6E0344D712EA5222CD,
	JSONNode_set_AsDouble_mC5255237373D9BFBB462959043D6A230D5E52642,
	JSONNode_get_AsInt_m3F0B6AC95E43D28183E6FF0E6680E8AEBFF5614D,
	JSONNode_set_AsInt_m19378B69037D4A783749882479D66937E3942CE2,
	JSONNode_get_AsFloat_mA5E740EE81C49B0A2B3653DD2D91EAC4D0DA38B2,
	JSONNode_set_AsFloat_m7389EDD5E4CAFCBF1B5FD2BC91E492736E0ECA65,
	JSONNode_get_AsBool_m468F905B830A77EDC94036AECAE06CCB2062D2F0,
	JSONNode_set_AsBool_mAF476F53D7ED6FAE388588E1C1DD2B4EF980F5EE,
	JSONNode_get_AsArray_mD6FAE94576DA50BCAF8E3EEF81CAB62C89966D3F,
	JSONNode_get_AsObject_m96C974C159160E812077411341FFA273B3BD4216,
	JSONNode_op_Implicit_mC6392F9282360F9ABD3AC734B18BF94C1FB7F107,
	JSONNode_op_Implicit_mE11B102085A3EDECED8D7593CE898C27BC363AAF,
	JSONNode_op_Implicit_mDB9E40DDE6449122804576F1F4FC2D1BD9FE9721,
	JSONNode_op_Implicit_m96884B0F42B9282CF3B94DF70EA790591CE5D70A,
	JSONNode_op_Implicit_m15B5C6A8F83AEFCA8E2422039495626A6B5BA9AD,
	JSONNode_op_Implicit_m1C2AE3220662C5E56E16142EFD9391B7C50A318C,
	JSONNode_op_Implicit_m04F10331C13E2A8FC82650B6B15E1187B045F3A7,
	JSONNode_op_Implicit_m9A07E4AE48A99860EC3766183C68D53FBD909A50,
	JSONNode_op_Implicit_m04174CC463B2FAAA3893DA0E1BC5F0EB0280C3B5,
	JSONNode_op_Implicit_m56F05EC7453355A68B50E4933297958F8D60701C,
	JSONNode_op_Implicit_m757755AFE21B579A47662DAA9BD32620FC7BC7F2,
	JSONNode_op_Equality_mEB349D8644B8E3F87CD33A35457A00480EFB329A,
	JSONNode_op_Inequality_m22851EA8F1ED30D4A1B977CD92AFEEB246DD098A,
	JSONNode_Equals_mBFF7A822B266FD96F6A6B54433B3F9A00CE06AA0,
	JSONNode_GetHashCode_mA0F3A3B708D8EAABFB5AAF1A3FE8EB86A5CC90BA,
	JSONNode_get_EscapeBuilder_m0ACB6810C49FC1E2246978F2E5C66F7344422458,
	JSONNode_Escape_m71E3F923CC34FB0961ACD615E516B61AB60BBE23,
	JSONNode_ParseElement_m78C5E1232F0AA824915F86ACA6114BD584BB1F71,
	JSONNode_Parse_m69AEF21E17F84854BF203846254C8B943E9FEF21,
	JSONNode__ctor_m1CBF4F8B6E1DECA2CC6252F7DE0BEF1D11A18A91,
	JSONNode__cctor_mD5545294D80A8A7EC2E166FDDF8C71A4EC3F09D0,
	JSONArray_get_Inline_mC00FE5764DBE022CB2A6BF50824C467CF01FCB01,
	JSONArray_set_Inline_mCBDA88B1041B9DDFB22CF1F95D2C9AF533C11231,
	JSONArray_get_Tag_mBA7D2708AFD83F55024FEE237CB4CC2B53ECFDDA,
	JSONArray_get_IsArray_m194C15DF5096C82E67284D79ECC9D52A783A5B21,
	JSONArray_GetEnumerator_m0B2C42AB0E7CD1C885EE73FFBC8A67964C58B18D,
	JSONArray_get_Item_mB91E086B47603F6EB9E27B95FCAD0E696B2D6F35,
	JSONArray_set_Item_mF4A3E324D160BF0AD1427B88A2B47F700EB69696,
	JSONArray_get_Item_m411ED75FE2A99BEA55FE44AB9C844D971A4BC815,
	JSONArray_set_Item_mF61DBA24AB5A0323BDBA8F38F1C4504FC7A7D3E6,
	JSONArray_get_Count_m9468CAC9D288419DE8A7CA235B32EBC9FBF558B7,
	JSONArray_Add_mFDEF8511E0C68436BDFEF07162C4F0AF4EAA2970,
	JSONArray_Remove_mD3958262883D23A7E689E01AEF74DF589AD323FE,
	JSONArray_Remove_m7142B83C03C15E06352847A651143A30DDD53E28,
	JSONArray_get_Children_mFF20142C5D77D438F218F14C2C7241E9083B15D8,
	JSONArray_WriteToStringBuilder_m80DDB88D53978AE9F8BC8CB95D98F10B92E5D64B,
	JSONArray__ctor_m5F3CE693516DF2278031527B3BEB830D430F78B1,
	JSONObject_get_Inline_m359D85BE012171E4E002285F20BC429E1A6BFA98,
	JSONObject_set_Inline_m0AEACDF3C151A715C9F63679BDFAC03171D47810,
	JSONObject_get_Tag_m511E4A135B46CF2AFFB7509BBFBF6ABAF93603E7,
	JSONObject_get_IsObject_m8DA1E1498537E1E1C66EB54EF30C3C60C6C0D3CB,
	JSONObject_GetEnumerator_m1AF735552661D6EEA2C54C3F190230835E407236,
	JSONObject_get_Item_m1A6BE59A0A8BAB7FE28A6C0BC87FA4BE32D4018B,
	JSONObject_set_Item_mB3A6227B0FEFE0A516EE7E2F694042FE168156EB,
	JSONObject_get_Item_m76D60CDACB0991A590F72FF75D1828EA2B39F39F,
	JSONObject_set_Item_m3CB015E02CB932589E57C00F6C3E27322293B3A0,
	JSONObject_get_Count_m19CC7B95F788DFF0CE6917F06CFDFE0062FBDB96,
	JSONObject_Add_m0268BD87069B407DE0F601028CFBE6A52F22D594,
	JSONObject_Remove_mE20DCDECF7FF984A389891EF2F08DC2088E7D8C6,
	JSONObject_Remove_mD648238FD174CC258C46E70A36BDFDBE3CC53EE8,
	JSONObject_Remove_m0F1ECF30C2B6C5607AFD8C72E37E87FE5D572EDB,
	JSONObject_get_Children_mD36A022F70752842908E542DF8A88E827A43153C,
	JSONObject_WriteToStringBuilder_mC9B657B6950EAD6BFADD62F0EFCFACC8E1E25DB4,
	JSONObject__ctor_mDC880B59ED4D826E15F67A626326822D1CD1F52C,
	JSONString_get_Tag_m7E09C62F10964D4D27854A83E0860A5AC985AFE3,
	JSONString_get_IsString_m5BF6795E2EC537F95E28F159B6760962D9F8CA4E,
	JSONString_GetEnumerator_m69464F52BB0DA089F154DCB55F5ED7302310CB9D,
	JSONString_get_Value_m4731F37BD04F956DABFB67BA0C421211C6C97F59,
	JSONString_set_Value_m2670EB54A9F28DF3F09476CA0AEF5A74F83EF45B,
	JSONString__ctor_m7E8C037643D81531C83ED7103B3B2E2847454981,
	JSONString_WriteToStringBuilder_mD04D1D9ECF6524AFE0BFFA950C5E28C6CCA58A81,
	JSONString_Equals_mBF62E77E4EA113962F09D5EF5AAB519F0C5DBE96,
	JSONString_GetHashCode_m0D03E5BEB56AD43BDEFC55A105FB80C808C22017,
	JSONNumber_get_Tag_m6B46BC7A9FAB14E69076796A4EC0EA692AE868F8,
	JSONNumber_get_IsNumber_m4EA2B0AAFDABFB616DC3F2F5BF2432E5FBC7DE1C,
	JSONNumber_GetEnumerator_m401DDA65F1DA380E495054994C9AABEAAD9E1DA3,
	JSONNumber_get_Value_mF47E99756A3D325CF16BE8BFD6FF7148DFCAB81D,
	JSONNumber_set_Value_mA2DD9A59619C9FD69ED20EAD8EE931E4506589A1,
	JSONNumber_get_AsDouble_m71742FA87FBA9CEC1B3CD7797BB12D940AA8C3D5,
	JSONNumber_set_AsDouble_m01EF626FFB8BCB29DDDB4AB4B6A6FD4973C0395E,
	JSONNumber__ctor_m540460B21D5ED37F9529F1734B24B6E868D432B8,
	JSONNumber__ctor_mAAF7F464DDCB064C83D2898FE50CCE90F77E4808,
	JSONNumber_WriteToStringBuilder_m918A4406F272535FBDEC2E67DA558DF0B3AB2264,
	JSONNumber_IsNumeric_mFAC1B74A6BD2705D6B2952D74DA6DF6D38C4F268,
	JSONNumber_Equals_mA8EBF84AAB284A4FF0E308F193B06A812E038A3F,
	JSONNumber_GetHashCode_m886C693586191D3BE953E1E1CC7D5548B1E737D3,
	JSONBool_get_Tag_mB9CA91991F00F4B74A0B8506DF446B77AF7872AB,
	JSONBool_get_IsBoolean_m4C847D82501287231AD2E08CBB6CADA94D980599,
	JSONBool_GetEnumerator_mB707A772B8651C727BABE7657335A6EFCFD2D407,
	JSONBool_get_Value_m7EDACE20FE016532F6714D1311EC851635DD4C53,
	JSONBool_set_Value_mE4AC78CF051938BCFBD45858D58A260709A210BC,
	JSONBool_get_AsBool_m2BB0201AE8B6118A20FCF2727F3F8D235505A552,
	JSONBool_set_AsBool_m3D364B4168376B1F5312DCF0F4B771C278BF014F,
	JSONBool__ctor_mBFA987A0D1492AFBC458BB89C88E7EC4AA2BE007,
	JSONBool__ctor_m9AAE9CA0B6044181BAF839B3CB43F9EB05DB2F8A,
	JSONBool_WriteToStringBuilder_m421FFE2D4A595460C5B0AA59C83E6EF5DECCC4E0,
	JSONBool_Equals_mEEF0FFBE41AB3814546ABA212A53AA57517D84B1,
	JSONBool_GetHashCode_m59F19469C64DA65D9B03D4BE6FBCBAAE2762C05E,
	JSONNull_CreateOrGet_mE2E06026E04958D2795026FBF38A47FBE14A7AEF,
	JSONNull__ctor_m774EAA6C8365C47B27BDB3FAD8C28686A1105033,
	JSONNull_get_Tag_m8CC0AC532BEF769781F75F62AC52009E154FB105,
	JSONNull_get_IsNull_m3B07A0182B1672877098FDC068A5ABA1AE336099,
	JSONNull_GetEnumerator_m4654CE7479238B4E8CB6219F9D859CBD7784AB18,
	JSONNull_get_Value_m553A353F6BB70066CDFC90AE29595B789CE3C0ED,
	JSONNull_set_Value_mBBFC09F30AC48C4DF47BF6FCA1F271A6C6AD814A,
	JSONNull_get_AsBool_m21B6CD1890428C491DBDC195ABD86C7A9B2CB520,
	JSONNull_set_AsBool_m9C680F040C87C7617AC04D1CCEC4DF04B3A062B1,
	JSONNull_Equals_m281EA55980AE7E71755B38A70914EDB521651CDE,
	JSONNull_GetHashCode_m11ACC117024154A6425EC0A92F92BE0850AB9AC8,
	JSONNull_WriteToStringBuilder_mA85B44E1E280A51A0E09A296D80CDF5735E8E4A6,
	JSONNull__cctor_mFBF013316CC0BC72EEF846918C4C18C8EDD2C75A,
	JSONLazyCreator_get_Tag_m3A6D074EB93F58B7070B64703D0ABCF7F75B1954,
	JSONLazyCreator_GetEnumerator_m1E8AD0743755E20E6E32B8B53643063DC5F3C13B,
	JSONLazyCreator__ctor_m0B79861AA63A30F62B32C3A2F3A87F152C1015F6,
	JSONLazyCreator__ctor_m6C390D7570282C672EB980117A97087DE5E763DF,
	JSONLazyCreator_Set_mC0B81E1660108B5070C659C1E3D6E8E715F5BA90,
	JSONLazyCreator_get_Item_m5679E58AD4E2A877E1B0DFDEE1966C91F22F6873,
	JSONLazyCreator_set_Item_mE8980B3490249E8442E0BBA0FFAF7A25579659FB,
	JSONLazyCreator_get_Item_m1E8CE199AA17884522F27832C69277BB00BABF07,
	JSONLazyCreator_set_Item_m0D0E3BF1D133B3E0DA46F6A127350770F4837A91,
	JSONLazyCreator_Add_m6AA4B279C70E27ED1B18AF98E92783CD97EA0889,
	JSONLazyCreator_Add_mA541314CB35828119821DF9D32C06C02E103D097,
	JSONLazyCreator_op_Equality_mCB25438D736BAEC78588A1C203AE79D31064898C,
	JSONLazyCreator_op_Inequality_m02BA9367F2E9AB8C9BCD5669C6BA9DE076178218,
	JSONLazyCreator_Equals_m14E80E99AF8A7CCD7EB9991FFABF3DB01980894F,
	JSONLazyCreator_GetHashCode_m20B43AF6AED7123FA9CA49F2019A4B214F523DF9,
	JSONLazyCreator_get_AsInt_m42A06E76316F1243A40AD6FD3C664575D8988758,
	JSONLazyCreator_set_AsInt_mD3942EF6D8F7D250F63D8358B626E3233E4E6D9A,
	JSONLazyCreator_get_AsFloat_m89017807E84A7B04CD9B072858E4B2780928CECF,
	JSONLazyCreator_set_AsFloat_m74C138C110E58F0F84C3E63844C9D10694D1BF9F,
	JSONLazyCreator_get_AsDouble_mBD1C53F2A1C5BEA7D0F16FC4427B361325D1DFF8,
	JSONLazyCreator_set_AsDouble_m490FE6CA5F4FB2BF477934745D8D184779BC746D,
	JSONLazyCreator_get_AsBool_mF27B6C9FC704065CDB2B178AED35069212EDDFCC,
	JSONLazyCreator_set_AsBool_m58268C31926C3B396083BF888747BC1F5E4BCBCB,
	JSONLazyCreator_get_AsArray_m7774E52E019503D6042D0AB8021466B5E617DFBC,
	JSONLazyCreator_get_AsObject_m258758F521BE31F4BA01AD8E3D769761D320CFCC,
	JSONLazyCreator_WriteToStringBuilder_m95D334FA623EB56E6BD461C32EA5CD864662F05B,
	JSON_Parse_mE96FEEA722459A42C836CCF97AE3D01A1912D85D,
	DOTweenModuleAudio_DOFade_mB0A1DEFE27E8F72B69B139BC8DC38C6FEA3E4BF9,
	DOTweenModuleAudio_DOPitch_mED79FD413B765CF753901D63A15E3BAB4F136BF5,
	DOTweenModuleAudio_DOSetFloat_mF674349FF317034791BEAE9809B40CCFA1869B48,
	DOTweenModuleAudio_DOComplete_mE45F3CE362B143FEE103CCF4529EB3F3618E0374,
	DOTweenModuleAudio_DOKill_m9F3A656D282397AE4D0E8C922EACA496F97451B9,
	DOTweenModuleAudio_DOFlip_mEC014BB3042476E266602A94C3A8D7B0F68F360F,
	DOTweenModuleAudio_DOGoto_m08C8ECCEE66D3351AA564933F5E3524FDADA3321,
	DOTweenModuleAudio_DOPause_m0CEEBD646BE6B1E365BFFC1A721AB1EC725C79F5,
	DOTweenModuleAudio_DOPlay_m36D439A7E650CF52D9FA10268BE2D2B051457CAB,
	DOTweenModuleAudio_DOPlayBackwards_mB87CC3019D26370C14B31E5CD685EAD9B4A6C807,
	DOTweenModuleAudio_DOPlayForward_m3F98B1B4E7FF894DECC8BDD7BFCFBEE05611D236,
	DOTweenModuleAudio_DORestart_mBBE976782787A691CACE2AF3F608061EE84066EA,
	DOTweenModuleAudio_DORewind_mAC2F193E7AE357B7262B943BBE8B3E90AE234E43,
	DOTweenModuleAudio_DOSmoothRewind_mE0ECA143026AE852C008B8F72830B1A54288712F,
	DOTweenModuleAudio_DOTogglePause_m50DDFEC4D606FEE2F10D06B2F14CB754130CBF43,
	DOTweenModulePhysics_DOMove_m7C104B86167199AAADD840A0F20230E771E23BAC,
	DOTweenModulePhysics_DOMoveX_mBF3A0D598A2B21C4A33BF6B57C693A80BA11F04E,
	DOTweenModulePhysics_DOMoveY_mE9A0F966009FF670A280698CABC105F2DEC53785,
	DOTweenModulePhysics_DOMoveZ_m74E324975AD7F83B146499FBA679D2E1CE2954F9,
	DOTweenModulePhysics_DORotate_mF410C6772EB409A68E6321B0FE25DB47D07E7ACB,
	DOTweenModulePhysics_DOLookAt_m9538B48E2E89A7D56026F886AF566CE3F54D4FBB,
	DOTweenModulePhysics_DOJump_m0BB90FDEDE9EA110CC979A9DDDE549A893E1048F,
	DOTweenModulePhysics_DOPath_m052A26EE319E0FDFE96993555AD285A3FDFFE130,
	DOTweenModulePhysics_DOLocalPath_mF29800CD9C3B067EBDD1038CA785AEA459FE3951,
	DOTweenModulePhysics_DOPath_mA689B9526195B1CC1ADC554D30F73A4BAAC31DCE,
	DOTweenModulePhysics_DOLocalPath_m9F7958E2826A48DA8B593A5980D22755B00635C3,
	DOTweenModulePhysics2D_DOMove_m987F6ECB82AAE0D4DB0B99E601383B9485112B5A,
	DOTweenModulePhysics2D_DOMoveX_mF18F09118B41EB441A9087FB0104331E3396DEDE,
	DOTweenModulePhysics2D_DOMoveY_m004E0B3A3AC6801B41D99E2EA737D89A61014075,
	DOTweenModulePhysics2D_DORotate_m3F2AAC844CABD53CA1E2F86D86CC76AFC8DDB0F0,
	DOTweenModulePhysics2D_DOJump_m0A7F94644F5BEFC8CE5043F90F3A165AEA3233FB,
	DOTweenModulePhysics2D_DOPath_m6FD2B95C52CE291024FB70DD01519F55F95E8E0B,
	DOTweenModulePhysics2D_DOLocalPath_mC425FC389DC5F01DA97D1974116ED8A30AF8A57B,
	DOTweenModuleSprite_DOColor_mC5483201DA04BD9EF3744EE9BB12039148EFDEB8,
	DOTweenModuleSprite_DOFade_mEC831B3B29368AFAF17BE5192BC298A5291E429A,
	DOTweenModuleSprite_DOGradientColor_m78FE9EFA7085410CE51EBA5E5A40B1DA89A923D7,
	DOTweenModuleSprite_DOBlendableColor_m811FFD010A4748CE26380DED4333685B6B34BB8F,
	DOTweenModuleUI_DOFade_mBE8F5B25D6DAB090178C1AE0A97896F09D42136C,
	DOTweenModuleUI_DOColor_mB297E6DBECD16B7917BEC5C66841B7F9A97EC1D2,
	DOTweenModuleUI_DOFade_m8684861AE6E5AC93CFDA8C324F1127994EFE1D21,
	DOTweenModuleUI_DOColor_mFB279C1658A0C33E18B893B56EB5918AF6B197E2,
	DOTweenModuleUI_DOFade_mC5103EC42ADFFC9414B8124AE06A78EEC6CB15E8,
	DOTweenModuleUI_DOFillAmount_m94B9C82653880C547041682B5584297E764D0FFB,
	DOTweenModuleUI_DOGradientColor_m7E77041E72693BD8665A823E9F240C9D3412A37E,
	DOTweenModuleUI_DOFlexibleSize_mAE13B0912532252843908941352094EF2CC51A62,
	DOTweenModuleUI_DOMinSize_mFA6A60AD7883E2F33B2C43180CDF505DD77BB3A1,
	DOTweenModuleUI_DOPreferredSize_mA76609A9F081BD2654D4806EC926BD9A22BE821A,
	DOTweenModuleUI_DOColor_mAB679CEC795E637B1C5EEAD7D8587FD585038685,
	DOTweenModuleUI_DOFade_mA29DCB435C24F977562C78B3F10DAA3CA4002BEA,
	DOTweenModuleUI_DOScale_m12677AAAF1D3E459962C2AC5822490CCC4462510,
	DOTweenModuleUI_DOAnchorPos_mF3F0D7B55596FAC36C1626BFE3AFBE8B62D8CEE7,
	DOTweenModuleUI_DOAnchorPosX_m2AF646AA8418FC9FA314040BF6ACEF0203C86885,
	DOTweenModuleUI_DOAnchorPosY_mC469FB1A568CA7D1AF1962779DE37AF4435EFDAE,
	DOTweenModuleUI_DOAnchorPos3D_m1E6E6DBDFAA9075D543B38E51897E7D3A1ECB6C3,
	DOTweenModuleUI_DOAnchorPos3DX_m4289CECA46B7121C85F77C1DE799F7299E6DF1CF,
	DOTweenModuleUI_DOAnchorPos3DY_m5D91164E28D3F06B217F5E58DEA39377575B8B07,
	DOTweenModuleUI_DOAnchorPos3DZ_mD88C597A518B248666A0D48B64BC1B58B56C9703,
	DOTweenModuleUI_DOAnchorMax_m197F8BF707AA13F292821A6ECB7F28C4FF8785C0,
	DOTweenModuleUI_DOAnchorMin_m35F8E0D8D4D25AF1267E6881F330375A2B8EEE42,
	DOTweenModuleUI_DOPivot_mD53B5CF825615686B097049CE62AA8E3491125A8,
	DOTweenModuleUI_DOPivotX_m1C0A12505CD967E3972BED5D67BF43E6E921AF79,
	DOTweenModuleUI_DOPivotY_mB5F9616AF3C60BE5C100098D6C036C7D5E7CB5AD,
	DOTweenModuleUI_DOSizeDelta_m78D3A33AECD966A309AF8C208D3D6B9314D38F32,
	DOTweenModuleUI_DOPunchAnchorPos_mD2BB31D6D9298D4E8495F5E1D10FC7C0B87F10EA,
	DOTweenModuleUI_DOShakeAnchorPos_mC1E8246B2F64FEABE7E605708E7F0698BA618D6A,
	DOTweenModuleUI_DOShakeAnchorPos_m2D790EA998982A8D6DCEFA40FEBF70AE38C0AD5A,
	DOTweenModuleUI_DOJumpAnchorPos_m2A51D08D3FB82651DB54F793EB4AD73421D13324,
	DOTweenModuleUI_DONormalizedPos_m939397030AB516930DBFD9C8D45A3AEDE321405C,
	DOTweenModuleUI_DOHorizontalNormalizedPos_mFACB6552CDAA4D2446AF2A9415FE05BDE892BE7A,
	DOTweenModuleUI_DOVerticalNormalizedPos_m57EFC967C31D427F90F677DA374751C0CB2956DA,
	DOTweenModuleUI_DOValue_m9907FC2755FEE780DBAAF0452BD4FC6A5CC328EA,
	DOTweenModuleUI_DOColor_mD7B708C07D203365A38EF934AFACD21C24B1C165,
	DOTweenModuleUI_DOCounter_m5FCE25042F0583B46E7E02B9C3C0A325CF115BC6,
	DOTweenModuleUI_DOFade_m8486AF190AF2A12A2447B9F151B26A6DFA15D712,
	DOTweenModuleUI_DOText_m492C7500E6933DC63649880C1109F5069F091704,
	DOTweenModuleUI_DOBlendableColor_mB08C0D34D2A3FEFF0467BF3D89D2205182AB8035,
	DOTweenModuleUI_DOBlendableColor_m1C41D6AF610C8794B0478893E179B99EFB1C4F04,
	DOTweenModuleUI_DOBlendableColor_m960BF5A6F36E365EC0772648264F4AB1528F7D1D,
	DOTweenModuleUnityVersion_DOGradientColor_m2481629D3A7249FF88BBA02967ACC65DE4B3BF1A,
	DOTweenModuleUnityVersion_DOGradientColor_m442F630721A1EB3B66DF29C18A2AEAF3B521D7D4,
	DOTweenModuleUnityVersion_WaitForCompletion_m088FEB04A09709E31A69211AB9DE0AEC43B95543,
	DOTweenModuleUnityVersion_WaitForRewind_mD4A412B8E08D883BE8B5B92987FA63E06ABD0526,
	DOTweenModuleUnityVersion_WaitForKill_m1569FF0CBF305C3C6EC2C68716FBF38B88FF459F,
	DOTweenModuleUnityVersion_WaitForElapsedLoops_m1778D8A9ADF3F4FFA79E3EBAB6435C641A78E72D,
	DOTweenModuleUnityVersion_WaitForPosition_mED12B668D115E6AD9AFDBB6105F79EF7ECA81940,
	DOTweenModuleUnityVersion_WaitForStart_mDF0DA79B71A0C48B7B9AB4357B7972EBA0A16342,
	DOTweenModuleUnityVersion_DOOffset_mA0198BD6CDD74693047812B24DE55E63AC62AEE3,
	DOTweenModuleUnityVersion_DOTiling_mCECBD1E9F943E2044524759B9C3701C07139BC60,
	DOTweenModuleUnityVersion_AsyncWaitForCompletion_mB902946F8997751FD34B45C02072CD521D2B488D,
	DOTweenModuleUnityVersion_AsyncWaitForRewind_m0A51B9394F968A1AC37304A430D241358916C7A9,
	DOTweenModuleUnityVersion_AsyncWaitForKill_mC24606201524AB6EEDA13517713DCCDEE84B683A,
	DOTweenModuleUnityVersion_AsyncWaitForElapsedLoops_mCC2A02BF06D5EBFD0BFE2CD8904D218375DBBBE2,
	DOTweenModuleUnityVersion_AsyncWaitForPosition_m8ACE33C300180234AA183ACC96518425BD77E3F8,
	DOTweenModuleUnityVersion_AsyncWaitForStart_m5C7F5659DDC019416014E896836394EDE186F2E2,
	DOTweenModuleUtils_Init_mD1FDBB7E353A91D49F518C9E053F0CD77814D0CD,
	DOTweenModuleUtils_Preserver_mA1D92B9161C90E3558CF28A7AC5DB75098605201,
	Enumerator_get_IsValid_mFA3BB4B7F43B1005535936519FD3A42F5B23981E_AdjustorThunk,
	Enumerator__ctor_m057901299D85978F34E86C2B99997599C904516F_AdjustorThunk,
	Enumerator__ctor_m1DBE7DBD0628D4C87A31D88096548AF2917CC576_AdjustorThunk,
	Enumerator_get_Current_mF9E718C3795EB611071FF790A62BB5CAFD377FA9_AdjustorThunk,
	Enumerator_MoveNext_m580CDFBD1C55B6794F5567E544DE9857A5618A6E_AdjustorThunk,
	ValueEnumerator__ctor_m9E8A702BF30F549C55B441B43EBBF823D1F8C3AF_AdjustorThunk,
	ValueEnumerator__ctor_mD13470952E4791B2DC48D9A64AD1735248EC0AF2_AdjustorThunk,
	ValueEnumerator__ctor_mCB3648E562A32EEA21FD3F4DB8C20160ED03722E_AdjustorThunk,
	ValueEnumerator_get_Current_mF69BBD9003C127D62E50E3A2AB4179EA3863B710_AdjustorThunk,
	ValueEnumerator_MoveNext_mFBBE4BD2BD4BEDC52A09992FFB2BE22E61571C4B_AdjustorThunk,
	ValueEnumerator_GetEnumerator_mB4E3F0A6AC7AA6140B861775571A15036BECB254_AdjustorThunk,
	KeyEnumerator__ctor_m13B6087FA46C7C03A3471C28B3FBC22DD4171FC7_AdjustorThunk,
	KeyEnumerator__ctor_mD48489EDADC6546248A26244922162520AD16C3B_AdjustorThunk,
	KeyEnumerator__ctor_m0747B3AC824951C3C8B818496CEDD6D88B9E7E8A_AdjustorThunk,
	KeyEnumerator_get_Current_m1E30F213CEB7C4567654D28C37E2841B4A81FAE0_AdjustorThunk,
	KeyEnumerator_MoveNext_m99B9BF2D4062F287B8D3A55C2C691537CAFE6F98_AdjustorThunk,
	KeyEnumerator_GetEnumerator_m0243DD23679D98AE2F30AD67FFFD4E27A16AB170_AdjustorThunk,
	LinqEnumerator__ctor_m3B100A94EC3DF14B4A94AE12D32050F84E72F65B,
	LinqEnumerator_get_Current_m414843B6351637AAAF627AAD81880DE4BFD61353,
	LinqEnumerator_System_Collections_IEnumerator_get_Current_m7F2D3716F0A2DAB0E37902FCC792A31BF9063758,
	LinqEnumerator_MoveNext_mC600DAAECE627F2900FA1975C41E3D95F79E30F5,
	LinqEnumerator_Dispose_m1E091AD169C66B9DA526BA37FFC6DC662ECE3272,
	LinqEnumerator_GetEnumerator_m8EE24F869680079C3280C231400F84085414E93E,
	LinqEnumerator_Reset_m5412BB4216090D65D10EE8E9E3D3006CAC45C43C,
	LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m74AF45D293634ECACC82EE9CB8987984DB4E6968,
	U3Cget_ChildrenU3Ed__39__ctor_m63F0D06333FBB49218C0B5014FECFBC5175BD0A5,
	U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mE120FE95B41F93A499A4D5DC467888E726B2F1E3,
	U3Cget_ChildrenU3Ed__39_MoveNext_m79E16CADBE6A9ADCE0E05EC107BBE0DCFA4FC44D,
	U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m5092BD25A20414E5F6EB32077296D953F0D581B6,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_m27A1C27A483D81AA0440A12DD10FE9AD9E3A6ACC,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_mAD0C2CEA294864931CA571CB5D7EFBCD0C670BE6,
	U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mA647F5A5EEA340473000B2E0E7F4B82337322ED6,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_m3B6F1A44882D66A956E6B435D261558891C87CE5,
	U3Cget_DeepChildrenU3Ed__41__ctor_m0C4C7559A4F8423F94519BECED7E7461F0C6C59C,
	U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_mE9B558A92556F9BC9B1DAE775781708631B23773,
	U3Cget_DeepChildrenU3Ed__41_MoveNext_mF6F808F7064BD1D12BE84B35B6DB06C667F5B202,
	U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally1_mFBED011F823B343DF2B3F5C760644DD0C9F476BE,
	U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally2_m2C4BE4DFF37F16458DDF888AED1FA19576F422CC,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mD96920D0B8440B39B9A5FB193E20F50C798CE42A,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_mC12AE3C207E5BE2C29D113F7524B3DF55C709B66,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_m8BB67D7F47460E168E96A9A294806D0A725D8B19,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBB66C6FD838719E9C3F687CE7E9E37C206DE6460,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m8F9A41F8EC3D989146B598EBFB8AF6F63FF9F014,
	U3Cget_ChildrenU3Ed__22__ctor_mFDAB07A9A4574778F9BE79CB24F5D25268081AEC,
	U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_m27D90B6AD18FAE9F1C3491048B45920A3221EC50,
	U3Cget_ChildrenU3Ed__22_MoveNext_m71D465B34326827965E057A2D08CE6A07D4FB0B5,
	U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_mE203663469D7E7DCC70B97ABC4E7DF17CE3AFD14,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m31F5F2527DFDC39F0317D8236755CA263E0A6051,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_mEFE1CB55600EC3E065CBE2DA326B800C5A6B3DF7,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m078E049917A635A9534AD2BBC91AE3F7C507F558,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF48300F880C2258127A0D59E244B611816290D87,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m02F5A03E66FDD6D8FBDD6731827D6490F4A04B29,
	U3CU3Ec__DisplayClass21_0__ctor_mE55EB0815ED94FB7DA4223EB2B1D83A792D49444,
	U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m32C80D793500A7AD9747C4B9BEF7E5902322DC8D,
	U3Cget_ChildrenU3Ed__23__ctor_mAAFF58023C08EF32D6720ED98EAAEBDDEC41A5D3,
	U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m181A10417CF3D7FB11597D6BC8980F1041538D6C,
	U3Cget_ChildrenU3Ed__23_MoveNext_m73899F4BB42E92BEEDCC9E6DE93498B3E5FA12E8,
	U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m5675C62761DE690A974233757AFF7ACA2F6588C5,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mEE728FF36A84F0406C9961667F0CF83A1D076EF8,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_m1878F871DF3F1DECBC29D1EA12F302E7C7D913C0,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_mEDC188741AB55EAA55CD475E71338FF617C4DB81,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m7F30C3A17067EAEBE1F620E688233DDA9E058C00,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m0435461165875637BB8E9DDD819E8C18BC824AA0,
	U3CU3Ec__DisplayClass0_0__ctor_mC9A0CAB6C9AE11C4941DFA9D053070CD2FD516BC,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m49FE38C026CC5480F88654B30AFC76E56349778C,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m08E2A8A3819BE9AE4A4C1AFAE018E27119B7DB13,
	U3CU3Ec__DisplayClass1_0__ctor_m0DDEA1A43F1A3843AA5C62C248793853ACB56FB8,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_m58DB59F47188A28A5127A320B556E44BA6EB22D1,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_m5DB4AFB51E6AAD83942F9E6DFB17DFE6C75E0F74,
	U3CU3Ec__DisplayClass2_0__ctor_m98D11882C35266F66659B1EE1BF86BD1A10B5682,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_mDF96B328362E424BCFAC0784FE3878D968810737,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_mEB3F4011A9EC3F36357547FE0C0DCF2B0657601B,
	U3CU3Ec__DisplayClass0_0__ctor_m63ADD398E427E85420399A7A291C8AB3E114E802,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_m9CCE2617F2DF17E4C81D16589545B91914B6E14F,
	U3CU3Ec__DisplayClass1_0__ctor_m118BC92350F85BB6B7D0E4DFD001AC7F14087869,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_mDAB4C01153C8CB75453B44B664EB12A541F3E8AC,
	U3CU3Ec__DisplayClass2_0__ctor_m89CE3B2AD0685D6644943F7F0F11C7A7E6833B50,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_mF843B9D21CD4FB10E190912B502BCE5936E62BA9,
	U3CU3Ec__DisplayClass3_0__ctor_mB16E21526E8B098B45C43A2E3CB12BC02845FEC0,
	U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_mE6BE0287D941A3382FE35DBA638FFE636B4BD5B4,
	U3CU3Ec__DisplayClass4_0__ctor_m4494A87C8737B4326800E02CC5E66162DCACEBA4,
	U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_mF2DACA96604E7E3FA529BB1F3BC1B8D1229D29B0,
	U3CU3Ec__DisplayClass5_0__ctor_m61B7AF03993F87E42AAF65A8EC56F6D595BF15FA,
	U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_m35E9F4DC31C1B39AE7AD17C7FD5494F86986983D,
	U3CU3Ec__DisplayClass6_0__ctor_m136EBFF341B7C238E9517606B9CAC933AF4DAE3A,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_mAB30C048A7DCC1D1B9656E86507A956EE374ACE5,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_m62C7B6DAAEC853E2C48E2F56A0918FC39DDA2E40,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_mC30FDCDD0BE010B84AD37D5F54585F3A77EC4AC5,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_m305EA5A202C011BB907DA1768C055BBF76C938D9,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_m39B304CB49D3BC14B2C9A7F94EF46A28E45E7089,
	U3CU3Ec__DisplayClass7_0__ctor_m7EB4080EE45F71C7B5C7FC8C4397C300F5ACBDD8,
	U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_m124D52228901D80260472F63AA26FD57C4D1055C,
	U3CU3Ec__DisplayClass8_0__ctor_m01CA622748CDB6ACEC270067D893562F467C794C,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_m14FC5E33F0A124AB11E785DA497603548DBA0C58,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m87A4D3BD60A9AE2ACF56C51350139E47D971B5FD,
	U3CU3Ec__DisplayClass9_0__ctor_m825F7F53873EAA6CE53B2CB914B3D2F51F87100F,
	U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mE6BEB9A34A2ACA95E5E4789FB0A41AD09144D6CC,
	U3CU3Ec__DisplayClass10_0__ctor_mD65EDC783B8DA7F8CE6DC579F670BE939068A9FB,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_m4AB566ED4A6AD33D97665AEDAF614628027AF26F,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_m19F363524123912BC25BEBEBBFB3B5406B699158,
	U3CU3Ec__DisplayClass0_0__ctor_mD5121B8CCB444BDD80CF1A9E3757B55A9ABB9C12,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mCFD816BA19520401F2B169ED5775476BB24DCC5D,
	U3CU3Ec__DisplayClass1_0__ctor_mBC28699A1759EF11EC626AECEB04BCA8E3A29E21,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_mFC38E73FD38BE1442DB5F8C6BE0642367BE20B73,
	U3CU3Ec__DisplayClass2_0__ctor_m99DCD72B6EBC0698DE92E5360DE96C3B77A08760,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_mB98366A51D3C8AAA935B99D11DE9CACF8BADF093,
	U3CU3Ec__DisplayClass3_0__ctor_m6284017530E3CAE718B57CE0B1A4B5538AE67A0B,
	U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_m30816E46E9AE2707BA6A40BB14AB1CC9FED758E0,
	U3CU3Ec__DisplayClass4_0__ctor_mCACB56B32C32686E61CBAF0DB42451D8E3C9D2D6,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_m07B218C5A79A8036DBC75F17CF4E719D58F515FF,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_mFA4C0E124D639B83AC861602527C4674B32BE5ED,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_mDE18B38CBAF272EC44518040B4ED92CBAAEBDB5D,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_mB6C41172DBCDE9EC8F7C7AC9FCCA12237257CF3F,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_m8F939BEEC32E261FC859B4001E445BF37B3C4480,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_m8256969EBC92F019390147EAF159D5D031B5D12C,
	U3CU3Ec__DisplayClass5_0__ctor_mAA50C827E27780810FD49A9186C8FE9730A8050D,
	U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__0_m60FACA3EED8EF251E50C4AF2359626EF8CBC7558,
	U3CU3Ec__DisplayClass5_0_U3CDOPathU3Eb__1_mB0F5F2CC8D39790B79757A33CE1DCD4547E7EB50,
	U3CU3Ec__DisplayClass6_0__ctor_m00FAAED8DFB2827420F97A994E63F3BD6082BD3B,
	U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__0_m99C1C98AA603CEA2F8466485D1F1FC02933A7F93,
	U3CU3Ec__DisplayClass6_0_U3CDOLocalPathU3Eb__1_m869C353E4D757757FF7EAA0086FB9A049CFB7B86,
	U3CU3Ec__DisplayClass0_0__ctor_m6CAF206E9568D6336FBD31D538A6A0FA34E81EF0,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m7DFAA0E76AA73088F8301C0887A6CA9FA7A393DD,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m4526CF01023DFFE3F5728C31BDAC0F90A6C9EBEC,
	U3CU3Ec__DisplayClass1_0__ctor_mBC0D7BDCA11CE5E38E24A61DE8168DD96AC1A840,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m9D7DE4E2377E078233BB9FF0DF4E3E39759A892B,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_m0EE03E1FA86827EC879708FEEC4D68BFF4AD0837,
	U3CU3Ec__DisplayClass3_0__ctor_m708AF2802A87C338E12EA52A4DA08D97BBE16375,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_mD3C2065A33BBBA7103E37DBB8B08FB825B1FF49A,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_m8A9D459501EE0E9844F947EE642963955F7E785F,
	Utils_SwitchToRectTransform_m03DE2A203997D266A289C397CCA1CD2FF868F224,
	U3CU3Ec__DisplayClass0_0__ctor_m141C1A36E04A1010B221331919A2DD643737E1F1,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m4DC14E1110C69A8007F90B04434A9667CDB71347,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m07B0D13C8F401F39AF1CAA6E711DAEB2631A0C6E,
	U3CU3Ec__DisplayClass1_0__ctor_mF08B02DF314825A9F3252AA0DB3FBA0438499B71,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_m4EB29EC972AC3CFE8D785A37D3EA0FE5919104FF,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m1C6DB448A2B33AE11FE43DC4D4D978802A846D08,
	U3CU3Ec__DisplayClass2_0__ctor_m940E80A7BD91EC634A3063A81705C1CA897441BA,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_mCEAD71FEAA2ABFC92DC1E9F2CF424E926C4B3C9E,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m5215F02965D28B7BE17F609BD81F4C94E34D9967,
	U3CU3Ec__DisplayClass3_0__ctor_mFC1D1AFA961EE54EC3AC45CBD6ACBB2198F9F4D1,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m0CD987CF99960944A1615486EBD5DECF4DC14263,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mFA47EF72CB09FD706B512F128D84F6FC5CEB06E7,
	U3CU3Ec__DisplayClass4_0__ctor_mAB4D14EE7D6F085ADAC49C1D4084D8E410904ADD,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m27D07F05CFF165E3EC68A3C4A9C313DF097CFE64,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_mAADA4AEF9FAD900BE9DF963E3D9FD4D1A390FFEB,
	U3CU3Ec__DisplayClass5_0__ctor_mA716A695CEF34C1DD9DDBCF87A32B3EA6AC56DEE,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m17681A9FFE53F44E317BB2D74151638AE6D6CA10,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_m98974BD33AD9B1EE1225CF9425A693E9E9A95BF9,
	U3CU3Ec__DisplayClass7_0__ctor_mC33276AAC40B1986BD7D786A13A75BE46D98E575,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_m0E752801FD65C3CECA2CBA2B0B41F1121273B7DF,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_m16B62B9FCEDD3323BB0E707581DCD1095A971E35,
	U3CU3Ec__DisplayClass8_0__ctor_mBFB31B6BD9C48D8BBBF9A246C359E814B2F1AA2A,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m4A4A66B3A15F7DFF6001C080DA64A84D26AA98E2,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_m3F88457C1647900570A9BC38EFE371F5A3446061,
	U3CU3Ec__DisplayClass9_0__ctor_m77573B5C974D508FD7B51B458EACEF80C8E97C5A,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mA5D6160C3BBF351768B63AE9547F3C39D878FE24,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m197C519DB62AF13D4B15A85F4C3B1F01A47930E0,
	U3CU3Ec__DisplayClass10_0__ctor_mC7ABBF2015432FC179739C7796F200D2A6B624CF,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_m902BA935FEFB26FB6274C2CFBFB54600CA093E04,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m443BDCBF61E18262396FD7CCCAB8EA5224C653DC,
	U3CU3Ec__DisplayClass11_0__ctor_mFF37C7CB9DDFF2D79FEDE2C307994557E7403CC7,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_m24EC0BECBC9CE42D544B0DA1A2610A5DF5AE8CB6,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_m580E11AA158861453455CEE077B169160022344A,
	U3CU3Ec__DisplayClass12_0__ctor_m37609BBBCE7805314FEA1780512A34444EFFAC30,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_m154033992493DB776542F0C4D9DF31201DB230FD,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_m6E6608DBFE9AB8B89C150C2FEECABEDC067EB020,
	U3CU3Ec__DisplayClass13_0__ctor_mEC3B6B763DF4E0887CCFE5EB7FE3F483F8C07979,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m74C05A6B7DA6E7E588B7D66913B49B7AB3041FB3,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_mC775BAB0715629CA4D07582CC2CF19A1BC8FFA0B,
	U3CU3Ec__DisplayClass14_0__ctor_m67EA9DE8A36ECA66FEE7C3C548E501E250BFDC46,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_mD702D92407483FB14C76144B9087419DCBE562EA,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_m8769407CA79C59C2CBD9E162CA195DCCAC96DB1D,
	U3CU3Ec__DisplayClass15_0__ctor_m279D9D9F290E99D443091BEA0CEA8AEC6B00B095,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mE6323DCC5517513C1D0B773E7D9C165CAFEE3CD2,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m6D806F973A634624EDCC3FFB4D19ACEE8379767B,
	U3CU3Ec__DisplayClass16_0__ctor_mE670638665FE55574F902F01FCE17088CC9B51F7,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_mD8F4CC891541EC9D948A06094D8261EA8AD116BC,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m7B125E2E19261A5A069B51EE5E94702F21A80507,
	U3CU3Ec__DisplayClass17_0__ctor_mCD8F6C7BF95D50D747AEE173EB435EF65A689609,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_m4419AAF2DAA16C2B4BAB3ECDC0DE54FB382CA62D,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_m6C5D7A6C16983482442E32A16A033B0150FE0CC6,
	U3CU3Ec__DisplayClass18_0__ctor_m86AF170A0925E08D0A65541F5FFDF4677DC26993,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m36926693350BD10E98C0FAB23A42BAAAD36A2493,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_mA1047AB2DA90FDA989A954328B6F1CB245632267,
	U3CU3Ec__DisplayClass19_0__ctor_mA57B296946A78BDE4C0996F6620A6BEA7560B293,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mF1D595979B05ADF33616C34C354882C41849573A,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_m807CF2B0A4B3E80FBB9AF6B1EB052C81287C638A,
	U3CU3Ec__DisplayClass20_0__ctor_mDF4289D1B1D2AE224E8B9C38ACE5AC948B7E02FC,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_m4793787A220BB6883365AAB5D49C15FCC3603437,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_mD4F8D97C6800C59B301EEF002B8AD3E159800DC7,
	U3CU3Ec__DisplayClass21_0__ctor_mCF9593E6690F3231C205ED9D783D2A7B7FDCDC39,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_mD3E3F3E0E5AF777235F92F8E889792A48F7D6D39,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m3FCAFBC63C0B808C424CBDEFBCA43E384278F078,
	U3CU3Ec__DisplayClass22_0__ctor_mA36790CF12C557DCAE53E28D7F414FEC023EAE31,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_m657FF3EF015F8C24EB07CAD829F7D7FDC79FE16F,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_mE61802FBC7AFA9C5F4C096B11AFE938EBDDE8FAA,
	U3CU3Ec__DisplayClass23_0__ctor_mA35A93D158559A282A5188A97BCCA9220AF67370,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_m4083146BB0967165276D99BCF35C2E61CACE3E8B,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m6190029053544AA7B28529A6D76199BEB5013BBA,
	U3CU3Ec__DisplayClass24_0__ctor_mEF252993D9AFCE312A8CD692AB8B341B0581798F,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_m6E0C3C507A99950035716FB3D920DA38FD6A52AB,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_m5C473C0B5F6E0D644CBD57C2F92031C6144E4C95,
	U3CU3Ec__DisplayClass25_0__ctor_mADF6707C33AE95BE5459C995658C8CB66D3B5E68,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_mAB34EEC0EF3CB109FC204F04F56B9EE5ED83EA7C,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m8B501473D4692E82386DBB0B1177501BC7B2CE26,
	U3CU3Ec__DisplayClass26_0__ctor_m739B5C4A5F9226F99D697145E1264BEB7C51E3FE,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_mF3376BA94ED3378565749B29617B355427895913,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_mD796519764D5AFA3E5E3ADF9DEE0CCB1E038314A,
	U3CU3Ec__DisplayClass27_0__ctor_m27FE3BB51DEF6D4EF9440F1784A6223FDEEFBE09,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_mF0A138BBFFEBD350A7A798C3CE1A6BAC8A12AAEC,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mD7C8B1865C3264C5A26A46DC1D818EAAD3811F5C,
	U3CU3Ec__DisplayClass28_0__ctor_m0749B705541B7643446DDC13757AB711FD6BA4EE,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_m937214C17C3B8994C19B4BF1FA11854C23805FE1,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_m8ECF7A3C7EEB609D3120E803190A59D791FEAABA,
	U3CU3Ec__DisplayClass29_0__ctor_m947221A2C6C3E7E7B6CF8BD5049E6F6156A220DE,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_m666972BFD02D183E9CD1A2802C99C8191CBBA172,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_m2E0B34BA143754440C4D00A44A562E0081EE8E89,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_m3B1ED4F1B580A89FD256913A9E8ADB05D37A1F59,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_mD88B3091CB8ABE147ABBD65A5C76D4786611715E,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_m3D79AB9382A23E9675B92DEADD0EF7380D4DD3D4,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m3A622CEE4C7EE2DD110F96D1D729246009B9C85B,
	U3CU3Ec__DisplayClass30_0__ctor_m03D1ABAB0A38CCE0780692DE6D93AF76F9C1EE44,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_m761B696496A585F9B0EA0A5D15A68CDF0F42E2F0,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_m0CDB4E0C9F474A8EAA81F963149AF56DC17BD6BF,
	U3CU3Ec__DisplayClass31_0__ctor_m2F12269007BF6FE3291F4FEF4A2FEB1976F3BB62,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_mF10881E3C73606BC0CC8612F300827EFA2F9421A,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mB241BF3812019CDF5065B8EFF948E109024906E5,
	U3CU3Ec__DisplayClass32_0__ctor_mAEFE6D86AB20D7486EDFA2FBC5AED13570E20F43,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_mEA15A9439D9B20F844F994FB19D718EE44E37429,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_m21343C52B802215FED725F28ABDB0CE1B95CB0AC,
	U3CU3Ec__DisplayClass33_0__ctor_m0696F3B811BADDC612F4542E58E1E66D128D50F1,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_mDF51D8475A640B2843B6560D25B5D0DA6D00AF9E,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_m5BBCAC5F25C4D6080CFCF57EB6674FB5D1D4A4DD,
	U3CU3Ec__DisplayClass34_0__ctor_mEDF045706162E16509229F389B46C7BA17394D9C,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_m84CB93B80C9F53FB9D43DF6B432D6F1A706B5DCB,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_m788D00617ABD0E5DFB574A47BB3AD963FEE9F16E,
	U3CU3Ec__DisplayClass35_0__ctor_m9FD740E362DCC48E7BD9D7AE5ECBCC3E43388C9D,
	U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__0_m8E958A032ED76D1ABFBB0CC1ADA05E5E0F04FC20,
	U3CU3Ec__DisplayClass35_0_U3CDOCounterU3Eb__1_m1229BCB1349621EBDF1E85929F5834E27D72EBCD,
	U3CU3Ec__DisplayClass36_0__ctor_m1C6DBE0C109255EC8DB7EF1D9DC96AFC25EE1534,
	U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__0_m0E974078AA51392F9B2F13D291E5E57BA14188CC,
	U3CU3Ec__DisplayClass36_0_U3CDOFadeU3Eb__1_m794BE13000F3747BBE5C768CFD317242C614DDB7,
	U3CU3Ec__DisplayClass37_0__ctor_mA6A78E6BA75EC11AF74C18ED8E5C9565575031F7,
	U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__0_mD143685572AC22B0B4C884C29F440FD7F76D18C1,
	U3CU3Ec__DisplayClass37_0_U3CDOTextU3Eb__1_m78502B08AADA48FCA7C6791831DE20C2EB1B3B9D,
	U3CU3Ec__DisplayClass38_0__ctor_m66A09CEBC52DE07C1D50D7B88D64AD0E268CA295,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_m5F023AA6E390243271D28465DBA5C741CE266450,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_m713281AFFE3CA99B69CAE844B7EEB8883DDBE8EB,
	U3CU3Ec__DisplayClass39_0__ctor_mB38B5B1876CD03E6025C16B11EAD4B328BB07E59,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_m038DCA3479DD16455C79432B77A7044FBDC39948,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_mB09861A4718E374CC6758B7C65213814FF5610E4,
	U3CU3Ec__DisplayClass40_0__ctor_mD773616857E0E577F59FFDC4E6118336C27AF59E,
	U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__0_m3B35E4644C4114D2A02539738B653A8FDA09212B,
	U3CU3Ec__DisplayClass40_0_U3CDOBlendableColorU3Eb__1_m590C89402F7307E38B48CF20810CD1CE043E7CD1,
	U3CU3Ec__DisplayClass8_0__ctor_m6F9D103276662BC49FE1ED157BE9C581A9363319,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m8C3BD8E42C1FDD4F8C07E91DBA4E0EF235953798,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m35102A9B07D6AA99A40902529D951CEFDFE5FA55,
	U3CU3Ec__DisplayClass9_0__ctor_mC70A530B3CAA981F3C397D1542383389796A3422,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_mD661636986CCD0D6637C7954BE9011DFCC662DCE,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_mE5BB70F0F913CEF02333C55808F5FB16BAB60CF1,
	U3CAsyncWaitForCompletionU3Ed__10_MoveNext_m19986616949ED62423EDBE99F4DAEB70FDD70C3D_AdjustorThunk,
	U3CAsyncWaitForCompletionU3Ed__10_SetStateMachine_mFA48688CDBEE844B795981A009A54F2B38F136F9_AdjustorThunk,
	U3CAsyncWaitForRewindU3Ed__11_MoveNext_mEF162B37908B99CFF948BD2617E73A362A6A2004_AdjustorThunk,
	U3CAsyncWaitForRewindU3Ed__11_SetStateMachine_mDD173C965929C8DC333851E28ADCAAB305681CBA_AdjustorThunk,
	U3CAsyncWaitForKillU3Ed__12_MoveNext_m05EA91DC717412C1F74FC0A58120B189AB135C4A_AdjustorThunk,
	U3CAsyncWaitForKillU3Ed__12_SetStateMachine_m765F310D1CF6A33B443185D95138C5C3E6798C6A_AdjustorThunk,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_MoveNext_m09697E4CFC135C583A20921B72A654D2554C62E9_AdjustorThunk,
	U3CAsyncWaitForElapsedLoopsU3Ed__13_SetStateMachine_m68993F95BF124F3B6F3ACF8C16F50F0EEB4EEA26_AdjustorThunk,
	U3CAsyncWaitForPositionU3Ed__14_MoveNext_m13A4D7405F3CEFA4E0A6CCCD3C3B4A51968F5718_AdjustorThunk,
	U3CAsyncWaitForPositionU3Ed__14_SetStateMachine_m0B4D683822C29D1CF7A86766AA82B51C5DA55D0C_AdjustorThunk,
	U3CAsyncWaitForStartU3Ed__15_MoveNext_m791B08379C3D728FE1118238797F3CB237C1D1B2_AdjustorThunk,
	U3CAsyncWaitForStartU3Ed__15_SetStateMachine_m0AA0EC0DA8DE0C4C885AFCF3414300244E13CD6B_AdjustorThunk,
	WaitForCompletion_get_keepWaiting_mF0E6ED9B79F7B3137E81D73BDCB871A7FFDD427E,
	WaitForCompletion__ctor_m7776A76A217F26D17792AAD45969CDBC5664CD94,
	WaitForRewind_get_keepWaiting_m72AB45283B51A6B583C8297262FD6D93E380B729,
	WaitForRewind__ctor_mA04F7E1D6B5FD87FE5E5CD1706B199C371BB6444,
	WaitForKill_get_keepWaiting_m9D5736D3C4B308CC106649597BBF5B591C5128FA,
	WaitForKill__ctor_m06E1809436484E10B0B514B8F593D8AD5FD1288E,
	WaitForElapsedLoops_get_keepWaiting_mBADBF1056A0555A2002FEE28B0F8C11081D4CD17,
	WaitForElapsedLoops__ctor_mB2E48FE099278BC6035ECDDFF7058119A1CBB692,
	WaitForPosition_get_keepWaiting_m1EDE94387F740F202869EB4B0F5A26E0A6942CE7,
	WaitForPosition__ctor_m91EA36AE55FB50AA7424FF345FCECD9B145EAC53,
	WaitForStart_get_keepWaiting_mB883320436C331DFF16A1C44A7F15DD913F3A276,
	WaitForStart__ctor_m14BC2358012D948605484214A1C77409082EBBF5,
	Physics_SetOrientationOnPath_mDB3D488E218E259EFF57A11D1BE4EBAC6DD81C8B,
	Physics_HasRigidbody2D_mC466D2DF937DC5188F923D116D8BBA0DF9C9B256,
	Physics_HasRigidbody_m8BE432ECF97A79843D7D4969D4FF4AFE2D291100,
	Physics_CreateDOTweenPathTween_mA90F09CAFFEFF7F4B9B1A756179E89EBFBA70E5D,
};
static const int32_t s_InvokerIndices[553] = 
{
	10,
	34,
	62,
	28,
	27,
	14,
	26,
	10,
	102,
	102,
	102,
	102,
	102,
	102,
	102,
	31,
	27,
	26,
	28,
	34,
	28,
	14,
	14,
	14,
	34,
	150,
	1923,
	14,
	1924,
	1925,
	395,
	279,
	10,
	32,
	663,
	278,
	102,
	31,
	14,
	14,
	0,
	0,
	90,
	235,
	89,
	424,
	43,
	186,
	740,
	109,
	1926,
	99,
	99,
	9,
	10,
	4,
	0,
	753,
	0,
	23,
	3,
	102,
	31,
	10,
	102,
	1923,
	34,
	62,
	28,
	27,
	10,
	27,
	34,
	28,
	14,
	150,
	23,
	102,
	31,
	10,
	102,
	1923,
	28,
	27,
	34,
	62,
	10,
	27,
	28,
	34,
	28,
	14,
	150,
	23,
	10,
	102,
	1923,
	14,
	26,
	26,
	150,
	9,
	10,
	10,
	102,
	1923,
	14,
	26,
	395,
	279,
	279,
	26,
	150,
	109,
	9,
	10,
	10,
	102,
	1923,
	14,
	26,
	102,
	31,
	31,
	26,
	150,
	9,
	10,
	4,
	23,
	10,
	102,
	1923,
	14,
	26,
	102,
	31,
	9,
	10,
	150,
	3,
	10,
	1923,
	26,
	27,
	26,
	34,
	62,
	28,
	27,
	26,
	27,
	99,
	99,
	9,
	10,
	10,
	32,
	663,
	278,
	395,
	279,
	102,
	31,
	14,
	14,
	150,
	0,
	1598,
	1598,
	1558,
	1575,
	1575,
	186,
	1577,
	186,
	186,
	186,
	186,
	186,
	186,
	186,
	186,
	1612,
	1613,
	1613,
	1613,
	1614,
	1617,
	1618,
	1622,
	1622,
	1623,
	1623,
	1932,
	1613,
	1613,
	1598,
	1933,
	1622,
	1622,
	1599,
	1598,
	1594,
	1599,
	1598,
	1599,
	1598,
	1599,
	1598,
	1598,
	1594,
	1932,
	1932,
	1932,
	1599,
	1598,
	1608,
	1932,
	1613,
	1613,
	1612,
	1613,
	1613,
	1613,
	1932,
	1932,
	1608,
	1598,
	1598,
	1932,
	1933,
	1620,
	1934,
	1933,
	1932,
	1613,
	1613,
	1613,
	1599,
	1935,
	1598,
	1936,
	1599,
	1599,
	1599,
	1594,
	1562,
	110,
	110,
	110,
	635,
	1637,
	110,
	1938,
	1938,
	0,
	0,
	0,
	152,
	1591,
	0,
	3,
	3,
	102,
	1927,
	1928,
	1929,
	102,
	1927,
	1928,
	1930,
	14,
	102,
	1925,
	1927,
	1928,
	1930,
	14,
	102,
	1924,
	26,
	1929,
	14,
	102,
	23,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	102,
	23,
	23,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	102,
	23,
	14,
	23,
	14,
	14,
	14,
	23,
	1931,
	32,
	23,
	102,
	23,
	14,
	23,
	14,
	14,
	14,
	23,
	663,
	278,
	23,
	663,
	278,
	23,
	663,
	278,
	23,
	1030,
	23,
	1030,
	23,
	1030,
	23,
	1030,
	23,
	1185,
	23,
	1185,
	23,
	1030,
	23,
	1030,
	1030,
	23,
	23,
	1030,
	23,
	1030,
	1031,
	23,
	1030,
	23,
	1030,
	1031,
	23,
	1038,
	23,
	1038,
	23,
	1038,
	23,
	663,
	23,
	1038,
	1039,
	23,
	1038,
	1039,
	23,
	23,
	1030,
	1031,
	23,
	1030,
	1031,
	23,
	1011,
	1012,
	23,
	1011,
	1012,
	23,
	1011,
	1012,
	1937,
	23,
	663,
	278,
	23,
	1011,
	1012,
	23,
	1011,
	1012,
	23,
	1011,
	1012,
	23,
	1011,
	1012,
	23,
	663,
	278,
	23,
	1038,
	1039,
	23,
	1038,
	1039,
	23,
	1038,
	1039,
	23,
	1011,
	1012,
	23,
	1011,
	1012,
	23,
	1038,
	1039,
	23,
	1038,
	1039,
	23,
	1038,
	1039,
	23,
	1038,
	1039,
	23,
	1030,
	1031,
	23,
	1030,
	1031,
	23,
	1030,
	1031,
	23,
	1030,
	1031,
	23,
	1038,
	1039,
	23,
	1038,
	1039,
	23,
	1038,
	1039,
	23,
	1038,
	1039,
	23,
	1038,
	1039,
	23,
	1038,
	1039,
	23,
	1030,
	1031,
	23,
	1030,
	1031,
	23,
	1030,
	1031,
	23,
	1038,
	1039,
	23,
	1038,
	1039,
	23,
	23,
	1038,
	1039,
	23,
	663,
	278,
	23,
	663,
	278,
	23,
	663,
	278,
	23,
	1011,
	1012,
	23,
	10,
	32,
	23,
	1011,
	1012,
	23,
	14,
	26,
	23,
	1011,
	1012,
	23,
	1011,
	1012,
	23,
	1011,
	1012,
	23,
	1038,
	1039,
	23,
	1038,
	1039,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	102,
	26,
	102,
	26,
	102,
	26,
	102,
	124,
	102,
	840,
	102,
	26,
	1708,
	109,
	109,
	1939,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	553,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
